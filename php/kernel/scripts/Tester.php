<?php
class Tester {
    private $session;
    private $sqlStuff;
    
    public function __construct(SqlStuff $sqlStuff, Session $session) {
        $this->sqlStuff = $sqlStuff;
        $this->session = $session;
    }
    public function doCommand($command, $request): Result {
        if ($this->sqlStuff->err) return $this->sqlStuff->message;
        $ret = null;
        $request->table = $request->table.'_discusion';
        switch ($command) {
        case 'readForeignNames':
            $ret = $this->readForeignNames();
            break;
        case 'readErareNames':
            $ret = $this->readErareNames();
            break;
        case 'readIdsNames':
            $ret = $this->readIdsNames();
            break;
        case 'loadById':
            $ret = $this->loadById($request);
            break;
        case 'save':
            $ret = $this->saveTest($request);
            break;
        case 'update':
            $ret = $this->updateTest($request);
            break;
        case 'remove':
            $ret = $this->removeTest($request);
            break;
        case 'getResultsInfo':
            $ret = $this->getResultsInfo($request);
            break;
        case 'saveResultsInfo':
            $ret = $this->saveResultsInfo($request);
            break;
        case 'saveResult':
            $ret = $this->saveResult($request);
            break;
        case 'readResults':
            $ret = $this->readResults($request);
            break;
        case 'remResults':
            $ret = $this->remResults($request);
            break;
        case 'createTables':
            $ret = $this->createTables();
            break;
        default:
            return Result::getErr('unknown command '.$command);
        }
        return $ret;
    }
    
    private function createTables() {
        if (!$this->session->isUberLogged()) {
            return $this->session->notPermissionRet();
        }
        $ret = new stdClass();
        
        $sql = 'CREATE TABLE IF NOT EXISTS `tests` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `name` varchar(20) CHARACTER SET utf16 COLLATE utf16_slovak_ci NOT NULL,
                `content` text CHARACTER SET utf16 COLLATE utf16_slovak_ci NOT NULL,
                `fromUserId` int(11) DEFAULT NULL,
                `private` tinyint(1) NOT NULL
               ) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;';
        $ret->tests = $this->sqlStuff->callQueryWithoutRes($sql);
        $sql = 'CREATE TABLE IF NOT EXISTS `test_results` (
                `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                `testId` int(11) NOT NULL,
                `result` text CHARACTER SET utf16 COLLATE utf16_slovak_ci NOT NULL,
                `groupId` int(11) NOT NULL,
                `userName` varchar(20) CHARACTER SET utf16 COLLATE utf16_slovak_ci NOT NULL
               ) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;';
        $ret->test_results = $this->sqlStuff->callQueryWithoutRes($sql);
        $sql = 'CREATE TABLE IF NOT EXISTS `results_groups` (
                `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                `creator` int(11) NOT NULL,
                `name` varchar(50) NOT NULL
               ) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;';
        $ret->results_groups = $this->sqlStuff->callQueryWithoutRes($sql);
        
        return ErrResult::getOk($ret);
    }
    private function getCreatorId($testId): Result {
        $id = addslashes($testId);
        $sql = "SELECT fromUserId FROM tests WHERE id = '$id'";
        $result = $this->sqlStuff->callQueryWithRes($sql);
        if (Result::isErr($result)) {
            return $result;
        }
        $row = $result->item->fetch_assoc();
        $ret = $row['fromUserId'];
        return Result::getOk($ret);
    }
    private function readForeignNames(): Result {
        if (!$this->session->isLogged()) {
            return $this->session->notPermissionRet();
        }
        $userId = addslashes($this->session->id);
    
        $sql = "SELECT name, id FROM tests WHERE NOT fromUserId='$userId' AND NOT private=0 ORDER BY fromUserId DESC";
        $result = $this->sqlStuff->callQueryWithRes($sql);
        
        if (ErrResult::isErr($result)) {
            return $result;
        }

        $ret = new stdClass();
        $numberOfRow = 0;
        while($row = $result->item->fetch_assoc()) {
            $ret->tests[$numberOfRow] = new stdClass();
            $ret->tests[$numberOfRow]->name = stripslashes($row['name']);
            $ret->tests[$numberOfRow]->id = $row['id'];
            $numberOfRow++;
        }
        return Result::getOk($ret);
    }
    private function readErareNames(): Result {
        $userId = addslashes($this->session->id);
  
        $sql = "SELECT name, id FROM tests WHERE NOT fromUserId='$userId' AND private=0 ORDER BY fromUserId DESC";
        $result = $this->sqlStuff->callQueryWithRes($sql);
        
        if (ErrResult::isErr($result)) {
            return $result;
        }

        $ret = new stdClass();
        $numberOfRow = 0;
        while($row = $result->item->fetch_assoc()) {
            $ret->tests[$numberOfRow] = new stdClass();
            $ret->tests[$numberOfRow]->name = stripslashes($row['name']);
            $ret->tests[$numberOfRow]->id = $row['id'];
            $numberOfRow++;
        }
        return Result::getOk($ret);
    }
    private function readIdsNames(): Result {
        if (!$this->session->isLogged()) {
            return $this->session->notLoggedRet();
        }
        $userId = addslashes($this->session->id);

        $sql = "SELECT name, id FROM tests WHERE fromUserId=$userId";
        $result = $this->sqlStuff->callQueryWithRes($sql);
        
        if (ErrResult::isErr($result)) {
            return $result;
        }

        $ret = new stdClass();
        $numberOfRow = 0;
        while($row = $result->item->fetch_assoc()) {
            $ret->tests[$numberOfRow] = new stdClass();
            $ret->tests[$numberOfRow]->name = stripslashes($row['name']);
            $ret->tests[$numberOfRow]->id = $row['id'];
            $numberOfRow++;
        }
        return Result::getOk($ret);
    }
    private function loadById($request): Result {
        $testId = addslashes($request->testId);
        
        $sql = "SELECT * FROM tests WHERE id=$testId";
        $result = $this->sqlStuff->callQueryWithRes($sql);
        
        if (ErrResult::isErr($result)) {
            return $result;
        }
        
        $row = $result->item->fetch_assoc();
        if ($row) {
            $ret = new stdClass();
            $ret->name = stripslashes($row['name']);
            $ret->fromUserId = stripslashes($row['fromUserId']);
            $ret->private = stripslashes($row['private']);
            $ret->test = json_decode(stripslashes($row['content']));
            return Result::getOk($ret);
        } else {
            return Result::getErr('cannot fetch result');
        }
    }
    private function saveTest($request): Result {
        if (!$this->session->isLogged()) {
            return $this->session->notLoggedRet();
        }
        $userId = addslashes($request->userId);
        if ($userId != $this->session->id) {
            if (!$this->session->isUberLogged()) {
                return ErrResult::getErr($this->session->notPermissionRet());
            }
        }
        $name = addslashes($request->name);
        $content = addslashes($request->content);
        $private = getTinyIntFromBool($request->private);

        $sql = "INSERT INTO tests VALUES (NULL, '$name', '$content', '$userId', '$private')";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    private function updateTest($request): Result {
        if (!$this->session->isLogged()) {
            return $this->session->notLoggedRet();
        }
        $creator = $this->getCreatorId($request->testId);
        if (Result::isErr($creator)) {
            return $creator;
        }
        if ($this->session->id != $creator->item) {
            if (!$this->session->isUberLogged()) {
                return ErrResult::getErr($this->session->notPermissionRet());
            }
        }
        $testId = addslashes($request->testId);
        $name = addslashes($request->name);
        $content = addslashes($request->content);
        $private = getTinyIntFromBool($request->private);

        $sql = "UPDATE tests SET name='$name', content='$content', private='$private' WHERE id='$testId'";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    
    private function removeTest($request): Result {
        if (!$this->session->isLogged()) {
            return $this->session->notLoggedRet();
        }
        $creator = $this->getCreatorId($request->testId);
        if (Result::isErr($creator)) {
            return $creator;
        }
        if ($this->session->id != $creator->item) {
            if (!$this->session->isUberLogged()) {
                return ErrResult::getErr($this->session->notPermissionRet());
            }
        }
        $testId = addslashes($request->testId);

        $sql = "DELETE FROM tests WHERE id=$testId";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    private function getResultsInfo($request): Result {
        if (!$this->session->isLogged()) {
            return $this->session->notLoggedRet();
        }
        $userId = addslashes($request->userId);
        if ($userId != $this->session->id) {
            if (!$this->session->isUberLogged()) {
                return ErrResult::getErr($this->session->notPermissionRet());
            }
        }
        $sql = "SELECT name, id FROM results_groups WHERE creator=$userId";
        $result = $this->sqlStuff->callQueryWithRes($sql);
        
        if (ErrResult::isErr($result)) {
            return $result;
        }

        $ret = new stdClass();
        $numberOfRow = 0;
        while($row = $result->item->fetch_assoc()) {
            $ret->results[$numberOfRow] = new stdClass();
            $ret->results[$numberOfRow]->name = stripslashes($row['name']);
            $ret->results[$numberOfRow]->id = $row['id'];
            $numberOfRow++;
        }
        return Result::getOk($ret);
    }
    private function saveResultsInfo($request): Result {
        if (!$this->session->isLogged()) {
            return $this->session->notLoggedRet();
        }
        $resultsInfos = $this->getResultsInfo($request);
        if (!ErrResult::isErr($resultsInfos)) {
            $resultsInfoSize = sizeof($resultsInfos->item->results);
            $haveWantedName = false;

            for ($i = 0; $i < $resultsInfoSize; $i++) {
                $resultsName = $resultsInfos->item->results[$i]->name;
                $ret->results[$i] = $resultsName;
                if ($resultsName === $request->name) {
                    $haveWantedName = true;
                }
            }
            if ($haveWantedName) {
                return ErrResult::getErr('have already name '.$request->name);
            }
        }
        $userId = addslashes($request->userId);
        if ($userId != $this->session->id) {
            if (!$this->session->isUberLogged()) {
                return ErrResult::getErr($this->session->notPermissionRet());
            }
        }
        $name = addslashes($request->name);

        $sql = "INSERT INTO results_groups (creator, name) VALUES ($userId, '$name');";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    private function saveResult($request): Result {
        $testId = addslashes($request->testId);
        $results = addslashes($request->results);
        $groupId = addslashes($request->groupId);
        $userName = addslashes($request->userName);

        $sql = "INSERT INTO test_results (testId, result, groupId, userName) VALUES ('$testId', '$results', '$groupId', '$userName');";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    function readResults($request): Result {
        if (!$this->session->isLogged()) {
            return $this->session->notLoggedRet();
        }
        $resultsId = addslashes($request->resultsId);
        if ($resultsId != $this->session->id) {
            if (!$this->session->isUberLogged()) {
                return ErrResult::getErr($this->session->notPermissionRet());
            }
        }
        
        $sql = "SELECT test_results.*, tests.name FROM test_results INNER JOIN tests ON tests.id=test_results.testId WHERE groupId=$resultsId";
        $result = $this->sqlStuff->callQueryWithRes($sql);
        
        if (ErrResult::isErr($result)) {
            return $result;
        }

        $ret = new stdClass();
        $numberOfRow = 0;
        while($row = $result->item->fetch_assoc()) {
            $ret->results[$numberOfRow] = new stdClass();
            $ret->results[$numberOfRow]->id = $row['id'];
            $ret->results[$numberOfRow]->testId = $row['testId'];
            $ret->results[$numberOfRow]->testName = $row['name'];
            $ret->results[$numberOfRow]->userName = $row['userName'];
            $ret->results[$numberOfRow]->result = json_decode(stripslashes($row['result']));
            $numberOfRow++;
        }
        return Result::getOk($ret);
    }
    function remResults($request): Result {
        if (!$this->session->isLogged()) {
            return $this->session->notLoggedRet();
        }
        $resultsId = addslashes($request->resultsId);
        if ($resultsId != $this->session->id) {
            if (!$this->session->isUberLogged()) {
                return ErrResult::getErr($this->session->notPermissionRet());
            }
        }

        $sql = "DELETE FROM test_results WHERE groupId = $resultsId;";
        $this->sqlStuff->callQueryWithoutRes($sql);

        $sql2 = "DELETE FROM results_groups WHERE id = $resultsId;";
        $ret2 = $this->sqlStuff->callQueryWithoutRes($sql2);

        return $ret2;
    }
}
