<?php
class Doodler {
    private $session;
    private $sqlStuff;
    private $games;

    public function __construct(SqlStuff $sqlStuff, Session $session) {
        $this->sqlStuff = $sqlStuff;
        $this->games = new Games($this->sqlStuff);
        $this->session = $session;
    }
    public function doCommand($command, $request): Result {
        if ($this->sqlStuff->err) return $this->sqlStuff->message;
        $this->removeOld();
        
        $ret = null;
        switch ($command) {
        case 'getNewId':
            $ret = $this->games->getNewId('doodle');
            break;
        case 'createNewLine':
            $ret = $this->createNewLine($request);
            break;
        case 'addNewLine':
            $ret = $this->addNewLine($request);
            break;
        case 'addPoints':
            $ret = $this->addPoints($request);
            break;
        case 'setPoints':
            $ret = $this->setPoints($request);
            break;
        case 'markLineAsDone':
            $ret = $this->markLineAsDone($request);
            break;
        case 'getNewLinesData':
            $ret = $this->getNewLinesData($request);
            break;
        case 'getDoneLinesData':
            $ret = $this->getDoneLinesData($request);
            break;
        case 'createTable':
            $ret = $this->createTable();
            break;
        default:
            return Result::getErr('unknown command '.$command);
        }
        return $ret;
    }

    private function createTable() {
        if (!$this->session->isUberLogged()) {
            return $this->session->notPermissionRet();
        }
        //$sql = "DROP TABLE doodle";
        //$ret = $this->sqlStuff->callQueryWithoutRes($sql);
        
        $sql = 'CREATE TABLE IF NOT EXISTS `doodle` (
                `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `userId` varchar(20) NOT NULL,
                `done`tinyint(1) NOT NULL,
                `date` int(11) NOT NULL,
                `data` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
                `color` varchar(20) NOT NULL
               ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf32 COLLATE=utf32_bin;';
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    private function removeOld() {
        $time = time() - (30 * 60);
        $sql = "DELETE FROM doodle WHERE date < $time";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }

    private function getLastTime(string $userId): Result {
        $sUserId = addslashes($userId);
        $sql = "SELECT MAX(date) AS date FROM doodle WHERE userId = '$sUserId'";
        $ret = $this->sqlStuff->callQueryWithRes($sql);
        if (ErrResult::isErr($ret)) {
            return $ret;
        }
        $dateRow = $ret->item->fetch_assoc();
        $lineTime = stripslashes($dateRow['date']);
        return ErrResult::getOk($lineTime);
    }
    private function getLineId(string $userId, string $time): Result {
        $sUserId = addslashes($userId);
        $sTime = addslashes($time);
        $sql = "SELECT MAX(id) AS id FROM doodle WHERE userId = '$sUserId' AND date = $sTime;";
        $ret = $this->sqlStuff->callQueryWithRes($sql);
        if (ErrResult::isErr($ret)) {
            return $ret;
        }
        $lineIdRow = $ret->item->fetch_assoc();
        $lineId = stripslashes($lineIdRow['id']);
        return ErrResult::getOk($lineId);
    }
    private function getLineData($lineId): Result {
        $sLineId = addslashes($lineId);
        $sql = "SELECT data FROM doodle WHERE id = $sLineId;";
        $ret = $this->sqlStuff->callQueryWithRes($sql);
        
        if (ErrResult::isErr($ret)) {
            return $ret;
        }
        $lineDataRow = $ret->item->fetch_assoc();
        $lineData = stripslashes($lineDataRow['data']);
        return ErrResult::getOk($lineData);
    }
    private function getAllUsers(): Result {
        $sql = "SELECT userId FROM doodle GROUP BY userId;";
        $allUsersResult = $this->sqlStuff->callQueryWithRes($sql);
        if (ErrResult::isErr($allUsersResult)) {
            return $allUsersResult;
        }
        $allUsers = [];
        $numberOfRow = 0;
        while($row = $allUsersResult->item->fetch_assoc()) {
            $user = stripslashes($row['userId']);
            $allUsers[$numberOfRow] = $user;
            $numberOfRow++;
        }
        return ErrResult::getOk($allUsers);
    }
    private function deleteLineWithId(string $lineId) {
        $sLineId = addslashes($lineId);
        $sql = "DELETE FROM doodle WHERE id = $sLineId";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    private function markLineAsDone($request) {
        $done = getTinyIntFromBool(true);
        $dateResult = $this->getLastTime($request->userId);
        if (ErrResult::isErr($dateResult)) {
            return ErrResult::getErr('could not get time');
        }
        $lineTime = $dateResult->item;
        
        $lineIdResult = $this->getLineId($request->userId, $lineTime);
        if (ErrResult::isErr($lineIdResult)) {
            return ErrResult::getErr('could not get id');
        }
        $lineId = $lineIdResult->item;
        
        $dataResult = $this->getLineData($lineId);
        if (ErrResult::isErr($dataResult)) {
            return ErrResult::getErr('could not get data');
        }
        $lineData = $dataResult->item;
        if (strlen($lineData) < 20) {
            return $this->deleteLineWithId($lineId);
        } else {
            $sql = "UPDATE doodle SET done = $done WHERE id = $lineId";
            $ret = $this->sqlStuff->callQueryWithoutRes($sql);
            return $ret;
        }
    }

    
    private function getUsersNewLines(string $userId): Result {
        $done = getTinyIntFromBool(false);
        $sql = "SELECT data, color FROM doodle WHERE userId = '$userId' AND done = $done;";
        $allUsersLinesResult = $this->sqlStuff->callQueryWithRes($sql);
        if (ErrResult::isErr($allUsersLinesResult)) {
            return $allUsersLinesResult;
        }
        $usersLines = [];
        $numberOfRow = 0;
        while($row = $allUsersLinesResult->item->fetch_assoc()) {
            $usersLines[$numberOfRow] = new stdClass();
            $usersLines[$numberOfRow]->data = stripslashes($row['data']);
            $usersLines[$numberOfRow]->color = stripslashes($row['color']);
            $numberOfRow++;
        }
        return ErrResult::getOk($usersLines);
    }
    private function addUserNewLinesToArray(string $user, array $whereTo): array {
        $userIndex = count($whereTo);
        $lines = $this->getUsersNewLines($user);
        if (!ErrResult::isErr($lines)) {
            $whereTo[$userIndex] = $lines->item;
        }
        return $whereTo;
    }
    private function getNewLinesData($request): Result {
        $allUsersResult = $this->getAllUsers();
        if (ErrResult::isErr($allUsersResult)) {
            return ErrResult::getErr('could not get all users');
        }
        $allUsers = $allUsersResult->item;
        $allLines = [];
        foreach ($allUsers as $user) {
            if ($user != $request->except) {
                $this->addUserNewLinesToArray($user, $allLines);
            }
        }
        
        $ret = new stdClass();
        $ret->lines = $allLines;
        return ErrResult::getOk($ret);
    }
    
    private function getUsersDoneLines(string $userId): Result {
        $done = getTinyIntFromBool(true);
        $sql = "SELECT data, color FROM doodle WHERE userId = '$userId' AND done = $done;";
        $allUsersLinesResult = $this->sqlStuff->callQueryWithRes($sql);
        if (ErrResult::isErr($allUsersLinesResult)) {
            return $allUsersLinesResult;
        }
        $usersLines = [];
        $numberOfRow = 0;
        while($row = $allUsersLinesResult->item->fetch_assoc()) {
            $usersLines[$numberOfRow] = new stdClass();
            $usersLines[$numberOfRow]->data = stripslashes($row['data']);
            $usersLines[$numberOfRow]->color = stripslashes($row['color']);
            $numberOfRow++;
        }
        return ErrResult::getOk($usersLines);
    }
    private function addUsersDoneLinesToArray(string $user, array $whereTo): array {
        $userIndex = count($whereTo);
        $lines = $this->getUsersDoneLines($user);
        if (!ErrResult::isErr($lines)) {
            $whereTo[$userIndex] = $lines->item;
        }
        return $whereTo;
    }
    private function getDoneLinesData($request): Result {
        $allUsersResult = $this->getAllUsers();
        if (ErrResult::isErr($allUsersResult)) {
            return ErrResult::getErr('could not get all users');
        }
        $allUsers = $allUsersResult->item;
        $allLines = [];
        foreach ($allUsers as $user) {
            if ($user != $request->except) {
                $allLines = $this->addUsersDoneLinesToArray($user, $allLines);
            }
        }
        $ret = new stdClass();
        $ret->lines = $allLines;
        return ErrResult::getOk($ret);
    }
    
    private function addPoints($request): Result {
        $dateResult = $this->getLastTime($request->userId);
        if (ErrResult::isErr($dateResult)) {
            return ErrResult::getErr('could not get time');
        }
        $lineTime = $dateResult->item;
        
        $lineIdResult = $this->getLineId($request->userId, $lineTime);
        if (ErrResult::isErr($lineIdResult)) {
            return ErrResult::getErr('could not get id');
        }
        $lineId = $lineIdResult->item;
        
        $lineDataResult = $this->getLineData($lineId);
        if (ErrResult::isErr($lineDataResult)) {
            return ErrResult::getErr('could not get line data');
        }
        $lineData = $lineDataResult->item;
        
        $newData = $lineData.addslashes($request->points);

        $sql = "UPDATE doodle SET data = '$newData' WHERE id = $lineId";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    private function setPoints($request): Result {
        //kontrola ci uz existuje userId v tabulke doodlera
        $dateResult = $this->getLastTime($request->userId);
        if (ErrResult::isErr($dateResult)) {
            return ErrResult::getErr('could not get time');
        }
        $lineTime = $dateResult->item;
        
        $lineIdResult = $this->getLineId($request->userId, $lineTime);
        if (ErrResult::isErr($lineIdResult)) {
            return ErrResult::getErr('could not get id');
        }
        $lineId = $lineIdResult->item;
        
        $newData = addslashes($request->points);

        $sql = "UPDATE doodle SET data = '$newData' WHERE id = $lineId";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    private function createNewLine($request): Result {
        $sUserId = addslashes($request->userId);
        $sColor = addslashes($request->color);
        $date = time();

        $sql = "INSERT INTO doodle VALUES (NULL, '$sUserId', 0, '$date', '', '$sColor')";
        
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    private function addNewLine($request): Result {
        $sPoints = addslashes($request->points);
        $sUserId = addslashes($request->userId);
        $sColor = addslashes($request->color);
        $date = time();

        $sql = "INSERT INTO doodle VALUES (NULL, '$sUserId', 1, $date, '$sPoints', '$sColor')";
        
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
}
