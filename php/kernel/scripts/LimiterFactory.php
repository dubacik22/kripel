<?php
include_once "Result.php";
include_once "LimiterGt.php";
include_once "LimiterLt.php";

class LimiterFactory {
    private $sqlStuff;
    public $limitsTableName = "temperaturerLimits";

    public function __construct(SqlStuff $sqlStuff) {
        $this->sqlStuff = $sqlStuff;
    }

    public function recreateLimitsTables($session) {
        if (!$session->isUberLogged()) return $session->notPermissionRet();
        $ret = $this->innerDropLimitsTables();
        if (Result::isErr($ret)) return $ret;
        $ret = $this->innerCreateLimitsTables();
        return $ret;
    }
    public function readLimitsForMeasId($measId): Result {
        $measId = addslashes($measId);
        $ltn = $this->limitsTableName;
        $sql = "SELECT " . $this->getLtnColumns() . " " .
               "FROM " . $ltn . " " .
               "WHERE " . $ltn . ".measId = " . $measId;
        $result = $this->sqlStuff->callQueryWithRes($sql);
        if (Result::isErr($result)) return $result;
        $ret = new stdClass();
        $numberOfRow = 0;
        while($row = $result->item->fetch_assoc()) {
            $ret->limiters[$numberOfRow] = $this->readLtnRow($row);
            $numberOfRow++;
        }
        return Result::getOk($ret);
    }
    public function eraseLimit($limitId): Result {
        $limitId = addslashes($limitId);
        $sql = "DELETE FROM $this->limitsTableName WHERE id = '$limitId'";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    public function createLimiters($measId) {
        $ret = $this->readLimitsForMeasId($measId);
        return $this->create($ret->item->limiters);
    }
    public function saveLimit($limit): Result {
        $ret = $limit->isOperationOk();
        if (Result::isErr($ret)) return $ret;
        if (empty($limit->measId)) return Result::getErr("meas ID is not known");
        $measId = addslashes($limit->measId);
        $value = addslashes($limit->value);
        $operation = addslashes($limit->operation);
        $email = addslashes($limit->email);
        $reached = ($limit->reached) ? 1 : 0;
        $sql = "INSERT INTO $this->limitsTableName VALUES (NULL, '$measId', '$value', '$operation', '$email', $reached)";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    public function updateLimit($limit): Result {
        $ltn = $this->limitsTableName;
        $limitId = addslashes($limit->limitId);
        $reached = ($limit->reached) ? 1 : 0;
        $sql = "UPDATE $ltn SET $ltn.reached = $reached WHERE $ltn.id = $limitId";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    public function create($limiterDefinitions) {
        $limiterIndex = 0;
        if (empty($limiterDefinitions)) return new stdClass();
        foreach ($limiterDefinitions as $definition) {
            $operation =  lcfirst($definition->operation)[0];
            switch ($operation) {
                case "g":
                    $limiter = new LimiterGt();
                    break;
                case "l":
                    $limiter = new LimiterLt();
                    break;
                break;
                default:
                    continue 2;
            }
            $limiter->limitId = $definition->limitId;
            $limiter->measId = $definition->measId;
            $limiter->email = $definition->email;
            $limiter->value = $definition->value;
            $limiter->reached = $definition->reached;
            $limiter->operation = $definition->operation;
            $limiters[$limiterIndex] = $limiter;
            $limiterIndex++;
        }
        return $limiters;
    }
    public function readLimitId($limitId): Result {
        $limitId = addslashes($limitId);
        $ltn = $this->limitsTableName;
        $sql = "SELECT " . $this->getLtnColumns() . " FROM $ltn WHERE $ltn.id = $limitId";
        $result = $this->sqlStuff->callQueryWithRes($sql);
        if (Result::isErr($result)) return $result;
        while($row = $result->item->fetch_assoc()) {
            $ret = $this->readLtnRow($row);
        }
        return Result::getOk($ret);
    }

    private function readLtnRow($row): stdClass {
        $ret = new stdClass();
        $ret->limitId = $row['limitId'];
        $ret->measId = $row['measId'];
        $ret->value = $row['value'];
        $ret->operation = $row['operation'];
        $ret->email = $row['email'];
        $ret->reached = $row['reached'];
        return $ret;
    }
    private function getLtnColumns() {
        return  $this->limitsTableName . ".id AS limitId," .
                $this->limitsTableName . ".measId," .
                $this->limitsTableName . ".value," .
                $this->limitsTableName . ".operation," .
                $this->limitsTableName . ".email, " .
                $this->limitsTableName . ".reached";
    }
    private function innerCreateLimitsTables(): Result {
        $sql = "CREATE TABLE IF NOT EXISTS $this->limitsTableName (
            `id` float NOT NULL PRIMARY KEY AUTO_INCREMENT,
            `measId` int NOT NULL,
            `value` float NOT NULL,
            `operation` varchar(10) NOT NULL,
            `email` varchar(30) NOT NULL,
            `reached` BIT NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    private function innerDropLimitsTables(): Result {
        $sql = "DROP TABLE IF EXISTS $this->limitsTableName";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
}