<?php
include_once "SqlServerInfo.php";
include_once "../PHPMailer/PHPMailer.php";
include_once "../PHPMailer/Exception.php";
include_once '../PHPMailer/SMTP.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

$tempPath = 'uploads/';
function addHeaders() {
  header("Access-Control-Allow-Origin: *");
  header('Content-Type: application/json');
  header('Content-Type: text/event-stream');

  header("Access-Control-Allow-Methods: DELETE, POST, GET, OPTIONS");
  header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
}
function sqlAccessCred() {
  $ret = new SqlServerInfo();
  if(strpos($_SERVER['HTTP_HOST'],"localhost") !== false) {
    $ret->serverName = 'localhost';
    $ret->dbName = 'kripel_database';
    $ret->userName = 'root';
    $ret->password = 'mysql';
  } else {
    $ret->serverName = "46.229.230.163";
    $ret->dbName = "ko023805db";
    $ret->userName = "ko023800";
    $ret->password= "zaj3beMpl1N";
  }

  return $ret;
}
function getTinyIntFromBool(bool $bool): int {
    if ($bool == true) {
        return 1;
    }
    if ($bool == false) {
        return 0;
    }
    if ($bool == 'true') {
        return 1;
    }
    if ($bool == 'false') {
        return 0;
    }
    if ($bool) {
        return 1;
    }
    return 0;
}
function getBoolFromTinyInt(int $int): bool {
    if ($int == 0) {
        return false;
    }
    return true;
}
function createUploadImgDir() {
  $testDir = "../../uploads/";
  return createDirIfNotexist($testDir);
}
function createDirIfNotexist($dir) {
  if(!is_dir($dir)) mkdir($dir);
  return $dir;
}
function getPostJsonInput() {
  $postdata = file_get_contents("php://input");
  $ret = json_decode($postdata);
  return $ret;
}
function getGetJsonInput() {
  $getStr = json_encode($_GET);
  $ret = json_decode($getStr);
  return $ret;
}
function getJsonInput() {
  $postRequest = getPostJsonInput();
  $getRequest = getGetJsonInput();
  if (isset($postRequest->toDo)) return $postRequest;
  return $getRequest;
}
function sendMail_obs(string $from, string $to, string $subject, string $text) {
  if (!filter_var($to, FILTER_VALIDATE_EMAIL)) return;
  $headers = 'From: ' . $from . "\r\n" .
              'X-Mailer: PHP/' . phpversion();
  mail($to, $subject, $text, $headers);
}
function sendMail(string $to, string $subject, string $text) {
  if (!filter_var($to, FILTER_VALIDATE_EMAIL)) return;
  try {
    $mail = new PHPMailer(true);
    $mail->isSMTP();
    $mail->Host='mail.webhouse.sk';
    $mail->SMTPAuth=true;
    $mail->Username='kripel@labkysandale.sk';
    $mail->Password='ultraMegaGEJ69';
    $mail->Port=587;
    $mail->Timeout=10;
    $mail->setFrom('noreply@labkysandale.sk', 'Hybajhore noreply');
    $mail->addAddress($to);
    $mail->isHTML(true);
    $mail->Subject=$subject;
    $mail->Body=$text;
    $mail->send();
  } catch (Exception $e) {
  }
}
function sjs() {
    $ret = new stdClass();
    //timo
    //$ret->session = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJsb2dpbk5hbWUiOiJ0aW1vIiwidXNlck5hbWUiOiJ0b21pIiwicGVybWlzc2lvbnMiOiJhbGwiLCJwYXNzIjoiLSJ9.mSI6qyfTF_kMzjMVuo0QxGgfY4hd1A4qZa-B3GXSiIg";
    //kimo
    //$ret->session = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjMiLCJsb2dpbk5hbWUiOiJraW1vIiwidXNlck5hbWUiOiJrb21pIiwicGVybWlzc2lvbnMiOiIiLCJwYXNzIjoiLSJ9.VUmiQJnUbwiBf4HeicgmA6wfGEWy91Da1Vp3rQHu6WQ";
    //$ret->table = "all";
    //$ret->toDo = 'messages_readAll';

    //$ret->toDo = 'messages_update';
    //$ret->id = '7';
    //$ret->message = "<DIV><SPAN>ahasdoj</SPAN></DIV>";


    //$ret->toDo = 'messages_erase';
    //$ret->id = '11';

    //$ret->toDo = 'users_login';
    //$ret->loginName = 'kimo';
    //$ret->password = '2db95e8e1a9267b7a1188556b2013b33';

    //$ret->toDo = 'users_readAllNames';

    //$ret->toDo = 'users_edit';
    //$ret->id = '3';
    //$ret->loginName = 'kimo';

    //$ret->loginName = "user";
    //$ret->password = "006d2143154327a64d86a264aea225f3";
    //$ret->session = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJsb2dpbk5hbWUiOiJ0aW1vIiwidXNlck5hbWUiOiJ0aW1vIiwicGVybWlzc2lvbnMiOiIiLCJwYXNzIjoiLSJ9.CNLf3YCfMnRW3jfY7rpYIId6T1EQqy7OUEHeTzfSZZY";
    //$ret->toDo = "users_changePass";
    $ret->toDo = "galaxy_getNewId";

    //$ret->session = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJsb2dpbk5hbWUiOiJ0aW1vIiwidXNlck5hbWUiOiJ0aW1vIiwicGVybWlzc2lvbnMiOiJhbGwiLCJwYXNzIjoiLSJ9.VG-eHnLwcjaBbdZdt-deXXQGP2hYWr4X1g8xX4i0yhY";
    //$ret->toDo = "tester_readIdsNames";
    //$ret->userId = "1";
    //$ret->toDo = 'rest_doSomething';
    //$ret->password = '2db95e8e1a9267b7a1188556b2013b33';
    return $ret;
}
?>
