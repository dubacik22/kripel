<?php
class SqlStuff {
    private $conn;
    public $err;
    public $message;


    public function __construct(SqlServerInfo $sqlServer) {
        $this->conn = new mysqli($sqlServer->serverName, $sqlServer->userName, $sqlServer->password, $sqlServer->dbName);
        if ($this->conn->connect_error) {
            $this->message = "Connection failed: " . $this->conn->connect_error;
            $this->success = false;
            return;
        }
        $this->success = true;
    }
    public function __destruct() {
        $this->conn->close();
    }
    function callQueryWithRes(string $query): Result {
        $result = $this->conn->query($query);

        if ($result->num_rows <= 0) {
            return ErrResult::getErr('read returned array with zero size');
        } else {
            return ErrResult::getOk($result);
        }
    }
    function callQueryWithoutRes(string $query): Result {
        $result = $this->conn->query($query);

        if ($result !== TRUE) {
            return ErrResult::getErr('Error updating record: '.$this->conn->error.' query:'.$query);
        } else {
            return ErrResult::getOk();
        }
    }
}
