<?php
class Galaxy {
    private $sqlStuff;
    private $games;

    public function __construct(SqlStuff $sqlStuff) {
        $this->sqlStuff = $sqlStuff;
        $this->games = new Games($this->sqlStuff);
    }
    public function doCommand($command, $request): Result {
        if ($this->sqlStuff->err) return $this->sqlStuff->message;
        $ret = null;
        switch ($command) {
        case 'getNewId':
            $ret = $this->games->getNewId('galaxy');
            break;
        case 'som':
            $ret = $this->som();
            break;
        default:
            return Result::getErr('unknown command '.$command);
        }
        return $ret;
    }

    private function som() {
        $sql = 'CREATE TABLE IF NOT EXISTS `doodler` (
  `userId` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `a` int(11) NOT NULL,
  `b` int(11) NOT NULL,
  `c` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;';
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }

}
