<?php
class Result {
    public $success;

    public static function getErr($message) {
        $ret = new ErrResult();
        $ret->message = $message;
        return $ret;
    }
    public static function getOk($item = null) {
        if ($item === null) $item = new stdClass();
        $ret = new ItemResult();
        $ret->item = $item;
        return $ret;
    }
    public static function isErr($ret) {
        if (!isset($ret->success)) return false;
        if ($ret->success == false) return true;
        else return false;
    }
}
