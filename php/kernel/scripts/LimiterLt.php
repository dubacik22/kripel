<?php
include_once "LimiterBase.php";

class LimiterLt extends LimiterBase {
    public function compare($value, $dname) {
        if ($value == $this->value) return;
        if ($value >= $this->value) $this->unalert("actual value: $value, in $dname is finally greater than limit:" . $this->value);
        else $this->alert("actual value: $value, in $dname is smaller (and who know what its now) than limit:" . $this->value);
    }
}