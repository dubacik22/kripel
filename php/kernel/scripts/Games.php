<?php
class Games {
    private $sqlStuff;
    
    public function __construct(SqlStuff $sqlStuff) {
        $this->sqlStuff = $sqlStuff;
    }
    

    public function getIdData($table, $gameId): Result {
        $sGameId = addslashes($gameId);
        $sTable = addslashes($table);
        $sql = "SELECT * FROM $sTable WHERE userId = '$sGameId'";
        $result = $this->sqlStuff->callQueryWithRes($sql);
        if (Result::isErr($result)) return $result;
        $row = $result->item->fetch_assoc();
        $ret = $row['id'];
        return Result::getOk($ret);
    }
    
    public function addId($gameId, $table): Result {
        $sGameId = addslashes($gameId);
        $sTable = addslashes($table);
        $sql = "INSERT INTO $sTable VALUES ('$sGameId', '1', '1', '1');";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }

    public function getNewId($table): Result {
        $iteratons = 0;
        $maxIterations = 10;
        do {
            $newId = $this->generateRandomString(20);
            $iteratons++;
        } while (!ErrResult::isErr($this->getIdData($table, $newId)) && ($iteratons < $maxIterations));
        if ($iteratons == $maxIterations) {
            return ErrResult::getErr('max iterations reached');
        }
        $ret = new stdClass();
        $ret->id = $newId;
        return ErrResult::getOk($ret);
    }
    private function generateRandomString($length = 10): string {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }
}
