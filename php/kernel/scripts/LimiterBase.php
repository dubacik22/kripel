<?php
include_once "res.php";
abstract class LimiterBase {
    public $limitId;
    public $email;
    public $measId;
    public $value;
    public $reached;
    public $operation;

    public abstract function compare($value, $dname);

    protected function alert($text) {
        if ($this->reached) return;
        //echo $text;
        $this->reached = 1;
        sendMail($this->email, "limit reached", $text);
    }
    protected function unalert($text) {
        if (!$this->reached) return;
        //echo $text;
        $this->reached = 0;
        sendMail($this->email, "limit ok", $text);
    }
    public function isOperationOk(): Result {
        if ($this->operation == "") return Result::getErr("operation not defined");
        $operation = lcfirst($this->operation)[0];
        if ($operation == 'g') return Result::getOk();
        if ($operation == 'l') return Result::getOk();
        return Result::getErr("unknown operation: " . $this->$operation);
    }
}