<?php
class ErrResult extends Result {
    public $message;
    
    public function __construct() {
        $this->success = false;
    }
}
