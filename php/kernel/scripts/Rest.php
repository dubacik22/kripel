<?php
include_once "res.php";
include_once "Result.php";

class Rest {
    private $session;
    private $sqlStuff;
    
    public function __construct(SqlStuff $sqlStuff, Session $session) {
        $this->sqlStuff = $sqlStuff;
        $this->session = $session;
    }
    public function doCommand($command, $request): Result {
        if ($this->sqlStuff->err) return $this->sqlStuff->message;
        $ret = null;
        switch ($command) {
        case 'sendMessage':
            $ret = $this->sendMessage($request);
            break;
        default:
            return Result::getErr('unknown command '.$command);
        }
        return $ret;
    }
    
    private function sendMessage($request) {
        if (empty($request->to)) return Result::getErr("there is no 'to'");
        if (empty($request->subject)) return Result::getErr("there is no 'subject'");
        if (empty($request->text)) return Result::getErr("there is no 'text'");
        sendMail($request->to, $request->subject, $request->text);
        return Result::getOk();
    }
    public function generateRandomString($length = 10) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }
}
