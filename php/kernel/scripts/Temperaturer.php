<?php
include_once "LimiterFactory.php";
include_once "Result.php";

class Temperaturer {
    private $session;
    private $sqlStuff;
    private $limiterFactory;
    private $measurementsTableName = "temperaturerMeasurements";
    private $valuesTableName = "temperaturerValues";

    public function __construct(SqlStuff $sqlStuff, Session $session) {
        $this->sqlStuff = $sqlStuff;
        $this->session = $session;
        $this->limiterFactory = new LimiterFactory($sqlStuff);
    }
    public function doCommand(string $command, $request): Result {
        if ($this->sqlStuff->err) return $this->sqlStuff->message;
        $ret = null;
        switch ($command) {
        case 'recreateAll':
            $ret = $this->recreateAllTables($request);
            break;
        case 'readAll':
            $ret = $this->readAllValues($request);
            break;
        case 'readAllMeases':
            $ret = $this->readAllMeasurements($request);
            break;
        case 'readAllUsersMeases':
            $ret = $this->readAllUserMeasurements($request);
            break;
        case 'readMeas':
            $ret = $this->readMeasurement($request);
            break;
        case 'readLimits4MeasId':
            $ret = $this->readLimitsForMeasId($request);
            break;
        case 'endUidsMeasId':
            $ret = $this->endUidsMeasId($request);
            break;
        case 'eraseMeasId':
            $ret = $this->eraseMeasId($request);
            break;
        case 'eraseLimit':
            $ret = $this->eraseLimit($request);
            break;
        case 'saveLimit':
            $ret = $this->saveLimit($request);
            break;
        case 'save':
            $ret = $this->saveTemperature($request);
            break;
        default:
            return Result::getErr('unknown command '.$command);
        }
        return $ret;
    }

    private function readAllValues($request): Result {
        $mtn = $this->measurementsTableName;
        $vtn = $this->valuesTableName;
        $ltn = $this->limiterFactory->limitsTableName;
        $ret = new stdClass();
        
        $sql = "SELECT " . $this->getLtnColumns() . " FROM $ltn";
        $result = $this->sqlStuff->callQueryWithRes($sql);
        if (Result::isErr($result)) return $result;
        $numberOfRow = 0;
        while($row = $result->item->fetch_assoc()) {
            $ret->limits[$numberOfRow] = $this->readLtnRow($row);
            $numberOfRow++;
        }

        $sql = "SELECT " . $this->getMtnColumns() . " FROM $mtn";
        $result = $this->sqlStuff->callQueryWithRes($sql);
        if (Result::isErr($result)) return $result;
        $numberOfRow = 0;
        while($row = $result->item->fetch_assoc()) {
            $ret->measurements[$numberOfRow] = $this->readMtnRow($row);
            $numberOfRow++;
        }

        $sql = "SELECT " . $this->getVtnColumns() . " FROM $vtn";
        $result = $this->sqlStuff->callQueryWithRes($sql);
        if (Result::isErr($result)) return $result;
        $numberOfRow = 0;
        while($row = $result->item->fetch_assoc()) {
            $ret->values[$numberOfRow] = $this->readVtnRow($row);
            $numberOfRow++;
        }
        return Result::getOk($ret);
    }
    private function readAllMeasurements($request): Result {
        $mtn = $this->measurementsTableName;
        $vtn = $this->valuesTableName;
        if (!isset($request->time)) $request->time = "0";
        $time = addslashes($request->time);
        $sql =  "SELECT " . $this->getMtnColumns() . ", " . $this->getVtnColumns() . " " .
                "FROM $vtn INNER JOIN $mtn ON $vtn.measId = $mtn.id " .
                "WHERE $vtn.time > $time " .
                "ORDER BY $mtn.id, valueId";
        $result = $this->sqlStuff->callQueryWithRes($sql);
        if (Result::isErr($result)) return $result;
        return $this->readSqlMeases($result);
    }
    private function readAllUserMeasurements($request): Result {
        if (!$this->session->isLogged()) return $this->session->notLoggedRet();
        $mtn = $this->measurementsTableName;
        $vtn = $this->valuesTableName;
        $userId = addslashes($this->session->id);
        if (!isset($request->time)) $request->time = "0";
        $time = addslashes($request->time);
        $sql =  "SELECT " . $this->getMtnColumns() . ", " . $this->getVtnColumns() . " " .
                "FROM $vtn INNER JOIN $mtn ON $vtn.measId = $mtn.id " .
                "WHERE $mtn.userId = $userId AND $vtn.time > $time " .
                "ORDER BY " . $mtn . ".id, valueId";
        $result = $this->sqlStuff->callQueryWithRes($sql);
        if (Result::isErr($result)) return $result;
        return $this->readSqlMeases($result);
    }
    private function readMeasurement($request): Result {
        $mtn = $this->measurementsTableName;
        $vtn = $this->valuesTableName;
        $measId = addslashes($request->measId);
        $sql =  "SELECT " . $this->getMtnColumns() . ", " . $this->getVtnColumns() . " " .
                "FROM $vtn INNER JOIN $mtn ON $vtn.measId = $mtn.id " .
                "WHERE $mtn.id = $measId " .
                "ORDER BY `valueId`";
        $result = $this->sqlStuff->callQueryWithRes($sql);
        if (Result::isErr($result)) return $result;
        $ret = new stdClass();
        $numberOfRow = 0;
        while($row = $result->item->fetch_assoc()) {
            if ($numberOfRow == 0) $ret->measurement = $this->readMtnRow($row);
            $ret->measurement->temperatures[$numberOfRow] = $this->readVtnRow($row);
            $numberOfRow++;
        }
        return Result::getOk($ret);
    }
    private function readLimitsForMeasId($request): Result {
        $measId = addslashes($request->measId);
        return $this->limiterFactory->readLimitsForMeasId($measId);
    }
    private function saveTemperature($request): Result {
        if (!$this->session->isLogged()) return $this->session->notLoggedRet();
        if (!isset($request->temp)) return Result::getErr("no temp defined");
        if (!isset($request->uid)) return Result::getErr("no uid defined");
        if (!isset($request->delay)) return Result::getErr("no delay defined");
        $isFirst = filter_var($request->isFirst, FILTER_VALIDATE_BOOLEAN);

        if ($isFirst) $this->endUidsMeas($request);
        $ret = $this->readLastRunningMeas($request->uid);
        if (Result::isErr($ret)) {
            $ret = $this->beginNewMeas($request);
            if (Result::isErr($ret)) return $ret;
            $ret = $this->readLastRunningMeas($request->uid);
            if (Result::isErr($ret)) return $ret;
        }

        $meas = $ret->item;
        $dname = $meas->dname;
        $measId = $meas->measId;
        if ($meas->userId != $this->session->id) return Result::getErr('cannot add points to meas that is not ours');
        $ret = $this->saveNewValue($measId, $request->delay, $request->temp);
        $limiters = $this->limiterFactory->createLimiters($measId);
        $this->evalLimiters($limiters, $request->temp, $dname);
        return $ret;
    }

    private function evalLimiters($limiters, $temp, $dname) {
        if (empty($limiters)) return;
        foreach ($limiters as $limiter) {
            $limiter->compare($temp, $dname);
            $this->limiterFactory->updateLimit($limiter);
        }
    }
    private function beginNewMeas($request): Result {
        $date = round(microtime(true) * 1000);
        $ret = $this->saveNewMeas($date, $request->uid, $request->dname);
        return $ret;
    }
    private function endUidsMeas($request): Result {
        $uid = addslashes($request->uid);
        $mtn = $this->measurementsTableName;
        $sql = "UPDATE $mtn SET uid = '' WHERE $mtn.uid = '$uid'";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    private function endUidsMeasId($request): Result {
        $uid = addslashes($request->uid);
        $measId = addslashes($request->measId);
        $mtn = $this->measurementsTableName;
        $sql = "UPDATE $mtn SET uid = '' WHERE $mtn.uid = '$uid' AND $mtn.id = '$measId'";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    private function saveNewMeas($date, $uid, $dname): Result {
        $userId = addslashes($this->session->id);
        $date = addslashes($date);
        $uid = addslashes($uid);
        $dname = addslashes($dname);
        $time = round(microtime(true) * 1000);
        $sql = "INSERT INTO $this->measurementsTableName VALUES (NULL, '$userId', '$date', '$time', '$uid', '$dname')";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    private function saveNewValue($measId, $delay, $temp): Result {
        $measId = addslashes($measId);
        $delay = addslashes($delay);
        $time = round(microtime(true) * 1000);
        $isNaN = strtolower(substr($temp, 0, 1)) == "n";
        $tempStr = ($isNaN) ? "NULL" : "'" . addslashes($temp) . "'";
        $sql = "INSERT INTO $this->valuesTableName VALUES (NULL, '$measId', '$delay', '$time', $tempStr)";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    private function saveLimit($request): Result {
        if (!$this->session->isLogged()) return $this->session->notLoggedRet();
        $ret = $this->readJustMeasId($request->measId);
        if (Result::isErr($ret)) return $ret;
        $meas = $ret->item;
        if (($meas->userId != $this->session->id) && !$this->session->isUberLogged()) return Result::getErr('cannot add limit to meas that is not ours');
        
        $limit = new stdClass();
        $limit->measId = addslashes($meas->measId);
        $limit->value = addslashes($request->value);
        $limit->operation = addslashes($request->operation);
        $limit->email = addslashes($request->email);
        $limit->reached = 0;
        $limits = $this->limiterFactory->create([$limit]);
        $ret = $this->limiterFactory->saveLimit($limits[0]);
        return $ret;
    }

    private function eraseLimit($request) {
        if (!$this->session->isLogged()) return $this->session->notLoggedRet();
        $ret = $this->limiterFactory->readLimitId($request->id);
        if (Result::isErr($ret)) return $ret;
        $limit = $ret->item;
        $ret = $this->readJustMeasId($limit->measId);
        if (Result::isErr($ret)) return $ret;
        $meas = $ret->item;
        if (($meas->userId != $this->session->id) && !$this->session->isUberLogged()) return Result::getErr('cannot erase limit that is not ours');
        return $this->limiterFactory->eraseLimit($request->id);
    }
    private function eraseMeasId($request): Result {
        if (!$this->session->isLogged()) return $this->session->notLoggedRet();
        $ret = $this->readJustMeasId($request->measId);
        if (Result::isErr($ret)) return $ret;
        $meas = $ret->item;
        if (($meas->userId != $this->session->id) && !$this->session->isUberLogged()) return Result::getErr('cannot erase meas that is not ours');
        return $this->internalEraseMeasId($meas->measId);
    }
    private function internalEraseMeasId($measId): Result {
        $measId = addslashes($measId);
        $vtn = $this->valuesTableName;
        $sql = "DELETE FROM $vtn WHERE $vtn.measId = '$measId'";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        if (Result::isErr($ret)) return $ret;
        $ltn = $this->limiterFactory->limitsTableName;
        $sql = "DELETE FROM $ltn WHERE $ltn.measId = '$measId'";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        if (Result::isErr($ret)) return $ret;
        $mtn = $this->measurementsTableName;
        $sql = "DELETE FROM $mtn WHERE $mtn.id = '$measId'";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    
    private function readJustMeasId($measId): Result {
        $measId = addslashes($measId);
        $mtn = $this->measurementsTableName;
        $sql = "SELECT " . $this->getMtnColumns() . " FROM $mtn WHERE $mtn.id = $measId";
        $result = $this->sqlStuff->callQueryWithRes($sql);
        if (Result::isErr($result)) return $result;
        while($row = $result->item->fetch_assoc()) {
            $ret = $this->readMtnRow($row);
        }
        return Result::getOk($ret);
    }
    private function readLastRunningMeas($uid): Result {
        $uid = addslashes($uid);
        $userId = $this->session->id;
        $mtn = $this->measurementsTableName;
        $sql = "SELECT " . $this->getMtnColumns() . " FROM $mtn WHERE $mtn.userId = $userId AND $mtn.uid = '$uid' " .
               "ORDER BY $mtn.id DESC LIMIT 1";
        $result = $this->sqlStuff->callQueryWithRes($sql);
        if (Result::isErr($result)) return $result;
        while($row = $result->item->fetch_assoc()) {
            $ret = $this->readMtnRow($row);
        }
        return Result::getOk($ret);
    }

    private function readVtnRow($row): stdClass {
        $ret = new stdClass();
        $ret->valueId = $row['valueId'];
        $ret->measId = $row['measId'];
        $ret->delay = $row['delay'];
        $ret->time = $row['valueTime'];
        $tempStr = $row['temp'];
        $ret->temp = ($tempStr == null) ? "NaN" : $tempStr;
        return $ret;
    }
    private function readMtnRow($row): stdClass {
        $ret = new stdClass();
        $ret->measId = $row['measId'];
        $ret->userId = $row['userId'];
        $ret->date = $row['date'];
        $ret->time = $row['measTime'];
        $ret->uid = $row['uid'];
        $ret->dname = $row['dname'];
        return $ret;
    }

    private function getMtnColumns() {
        return  $this->measurementsTableName . ".id AS measId," .
                $this->measurementsTableName . ".userId," .
                $this->measurementsTableName . ".date," .
                $this->measurementsTableName . ".time as measTime," .
                $this->measurementsTableName . ".uid," .
                $this->measurementsTableName . ".dname";
    }
    private function getVtnColumns() {
        return  $this->valuesTableName . ".id AS valueId," .
                $this->valuesTableName . ".measId," .
                $this->valuesTableName . ".delay," .
                $this->valuesTableName . ".time as valueTime," .
                $this->valuesTableName . ".temp";
    }

    private function recreateAllTables($request) {
        if (!$this->session->isUberLogged()) return $this->session->notPermissionRet();
        $ret = $this->recreateDataTables($request);
        if (Result::isErr($ret)) return $ret;
        $ret = $this->limiterFactory->recreateLimitsTables($this->session);
        return $ret;
    }
    private function recreateDataTables($request) {
        if (!$this->session->isUberLogged()) return $this->session->notPermissionRet();
        $ret = $this->innerDropDataTables();
        if (Result::isErr($ret)) return $ret;
        $ret = $this->innerCreateDataTables();
        return $ret;
    }

    private function innerCreateDataTables(): Result {
        $sql = "CREATE TABLE IF NOT EXISTS $this->measurementsTableName (
            `id` float NOT NULL PRIMARY KEY AUTO_INCREMENT,
            `userId` int NOT NULL,
            `date` bigint NOT NULL,
            `time` bigint NOT NULL,
            `uid` varchar(15) NOT NULL,
            `dname` varchar(30) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        if (Result::isErr($ret)) return $ret;
        $sql = "CREATE TABLE IF NOT EXISTS $this->valuesTableName (
            `id` float NOT NULL PRIMARY KEY AUTO_INCREMENT,
            `measId` int NOT NULL,
            `delay` bigint NOT NULL,
            `time` bigint NOT NULL,
            `temp` float
           ) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    private function innerDropDataTables(): Result {
        $sql = "DROP TABLE IF EXISTS $this->measurementsTableName";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        if (Result::isErr($ret)) return $ret;
        $sql = "DROP TABLE IF EXISTS $this->valuesTableName";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        if (Result::isErr($ret)) return $ret;
        return $ret;
    }
    private function readSqlMeases($sqlMeases): Result {
        $ret = new stdClass();
        $nTemp = 0;
        $nMeas = 0;
        $lastMeasId = -1;
        $actMeas = new stdClass();
        while ($row = $sqlMeases->item->fetch_assoc()) {
            $valueRow = $this->readVtnRow($row);
            if ($lastMeasId == $valueRow->measId) {
                $actMeas->temperatures[$nTemp] = $valueRow;
                $nTemp++;
            } else {
                $lastMeasId = $valueRow->measId;
                $actMeas = $this->readMtnRow($row);
                $ret->measurements[$nMeas] = $actMeas;
                $nMeas++;
                $nTemp = 0;
                $actMeas->temperatures[$nTemp] = $valueRow;
                $nTemp++;
            }
        }
        return Result::getOk($ret);
    }
}
