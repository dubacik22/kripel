<?php
include_once "res.php";
class MessengerRequest {
    public $table;
}
class Messenger {
    private $session;
    private $sqlStuff;
    
    public function __construct(SqlStuff $sqlStuff, Session $session) {
        $this->sqlStuff = $sqlStuff;
        $this->session = $session;
    }
    public function doCommand(string $command, $request): Result {
        if ($this->sqlStuff->err) return $this->sqlStuff->message;
        $ret = null;
        $request->table = $request->table.'_discusion';
        switch ($command) {
        case 'readAll':
            $ret = $this->readMessages($request);
            break;
        case 'update':
            $ret = $this->updateMessage($request);
            break;
        case 'erase':
            $ret = $this->eraseMessage($request);
            break;
        case 'save':
            $ret = $this->saveMessage($request);
            break;
        case 'createTable':
            $ret = $this->createTable($request);
            break;
        default:
            return Result::getErr('unknown command '.$command);
        }
        return $ret;
    }
    
    private function createTable($request) {
        if (!$this->session->isUberLogged()) return $this->session->notPermissionRet();
        $tableName = addslashes($request->tableName);
        $sql = 'CREATE TABLE IF NOT EXISTS `'.$tableName.'_discusion` (
                `messageId` float NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `fromUser` int(11) NOT NULL,
                `toMessageIds` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
                `message` varchar(5000) CHARACTER SET utf16 COLLATE utf16_slovak_ci DEFAULT NULL,
                `time` int(11) NOT NULL
               ) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;';
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    private function readMessages($request): Result {
        $sql = "SELECT * FROM $request->table LEFT JOIN `users` ON `$request->table`.`fromUser`=`users`.`id` ORDER BY `messageId`";
        $result = $this->sqlStuff->callQueryWithRes($sql);
        if (Result::isErr($result)) {
            return $result;
        }
        $ret = new stdClass();
        $numberOfRow = 0;
        while($row = $result->item->fetch_assoc()) {
            $ret->messages[$numberOfRow] = new stdClass();
            $ret->messages[$numberOfRow]->messageId = $row['messageId'];
            $ret->messages[$numberOfRow]->message = stripslashes($row['message']);
            $ret->messages[$numberOfRow]->fromLoginName = $row['loginName'];
            $ret->messages[$numberOfRow]->fromUserName = $row['userName'];
            $ret->messages[$numberOfRow]->toMessageIds = $row['toMessageIds'];
            $ret->messages[$numberOfRow]->time = $row['time'];
            $numberOfRow++;
        }
        return Result::getOk($ret);
    }
    private function getCreatorId($request) : Result{
        $id = addslashes($request->id);
        $sql = "SELECT fromUser FROM $request->table WHERE messageId = '$id'";
        $result = $this->sqlStuff->callQueryWithRes($sql);
        if (Result::isErr($result)) {
            return $result;
        }
        $row = $result->item->fetch_assoc();
        $ret = $row['fromUser'];
        return Result::getOk($ret);
    }

    private function updateMessage($request): Result {
        if (!$this->session->isLogged()) return $this->session->notLoggedRet();
        $creator = $this->getCreatorId($request);
        if (Result::isErr($creator)) return $creator;
        $message = addslashes($request->message);
        $id = addslashes($request->id);
        $myLoginId = addslashes($this->session->id);

        if ($this->session->isUberLogged()) {
            $sql = "UPDATE $request->table SET message = '$message' WHERE CONCAT(messageId) = '$id';";
        } else {
            if ($this->session->id != $creator->item) {
                return $this->session->notPermissionRet();
            }
            $sql = "UPDATE $request->table SET message = '$message' WHERE CONCAT(messageId) = '$id' AND fromUser = '$myLoginId';";
        }

        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    private function eraseMessage($request): Result {
        if (!$this->session->isLogged()) return $this->session->notLoggedRet();
        if (!$this->session->isUberLogged()) return $this->session->notPermissionRet();
        $id = addslashes($request->id);
        $myLoginId = addslashes($this->session->id);
        
        $sql = "DELETE FROM $request->table WHERE messageId = '$id'";
        
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    private function saveMessage($request): Result {
        if (!$this->session->isLogged()) return $this->session->notLoggedRet();
        $date = time();
        $message = addslashes($request->message);
        $to = addslashes($request->to);
        $myLoginId = addslashes($this->session->id);
        $sql = "INSERT INTO $request->table VALUES (NULL, '$myLoginId', '$to', '$message', '$date')";
        
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
}
