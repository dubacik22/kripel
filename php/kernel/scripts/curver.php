<?php
include_once "SqlServerInfo.php";

$tempPath = 'uploads/';
function addHeaders() {
  header("Access-Control-Allow-Origin: *");
  header('Content-Type: application/json');
  header('Content-Type: text/event-stream');

  header("Access-Control-Allow-Methods: DELETE, POST, GET, OPTIONS");
  header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
}
function sqlAccessCred() {
  $ret = new SqlServerInfo();
  if(strpos($_SERVER['HTTP_HOST'],"localhost") !== false) {
    $ret->serverName = 'localhost';
    $ret->dbName = 'kripel_database';
    $ret->userName = 'root';
    $ret->password = 'mysql';
  } else {
    $ret->serverName = "46.229.230.163";
    $ret->dbName = "ko023805db";
    $ret->userName = "ko023800";
    $ret->password= "zaj3beMpl1N";
  }

  return $ret;
}
function getTinyIntFromBool(bool $bool): int {
    if ($bool == true) {
        return 1;
    }
    if ($bool == false) {
        return 0;
    }
    if ($bool == 'true') {
        return 1;
    }
    if ($bool == 'false') {
        return 0;
    }
    if ($bool) {
        return 1;
    }
    return 0;
}
function getBoolFromTinyInt(int $int): bool {
    if ($int == 0) {
        return false;
    }
    return true;
}
function getOnlyJsonInput() {
    $ret = new stdClass();
    $ret->toDo = "doodler_getDoneLinesData";
    return $ret;
}
?>