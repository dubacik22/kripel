<?php
class Users {
    private $session;
    private $sqlStuff;

    public function __construct(SqlStuff $sqlStuff, Session $session) {
        $this->sqlStuff = $sqlStuff;
        $this->session = $session;
    }

    public function doCommand($command, $request): Result {
        if ($this->sqlStuff->err) return $this->sqlStuff->message;
        $ret = new stdClass();
        switch ($command) {
        case 'login':
            $ret = $this->loginUser($request);
            break;
        case 'edit':
            $ret = $this->editUser($request);
            break;
        case 'readAllNames':
            $ret = $this->readAllNames();
            break;
        case 'save':
            $ret = $this->saveUser($request);
            break;
        case 'changePass':
            $ret = $this->changePass($request);
            break;
        case 'erase':
            $ret = $this->eraseUser($request);
            break;
        case 'createTable':
            $ret = $this->createTable($request);
            break;
        case 'makeDubacik22Uber':
            $ret = $this->makeDubacik22Uber();
            break;
        case 'makeDubacik22OnlyUber':
            $ret = $this->makeDubacik22OnlyUber();
            break;
        default:
            return Result::getErr('unknown command '.$command);
        }
        return $ret;
    }
    private function createTable($request) {
        if (!$this->session->isUberLogged()) return $this->session->notPermissionRet();
        $tableName = addslashes($request->tableName);
        $sql = 'CREATE TABLE IF NOT EXISTS `users` (
                `id` int(11) NOT NULL PRIMARY KEY UNIQUE KEY AUTO_INCREMENT,
                `loginName` varchar(20) CHARACTER SET utf16 COLLATE utf16_slovak_ci NOT NULL,
                `userName` varchar(20) CHARACTER SET utf32 COLLATE utf32_slovak_ci NOT NULL,
                `email` varchar(255) CHARACTER SET utf32,
                `pass` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
                `permissions` varchar(30) NOT NULL
               ) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;';
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    private function changePass($request) {
        if (!$this->session->isLogged()) return $this->session->notLoggedRet();
        if (!$this->session->isUberLogged()) return $this->session->notPermissionRet();
        $pass = addslashes($request->password);
        $sql = "UPDATE users SET pass = '$pass' WHERE loginName = '$request->loginName'";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    
    private function eraseUser($request) {
        if (!$this->session->isLogged()) return $this->session->notLoggedRet();
        if (!$this->session->isUberLogged()) return $this->session->notPermissionRet();
        $loginName = addslashes($request->loginName);

        $sql = "DELETE FROM users WHERE loginName = '$loginName'";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }
    private function readAllNames(): Result {
        if (!$this->session->isLogged()) return $this->session->notLoggedRet();
        if (!$this->session->isUberLogged()) return $this->session->notPermissionRet();
        $sql = "SELECT id, loginName, userName FROM users";
        $result = $this->sqlStuff->callQueryWithRes($sql);

        if (Result::isErr($result)) {
            return Result::getErr($this->sqlStuff->message);
        }
        $numberOfRow = 0;
        $ret = new stdClass();
        while($row = $result->item->fetch_assoc()) {
          $ret->users[$numberOfRow] = new stdClass();
          $ret->users[$numberOfRow]->id = $row['id'];
          $ret->users[$numberOfRow]->loginName = $row['loginName'];
          $ret->users[$numberOfRow]->userName = $row['userName'];
          $ret->users[$numberOfRow]->email = $row['email'];
          $numberOfRow++;
        }
        return Result::getOk($ret);
    }
    public function innerFindUser(string $parName, string $par): Result {
        $parNameS = addslashes($parName);
        $parS = addslashes($par);
        $sql = "SELECT * FROM users WHERE $parNameS='$parS'";
        $result = $this->sqlStuff->callQueryWithRes($sql);
        if (Result::isErr($result)) return Result::getErr($this->sqlStuff->message." user with ".$parNameS.": ".$parS." probably not exist");
        $row = $result->item->fetch_assoc();
        if (!$row) return Result::getErr('cannot fetch result');
        $ret = new UserInfo();
        $ret->userName = stripslashes($row['userName']);
        $ret->loginName = stripslashes($row['loginName']);
        $ret->email = stripslashes($row['email']);
        $ret->pass = stripslashes($row['pass']);
        $ret->permissions = stripslashes($row['permissions']);
        $ret->id = stripslashes($row['id']);

        return Result::getOk($ret);
    }
    private function loginUser($request): Result {
        $ret = $this->innerFindUser('loginName', $request->loginName);
        if (Result::isErr($ret)) return $ret;
        if (stripslashes($ret->item->pass) != $request->password) return Result::getErr('pass not correct');
        $user = $ret->item;
        $user->pass = '-';
        unset($ret->item);
        $ret->item = new stdClass();
        $ret->item->user = $user;
        $ret->item->session = Session::createSessionToken($ret->item->user);
        return $ret;
    }
    private function innerEditUser($request): Result {
        if (!$this->session->isLogged()) return $this->session->notLoggedRet();
        $userName = addslashes($request->userName);
        $loginName = addslashes($request->loginName);
        $pass = addslashes($request->password);
        $email = addslashes($request->email);
        $id = addslashes($request->id);
        $myLoginId = addslashes($this->session->id);
        
        if ($this->session->isUberLogged()) {
            if ($pass) {
                $sql = "UPDATE users SET loginName = '$loginName', userName = '$userName', email = '$email', pass = '$pass' WHERE id = $id";
            } else {
                $sql = "UPDATE users SET loginName = '$loginName', userName = '$userName', email = '$email' WHERE id = $id";
            }
        } else {
            if ($this->session->id != $request->id) return $this->session->notPermissionRet();
            if ($pass) {
                $sql = "UPDATE users SET loginName = '$loginName', userName = '$userName', email = '$email', pass = '$pass' WHERE id = $id AND id = $myLoginId";
            } else {
                $sql = "UPDATE users SET loginName = '$loginName', userName = '$userName', email = '$email' WHERE id = $id AND id = $myLoginId";
            }
        }
        
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        return $ret;
    }

    private function editUser($request): Result {
        $request->table = 'users';
        $origUser = $this->innerFindUser('id', $request->id);
        if (!Result::isErr($origUser)) {
            if (($origUser->item->loginName == $request->loginName) || Result::isErr($this->innerFindUser('loginName', $request->loginName))) {
                return $this->innerEditUser($request);
            } else return Result::getErr('user already exist');
        } else return Result::getErr('user doesnt exist');
    }
    private function saveUser($request): Result {
        if (!ErrResult::isErr($this->innerFindUser('loginName', $request->loginName))) {
            return ErrResult::getErr('user already exist');
        }
        $userName = addslashes($request->userName);
        $loginName = addslashes($request->loginName);
        $email = addslashes($request->email);
        $pass = addslashes($request->password);
        
        $sql = "INSERT INTO users VALUES (NULL, '$loginName', '$userName', '$email', '$pass', 'none')";
        
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);
        if (!ErrResult::isErr($ret)) {
            $ret->userName = $userName;
            $ret->loginName = $loginName;
            $ret->pass = $pass;
        }
        return $ret;
    }
    private function makeDubacik22Uber() {
        $sql = "UPDATE users SET permissions = 'all' WHERE loginName='dubacik22';";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql);

        return $ret;
    }
    private function makeDubacik22OnlyUber() {
        $sql1 = "UPDATE users SET permissions = '';";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql1);
        $sql2 = "UPDATE users SET permissions = 'all' WHERE loginName='dubacik22';";
        $ret = $this->sqlStuff->callQueryWithoutRes($sql2);

        return $ret;
    }
}
