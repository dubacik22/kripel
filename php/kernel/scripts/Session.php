<?php
class Session extends UserInfo {
    public $logged;
    public $message;

    public function __construct() {
        $this->logged = false;
    }
    
    public function setSession($data, Users $users) {
        $this->logged = false;
        if ($data == null) return;
        $session = JWT::decode($data, '$ret->pass');
        $foundUser = $users->innerFindUser('loginName', $session->loginName);
        if (Result::isErr($foundUser)) {
            $this->message = $users->message;
            return;
        }
        $this->logged = true;
        $this->setData(UserInfo::createUserInfo($session));
        $this->permissions = $foundUser->item->permissions;
        $this->userName = $foundUser->item->userName;
    }
    public function setUser($uname, $pwd, Users $users) {
        $this->logged = false;
        $foundUser = $users->innerFindUser('loginName', $uname);
        if (Result::isErr($foundUser)) {
            $this->message = $users->message;
            return;
        }
        if ($foundUser->item->pass != $pwd) {
            $this->message = "hash of password is wrong,";
            return;
        }
        $this->logged = true;
        $this->loginName = $foundUser->item->loginName;
        $this->id = $foundUser->item->id;
        $this->permissions = $foundUser->item->permissions;
        $this->userName = $foundUser->item->userName;
    }
    private function setData(UserInfo $session) {
        $this->loginName = $session->loginName;
        $this->id = $session->id;
    }
    public function isLogged(): bool {
        if (!$this->logged) return false;
        return true;
    }
    public function isUberLogged(): bool {
        if (!$this->isLogged()) return false;
        if ($this->permissions == 'all') return true;
        return false;
    }
    public static function createSessionToken(UserInfo $user): string {
        $ret = JWT::encode($user, '$ret->pass');
        return $ret;
    }
    public function notPermissionRet(): Result {
        $ret = Result::getErr('have no permissions');
        return $ret;
    }
    public function notLoggedRet(): Result {
        $ret = Result::getErr('not logged');
        return $ret;
    }
}
