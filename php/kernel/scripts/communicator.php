<?php

include_once "res.php";
include_once "jwt_helper.php";
include_once "Messenger.php";
include_once 'Games.php';
include_once "Galaxy.php";
include_once "UserInfo.php";
include_once "Session.php";
include_once "SqlStuff.php";
include_once "Users.php";
include_once "Tester.php";
include_once "Temperaturer.php";
include_once "Doodler.php";
include_once "Rest.php";
include_once "Result.php";
include_once "ErrResult.php";
include_once "ItemResult.php";
addHeaders();
$sqlServer = sqlAccessCred();
$request = (true) ? getJsonInput() : sjs();
$uPos = strpos($request->toDo, '_');
if ($uPos === false) {
    $classType = '';
    $command = $request->toDo;
} else {
    $classType = substr($request->toDo, 0, $uPos);
    $command = substr($request->toDo, $uPos + 1);
}

$sqlStuff = new SqlStuff($sqlServer);
$session = new Session();
$users = new Users($sqlStuff, $session);
if (isset($request->session)) $session->setSession($request->session, $users);
else if (isset($request->uname) && isset($request->pwd)) $session->setUser($request->uname, $request->pwd, $users);

$ret = new stdClass();
switch ($classType) {
case 'temperaturer':
    $temperaturer = new Temperaturer($sqlStuff, $session);
    $ret = $temperaturer->doCommand($command, $request);
    if (!Result::isErr($ret)) {
        $ret = $ret->item;
        $ret->success = true;
    }
    break;
case 'messenger':
    $messenger = new Messenger($sqlStuff, $session);
    $ret = $messenger->doCommand($command, $request);
    if (!Result::isErr($ret)) {
        $ret = $ret->item;
        $ret->success = true;
    }
    break;
case 'users':
    $ret = $users->doCommand($command, $request);
    if (!Result::isErr($ret)) {
        $ret = $ret->item;
        $ret->success = true;
    }
    break;
case 'tester':
    $tester = new Tester($sqlStuff, $session);
    $ret = $tester->doCommand($command, $request);
    if (!Result::isErr($ret)) {
        $ret = $ret->item;
        $ret->success = true;
    }
    break;
case 'rest':
    $rest = new Rest($sqlStuff, $session);
    $ret = $rest->doCommand($command, $request);
    if (!Result::isErr($ret)) {
        $ret = $ret->item;
        $ret->success = true;
    }
    break;
case 'doodler':
    $rest = new Doodler($sqlStuff, $session);
    $ret = $rest->doCommand($command, $request);
    if (!Result::isErr($ret)) {
        $ret = $ret->item;
        $ret->success = true;
    }
    break;
case 'galaxy':
    $rest = new Galaxy($sqlStuff);
    $ret = $rest->doCommand($command, $request);
    if (!Result::isErr($ret)) {
        $ret = $ret->item;
        $ret->success = true;
    }
    break;
case '':
    if ($command == 'ping') $ret = Result::getOk();
    else $ret = Result::getErr('unknown command '.$command);
    break;
default:
    $ret = Result::getErr('unknown class '.$classType);
}
echo json_encode($ret);
flush();
?>
