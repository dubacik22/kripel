<?php
class UserInfo {
    public $id;
    public $loginName;
    public $userName;
    public $permissions;
    public $pass;


    public static function createUserInfo($data) {
        $ret = new UserInfo();
        $ret->id = $data->id;
        $ret->loginName = $data->loginName;
        $ret->userName = $data->userName;
        $ret->permissions = $data->permissions;
        return $ret;
    }
}
