<?php
include_once "res.php";
addHeaders();
$ret=new Returned();
$ret->success=false;
$extensions = ['jpg', 'jpeg', 'png', 'gif'];
$files=$_FILES['files'];
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    foreach($files['name'] as $key=>$val) {
        $fileName = $files['name'][$key];
        $wholeFileName = $_POST['path'].'/'.$fileName;
        $file_ext = pathinfo($fileName, PATHINFO_EXTENSION);
        if (in_array($file_ext, $extensions)) {
            if(move_uploaded_file($files['tmp_name'][$key],$wholeFileName)){
                $ret->success=true;
            } else {
                $ret->success=false;
            }
        } else $ret->success=false;
    }
}else{
    $ret->success=false;
}
$retJSON = json_encode($ret);
echo $retJSON;
?>