import { Component, AfterViewInit, OnDestroy, ChangeDetectorRef, Input, Output, EventEmitter } from '@angular/core';
import { BooleanWrapper } from './res/BooleanWrapper'

@Component({
  selector: 'tbutton',
  templateUrl: './tbutton.component.html',
  styleUrls: ['./tbutton.component.css']
})
export class TButton implements AfterViewInit, OnDestroy {
  private _pressed: boolean = false;
  @Input()
  public get pressed(): boolean {
    return this._pressed;
  }
  public set pressed(value: boolean) {
    if (this._pressed == value) return;
    this._pressed = value;
    this.detectChanges();
  }
  @Output()
  public pressedChanged: EventEmitter<boolean> = new EventEmitter();
  @Output('buttonClicked')
  public click: EventEmitter<BooleanWrapper> = new EventEmitter();
  
  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private chDref: ChangeDetectorRef) {}
  public ngAfterViewInit() {
    this.chDref.detach();
  }
  public ngOnDestroy() {
  }
  public buttonClick() {
    this.pressed = !this._pressed;
    let wrapper: BooleanWrapper = BooleanWrapper.get(this._pressed);
    this.click.emit(wrapper);
    this._pressed = wrapper.value;
    this.pressedChanged.emit(this._pressed);
    this.detectChanges();
  }
}