export class BooleanWrapper {
    public value: boolean;

    public static get(value: boolean): BooleanWrapper {
        let ret: BooleanWrapper = new BooleanWrapper();
        ret.value = value;
        return ret;
    }
}