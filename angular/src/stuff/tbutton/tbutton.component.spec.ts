import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TButton } from './tbutton.component';

describe('TButton', () => {
  let component: TButton;
  let fixture: ComponentFixture<TButton>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TButton ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TButton);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
