import { Vector } from './solaris/Vector';

export class MathExt {
    
    public static get randomBool(): boolean {
        if (Math.random() <= 0.5) return true;
        return false;
    }

    public static randomNumTo(to: number): number {
        return Math.random() * to;
    }
    public static randomWholeNumTo(to: number): number {
        return Math.floor(MathExt.randomNumTo(to));
    }

    public static randomNumFromTo(from: number, to: number): number {
        if (to <= from) {
            if (to === from) return to;
            return NaN;
        }
        var rng = to - from;
        return from + (Math.random() * rng);
    }
    public static randomWholeNumFromTo(from: number, to: number): number {
        return Math.floor(MathExt.randomNumFromTo(from, to));
    }

    public static randomVectorFromTo(from: number, to: number): Vector {
        return MathExt.randomVectorFromTo3(from, to, from, to, from, to);
    }
    public static randomVectorFromTo3(fromx: number, tox: number, fromy: number, toy: number, fromz: number, toz: number): Vector {
        var x = MathExt.randomNumFromTo(fromx, tox);
        var y = MathExt.randomNumFromTo(fromy, toy);
        var z = MathExt.randomNumFromTo(fromz, toz);
        return new Vector(x, y, z);
    }
    public static randomVectorTo(to: number): Vector {
        return MathExt.randomVectorFromTo(0, to);
    }
    public static randomVectorTo3(tox: number, toy: number, toz: number): Vector {
        return MathExt.randomVectorFromTo3(0, tox, 0, toy, 0, toz);
    }
    public static randomVectorToV(to: Vector): Vector {
        return MathExt.randomVectorFromTo3(0, to.x, 0, to.y, 0, to.z);
    }
}