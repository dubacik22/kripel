import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KindaPerspectiveComponent } from './kinda-perspective.component';

describe('KindaPerspectiveComponent', () => {
  let component: KindaPerspectiveComponent;
  let fixture: ComponentFixture<KindaPerspectiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KindaPerspectiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KindaPerspectiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
