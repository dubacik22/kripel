import { OnInit, Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'kinda-perspective',
  templateUrl: './kinda-perspective.component.html',
  styleUrls: ['./kinda-perspective.component.css']
})
export class KindaPerspectiveComponent implements OnInit {
  @Output() execute: EventEmitter<void> = new EventEmitter();

  @Input() foregroundImage: string;
  @Input() frgMarginLeft: string = '0';
  @Input() frgMarginTop: string = '0';
  @Input() frgWidth: string = 'auto';
  @Input() frgHeight: string = 'auto';

  @Input() middleImage: string;
  @Input() mddMarginLeft: string = '0';
  @Input() mddMarginTop: string = '0';
  @Input() mddWidth: string = 'auto';
  @Input() mddHeight: string = 'auto';

  @Input() backgroundImage: string;
  @Input() bkgMarginLeft: string = '0';
  @Input() bkgMarginTop: string = '0';
  @Input() bkgWidth: string = 'auto';
  @Input() bkgHeight: string = 'auto';
  @Input() bkgColor: string = '';

  @Input()
  public compact: boolean = false;

  constructor() { }
  ngOnInit(){
    
  }
  clicked() {
    this.execute.emit();
  }

}
