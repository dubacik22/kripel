import { Directive, OnInit, Input, ElementRef, HostListener, OnChanges, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[scrollableStyles]'
})
export class ScrollableStylesDirective implements OnInit, OnChanges {
  @Input('ssHeight') height: number;
  @Input('ssStyleUntil') classUntil: string;
  @Input('ssStyleAfter') classAfter: string;
  @Input('ssSetOnUndef') setOnUndef: boolean;
  private isLower:boolean=false;
  private element:HTMLElement;
  constructor(elementRef:ElementRef) {
    this.element=elementRef.nativeElement;
  }
  ngOnInit(){
    this.renew();
  }

  @HostListener("window:scroll", [])
  onElementScroll(event: Event){
    /*if (this.waiting) return;
    this.waiting = true;
    setTimeout(()=>{
      this.waiting = false;//*/
      this.scrolled();
    //}, 50);
  }
  ngOnChanges(changes: SimpleChanges){
    this.renew();
  }
  private renew() {
    if (this.height == undefined) {
      if (this.setOnUndef) {
        if (this.classUntil!=undefined) this.addClass(this.element,this.classUntil);
        if (this.classAfter!=undefined) this.removeClass(this.element,this.classAfter);
      } else {
        this.removeClass(this.element,this.classUntil);
        this.removeClass(this.element,this.classAfter);
      }
      return;
    }
    let scrolledOffset: number = window.scrollY;
    if (scrolledOffset > this.height) this.isLower = false;
    else this.isLower=true;
    this.setClasses(this.isLower, this.classUntil, this.classAfter);
  }
  private scrolled() {
    if (this.height == undefined) return;
    let scrolledOffset: number = window.scrollY;
    if (scrolledOffset > this.height) {
      if (this.isLower) {
        this.isLower = false;
        this.setClasses(this.isLower, this.classUntil, this.classAfter);
      }
    } else {
      if (!this.isLower) {
        this.isLower = true;
        this.setClasses(this.isLower, this.classUntil, this.classAfter);
      }
    }
  }
  private setClasses(value: boolean, classUntil: string, classAfter: string) {
    if (value) {
      if (classUntil) this.addClass(this.element, classUntil);
      if (classAfter) this.removeClass(this.element, classAfter);
    } else {
      if (classUntil) this.removeClass(this.element, classUntil);
      if (classAfter) this.addClass(this.element, classAfter);
    }
  }
  private addClass(element:HTMLElement,className:string){
    element.classList.add(className);
  }
  private removeClass(element:HTMLElement,className:string){
    element.classList.remove(className);
  }
}
