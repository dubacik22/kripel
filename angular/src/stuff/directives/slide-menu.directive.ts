import { Directive, ElementRef, Input, AfterViewInit, OnChanges, SimpleChanges, ViewContainerRef, TemplateRef } from '@angular/core';
import { AnimationBuilder, AnimationMetadata, style, animate, AnimationPlayer } from '@angular/animations';

@Directive({
  selector: '[slideMenuShow]',
})

export class SlideMenuDirective implements AfterViewInit, OnChanges {
  @Input('slideMenuShow') slideMenuShow: boolean;
  @Input('smForceAppear') forceAppear: boolean;

  private active:boolean;
  private player: AnimationPlayer;

  constructor(private elref:ElementRef, private viewContainer: ViewContainerRef,
    private template: TemplateRef<object>, private animationBuilder: AnimationBuilder) {
    viewContainer.clear();
  }
  ngAfterViewInit(){
  }
  private elementToAnimate: HTMLElement;
  private startShow() {
    let showMeta: AnimationMetadata[];
    let rand: number = Math.random();
    if (rand < 0.1) showMeta = this.showByAppearNBlink();
    else if (rand < 0.4) showMeta = this.showByAppear();
    else if (rand < 0.7) showMeta = this.showByHSlideRight();
    else showMeta = this.showByHSlideLeft();

    if (this.forceAppear) showMeta = this.showByAppear();

    this.viewContainer.createEmbeddedView(this.template);
    this.elementToAnimate = this.viewContainer.element.nativeElement.nextSibling;

    if (this.player) this.player.destroy();
    const factory = this.animationBuilder.build(showMeta);
    const player = factory.create(this.elementToAnimate);

    let positon: string = this.elementToAnimate.style.position;
    player.onStart(() => {
      this.elementToAnimate.style.position='absolute';
    });
    player.play();
    player.onDone(() => {
      player.destroy();
      this.elementToAnimate.style.position = positon;
      this.elementToAnimate.style.transform = '';
    });
  }
  private startHide() {
    let hideMeta: AnimationMetadata[];
    let rand = Math.random();
    if (rand < 0.3) hideMeta = this.hideByHSlideLeft();
    else if (rand < 0.6) hideMeta = this.hideByHSlideRight();
    else hideMeta = this.hideByDissappear();

    //hideMeta=this.hideByDissappear();

    if (this.player) this.player.destroy();
    const factory = this.animationBuilder.build(hideMeta);
    const player = factory.create(this.elementToAnimate);

    let positon:string = this.elementToAnimate.style.position;
    player.onStart(() => {
      this.elementToAnimate.style.position = 'absolute';
    });
    player.play();
    player.onDone(() => {
      this.viewContainer.clear();
      player.destroy();
      this.elementToAnimate.style.position = positon;
    });
  }

  ngOnChanges(changes: SimpleChanges){
    if (changes.slideMenuShow){
      this.showOrHide();
    }
  }
  private _slideMenuShow: boolean = false;
  private showOrHide() {
    if (this._slideMenuShow == this.slideMenuShow) return;
    if (this.slideMenuShow) {
      this.active = true;
      this.startShow();
    } else {
      if (this.active) {
        this.active = false;
        this.startHide();
      }
    }
    this._slideMenuShow = this.slideMenuShow;
  }
  private showByHSlideRight(): AnimationMetadata[] {
    let offset: number = window.innerWidth;
    let transformBegin: string = 'translate('+offset+'px,0px)';
    let transformFinal: string = 'translate(0px,0px)';
    let overTransform: string = 'translate(-30px,0px)';
    let ret: AnimationMetadata[] = [
      style({ transform: transformBegin, opacity: 0 }),
      animate('600ms ease', style({ transform: overTransform, opacity: 0.7 })),
      animate('300ms ease-out', style({ transform: transformFinal, opacity: 1 })),
    ];
    return ret;
  }
  private showByHSlideLeft(): AnimationMetadata[] {
    let offset: number = -window.innerWidth;
    let transformBegin: string = 'translate('+offset+'px,0px)';
    let transformFinal: string = 'translate(0px,0px)';
    let overTransform: string = 'translate(30px,0px)';
    let ret: AnimationMetadata[] = [
      style({ transform: transformBegin, opacity: 0 }),
      animate('600ms ease', style({ transform: overTransform, opacity: 0.7 })),
      animate('300ms ease-out', style({ transform: transformFinal, opacity: 1 })),
    ];
    return ret;
  }
  private showByAppearNBlink(): AnimationMetadata[] {
    let transformFinal: string = 'translate(0px,0px)';
    let ret: AnimationMetadata[] = [
      style({ transform: transformFinal, opacity: 0 }),
      animate('300ms ease', style({ opacity: 1 })),
      animate('50ms ease', style({ opacity: 0.5 })),
      animate('50ms ease', style({ opacity: 1 })),
    ];
    return ret;
  }
  private showByAppear(): AnimationMetadata[] {
    let transformFinal: string = 'translate(0px,0px)';
    let ret: AnimationMetadata[] = [
      style({ transform: transformFinal, opacity: 0 }),
      animate('300ms ease', style({ opacity: 1 }))
    ];
    return ret;
  }

  private hideByHSlideLeft(): AnimationMetadata[] {
    let offset: number = window.innerWidth;
    let transform: string = 'translate('+-offset+'px,0px)';
    let transform0: string = 'translate(0px,0px)';
    let ret: AnimationMetadata[] = [
      style({ transform: transform0, opacity: 1 }),
      animate('750ms ease', style({ transform: transform, opacity: 0 })),
    ];
    return ret;
  }
  private hideByHSlideRight(): AnimationMetadata[] {
    let offset: number = window.innerWidth;
    let transform: string = 'translate('+offset+'px,0px)';
    let transform0: string = 'translate(0px,0px)';
    let ret: AnimationMetadata[] = [
      style({ transform: transform0, opacity: 1 }),
      animate('750ms ease', style({ transform: transform, opacity: 0 })),
    ];
    return ret;
  }
  private hideByDissappear(): AnimationMetadata[] {
    let ret: AnimationMetadata[] = [
      animate('600ms ease', style({ opacity: 0 })),
    ];
    return ret;
  }
  private hideMeta3(): AnimationMetadata[] {
    let ret: AnimationMetadata[] = [
      animate('0ms ease', style({ opacity: 0 })),
    ];
    return ret;
  }
}
