import { Directive, Input, ElementRef, OnInit, AfterViewInit, OnChanges, SimpleChanges, OnDestroy, HostListener } from '@angular/core';

@Directive({
  selector: '[FixedScrollable]'
})
export class FixedScrollableDirective implements OnChanges, OnInit, AfterViewInit, OnDestroy {//AfterViewChecked
  @Input('fsLastScroll') lastScroll: number;
  @Input('fsFirstHeight') firstHeight: number; //je vhodne nastavovat ak pouzivam animaciu na zmenu vysky
  private innerFirstHeight: number;
  @Input('fsLastHeight') lastHeight: number;
  private innerLastHeight: number;
  @Input('fsFirstPos') firstPos: number; //je vhodne nastavovat ak pouzivam animaciu na zmenu polohy
  private innerFirstPos: number;
  @Input('fsLastPos') lastPos: number;
  private innerLastPos: number;

  @Input('fsVerbose') verbose: boolean = false;

  private initialPosition: string;
  private element: HTMLElement;
  private sameMoveVel: boolean = false;
  private refreshSameMoveVel() {
    let moveRng = -(this.innerLastPos-this.innerFirstPos);
    if ((moveRng == undefined) || (moveRng == this.lastScroll)) this.sameMoveVel = true;
    else this.sameMoveVel = false;
  }
  private changes: MutationObserver;
  constructor(elementRef: ElementRef) {
    this.element = elementRef.nativeElement;
    this.element.style.position = 'absolute';

    this.changes = new MutationObserver((mutations: MutationRecord[]) => {
        mutations.forEach((mutation: MutationRecord)=>{
          if (mutation.attributeName == 'class') {
            this.onElementMovedOrResized();
          }
        });
      }
    );

    this.changes.observe(this.element, {
      attributes: true,
    });

  }
  ngOnInit() {
    if (this.firstPos != undefined) this.innerFirstPos = this.firstPos;
    if (this.lastPos != undefined) this.innerLastPos = this.lastPos;
    if (this.firstHeight != undefined) this.innerFirstHeight = this.firstHeight;
    if (this.lastHeight != undefined) this.innerLastHeight = this.lastHeight;

    if (this.firstPos == undefined) this.refreshSameMoveVel();
    if (this.sameMoveVel) this.initialPosition = 'absolute';
    else this.initialPosition = 'fixed';
    this.element.style.position = this.initialPosition;
  }
  ngAfterViewInit() {
    if (this.firstHeight == undefined) this.innerFirstHeight = this.element.offsetHeight;
    if (this.firstPos == undefined) this.innerFirstPos = this.element.offsetTop;
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes.lastPos || changes.lastHeight || changes.lastScroll || changes.firstHeight) {
      this.onElementMovedOrResized();
    }
  }
  ngOnDestroy() {
    this.changes.disconnect();
  }
  onElementMovedOrResized() {
    this.element.style.top = '';
    this.element.style.height = '';

    if (this.firstHeight != undefined) this.innerFirstHeight = this.firstHeight;
    else this.innerFirstHeight = this.element.offsetHeight;
    if (this.firstPos != undefined) this.innerFirstPos = this.firstPos;
    else this.innerFirstPos = this.element.offsetTop;

    this.innerLastPos = this.lastPos;
    this.innerLastHeight = this.lastHeight;

    this.refreshSameMoveVel();
    this.scrolled();
  }
  
  @HostListener("window:scroll", [])
  onElementScroll(event: Event) {
    this.scrolled();
  }
  private stopped: boolean = false;
  scrolled() {
    let scrolledOffset: number = window.scrollY;
    if (this.innerLastPos !== undefined) {
      if (this.sameMoveVel) {
        let elY = this.innerFirstPos-scrolledOffset;
        if (elY < this.innerLastPos) {
          if (!this.stopped) {
            this.element.style.position = 'fixed';
            this.stopped = true;
            this.element.style.top = this.innerLastPos+'px';
          }
        } else {
          if (this.stopped) {
            this.element.style.position = this.initialPosition;
            this.stopped = false;
            this.element.style.top = this.innerFirstPos+'px';
          }
        }
      } else {
        if (scrolledOffset < this.lastScroll) {
          this.element.style.top = (this.innerFirstPos-(this.innerFirstPos-this.innerLastPos)*scrolledOffset/this.lastScroll)+'px';
        } else {
          this.element.style.top = this.innerLastPos+'px';
        }
      }
    }
    if (this.innerLastHeight !== undefined) {
      if (scrolledOffset < this.lastScroll) {
        this.element.style.height = (this.innerFirstHeight-(this.innerFirstHeight-this.innerLastHeight)*scrolledOffset/this.lastScroll)+'px';
      } else {
        this.element.style.height = this.innerLastHeight+'px';
      }
    }
  }

  private trace(msg: any) {
    if (this.verbose) console.log(msg);
  }
}