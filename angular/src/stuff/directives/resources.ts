export class NumberStringer{
    private str:string='';
    add(n:number){
        if (this.str) this.str+=' ';
        this.str+=n;
    }
    toString(){
        return this.str;
    }
}
export class ElementPos{
    public top:number=0;
    public bottom:number=0;
    public left:number=0;
    public right:number=0;
    public centerTop:number=0;
    public centerLeft:number=0;
    public dummySquareEdge:number=0;
    public dummySquareTop:number=0;
    public dummySquareBottom:number=0;
    public dummySquareLeft:number=0;
    public dummySquareRight:number=0;
  
    constructor(){}
    refreshFromElement(element:HTMLElement){
      this.initFromRect(element.getBoundingClientRect());
    }
    refreshFromElementWithoutBounce(element:HTMLElement){
      this.top=element.clientTop;
      this.bottom=this.top+element.clientHeight;
      this.left=element.clientLeft;
      this.right=this.left+element.clientWidth;
      this.centerTop=this.top+((this.bottom-this.top)/2);
      this.centerLeft=this.left+((this.right-this.left)/2);
      this.dummySquareEdge=(element.clientHeight+element.clientWidth)/2;
      let dummySquareHalfEdge:number=this.dummySquareEdge/2;
      this.dummySquareTop=this.centerTop-dummySquareHalfEdge;
      this.dummySquareBottom=this.centerTop+dummySquareHalfEdge;
      this.dummySquareLeft=this.centerLeft-dummySquareHalfEdge;
      this.dummySquareRight=this.centerLeft+dummySquareHalfEdge;
    }
    initFromRect(rect:ClientRect|DOMRect){
      this.top=rect.top;
      this.bottom=rect.bottom;
      this.left=rect.left;
      this.right=rect.right;
      this.centerTop=this.top+((this.bottom-this.top)/2);
      this.centerLeft=this.left+((this.right-this.left)/2);
      this.dummySquareEdge=(rect.width+rect.height)/2;
      let dummySquareHalfEdge:number=this.dummySquareEdge/2;
      this.dummySquareTop=this.centerTop-dummySquareHalfEdge;
      this.dummySquareBottom=this.centerTop+dummySquareHalfEdge;
      this.dummySquareLeft=this.centerLeft-dummySquareHalfEdge;
      this.dummySquareRight=this.centerLeft+dummySquareHalfEdge;
    }
    isInMe(x:number,y:number):boolean{
      if (x<this.left) return false;
      if (x>this.right) return false;
      if (y<this.top) return false;
      if (y>this.bottom) return false;
      return true;
    }
    isInDummySquare(x:number,y:number):boolean{
      if (x<this.dummySquareLeft) return false;
      if (x>this.dummySquareRight) return false;
      if (y<this.dummySquareTop) return false;
      if (y>this.dummySquareBottom) return false;
      return true;
    }
  }