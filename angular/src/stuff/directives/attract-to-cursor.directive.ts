import { Directive, HostListener, ElementRef, AfterViewInit, Input } from '@angular/core';
import { ElementPos} from './resources';

@Directive({
  selector: '[attractToCursor]'
})
//bolo byfajn sa chytit eventu zmeny suradnic a velkosti mojho elementu a scrollu
// lebo ak sa upravi velkost a pozicia medzi initom a samotnym pouzitim direktivy.
// tak pracujem so starymi udajmi.
export class AttractToCursorDirective implements AfterViewInit {
  @Input('atcImpactDist') impactDistance: number;//od akej vzdialenosti mam vplyv
  @Input('atcMaxClosure') maxClosure: number;//ako velmi sa pritahuje ku poiteru
  private initPos:ElementPos=new ElementPos();
  private element:HTMLElement;

  constructor(elementRef:ElementRef) {
    this.element=elementRef.nativeElement;
  }
  ngAfterViewInit(){
    this.element.style.transition='transform 2s ease-in-out';
  }

  private scrollWaiting:boolean=false;
  @HostListener("window:scroll", [])
  onElementScroll(){
    if (this.scrollWaiting) return;
    this.scrollWaiting = true;
    setTimeout(()=>{
      this.scrollWaiting=false;
      this.initPos.refreshFromElement(this.element);
    },100);
  }
  private mouseMoveWaiting:boolean=false;
  private needPosRefresh:boolean=true;
  private refreshPos(){
    this.initPos.refreshFromElement(this.element);
  }
  rou(val:number):number{
    return Math.round(val*100)/100
  }
  @HostListener("document:mousemove", ['$event'])
  onMouseMove(e:MouseEvent) {
    if (this.mouseMoveWaiting) return;
    this.mouseMoveWaiting = true;
    setTimeout(()=>{
      this.mouseMoveWaiting = false;
      let xDist:number=e.clientX-this.initPos.centerLeft;
      let yDist:number=e.clientY-this.initPos.centerTop;
      let dist:number=Math.sqrt(xDist*xDist+yDist*yDist);
      
      if (dist<this.impactDistance) {
        if (this.initPos.isInDummySquare(e.clientX,e.clientY)) {
          let dsd:number=this.impactDistance/this.initPos.dummySquareEdge;
          let div:number=(dist/this.impactDistance)*dsd;
          let xDiv:number=this.rou(xDist/dist*div);
          let yDiv:number=this.rou(yDist/dist*div);
          let xIncrement:number=xDiv*this.maxClosure;
          let yIncrement:number=yDiv*this.maxClosure;
          let transform:string='translate('+xIncrement+'px,'+yIncrement+'px)';
          this.element.style.transform=transform;
        } else {
          let div:number=dist/this.impactDistance-1; //dalej 0 blizko 1
          let xDiv:number=this.rou(xDist/dist*div);
          let yDiv:number=this.rou(yDist/dist*div);
          let xIncrement:number=-xDiv*this.maxClosure;
          let yIncrement:number=-yDiv*this.maxClosure;
          let transform:string='translate('+xIncrement+'px,'+yIncrement+'px)';
          this.element.style.transform=transform;
        }

        this.needPosRefresh=true;
      } else {
        if (this.needPosRefresh) {
          this.element.style.transform='';
          this.refreshPos();
          this.needPosRefresh=false;
        }
      }
    }, 250);
  }
}