import { Directive, HostListener, ElementRef, OnChanges, OnInit, Input, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[kindaPerspective]'
})
export class KindaPerspectiveDirective implements OnChanges, OnInit {
  @Input('kpRange') public range: number;
  @Input('kpShiftScale') public shiftScale: number = 1;
  @Input('kpRotateScale') public rotateScale: number = 1;
  @Input('kpPerspectiveScale') public perspectiveScale: number = 1;
  @Input('kpMaxShift') public maxShift: number;
  @Input('kpVerbose') public verbose: boolean = false;
  private element: HTMLElement;

  constructor(elementRef: ElementRef) {
    this.element = elementRef.nativeElement;
  }

  ngOnChanges(changes: SimpleChanges) {
  }
  ngOnInit() {
    if (this.range > 1) this.range = 1;
    this.scrolled = window.scrollY;
  }
  private mouseX: number;
  private mouseY: number;
  private scrolled: number;
  @HostListener("document:mousemove", ['$event'])
  onMouseMove(e: MouseEvent) {
    this.mouseX = e.clientX;
    this.mouseY = e.clientY;
    this.setElementPosition(this.mouseX, this.mouseY, this.scrolled);
  }
  @HostListener("window:scroll", [])
  onElementScroll(event:Event){
    this.scrolled = window.scrollY;
    this.setElementPosition(this.mouseX, this.mouseY, this.scrolled);
  }
  setElementPosition(mouseX: number, mouseY: number, scrolled: number) {
    let el: HTMLElement = this.element;
    let offsetLeft: number = 0;
    let offsetTop: number = 0;
    do {
      offsetLeft += el.offsetLeft;
      offsetTop += el.offsetTop;
      //if(this.verbose)console.log(el.offsetLeft+'-'+el.offsetTop)
      el = <HTMLElement>el.offsetParent;
    } while(el);
    //if(this.verbose)console.log('cv '+this.element.clientWidth+'x'+this.element.clientHeight)
    offsetLeft += this.element.clientWidth/2;
    offsetTop += this.element.clientHeight/2-scrolled;

    let mouseDistanceX: number = offsetLeft-mouseX;
    let mouseDistanceXRelativeToWin: number;
    let mouseDistanceY: number = offsetTop-mouseY;
    let mouseDistanceYRelativeToWin: number;
    //if(this.verbose)console.log('md '+mouseDistanceX+'x'+mouseDistanceY)
    //if(this.verbose)console.log('m  '+mouseX+'x'+mouseY)

    //if(this.verbose)console.log('--------------')

    mouseDistanceXRelativeToWin = mouseDistanceX / window.innerWidth;
    mouseDistanceYRelativeToWin = mouseDistanceY / window.innerHeight;

    let yRot: number = this.calcRotation(-mouseDistanceXRelativeToWin);
    let xRot: number = this.calcRotation(mouseDistanceYRelativeToWin);
    if (this.verbose) console.log('rot ' + xRot + 'x' + yRot);

    let xMov: number = this.calcMove(-mouseDistanceXRelativeToWin);
    let yMov: number = this.calcMove(-mouseDistanceYRelativeToWin);
    if (this.verbose) console.log('mov ' + xMov + 'x' + yMov);

    let per: number = this.calcPerspective();
    if (this.verbose) console.log('per ' + per);
    
    if (this.maxShift) {
      let movedDist: number = Math.sqrt(xMov / this.range * xMov / this.range + yMov / this.range * yMov / this.range);
      if (movedDist > this.maxShift) {
        let xMovDiv: number = xMov / movedDist;
        let yMovDiv: number = yMov / movedDist;
        xMov = this.maxShift * xMovDiv;
        yMov = this.maxShift * yMovDiv;
      }
    }
    let xMovS: string = xMov.toFixed(1);
    let yMovS: string = yMov.toFixed(1);
    let xRotS: string = xRot.toFixed(1);
    let yRotS: string = yRot.toFixed(1);

    let transform: string = '';
    if (per !== undefined) transform += 'perspective(' + per.toFixed(1) + 'px) ';
    transform += 'translateX(' + xMovS + 'px) translateY(' + yMovS + 'px) ';
    transform += 'rotateX(' + xRotS + 'deg) rotateY(' + yRotS + 'deg)';
    this.element.style.transform = transform;
  }
  calcRotation(value: number): number {
    value = value * 90;
    value = value * this.range;
    value = value * this.rotateScale;
    return value;
  }
  calcMove(value: number): number {
    value = value * 200;
    value = value * this.range;
    value = value * this.shiftScale;
    return value;
  }
  calcPerspective(): number {
    let value: number = 1 - this.range;
    if (this.perspectiveScale == 0) return undefined;
    value = value * 1000;
    value = value / this.perspectiveScale;
    return value;
  }
}
