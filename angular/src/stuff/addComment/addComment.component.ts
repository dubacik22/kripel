import { Component, AfterViewInit, ChangeDetectorRef, Input, OnDestroy } from '@angular/core';
import { Communicator } from './res/Communicator';
import { Message } from './res/Message'
import { RichTextBoxComponent } from '../rich-text-box/rich-text-box.component';
import { Subscription } from 'rxjs';
import { Session } from '../injectables/Session';
import { SessionValue } from '../injectables/SessionValue';
import { Strings } from 'src/app/Strings';
import { User } from 'src/app/app-login/res/User';

@Component({
  selector: 'add-comment',
  templateUrl: './addComment.component.html',
  styleUrls: ['./addComment.component.css']
})
//php session
export class AddCommentComponent implements AfterViewInit, OnDestroy {

  public get loginNamePropName(): string {
    return User.loginNamePropName;
  }
  private _scdStart:number;
  @Input()
  public get scdStart(): number {
    return this._scdStart;
  }
  public set scdStart(value: number) {
    this._scdStart = value;
    this.detectChanges();
  }
  private _scdEnd:number;
  @Input()
  public get scdEnd(): number {
    return this._scdEnd;
  }
  public set scdEnd(value: number) {
    this._scdEnd = value;
    this.detectChanges();
  }
  private _compactSubClass: string;
  @Input()
  public get compactSubClass(): string {
    return this._compactSubClass;
  }
  public set compactSubClass(value: string) {
    if (this._compactSubClass == value) return;
    this._compactSubClass = value;
    this.detectChanges();
  }
  public get ultraCompact(): string {
    return Strings.compactSubClassUltraCompact;
  }
  private _table: string;
  @Input() public get table(): string {
    return this._table;
  }
  public set table(value: string) {
    if (value == this._table) return;
    this._table = value;
    this.recieveAll();
    this.communicator.createTable(this._table, this.session.getEncryptedSession());
  }

  private _respondText: string = '';
  public get respondText(): string {
    return this._respondText;
  }
  public set respondText(value: string) {
    if (value == this._respondText) return;
    this._respondText = value;
  }
  private msgToResponse: Message;
  private origTextOnEdit: string;
  private newText: string;
  public respondVisible: boolean = false;

  private _messages: Message[] = [];
  public get messages(): Message[] {
    return this._messages;
  }
  public set messages(value: Message[]) {
    if (Message.isArraySame(value, this._messages)) return;
    this._messages = value;
  }

  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private communicator: Communicator, private chDref: ChangeDetectorRef,
    public session: Session) {}
  private inited: boolean;
  private userSubscribtion: Subscription;
  ngAfterViewInit() {
    this.chDref.detach();
    this.userSubscribtion = this.session.values$.subscribe((values: Map<string, SessionValue>) => {
      this.detectChanges();
    });
    this.inited = true;
    this.recieveAll();
  }
  ngOnDestroy() {
    this.userSubscribtion.unsubscribe();
  }

  private recieveAll() {
    if (!this.inited) return;
    this.messages = [];
    if (!this.table) return;
    this.communicator.readMessages(this.table, this.session.getEncryptedSession(), (messages: any[]) => {
      let msgs: Message[] = Message.generateArray4Use(messages);
      this.sort(msgs, this.messages);
      this.detectChanges();
    });
  }
  public deleteResponse(response: Message) {
    this.communicator.eraseMessage(response.id.toString(), this.table, this.session.getEncryptedSession(), () => {
      this.recieveAll();
    });
  }
  public showResponseTo(to: Message) {
    this.innerShowResponseTo(to);
    this.detectChanges();
  }
  public innerShowResponseTo(to: Message) {
    this.innerHideResponse();
    this.msgToResponse = to;
    if (this.msgToResponse) {
      this.msgToResponse.respondVisible = true;
      this.newText = this.msgToResponse.text;
    } else {
      this.respondVisible = true;
    }
  }
  public hideResponse() {
    this.innerHideResponse();
    this.detectChanges();
  }
  private innerHideResponse() {
    if (this.msgToResponse) this.msgToResponse.respondVisible = false;
    this.respondVisible = false;
  }
  public sendResponse() {
    if (!this.session.isLogged()) return;
    if (RichTextBoxComponent.isEmpty(this.respondText)) return;
    let toId: string;
    if (this.msgToResponse) {
      if (this.msgToResponse.toMessageIds) {
        toId = this.msgToResponse.stringId+','+this.msgToResponse.toMessageIds;
      } else {
        toId = this.msgToResponse.stringId;
      }
    } else {
      toId = '';
    }
    this.communicator.saveMessage(this.respondText, this.table, this.session.getItem(User.userIdPropName),
      toId, this.session.getEncryptedSession(), () => {
      this.respondText = '';
      this.hideResponse();
      this.recieveAll();
    });
  }
  public updateResponse() {
    if (!this.session.isLogged()) return;
    if (RichTextBoxComponent.isEmpty(this.newText)) return;
    this.communicator.updateMessage(this.msgToResponse.id, this.newText, this.table,
      this.session.getEncryptedSession(), () => {
      this.respondText = '';
      this.hideResponse();
      this.recieveAll();
    });
  }
  private sort(messagesToSort: Message[], whereToSort: Message[]) {
    messagesToSort.forEach((msg: Message) => {
      if ((!msg.toMessageIds) || (msg.toMessageIds == '0')) {
        msg.depth = 0;
        whereToSort.push(msg);
      } else {
        let toMessageIds: string[] = msg.toMessageIds.split(',');
        let possibleTo: Message[] = whereToSort;
        for (let i: number = toMessageIds.length-1; i >= 0; i--) {
          let to: string = toMessageIds[i];
          let farestToMsg: Message = Message.findMessageIdInArr(possibleTo, to);
          if (farestToMsg) {
            possibleTo = farestToMsg.responses;
          } else {
            break;
          }
        }
        possibleTo.push(msg);
      }
    });
  }
}