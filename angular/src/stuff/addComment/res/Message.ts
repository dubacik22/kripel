export class Message {
  public id: number;
  public stringId: string;
  public toMessageIds: string;
  public time: number;

  public fromLoginName: string;
  public fromUserName: string;
  public text: string;

  public responses: Message[] = [];
  public depth: number;
  public respondVisible: boolean = false;
  
  public empty(): boolean {
    if (!this.text) return true;
    if (this.text == '') return true;
    return false;
  }
  public static generateArray4Use(savedItems: any[]): Message[] {
    let ret: Message[] = [];
    savedItems.forEach((savedItem: any) => {
      let msg: Message = new Message();
      msg.generate4Use(savedItem);
      ret.push(msg);
    });
    return ret;
  }
  private generate4Use(savedItem: any) {
    this.id = savedItem.messageId;
    this.stringId = savedItem.messageId;
    this.text = savedItem.message;
    this.fromUserName = savedItem.fromUserName;
    this.fromLoginName = savedItem.fromLoginName;
    this.toMessageIds = savedItem.toMessageIds;
    this.time = savedItem.time;
    if (this.toMessageIds == '0') this.toMessageIds = '';
  }
  public static findMessageIdInArr(arr: Message[], messageId: string): Message {
    return arr.find((item: Message) => {
      return item.id.toString() == messageId;
    });
  }
  public static isArraySame(a1: Message[], a2: Message[]) {
    if (!a1 && !a2) return true;
    if (!a1 || !a2) return false;
    if (a1 == a2) return true;
    if (a1.length != a2.length) return false;
    for (let i: number = 0; i < a1.length; i++) {
      let a1i: Message = a1[i];
      let a2i: Message = a2[i];
      if (a1i || a2i) {
        if (a1i && a2i) {
          if (!a1i.isSame(a2i)) return false;
        }
        return false;
      }
    }
    return true;
  }
  public isSame(other: Message): boolean {
    if (!other) return false;
    if (this.id != other.id) return false;
    return true;
   /* if (!Message.isArraySame(this.responses, other.responses)) return false;
    if (this.text != other.text) return false;
    if (this.from != other.from) return false;
    if (this.toMessageId != other.toMessageId) return false;
    return true;*/
  }
}