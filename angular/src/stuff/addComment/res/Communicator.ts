import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Paths } from 'src/stuff/injectables/Paths';

@Injectable()
export class Communicator{
    public verbose = false;
    private static createMessageDataToSave(table: string, message: string, fromLoginId: string, to: string,
        session: string): any {
        let ret = {
            'toDo': 'messenger_save',
            'table': table,
            'message': message,
            'fromLoginId': fromLoginId,
            'to': to,
            'session': session,
        }
        return ret;
    }
    private static createMessageDataToUpdate(table: string, id: number, message: string, session: string): any {
        let ret = {
            'toDo': 'messenger_update',
            'table': table,
            'message': message,
            'id': id,
            'session': session,
        }
        return ret;
    }
    private static createMessageDataToRead(table: string, session: string): any {
        let ret = {
            'toDo': 'messenger_readAll',
            'table': table,
            'session': session,
        }
        return ret;
    }
    private static createMessageDataToErase(id: string, table: string, session: string): any {
        let ret = {
            'toDo': 'messenger_erase',
            'table': table,
            'id': id,
            'session': session
        }
        return ret;
    }
    private static createCreateTablesData(table: string, session: string): any {
        let ret = {
            'toDo': 'messenger_createTable',
            'tableName': table,
            'session': session
        }
        return ret;
    }
    constructor(private http: HttpClient, private paths: Paths) {
    }
    public createTable(table: string, session: string, whatNext?: () => any) {
        if (!table) return;
        let messageData = Communicator.createCreateTablesData(table, session);
        if (this.verbose) {
            console.log("We sent to server createTable:");
            console.log(messageData);
        }
        
        this.http.post(this.paths.phpCommunicator, messageData, {responseType: 'json'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at createTable:");
                console.log(retData);
            }
            if (retData && retData.success && whatNext) {
                whatNext();
            }
        });
    }
    public eraseMessage(id: string, table: string, session: string, whatNext?: () => any) {
        if (!id) return;
        let messageData = Communicator.createMessageDataToErase(id, table, session);
        if (this.verbose) {
            console.log("We sent to server eraseMessage:");
            console.log(messageData);
        }
        
        this.http.post(this.paths.phpCommunicator, messageData, {responseType: 'json'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at eraseMessage:");
                console.log(retData);
            }
            if (retData && retData.success && whatNext) {
                whatNext();
            }
        });
    }
    public updateMessage(id: number, message: string, table: string,
        session: string, whatNext?: () => any) {
        if (!message) return;
        let messageData = Communicator.createMessageDataToUpdate(table, id, message, session);
        if (this.verbose) {
            console.log("We sent to server updateMessage:");
            console.log(messageData);
        }
        
        this.http.post(this.paths.phpCommunicator, messageData, {responseType: 'json'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at updateMessage:");
                console.log(retData);
            }
            if (retData && retData.success && whatNext) {
                whatNext();
            }
        });
    }
    public saveMessage(message: string, table: string, fromLoginId: string,
        to: string, session: string, whatNext?: () => any) {
        if (!message) return;
        let messageData = Communicator.createMessageDataToSave(table, message, fromLoginId, to, session);
        if (this.verbose) {
            console.log("We sent to server saveMessage:");
            console.log(messageData);
        }
        
        this.http.post(this.paths.phpCommunicator, messageData, {responseType: 'json'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at saveMessage:");
                console.log(retData);
            }
            if (retData && retData.success && whatNext) {
                whatNext();
            }
        });
    }
    public readMessages(table: string, session: string, whatNext?: (messages: any[]) => void) {
        let messageData = Communicator.createMessageDataToRead(table, session);
        if (this.verbose) {
            console.log("We sent to server readMessages:");
            console.log(messageData);
        }
    
        this.http.post(this.paths.phpCommunicator, messageData, {responseType: 'json'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at readMessages:");
                console.log(retData);
            }
            if (retData && retData.success && whatNext) {
                whatNext(retData.messages);
            }
        });
    }
}
