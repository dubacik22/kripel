import { Component, ViewChild, ElementRef, Input, Output, EventEmitter, OnInit,
    OnDestroy, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { GlyphsBase } from './res/GyphsBase';
import { CreatedNode } from './res/CreatedNode';
import { OffsetNode } from './res/OffsetNode';
import { NodeFinder } from './res/NodeFinder';
import { NodeDivider } from './res/NodeDivider';
import { CreationReturn } from './res/CreationReturn';


import { DividedNode } from './res/DividedNode';
import { CaretPositionType } from './res/CaretPositionType';
import { NodeCreator } from './res/NodeCreator';
import { NodeStringer } from './res/NodeStringer';
import { StringRepresentationOfNode } from './res/StringRepresentationOfNode';
import { BooleanWrapper } from 'src/stuff/tbutton/res/BooleanWrapper';
//import * as _ from 'lodash';


@Component({
  selector: 'rich-text-box',
  templateUrl: './rich-text-box.component.html',
  styleUrls: ['./rich-text-box.component.css']
})
//ak dam backspace/delete/zacnem pisat ak mam oznacenu nejaku cast (v skratke buttonPressed) tak najst a odstranit vsetky gliphy ktore nemaju obsah
export class RichTextBoxComponent implements AfterViewInit, OnDestroy{
  @ViewChild('valHolder') valHolderRef: ElementRef;
  public get valHolder(): HTMLElement {
    return this.valHolderRef.nativeElement;
  }
  private _text: string = '';
  @Input()
  public get text(): string {
    return this._text;
  }
  public set text(value: string) {
    if (value == undefined) value = '';
    if (value == this._text) return;
    let node: Node = this.creator.createNodes(value, false); //pri starte testovania sa vola toto pri vkladani textu z jsonu. preto readonly=true
    let nodes: NodeListOf<ChildNode> = node.childNodes;
    this.removeAll();
    let vh: Node = this.valHolderRef.nativeElement;
    while (nodes.length > 0) {
      let child: Node = nodes.item(0);
      vh.appendChild(child);
    }
    this._text = this.getTextFromValueHolder();
  }
  @Output('textChange') textChange: EventEmitter<string> = new EventEmitter();

  public buttonsVisible: boolean = true;
  @Input()
  public get readonly(): boolean {
    let ce: string = this.valHolder.contentEditable;
    if (ce.toLowerCase() == 'true') return false;
    else return true;
  }
  public set readonly(value: boolean) {
    if (value) {
      this.valHolder.contentEditable = 'false';
      this.buttonsVisible = false;
      //this.unselectable=true;
    } else {
      this.valHolder.contentEditable = 'true';
      this.buttonsVisible = true;
      this.unselectable = false;
    }
    this.detectChanges();
  }
  @Input()
  public get unselectable(): boolean{
    let ce: string = this.valHolder['unselectable'];
    if (ce.toLowerCase() == 'on') return true;
    else return false;
  }
  public set unselectable(value: boolean) {
    this.valHolder.removeEventListener('mousedown', this.disableSelection);
    if (value) {
      this.valHolder.addEventListener('mousedown', this.disableSelection);
      document.getSelection().removeAllRanges();
      this.valHolder.style.cursor = 'default';
    } else {
      this.valHolder.style.cursor = 'text';
    }
  }
  public canRemBold: boolean = false;
  public canRemItalic: boolean = false;
  public canRemSubs: boolean = false;
  public canRemSuper: boolean = false;
  public canRemBigger: boolean = false;
  public canRemSmaller: boolean = false;
  public canRemRed: boolean = false;
  public canRemGreen: boolean = false;
  public disableSelection(event:MouseEvent) {
    event.preventDefault();
  }

  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  
  private finder: NodeFinder = new NodeFinder();

  constructor(private chDref: ChangeDetectorRef,
    private stringer: NodeStringer,
    private creator: NodeCreator,
  //  private imgCreator:imgCreatorAndGetter,
  //  textBoxGetter:richTextBoxGetter,
  ) {}
  ngAfterViewInit() {
    this.chDref.detach();
    this.finder.parentNode = this.valHolder;
  }
  ngOnDestroy() {
  }

  private removeAll() {
    while (this.valHolder.childNodes.length > 0) {
      this.valHolder.removeChild(this.valHolder.firstChild);
    }
  }
  private getRange(): Range {
    let sel: Selection = document.getSelection();
    if (sel.rangeCount === 0) return;
    return sel.getRangeAt(0);
  }
  
  private getNode(pnode:Node, which: StringRepresentationOfNode): Node {
    do {
      let thisTags: StringRepresentationOfNode = this.stringer.getTagsForNode(pnode);
      if (which.isSame(thisTags)) return pnode;
      pnode = pnode.parentNode;
    } while (pnode !== this.valHolder);
    return undefined;
  }
  private getTextFromValueHolder(): string {
    return this.stringer.toString(this.valHolder);
  }
  public textChanged() {
    this._text = this.getTextFromValueHolder();
    this.textChange.emit(this._text);
    this.detectChanges();
  }
  private static insertAfter(parentNode: Node, after: Node, what: Node) {
    if (after) {
      let snode: Node = after.nextSibling;
      if (snode) parentNode.insertBefore(what, snode);
      else parentNode.appendChild(what);
    } else {
      let fc: Node = parentNode.firstChild;
      if (fc) parentNode.insertBefore(what, fc);
      else parentNode.appendChild(what);
    }
  }
  private static insertNewNodes(newNodes: CreatedNode, where: Node, after: Node) {
    newNodes.allNodes.forEach((newNode: Node) => {
      RichTextBoxComponent.insertAfter(where,after, newNode);
      after = newNode;
    });
  }
  private addGlyph(tnode: OffsetNode, which: string, wantAdd: boolean) {
    if (!wantAdd) return;
    let pnode: Node = tnode.node.parentNode;
    let tNodeT: string = tnode.node.textContent;
    let newT: string;
    let newNodes: CreatedNode;
    let lastT: string = '';

    tnode.node.textContent = tNodeT.substr(0, tnode.offsetInNode);
    newT = tNodeT.substring(tnode.offsetInNode, tnode.end);
    lastT = tNodeT.substring(tnode.end, tNodeT.length);
    if (!newT) {
      if (lastT) tnode.node.textContent += lastT;
      return;
    }
    let created: CreationReturn<any> = this.creator.createElementFromString(which, false);
    newNodes = new CreatedNode(created.domElement);

    RichTextBoxComponent.insertNewNodes(newNodes, pnode, tnode.node);
    let newTnode: Node = document.createTextNode(newT);
    newNodes.mainNode.appendChild(newTnode);
    if (tnode.offsetInNode == 0) pnode.removeChild(tnode.node);
    if (lastT) {
      RichTextBoxComponent.insertAfter(pnode, newNodes.allNodes[newNodes.allNodes.length-1],document.createTextNode(lastT));
    }
  }
  //ak je offsetInNode rovny jeho velkosti tak ist prec
  private remGlyph(tnode: OffsetNode, which: string, nodeToRem: Node, wantRem: boolean) {
    if (!wantRem) return;
    if (tnode.offsetInNode == tnode.end) return;
    let pnode: Node = nodeToRem.parentNode;
    while (nodeToRem.childNodes.length > 0) {
      pnode.insertBefore(nodeToRem.firstChild, nodeToRem);
    }
    pnode.removeChild(nodeToRem);
    let origStart: number = tnode.numberOfCharsBeforeNode + tnode.offsetInNode;
    let origEnd: number = tnode.numberOfCharsBeforeNode + tnode.end;
    let wholeStart: number = tnode.numberOfCharsBeforeNode;
    let wholeLen: number = tnode.numberOfCharsBeforeNode + tnode.node.textContent.length;
    if (wholeStart != origStart) {
      this.insertGlyph(wholeStart, origStart, which);
    }
    if (origEnd != wholeLen) {
      this.insertGlyph(origEnd, wholeLen, which);
    }
  }
  private willAdd(start: number, end: number, which: StringRepresentationOfNode): boolean {
    let have: number = 0;
    let dontHave: number = 0;
    let nodes: OffsetNode[] = this.finder.getTextNodes(start, end);
    nodes = this.finder.trim(nodes);
    nodes.forEach((on:OffsetNode) => {
      let pnode: Node = on.node.parentNode;
      let haveIt: boolean;
      do {
        let thisTags: StringRepresentationOfNode = this.stringer.getTagsForNode(pnode);
        haveIt = which.isSame(thisTags);
        if (haveIt) break;
        if (pnode == this.valHolder) break;
        pnode = pnode.parentNode;
      } while (pnode !== this.valHolder);
      let len: number = on.node.textContent.substr(on.offsetInNode, on.end).length;
      if (haveIt) have += len;
      else dontHave += len;
    });
    if (dontHave >= have) return true;
    return false;
  }
  private insertGlyph(start: number, end: number, which: string): boolean {
    if ((start == undefined) || (end == undefined)) return undefined;
    if (start > end) return undefined;
    let whichStrRepr: StringRepresentationOfNode = this.stringer.getTagsForNode(this.creator.createElementFromString(which, false).domElement);
    let wantAdd: boolean = this.willAdd(start, end, whichStrRepr);
    let tnodes: OffsetNode[];
    tnodes = this.finder.getTextNodes(start, end);
    tnodes = this.finder.trim(tnodes);

    tnodes.forEach((tnode: OffsetNode) => {
      let nodeToRem: Node = this.getNode(tnode.node, whichStrRepr);
      if (nodeToRem) {
        this.remGlyph(tnode, which, nodeToRem, !wantAdd);
      } else {
        this.addGlyph(tnode, which, wantAdd);
      }
    });
    this.textChanged();
    return wantAdd;
  }
  private inserGlyphAndRefreshButton(glyphSign: string, buttonWrapper: BooleanWrapper) {
    let caretPosition: CaretPositionType = this.getCaretOffsetFrom0();
    if (caretPosition.start > caretPosition.end) return;
    let inserted: boolean = this.insertGlyph(caretPosition.start, caretPosition.end, glyphSign);
    this.detectChanges();
    this.setCaretOffsetFrom0(caretPosition);
    buttonWrapper.value = inserted;
  }
  onKeyUp(event: KeyboardEvent) {
    let valHolder: Node = this.valHolder;
    if (valHolder.childNodes.length == 0) {
      let span: Node = document.createElement('SPAN');
      let div: Node = document.createElement('DIV');
      div.appendChild(span);
      valHolder.appendChild(div);
    }
    valHolder.childNodes.forEach((valHolderChild: Node) => {
      if (valHolderChild.nodeName !== 'DIV') {
        let div: Node = document.createElement('DIV');
        let span: Node = document.createElement('SPAN');
        div.appendChild(span);
        valHolder.insertBefore(div, valHolderChild);
        span.appendChild(valHolderChild);
      } else {
        valHolderChild.childNodes.forEach((valHolderChildChild: Node) => {
          if (valHolderChildChild.nodeName !== 'SPAN') {
            let span: Node = document.createElement('SPAN');
            valHolderChild.insertBefore(span, valHolderChildChild);
            span.appendChild(valHolderChildChild);
          }
        });
      }
    });
    this.textChanged();
  }
  onKeyDown(event: KeyboardEvent) {
    let valHolder:Node=this.valHolder;
    //vh.firstChild.insertBefore(document.createElement('span'),vh.firstChild.firstChild)
    if (event.ctrlKey) {
      let ma:number = GlyphsBase.marksAbr.indexOf(event.key);
      if (ma<0) return;

      event.preventDefault();

      let caretPosition:CaretPositionType=this.getCaretOffsetFrom0();
      if (caretPosition.start>caretPosition.end) return;
      this.insertGlyph(caretPosition.start,caretPosition.end,GlyphsBase.marks[ma]);
      this.setCaretOffsetFrom0(caretPosition);
    }
  //  if (event.key=='Enter') {
  //    let cp:caretPositionType=this.getCaretOffsetFrom0();
  //    this.divider.divide(cp.start);
  //    event.preventDefault();
  //  }
    //if (this.specialKeys.indexOf(event.key)>=0) return;
  }
  public onCursorPosChanged(event: MouseEvent) {
    let caretPosition: CaretPositionType = this.getCaretOffsetFrom0();
    caretPosition = this.getCaretOffsetFrom0();
    if (caretPosition.start > caretPosition.end) return;
    if (this.readonly) return;
    setTimeout(() => {
      caretPosition = this.getCaretOffsetFrom0();
      if (caretPosition.start == 0) {
        this.canRemBold = false;
        this.canRemItalic = false;
        this.canRemSubs = false;
        this.canRemSuper = false;
        this.canRemBigger = false;
        this.canRemSmaller = false;
        this.canRemRed = false;
        this.canRemGreen = false;
      } else {
        caretPosition.start--;
        this.canRemBold = this.getButtonSelected(caretPosition.start, caretPosition.end, 'B');
        this.canRemItalic = this.getButtonSelected(caretPosition.start, caretPosition.end, 'I');
        this.canRemSubs = this.getButtonSelected(caretPosition.start, caretPosition.end, 'SUB');
        this.canRemSuper = this.getButtonSelected(caretPosition.start, caretPosition.end, 'SUP');
        this.canRemBigger = this.getButtonSelected(caretPosition.start, caretPosition.end, 'BIG');
        this.canRemSmaller = this.getButtonSelected(caretPosition.start, caretPosition.end, 'SMALL');
        this.canRemRed = this.getButtonSelected(caretPosition.start, caretPosition.end, 'SPAN style = \'color:red\'');
        this.canRemGreen = this.getButtonSelected(caretPosition.start, caretPosition.end, 'SPAN style = \'color:green\'');
      }
      this.detectChanges();
    });
  }
  private getButtonSelected(start: number, end: number, which: string): boolean {
    if (start > end) return undefined;
    if (this.readonly) return;
    let whichStrRepr: StringRepresentationOfNode = this.stringer.getTagsForNode(this.creator.createElementFromString(which, false).domElement);
    return !this.willAdd(start, end, whichStrRepr);
  }
  private getButtonSelec(start: number, end: number, which: string): boolean {
    if (start > end) return undefined;
    if (this.readonly) return;
    let whichStrRepr: StringRepresentationOfNode = this.stringer.getTagsForNode(this.creator.createElementFromString(which, false).domElement);
    return !this.willAdd(start, end, whichStrRepr);
  }
  private getCaretOffsetFrom0(): CaretPositionType {
    let element: Node = <Node>this.valHolder;
    let ret: CaretPositionType = new CaretPositionType();
    let range: Range = this.getRange();
    if (!range) {
      ret.start = 0;
      ret.end = 0;
      return ret;
    }
    let caretRange: Range = range.cloneRange();
    caretRange.selectNodeContents(element);
    ret.startN = range.startContainer;
    let startO: number = range.startOffset;
    caretRange.setEnd(ret.startN, startO);
    ret.start = caretRange.toString().length;
    ret.endN = range.endContainer;
    let endO: number = range.endOffset;
    caretRange.setEnd(ret.endN, endO);
    ret.end = caretRange.toString().length;
    return ret;
  }
  private setCaretOffsetFrom0(caretPosition:CaretPositionType) {
    let range = document.createRange();
    let sel = window.getSelection();
    let al: number = (<Node>this.valHolder).textContent.length;
    if (caretPosition.start == al) {
      let on: OffsetNode = this.finder.getTextNode(caretPosition.start-1 ,caretPosition.end-1, 0);
      let nl: number = on.node.textContent.length;
      range.setStart(on.node, nl);
      range.setEnd(on.node, nl);
    } else {
      let ons: OffsetNode[] = this.finder.getTextNodes(caretPosition.start, caretPosition.end);
      range.setStart(ons[0].node, ons[0].offsetInNode);
      let lastON: OffsetNode;
      let length: number = caretPosition.end-caretPosition.start+ons[0].offsetInNode;
      ons.forEach((on: OffsetNode) => {
        length -= on.node.textContent.length;
        lastON = on;
      });
      length += lastON.node.textContent.length;
      range.setEnd(lastON.node, length);
    }
    sel.removeAllRanges();
    sel.addRange(range);
  }

  public addBold(add: BooleanWrapper) {
    this.inserGlyphAndRefreshButton('B', add);
  }
  public addItalic(add: BooleanWrapper) {
    this.inserGlyphAndRefreshButton('I', add);
  }
  public addSubs(add: BooleanWrapper) {
    this.inserGlyphAndRefreshButton('SUB', add);
  }
  public addSuper(add: BooleanWrapper) {
    this.inserGlyphAndRefreshButton('SUP', add);
  }
  public addBigger(add: BooleanWrapper) {
    this.inserGlyphAndRefreshButton('BIG', add);
  }
  public addSmaller(add: BooleanWrapper) {
    this.inserGlyphAndRefreshButton('SMALL', add);
  }
  public addRed(add: BooleanWrapper) {
    this.inserGlyphAndRefreshButton('SPAN style = \'color:red\'', add);
  }
  public addGreen(add: BooleanWrapper) {
    this.inserGlyphAndRefreshButton('SPAN style = \'color:green\'', add);
  }

  public static isEmpty(value: string): boolean {
    if (!value) return true;
    if (value == '<SPAN></SPAN>') return true;
    if (value == '<BR/>') return true;
    if (value == '<SPAN><BR/></SPAN>') return true;
    if (value == '<DIV><SPAN></SPAN></DIV>') return true;
    if (value == '<DIV></DIV>') return true;
    if (value == '<DIV><SPAN><BR/></SPAN></DIV>') return true;
    if (value == '<DIV><SPAN><BR></BR></SPAN></DIV>') return true;
    return false;
  }
}