export class CreatedNode {
    mainNode: Node;
    allNodes: Node[];
    constructor(node: Node) {
        this.resetAll(node);
    }
    private resetAll (node: Node) {
        this.mainNode = node;
        this.allNodes = [];
        this.allNodes.push(node);
       // this.allNodes=[node];
    }
}