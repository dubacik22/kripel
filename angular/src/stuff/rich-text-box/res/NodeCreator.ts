import { Injectable } from '@angular/core';
//import { ImgBoxComponent } from '../../img-box/img-box.component';
//import { ImgCreator } from './ImgCreator';
import { CreationReturn } from './CreationReturn'
//import { ImgGetter } from '../../../resources/ImgGetter';

@Injectable()
export class NodeCreator {
    private text: string = '';
    private i: number = 0;
    private actNodes: Node[] = [];
    private readonly glyphStart: string = '<';
    private readonly glyphEnd: string = '>';

    constructor(/*private imgCreator:ImgCreator,private imgGetter:ImgGetter*/) { //) {
    }
    public static nameOfElem(elem: string): string {
        elem = elem.trim();
        let spaceI: number = elem.indexOf(' ');
        if (spaceI < 0) spaceI = elem.length;
        return elem.substring(0, spaceI).trim();
    }
    public static attributesOfElem(elem: string): string {
        elem = elem.trim();
        let spaceI: number = elem.indexOf(' ');
        let endiI: number = elem.length;
        if (spaceI < 0) return undefined;
        return elem.substring(spaceI, endiI).trim();
    }
    public static createAttributes(attributes: string): Map<string, string> {
        let ret: Map<string, string> = new Map;
        if (!attributes) return ret;
        let equalsPos: number;
        let fromPos: number = 0;
        let attrName: string;
        let attrValue: string;
        do{
            equalsPos = attributes.indexOf('=', fromPos+1);
            if (equalsPos > 0) {
                attrName = attributes.substring(fromPos, equalsPos).trim();
                attrValue = attributes.substring(equalsPos+1).trim();
                fromPos = attrValue.indexOf(' ', equalsPos);
                if (fromPos < 0) fromPos = attributes.length
                attrValue = attrValue.substring(0, fromPos);
                let first: string = attrValue.substr(0,1);
                let last: string = attrValue.substr(-1);

                if (((first == '"') || (first == '\'')) && ((last == '"') || (last == '\''))) {
                    attrValue = attrValue.substring(1, attrValue.length-1).trim();
                }
                ret[attrName] = attrValue;
                //ret.set(attrName,attrValue);
            }
        }while(equalsPos > 0)

        return ret;
    }
    public static insertAttribute(where: HTMLElement, what: string, value: string) {
        let at: Attr = document.createAttribute(what);
        at.textContent = value;
        where.attributes.setNamedItem(at);
    }
    public static setAttributes(attrs: Map<string, string>, where: any) {
        for(let key in attrs) {
            NodeCreator.insertAttribute(where, key, attrs[key]);
        }
    }
    private static setAttributesToComponent(attrs: Map<string, string>, where:any) {
        for(let key in attrs) {
            where[key] = attrs[key];
        }
    }
    public static readonly imgNodeName: string = 'APP-IMG-BOX';
    public static isImgNode(name: string): boolean{
        return name == NodeCreator.imgNodeName;
    }
    public static isBase64(src: string) { //presunut do stringeru
        let base64str: string = 'base64';
        if (src.substr(0, base64str.length) == base64str) return true;
        let coma:number = src.indexOf(';');
        if (src.substr(coma, base64str.length) == base64str) return true;
        return false;
    }
    public createElementfromNameAndMapAttrs(name: string, attributes: Map<string, string>, readonly: boolean): CreationReturn<any> {
        let whereToSetAttrs: any;
        let ret: CreationReturn<any>;
    /*    if (NodeCreator.isImgNode(name)) {
            ret=this.imgCreator.create();
            this.imgGetter.created(ret);
            let imgComponent:ImgBoxComponent=ret.component;
            imgComponent.readonly=readonly;
            NodeCreator.setAttributesToComponent(attributes,imgComponent);
            whereToSetAttrs=undefined;
        } else*/ {
            ret = new CreationReturn();
            ret.domElement = document.createElement(name);
            whereToSetAttrs = ret.domElement;
        }
        if (whereToSetAttrs) NodeCreator.setAttributes(attributes, whereToSetAttrs);
        return ret;
    }
    public createElementFromNameAndStringAttrs(name: string, attributes: string, readonly: boolean): CreationReturn<any> {
        let attrs: Map<string, string> = NodeCreator.createAttributes(attributes);
        return this.createElementfromNameAndMapAttrs(name, attrs, readonly);
    }
    public createElementFromString(stringRepr: string, readonly: boolean): CreationReturn<any> {
        let name: string = NodeCreator.nameOfElem(stringRepr);
        let attrs: string = NodeCreator.attributesOfElem(stringRepr);
        return this.createElementFromNameAndStringAttrs(name, attrs, readonly);
    }
    private addElement(name: string, attributes: string, into: Node[], selfEnd: boolean, readonly: boolean) {
        name = name.toUpperCase();
        let ret: Node = this.createElementFromNameAndStringAttrs(name, attributes, readonly).domElement;
        into[into.length-1].appendChild(ret);
        if (selfEnd) return;
        into.push(ret);
    }
    private remElement(name: string, into: Node[]) {
        name = name.toUpperCase();
        for (let i: number = into.length-1; i>=0; i--) {
            if (into[i].nodeName.toUpperCase() == name) {
                into.splice(i, 1);
                return;
            }
        }
    }
    private addOrRemElement(element: string, into: Node[], readonly: boolean) {
        let selfEnd: boolean = false;
        if (element.substr(-1) == '/') {
            selfEnd = true;
            element = element.substring(0, element.length-1);
        }
        let name: string = NodeCreator.nameOfElem(element);
        let attributes: string = NodeCreator.attributesOfElem(element);
        if (element.substring(0, 1) != '/') {
            this.addElement(name, attributes, into, selfEnd, readonly);
        } else {
            this.remElement(name.substring(1, name.length), into);
        }
    }
    private getNextGlyphText(glyphSeparator: string): string {
        let ret: string = '';
        let glyphLen: number = glyphSeparator.length;
        while (this.i < this.text.length) {
            if (this.text.substr(this.i, glyphLen) == glyphSeparator) break;
            ret += this.text.substr(this.i, 1);
            this.i++;
        }
        if (ret) {
            this.i += glyphLen-1;
        }
        return ret;
    }
    private createNode(into: Node[], readonly: boolean) {
        let nodeText: string;
        if (this.text.substr(this.i, 1) == this.glyphStart) {
            this.i += this.glyphStart.length;
            nodeText = this.getNextGlyphText(this.glyphEnd);
            this.addOrRemElement(nodeText, into, readonly);
            this.i += this.glyphStart.length;
            return;
        }
        let glyphText: string = this.getNextGlyphText(this.glyphStart);
        if (glyphText) {
            glyphText = decodeURI(glyphText);
            into[into.length-1].appendChild(document.createTextNode(glyphText));
        }
    }
    public createNodes(text: string, readonly: boolean): Node {
        this.text = text;
        let ret: Node = document.createElement('DIV');
        this.actNodes = [ret];
        this.i = 0;
        while (this.i < this.text.length) {
            this.createNode(this.actNodes, readonly);
        }
        return ret;
    }
}