//import { ImgBoxComponent } from "../../img-box/img-box.component";
import { Injectable } from "@angular/core";
import { StringRepresentationOfNode } from './StringRepresentationOfNode'
//import { ImgGetter } from "./ImgGetter";
//import { TestHolder } from "../../../resources/TestHolder";
//import { Paths } from "../../../resources/Paths";
//import { imgCreatorAndGetter } from "src/app/resources/imgCreatorAndGetter";

@Injectable()
export class NodeStringer {

    constructor(/*private imgGetter:ImgGetter,private testHolder:TestHolder,private paths:Paths*/){}

    public toString(node: Node): string {
        let ret: string = '';
        ret = this.doNode(node);
        return ret;
    }
    private doNode(node: Node): string {
        let ret: string = '';
        if (node.hasChildNodes()) {
            for (let i: number = 0; i < node.childNodes.length; i++) {
                let childNode: Node = node.childNodes[i];
                if (childNode.nodeType == Node.TEXT_NODE) {
                    ret += encodeURI(childNode.textContent);
                } else if (childNode.nodeName=='APP-IMG-BOX') {
                    ret += this.imgToString(childNode);
                } else {
                    ret += '<'+childNode.nodeName;
                    if (childNode.nodeName == 'SPAN') {
                        ret += this.spanToString(<HTMLElement>childNode);
                    }
                    ret += '>';
                    ret += this.doNode(childNode);
                    ret += '</'+childNode.nodeName+'>';
                }
            }
        }
        return ret;
    }
    private spanToString(span: HTMLElement): string {
        let ret: string = '';
        let color: string = span.style.color;
        if (color) {
            ret += ' style=\'color:';
            ret += color;
            ret += '\'';
        }
        return ret;
    }
    private imgToString(node: Node) {
    //    let imgBoxComponent:ImgBoxComponent=this.imgGetter.getFromDom(<HTMLElement>node)
        let ret: string = '';
        let img: any = node.firstChild.lastChild;
        ret += '<APP-IMG-BOX startingHeight=\'';
        ret += img.height;
        ret += '\' startingWidth=\'';
        ret += img.width;
        ret += '\' src=\'';
    //    ret+=this.paths.imgServer;
    //    if (this.testHolder.path) ret+=this.testHolder.path+'/';
    //    ret+=imgBoxComponent.name;
        ret += '\' name=\'';
    //    ret+=imgBoxComponent.name;
        ret += '\'/>';
        return ret;
    }

    public getTagsForNode(node: Node): StringRepresentationOfNode {
        let div: Node = document.createElement('DIV');
        div.appendChild(node.cloneNode(true));
        let stringReprString: string = this.doNode(div);
        let ret: StringRepresentationOfNode = new StringRepresentationOfNode();
        ret.openingTag = this.getOpeningTag(stringReprString);
        ret.closingTag = this.getClosingTag(stringReprString);
        return ret;
    }
    private getOpeningTag(str: string): string {
        let firstGlyhpStartPos: number = str.indexOf('<');
        let firstGlyhpEndPos: number = str.indexOf('>')+1;
        return str.substring(firstGlyhpStartPos, firstGlyhpEndPos);
    }
    private getClosingTag(str: string): string {
        let lastGlyhpStartPos: number = str.lastIndexOf('<');
        let lastGlyhpEndPos: number = str.lastIndexOf('>') + 1;
        return str.substring(lastGlyhpStartPos, lastGlyhpEndPos);
    }
}