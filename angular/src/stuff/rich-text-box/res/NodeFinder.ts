import { Injectable } from '@angular/core';
import { OffsetNode } from './OffsetNode';
//import { ImgBoxComponent } from '../../img-box/img-box.component';
//import { ImgGetter } from '../../../resources/ImgGetter';
@Injectable()
export class NodeFinder{
    public parentNode: Node;
  
    constructor(/*private getter:ImgGetter*/){
    }
    public getTextNode(start: number, end: number, offset: number): OffsetNode {
        if (offset < 0) return undefined;
        let igtnR: innerGetTextNodeRet = new innerGetTextNodeRet();
        igtnR.start = start;
        igtnR.end = end;
        igtnR.offset = offset;
        let ret: OffsetNode = this.innerGetTextNode(this.parentNode, igtnR);
        if (ret) return ret;
        return undefined;
    }
    public getTextNodes(start: number, end: number): OffsetNode[] {
        let foundNode: OffsetNode = undefined;
        let i: number = 0;
        let ret: OffsetNode[] = [];
        do {
            foundNode = this.getTextNode(start, end, i);
            if (foundNode) {
                ret.push(foundNode);
            }
            i++;
        } while (foundNode != undefined)
        if (ret.length > 0) return ret;
        return undefined;
    }
    public trim(ons: OffsetNode[]): OffsetNode[] {
        if (!ons) return undefined;
        let ret: OffsetNode[] = [];
        ons.forEach((on: OffsetNode) => {
            if ((on.end-on.offsetInNode) != 0) ret.push(on);
            //ret.push(on);
        });
        return ret;
    }
    private innerGetTextNode(value: Node, igtnR: innerGetTextNodeRet): OffsetNode {
        let offsetNode: OffsetNode = new OffsetNode();
        if (value.hasChildNodes()) {
            for (let i: number = 0; i < value.childNodes.length; i++) {
                let childNode: Node = value.childNodes[i];
                if (igtnR.end < 0) return undefined;
                if (childNode.nodeType == childNode.TEXT_NODE) {
                    let tl: number = childNode.textContent.length;
                    if (igtnR.start <= tl) {
                        if (igtnR.offset == 0) {
                            offsetNode.offsetInNode = igtnR.start;
                            offsetNode.end = Math.min(igtnR.end, tl);
                            offsetNode.node = childNode;
                            offsetNode.numberOfCharsBeforeNode = igtnR.numberOfCharsBeforeNode;
                            return offsetNode;
                        }
                        igtnR.offset--;
                        igtnR.start = 0;
                    } else {
                        igtnR.start -= tl;
                    }
                    igtnR.numberOfCharsBeforeNode += tl;
                    igtnR.end -= tl;
                } else {
                    let ret: OffsetNode = this.innerGetTextNode(childNode, igtnR);
                    if (ret) return ret;
                }
            }
        }
        return undefined;
    }
    /*public forE(whatToDo:(imgBox:ImgBoxComponent)=>any){
        return this.innerForE(this.parentNode,whatToDo);
    }
    private innerForE(value:Node,whatToDo:(imgBox:ImgBoxComponent)=>any){
        if (value.nodeName=='APP-IMG-BOX') {
            let imgBox:ImgBoxComponent=this.getter.getFromDom(<HTMLElement>value);
            if (imgBox) whatToDo(imgBox)
        } else if (value.hasChildNodes()){
            for(let i:number=0;i<value.childNodes.length;i++){
                let childNode:Node=value.childNodes[i];
                this.innerForE(childNode,whatToDo);
            }
        }
    }*/
    public showNode(): string {
        return this.innerShowNode(this.parentNode, 0);
    }
    public innerShowNode(value: Node, deep: number): string {
        let ret: string = ' '+deep+': <'+value.nodeName;
        if (value.hasChildNodes()) {
            for (let i: number = 0; i < value.childNodes.length; i++) {
                let childNode: Node = value.childNodes[i];
                ret += this.innerShowNode(childNode, deep+1);
            }
        } else if (value.nodeType == value.TEXT_NODE) {
            ret += '<t: '+value.textContent+'>';
        } else if (value.nodeName == 'IMG') {
            let aval: any = value;
            ret += ' src='+aval.src;
            ret += ' w='+aval.width;
            ret += ' h='+aval.height;
        }
        ret += '>'
        return ret;
    }
}
class innerGetTextNodeRet{
    public start: number;
    public end: number;
    public offset: number = 0;
    public offsetNode: OffsetNode;
    public numberOfCharsBeforeNode: number = 0;
}