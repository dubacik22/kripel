import { CreatedNode } from "./CreatedNode";

export class NodeTuple{
    deepest: CreatedNode = undefined;
    first: CreatedNode = undefined;
    resetAll(node: Node){
        this.deepest = new CreatedNode(node);
        this.first = new CreatedNode(node);
    }
}