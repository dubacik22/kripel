export class DividedNode {
    public node: Node;
    public nodeBefore: Node;
    public offseInNode: number;
}