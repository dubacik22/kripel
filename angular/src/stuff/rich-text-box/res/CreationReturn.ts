export class CreationReturn<T> {
    domElement: HTMLElement;
    component: T;
}