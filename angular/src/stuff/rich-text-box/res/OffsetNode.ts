export class OffsetNode {
    public node: Node;
    public offsetInNode: number = 0;
    public numberOfCharsBeforeNode: number = 0;
    public end: number;
}