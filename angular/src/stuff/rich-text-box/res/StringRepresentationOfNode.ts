export class StringRepresentationOfNode {
    public openingTag: string;
    public closingTag: string;

    public isSame(other: StringRepresentationOfNode): boolean{
        if (this.openingTag != other.openingTag) return false;
        if (this.closingTag != other.closingTag) return false;
        return true;
    }
}