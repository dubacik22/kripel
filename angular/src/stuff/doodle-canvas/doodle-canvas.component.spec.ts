import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoodleCanvasComponent } from './doodle-canvas.component';

describe('DoodleCanvasComponent', () => {
  let component: DoodleCanvasComponent;
  let fixture: ComponentFixture<DoodleCanvasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoodleCanvasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoodleCanvasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
