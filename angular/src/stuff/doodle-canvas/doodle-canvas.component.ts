import { Component, ChangeDetectorRef, AfterViewInit, ViewChild, ElementRef, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { iNotifyPropertyChanged } from 'src/stuff/iNotifyProperyChanged';
import { Stroke, SvgPath } from 'angular-svg';
import { Curves } from './res/Curves';
import { SvgPathComponent } from 'angular-svg/lib/modules/svgpath/svgpath.component';
import { Curve } from './res/Curve';
import { Communicator } from './res/Communicator';
import { RecievedCurve } from './res/RecievedCurve';
import { RecievedCurves } from './res/RecievedCurves';
import { Session } from 'src/stuff/injectables/Session';
import { User } from 'src/app/app-login/res/User';
import { RecievedUsersCurves } from './res/RecievedUsersCurve';

@Component({
  selector: 'doodle-canvas',
  templateUrl: './doodle-canvas.component.html',
  styleUrls: ['./doodle-canvas.component.css']
})

export class DoodleCanvasComponent implements AfterViewInit, OnDestroy {
  private thisId: string;
  private drovenCurve: Curve;
  private sendInterval: any; //NodeJS.Timer
  private recieveInterval: any; //NodeJS.Timer

  @ViewChild('svgHolder') svgHolderRef: ElementRef;
  private get svgHolder(): HTMLElement {
    return this.svgHolderRef.nativeElement;
  }
  public restDoneCurves: RecievedCurves = new RecievedCurves();
  private _curves: Curves = new Curves();
  public get curves(): Curves {
    return this._curves;
  }
  public set curves(value: Curves) {
    this._curves = value;
    this.detectChanges();
  }

  private _lineColor: string = 'white';
  @Input()
  public get lineColor(): string {
    return this._lineColor;
  }
  public set lineColor(value: string) {
    if (!value) return;
    if (this._lineColor == value) return;
    this._lineColor = value;
  }
  public firstX: number;
  public firstY: number;

  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private chDref: ChangeDetectorRef, private communicator: Communicator,
    private session: Session) { 
  }
  ngOnInit() {
    this._curves.communicator = this.communicator;
  }
  ngAfterViewInit() {
    this.communicator.verbose=true;
    this.communicator.createTables(this.session.getEncryptedSession());
    this.chDref.detach();
    this.communicator.getNewId('', (id: string) => {
      this.thisId = id;
      this._curves.id = id;
      this.recieveOthersDoneCurves();
      this.startRecieveTimer();
    });
  }
  ngOnDestroy() {
    this.communicator.disconnectFromServerEvent();
    this.endRecieveTimer();
  }
  private recieveOthersDoneCurves() {
    this.communicator.getDoneLinesData(this.thisId, '', (linesData: RecievedUsersCurves[]) => {
      this.restDoneCurves.curves = linesData;
      this.restDoneCurves.curves.forEach((curves: RecievedUsersCurves) => {
        curves.curves.forEach((curve: RecievedCurve) => {
          curve.stroke = this.getStroke(curve.color);
        });
      });
      this.detectChanges();
    });
  }
  public mouseDown(event: MouseEvent) {
    this.newLine();
  }
  public touchStart(event: TouchEvent) {
    this.newLine();
  }
  public mouseMove(event: MouseEvent) {
    if (event.buttons != 1) {
      return;
    }
    let clientX: number = event.clientX;
    let clientY: number = event.clientY;
    this.addToCurve(clientX, clientY);
  }
  public touchMove(event: TouchEvent) {
    let touch: Touch = event.touches[0];
    let clientX: number = touch.clientX;
    let clientY: number = touch.clientY;
    this.addToCurve(clientX, clientY);
  }
  public mouseUp(event: MouseEvent) {
    this.endLine();
  }
  public touchEnd(event: TouchEvent) {
    this.endLine();
  }
  private startSendTimer() {
    this.sendInterval = setInterval(() => {
      this.sendNextPoints();
    }, 330)
  }
  private startRecieveTimer() {
    this.recieveInterval = setInterval(() => {
      this.recieveOthersDoneCurves();
    }, 3000)
  }
  private endSendTimer() {
    clearInterval(this.sendInterval);
  }
  private endRecieveTimer() {
    clearInterval(this.recieveInterval);
  }
  private sendNewCurveAndStartSendingNextPoint() {
    this._curves.communicateStartNewLine(this._lineColor);
    this.startSendTimer();
  }
  private sendNextPoints() {
    if (!this.drovenCurve) return;
    if (this.drovenCurve.containsUnsentStuff) {
      let points: string = this.drovenCurve.getUnsentStringAndMarkPartsAsSent();
      if (points && (points != ' ')) {
        this._curves.communicateAddPoints(points);
      }
    }
  }
  private newLine() {
    let holderPos: ClientRect | DOMRect = this.svgHolder.getBoundingClientRect();
    this.firstY = holderPos.top;
    this.firstX = holderPos.left;

    this._curves.startNewCurve();
    let lc: Curve = this._curves.getLastCurve();
    if (lc) lc.stroke = this.getStroke(this.lineColor);
    this.drovenCurve = lc;
    this.sendNewCurveAndStartSendingNextPoint();
    this.detectChanges();
  }
  private addToCurve(newPointX: number, newPointY: number) {
    let lc: Curve = this._curves.getLastCurve();
    if (!lc) {
      this.newLine();
      return;
    }
    newPointX = Math.round(newPointX - this.firstX);
    newPointY = Math.round(newPointY - this.firstY);
    lc.addCoordinates(newPointX, newPointY);
    this.detectChanges();
  }
  private endLine() {
    let lc: Curve = this._curves.getLastCurve();
    if (lc) lc.done = true;
    this.endSendTimer();
    this.sendNextPoints();
    this._curves.communicateMarkLineAsDone();
  }
  
  private getStroke(color: string) {
    return new Stroke("transparent", color, 0, 2, 1);
  }

  unlistenTo(subscribtion: Subscription) {
    if (subscribtion) subscribtion.unsubscribe();
  }
  listenTo(to: iNotifyPropertyChanged, toDo: (propName: string) => void): Subscription {
      if (to) return to.propertyChange.subscribe(toDo);
      return undefined;
  }
}