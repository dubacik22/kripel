import { RecievedCurve } from './RecievedCurve';
import { RecievedUsersCurves } from './RecievedUsersCurve';

export class RecievedCurves {
    public curves: RecievedUsersCurves[] = [];
    public isSame(other: RecievedCurve[][]): boolean {
        if (!other) return false;
        if (other.length != this.curves.length) return false;
        for (let i: number = 0; i < this.curves.length; i++) {
            if (!this.isArraySame(i, other[i])) return false;
        }
        return true;

    }
    private isArraySame(index: number, otherArray: RecievedCurve[]): boolean {
        let myArray: RecievedUsersCurves = this.curves[index];
        if (!myArray && !otherArray) return true;
        if (!myArray || !otherArray) return false;
        for (let i: number = 0; i < myArray.curves.length; i++) {
            if (!this.isRecievedCurveSame(myArray[i], otherArray[i])) return false;
        }
        return true;
    }
    private isRecievedCurveSame(one: RecievedCurve, twoo: RecievedCurve): boolean {
        if (!one && !twoo) return true;
        if (!one || !twoo) return false;
        if (one.color != twoo.color) return false;
        if (one.data != twoo.data) return false;
        return true;
    }
}