export class Point{
    public x: number;
    public y: number;
    public sent: boolean = false;
  
    public setCoordinates(x: number, y: number) {
      this.x = x;
      this.y = y;
    }
  }