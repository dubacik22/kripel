import { Point } from "./Point";

export class CurvePart{
    private middle: Point;
    private end: Point;
    public set sent(value: boolean) {
      if (this.middle) this.middle.sent = value;
      if (this.end) this.end.sent = value;
    }
    public get sent(): boolean {
      if (!this.middle || !this.end) return false;
      if (!this.middle.sent) return false;
      if (!this.end.sent) return false;
      return true;
    }

    public isFull(): boolean {
      if (!this.middle) return false;
      if (!this.end) return false;
      return true;
    }
    public addCoordinates(x: number, y: number) {
      let actP: Point = new Point;
      actP.setCoordinates(x, y);
      if (!this.middle) {
        this.middle = actP;
      } else if (!this.end) {
        this.end = actP;
      }
    }
    public getString(): string {
      if (!this.isFull()) return '';
      return 'Q '+this.middle.x+' '+this.middle.y+', '+this.end.x+' '+this.end.y
    }
  }