import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Paths } from 'src/stuff/injectables/Paths';
import { RecievedCurve } from './RecievedCurve';
import { RecievedUsersCurves } from './RecievedUsersCurve';

@Injectable()
export class Communicator{
    public verbose = false;
    private serverEventSource: EventSource;
    //private addToDo: boolean = false;
    private toDoList: ((data: any) => void)[] = [];
    private static createGetNewIdData(session: string): any {
        let ret = {
            'toDo': 'doodler_getNewId',
            'session': session,
        }
        return ret;
    }
    private static createCreateNewLineData(id: string, color: string, session: string): any {
        let ret = {
            'toDo': 'doodler_createNewLine',
            'userId': id,
            'color': color,
            'session': session,
        }
        return ret;
    }
    private static createAddNewLineData(id: string, data: string, color: string, session: string): any {
        let ret = {
            'toDo': 'doodler_addNewLine',
            'userId': id,
            'points': data,
            'color': color,
            'session': session,
        }
        return ret;
    }
    private static createSetPointsData(id: string, points: string, session: string): any {
        let ret = {
            'toDo': 'doodler_setPoints',
            'points': points,
            'userId': id,
            'session': session,
        }
        return ret;
    }
    private static createAddPointsData(id: string, points: string, session: string): any {
        let ret = {
            'toDo': 'doodler_addPoints',
            'points': points,
            'userId': id,
            'session': session,
        }
        return ret;
    }
    private static createDoneLineData(id: string, session: string): any {
        let ret = {
            'toDo': 'doodler_markLineAsDone',
            'userId': id,
            'session': session,
        }
        return ret;
    }
    private static createGetDoneLinesDataData(id: string, session: string): any {
        let ret = {
            'toDo': 'doodler_getDoneLinesData',
            'except': id,
            'session': session,
        }
        return ret;
    }
    private static createGetLinesDataData(id: string, session: string): any {
        let ret = {
            'toDo': 'doodler_getLinesData',
            'except': id,
            'session': session,
        }
        return ret;
    }
    private static createCreateTablesData(session: string): any {
        let ret = {
            'toDo': 'doodler_createTable',
            'session': session,
        }
        return ret;
    }
    constructor(private http: HttpClient, private paths: Paths) {
    }
    public connectToServerEvent() {
        if (this.serverEventSource) return;
        this.serverEventSource = new EventSource("http://localhost/kernel/scripts/communicator.php");//this.paths.phpCommunicator
        this.serverEventSource.onmessage = (event: MessageEvent) => {
            console.log(event)
            this.toDoList.forEach((toDo: (data: any) => void) => {
                toDo(event);
            });
        };
        this.serverEventSource.onopen = (event: any) => {
            //console.log(event)
        };
    }
    public disconnectFromServerEvent() {
        if (!this.serverEventSource) return;
        this.serverEventSource.close();
        this.serverEventSource = undefined;
    }
    public doOnServerEvent(toDo: (data: any) => void) {
        //this.addToDo = true;
        this.toDoList.push(toDo);
        //this.addToDo = false;
    }
    public getNewId(session: string, whatNext?: (id: string) => any) {
        let messageData = Communicator.createGetNewIdData(session);
        if (this.verbose) {
            console.log("We sent to server getNewId:");
            console.log(messageData);
        }
        
        this.http.post(this.paths.phpCommunicator, messageData, {responseType: 'json'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at getNewId:");
                console.log(retData);
            }
            if (retData && retData.success && whatNext) {
                whatNext(retData.id);
            }
        });
    }
    public createNewLine(id: string, color: string, session: string, whatNext?: (success: boolean) => any) {
        if (!id) return;
        console.log('snl')
        let messageData = Communicator.createCreateNewLineData(id, color, session);
        if (this.verbose) {
            console.log("We sent to server createNewLine:");
            console.log(messageData);
        }
        
        this.http.post(this.paths.phpCommunicator, messageData, {responseType: 'json'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at createNewLine:");
                console.log(retData);
            }
            if (retData && whatNext) {
                whatNext(retData.success);
            }
        });
    }
    public addNewLine(id: string, data: string, color: string, session: string, whatNext?: () => any) {
        if (!id) return;
        let messageData = Communicator.createAddNewLineData(id, data, color, session);
        if (this.verbose) {
            console.log("We sent to server addNewLine:");
            console.log(messageData);
        }
        
        this.http.post(this.paths.phpCommunicator, messageData, {responseType: 'json'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at addNewLine:");
                console.log(retData);
            }
            if (retData && retData.success && whatNext) {
                whatNext();
            }
        });
    }
    public setPoints(id: string, points: string, session: string, whatNext?: () => any) {
        let messageData = Communicator.createSetPointsData(id, points, session);
        if (this.verbose) {
            console.log("We sent to server setPoints:");
            console.log(messageData);
        }
        
        this.http.post(this.paths.phpCommunicator, messageData, {responseType: 'json'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned setPoints:");
                console.log(retData);
            }
            if (retData && retData.success && whatNext) {
                whatNext();
            }
        });
    }
    public addPoints(id: string, points: string, session: string, whatNext?: (success: boolean) => void) {
        let messageData = Communicator.createAddPointsData(id, points, session);
        if (this.verbose) {
            console.log("We sent to server addPoints:");
            console.log(messageData);
        }
    
        this.http.post(this.paths.phpCommunicator, messageData, {responseType: 'json'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at addPoints:");
                console.log(retData);
            }
            if (retData && whatNext) {
                whatNext(retData.success);
            }
        });
    }
    public markLineAsDone(id: string, session: string, whatNext?: (success: boolean) => void) {
        let messageData = Communicator.createDoneLineData(id, session);
        if (this.verbose) {
            console.log("We sent to server doneLine:");
            console.log(messageData);
        }
    
        this.http.post(this.paths.phpCommunicator, messageData, {responseType: 'json'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at doneLine:");
                console.log(retData);
            }
            if (retData && whatNext) {
                whatNext(retData.success);
            }
        });
    }
    public getDoneLinesData(id: string, session: string, whatNext?: (lines: RecievedUsersCurves[]) => void) {
        let messageData = Communicator.createGetDoneLinesDataData(id, session);
        if (this.verbose) {
            console.log("We sent to server getDoneLinesData:");
            console.log(messageData);
        }
    
        this.http.post(this.paths.phpCommunicator, messageData, {responseType: 'json'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at getDoneLinesData:");
                console.log(retData);
            }
            if (retData && retData.success && whatNext) {
                whatNext(retData.lines);
            }
        });
    }
    public getNewLinesData(id: string, session: string, whatNext?: (lines: RecievedCurve[][]) => void) {
        let messageData = Communicator.createGetLinesDataData(id, session);
        if (this.verbose) {
            console.log("We sent to server getLinesData:");
            console.log(messageData);
        }
    
        this.http.post(this.paths.phpCommunicator, messageData, {responseType: 'json'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at getLinesData:");
                console.log(retData);
            }
            if (retData && retData.success && whatNext) {
                whatNext(retData.lines);
            }
        });
    }
    public createTables(session: string, whatNext?: () => void) {
        let sentData = Communicator.createCreateTablesData(session);
        if (this.verbose) {
            console.log("We sent to server createTables:");
            console.log(sentData);
        }
        this.http.post(this.paths.phpCommunicator, sentData, {responseType:'json'}).subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at createTables:");
                console.log(retData);
            }
            if (retData && retData.success && whatNext) {
                whatNext();
            }
        });
    }
}
