import { Point } from "./Point";
import { CurvePart } from './CurvePart';
import { Stroke } from 'angular-svg';

export class Curve{
    private startingPoint: Point;
    private parts: CurvePart[] = [];
    public stroke: Stroke;
    public done: boolean = false;
    private _sent: boolean = false;
    public get sent(): boolean {
      return this._sent;
    }
    public set sent(value: boolean) {
      this._sent = value;
    }

    public get containsUnsentStuff(): boolean {
      if (this.startingPoint) {
        if (!this.startingPoint.sent) return true;
      } else return true;
      for (let i: number = 0; i < this.parts.length; i++) {
        let part: CurvePart = this.parts[i];
        if (!part.sent) return true;
      }
      return false;
    }
    public set containsUnsentStuff(value: boolean) {
      this.startingPoint.sent = !value;
      this.parts.forEach((part: CurvePart) => {
        part.sent = !value;
      });
    }
    public getUnsentStringAndMarkPartsAsSent(): string {
      let ret: string = '';
      if (!this.startingPoint) return ret;
      if (this.startingPoint.sent) {
        this.parts.forEach((part: CurvePart) => {
          if (!part.sent) {
            ret += ' '+part.getString();
            part.sent = true;
          }
        });
      } else {
        ret = this.getString();
        this.startingPoint.sent = true;
        this.parts.forEach((part: CurvePart) => {
          part.sent = true;
        });
      }
      return ret;
    }
    public getColor(): string {
      return this.stroke.strokeFill;
    }

    public addCoordinates(x: number, y: number) {
      if (!this.startingPoint) {
        this.startingPoint = new Point();
        this.startingPoint.setCoordinates(x, y);
        return;
      }
      let lc: CurvePart = this.getLastPart();
      if (!lc) {
        lc = new CurvePart();
        this.parts.push(lc);
      }
      if (lc.isFull()) {
        lc = new CurvePart();
        this.parts.push(lc);
      }
      lc.addCoordinates(x, y);
    }
    private getLastPart(): CurvePart {
      if (!this.parts) return undefined;
      if (this.parts.length == 0) return undefined;
      return this.parts[this.parts.length-1];
    }
    public getString(): string {
      let ret: string = '';
      if (!this.startingPoint) return ret;
      ret = 'M'+this.startingPoint.x+' '+this.startingPoint.y;
      this.parts.forEach((part: CurvePart) => {
        ret += ' '+part.getString();
      });
      return ret;
    }
    public isEmpty(): boolean {
      return (this.parts.length == 0);
    }
  }