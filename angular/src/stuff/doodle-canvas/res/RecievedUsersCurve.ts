import { RecievedCurve } from './RecievedCurve';

export class RecievedUsersCurves {
    public user: string;
    public curves: RecievedCurve[];
}