import { Stroke } from 'angular-svg';

export class RecievedCurve {
    public data: string;
    public color: string;

    public stroke: Stroke;
}