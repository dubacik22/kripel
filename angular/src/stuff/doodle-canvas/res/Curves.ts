import { Curve } from './Curve';
import { Communicator } from './Communicator';
class toto {
  toto: () => void;
  kto: string;
  constructor (kto: string, toto: () => void) {
    this.kto = kto;
    this.toto = toto;
  }
}
export class Curves {
    private curves: Curve[] = [];
    private communication: toto[] = [];//(() => void)[] = [];
    public id: string;
    public communicator: Communicator;
    constructor () {}
    public get getCurves(): Curve[] {
      return this.curves;
    }

    public startNewCurve() {
      let lc: Curve = this.getLastCurve();
      if (lc && lc.isEmpty()) return;
      this.curves.push(new Curve());
    }
  
    public getLastCurve(): Curve {
      if (!this.curves) return undefined;
      if (this.curves.length == 0) return undefined;
      return this.curves[this.curves.length-1];
    }

    private talking: boolean = false;
    private nextCommunication() {
      if (this.talking) return;
      if (this.communication.length > 0) {
        let comPart: toto = this.communication[0];
        this.communication.splice(0, 1);
        comPart.toto();
      }
    }
    public communicateStartNewLine(color: string) {
      this.communication.push(new toto('nl', () => {
        this.talking = true;
        this.communicator.createNewLine(this.id, color, '', () => {
          this.talking = false;
          setInterval(() => this.nextCommunication(), 0);
        });
      }));
      this.nextCommunication();
    }
    //a:number=0;
    public communicateAddPoints(points: string) {
      //points =this.a.toString()+'ste:11 83 Q 418 80, 415 74 Q 414 70, 414 68 Q 412 66, 412 64 Q 412 63, 412 62 Q 412 60, 412 60  Q 412 58, 412 57 Q 412 57, 413 57 Q 418 57, 420 57 Q 424 57, 427 57 Q 428 57, 431 60 Q 435 66, 436 69 Q 438 73, 439 77 Q 439 80, 439 83 Q 439 86, 439 87 Q 439 90, 439 90  Q 439 91, 438 91 Q 436 91, 435 91 Q 430 91, 429 91 Q 426 90, 422 85 Q 421 82, 418 77 Q 418 73, 418 71 Q 416 68, 416 66 Q 416 65, 416 63 Q 416 62, 416 61 Q 416 60, 416 60  Q 416 58, 416 58 Q 416 57, 416 57 Q 416 57, 418 56 Q 421 56, 425 56 Q 427 56, 428 56'
      //this.a++;
      this.communication.push(new toto('ap', () => {
        this.talking = true;
        this.communicator.addPoints(this.id, points, '', () => {
          this.talking = false;
          setInterval(() => this.nextCommunication(), 0);
        });
      }));
      this.nextCommunication();
    }
    public communicateMarkLineAsDone() {
      this.communication.push(new toto('mld', () => {
        this.talking = true;
        this.communicator.markLineAsDone(this.id, '', () => {
          this.talking = false;
          setInterval(() => this.nextCommunication(), 0);
        });
      }));
      this.nextCommunication();
    }
  }