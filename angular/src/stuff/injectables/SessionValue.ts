export class SessionValue {
    public value: string;
    public inLocalStorage: boolean;

    public static createLocalValue(value: string): SessionValue {
        let ret: SessionValue = new SessionValue();
        ret.value = value;
        ret.inLocalStorage = true;
        return ret;
    }
    public static createSessionValue(value: string): SessionValue {
        let ret: SessionValue = new SessionValue();
        ret.value = value;
        ret.inLocalStorage = false;
        return ret;
    }
}