import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from 'src/app/app-login/res/User';
import { DialogBoxComponent } from 'src/stuff/dialogs/modalDialog.component';
import { ButtonConf } from 'src/stuff/dialogs/res/ButtonConf';
import { SessionValue } from './SessionValue';

@Injectable()
export class Session {
  public static readonly canStoreCookiesPropName: string = 'canStore';
  private _values: Map<string, SessionValue> = new Map();
  private valuesSource = new BehaviorSubject<Map<string, SessionValue>>(this._values);
  public values$ = this.valuesSource.asObservable();
  public addItems2Local(values: Map<string, string>) {
    if (!values) return;
    values.forEach((value: string, key: string) => {
      this.innerAdditem2Local(key, value);
    });
    this.insertSession();
    this.valuesSource.next(this._values);
  }
  public additem2Local(key: string, value: string) {
    if (!key || !value) return;
    this.innerAdditem2Local(key, value)
    this.insertSession();
    this.valuesSource.next(this._values);
  }
  private innerAdditem2Local(key: string, value: string) {
    if (!this._values) this._values = new Map();
    this._values.set(key, SessionValue.createLocalValue(value));
  }

  public addItems2Session(values: Map<string, string>) {
    if (!values) return;
    values.forEach((value: string, key: string) => {
      if (!key || !value) return;
      this.innerAdditem2Session(key, value);
    });
    this.insertSession();
    this.valuesSource.next(this._values);
  }
  public addItem2Session(key: string, value: string) {
    if (!key || !value) return;
    this.innerAdditem2Session(key, value)
    this.insertSession();
    this.valuesSource.next(this._values);
  }
  private innerAdditem2Session(key: string, value: string) {
    if (!this._values) this._values = new Map();
    this._values.set(key, SessionValue.createSessionValue(value));
  }

  public hasItem(key: string): boolean {
    return this._values.has(key);
  }
  public getItem(key: string): string {
    if (!this._values.has(key)) return undefined;
    return this._values.get(key).value;
  }
  public remItems(keys: string[]) {
    if (!keys) return;
    if (!this._values) {
      this._values = new Map();
      return
    }
    keys.forEach((key: string) => {
      if (!key) return;
      this.innerRemItem(key);
    });
    this.insertSession();
    this.valuesSource.next(this._values);
  }
  public remItem(key: string) {
    if (!key) return;
    this.innerRemItem(key);
    this.insertSession();
    this.valuesSource.next(this._values);
  }
  private innerRemItem(key: string) {
    if (!this._values) {
      this._values = new Map();
      return
    }
    this._values.delete(key);
  }

  private insertSession() {
    if (!this.canStore()) {
      let buttons: ButtonConf[] = [];
      buttons.push(new ButtonConf('A tak OK.', () => {
        this.innerAdditem2Local(Session.canStoreCookiesPropName, 'true');
        this.innerInsert();
      }));
      buttons.push(new ButtonConf('Čo si!'));
      DialogBoxComponent.set('Táto stránka chce používať a strašným spôsobom zneužívať cookies. Môže?', buttons);
      DialogBoxComponent.show();
    } else {
      this.innerInsert();
    }
  }
  private innerInsert() {
    let sessionStorage: Storage = window.sessionStorage;
    let localStorage: Storage = window.localStorage;
    localStorage.clear();
    sessionStorage.clear();
    this._values.forEach((value: SessionValue, key: string) => {
      if (value.inLocalStorage) localStorage.setItem(key, value.value);
      else sessionStorage.setItem(key, value.value);
    });
  }
  private retrieve() {
    let sessionStorage: Storage = window.sessionStorage;
    let localStorage: Storage = window.localStorage;
    for (let i: number = 0; i < localStorage.length; i++) {
      let key: string = localStorage.key(i);
      let value: SessionValue = SessionValue.createLocalValue(localStorage.getItem(key));
      this._values.set(key, value);
    }
    for (let i: number = 0; i < sessionStorage.length; i++) {
      let key: string = sessionStorage.key(i);
      let value: SessionValue = SessionValue.createSessionValue(sessionStorage.getItem(key));
      this._values.set(key, value);
    }
    this.valuesSource.next(this._values);
  }
  constructor() {
    this._values.clear();
    this.retrieve();
  }
  public canStore(): boolean {
    return Session.canStore(this._values);
  }
  public isUberAdmin(): boolean {
    return Session.isUberAdmin(this._values);
  }
  public isLogged(): boolean {
    return Session.isLogged(this._values);
  }
  public getEmail(): string {
    return this.getItem(User.emailPropName);
  }
  public getLoginName(): string {
    return this.getItem(User.loginNamePropName);
  }
  public getEncryptedSession(): string {
    return this.getItem(User.encryptedSessionPropName);
  }
  public static canStore(values: Map<string, SessionValue>): boolean {
    return values.has(Session.canStoreCookiesPropName);
  }
  public static isUberAdmin(values: Map<string, SessionValue>): boolean {
    let isUberAdmin: boolean = Session.isLogged(values) && 
    values.has(User.permissionsPropName) &&
    values.get(User.permissionsPropName) &&
    (values.get(User.permissionsPropName).value == 'all');
    return isUberAdmin;
  }
  public static isLogged(values: Map<string, SessionValue>): boolean {
    return values.has(User.encryptedSessionPropName);
  }
  private isSame(i1: Map<string, string>, i2: Map<string, string>): boolean {
    if (!i1 && !i2) return true;
    if (!i1 || !i2) return false;
    let i1s: number = i1.size;
    let i2s: number = i2.size;
    if (i1s != i2s) return false;
    let i1a: [string, string][] = Array.from(i1.entries());
    let i2a: [string, string][] = Array.from(i2.entries());
    for (let i: number = 0; i < i1s; i++) {
      if (i1a[i][0] != i2a[i][0]) return false; 
      if (i1a[i][1] != i2a[i][1]) return false; 
    }
    return true;
  }
}