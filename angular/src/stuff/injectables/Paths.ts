import { Injectable } from "@angular/core";
@Injectable()
export class Paths{
    private isLocalHost: boolean = false;
    public get imgServer(): string {
        let ret: string = this.server;
        ret += this.mainPath;
        return ret;
    }
    public get phpCommunicator(): string {
        return this.server + this.phpAddix + 'communicator.php';
    }
    private get server(): string {
        let ret: string = (this.isLocalHost) ? 'http://localhost/' : '';
        return ret;
    }
    private phpAddix: string = 'kernel/scripts/';
    public mainPath: string = 'tests/';

    constructor(){
        this.isLocalHost = (window.location.href.toLowerCase().indexOf('localhost') >= 0) ? true : false; 
    }
}