import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

export class routerListener {
  public whatToDo: (queries: Map<string, string>) => void;
  private router: MyRouting;

  constructor(router:MyRouting, whatToDo: (queries: Map<string, string>) => void) {
    this.whatToDo = whatToDo;
    this.router = router;
  }

  public unsubscribe() {
    this.router.workingWithTodo = true;
    this.router.toDoList.splice(this.router.toDoList.indexOf(this.whatToDo), 1);
    this.router.workingWithTodo = false;
  }
}

@Injectable()
export class MyRouting {
  private newQueries: Map<string, string> = new Map();
  private origQueries: Map<string, string> = new Map();
  private _workingWintTodo: boolean = false;
  public set workingWithTodo(value: boolean) {
    if (this._workingWintTodo == value) return;
    this._workingWintTodo = value;
    if (!value) {
      if (this.wantCall) this.notifyOthers();
    }
  }
  private wantCall: boolean = false;
  private _unlisten: number = 0;
  private _ignore: number = 0;
  public toDoList: ((arg: Map<string, string>) => void)[] = [];
  constructor(private activatedRoute: ActivatedRoute ,private _router: Router) {
    this.beginListen();
  }
  private beginListen() {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.origQueries.clear();
      Object.keys(params).forEach((key: string) => {
        this.origQueries.set(key, params[key]);
      });
      this.newQueries = this.duplicateQueries(this.origQueries);
      if (this._unlisten > 0) {
        this._unlisten--;
        return;
      }
      if (this._ignore) {
        return;
      }
      if (this.workingWithTodo) {
        this.wantCall;
        return;
      }
      this.notifyOthers();
    });
    return; 
  }
  private notifyOthers() {
    this.toDoList.forEach((toDo: (arg: Map<string, string>) => void) => { toDo(this.duplicateQueries(this.origQueries)); });
  }
  private duplicateQueries(queries: Map<string, string>): Map<string, string> {
    let ret: Map<string, string> = new Map();
    queries.forEach((value: string, key: string) => {
      ret.set(key, value);
    });
    return ret;
  }
  public onChangeRouteQueries(whatToDo: (queries: Map<string, string>) => void): routerListener {
    this.workingWithTodo = true;
    let ret: routerListener = new routerListener(this, whatToDo);
    this.toDoList.push(whatToDo);
    this.workingWithTodo = false;
    if (this._unlisten)  return ret;
    whatToDo(this.duplicateQueries(this.origQueries));
    return ret;
  }
  public getSentQueries(): Map<string, string> {
    return this.duplicateQueries(this.origQueries);
  }
  public getSentQuerie(key: string): string {
    return this.origQueries.get(key);
  }
  public haveSentQuerie(key: string): boolean {
    return this.origQueries.has(key);
  }
  public unlistenOnce() {
    this._unlisten = 1;
  }
  public ignore() {
    this._ignore++;
  }
  public unignore() {
    this._ignore--;
  }
  public insertQueryS(name: string, value: string) {
    this.newQueries.set(name, value);
  }
  public remQuery(name: string) {
    this.newQueries.delete(name);
  }
  public clearAll() {
    this.newQueries.clear();
  }
  public insert() {
    if (this._ignore) return;
    if (this.isQueriesSame(this.newQueries, this.origQueries)) {
      if (this._unlisten > 0) {
        this._unlisten--;
      }
      return;
    }
    let insertedMap: any = {};
    this.newQueries.forEach((value: string, key: string)=>{
      insertedMap[key] = value;
    });
    //console.log('inserting qs')
    //console.log(insertedMap)
    this._router.navigate([], {
      queryParams: insertedMap,
    });
  }
  private isQueriesSame(one: Map<string, string>, twoo: Map<string, string>): boolean {
    if (!one && !twoo) return true;
    if (!one || !twoo) return false;
    if (one.size != twoo.size) return false;
    let oneEntries: [string, string][] = Array.from(one.entries());
    let twooEntries: [string, string][] = Array.from(twoo.entries());

    for (let i: number = 0; i < oneEntries.length; i++) {
      let oneEntry: [string, string] = oneEntries[i];
      let twooEntry: [string, string] = twooEntries[i];
      if (oneEntry[0] != twooEntry[0]) return false;
      if (oneEntry[1] != twooEntry[1]) return false;
    }
    return true;
  }
}