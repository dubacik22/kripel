import { Planet } from './Planet';
import { Vector } from './Vector';
import { MessengerFactory } from 'src/stuff/messenger/MessengerFactory';
import { SolarisMessenger } from './SolarisMessenger';
import { Cross } from './Cross';

export class PlanetSystem {
    public planets: Planet[] = [];
    public G: number = 6.67e-11;
    public refreshRate: number = 100;
    public radiusDivMassPow2: number = 0;
    public displayNames: boolean = true;
    public crosses: Cross[] = [];

    private messenger: SolarisMessenger = new SolarisMessenger();

    private _stepLength: number = 0.1;
    public get stepLength(): number {
        return this._stepLength;
    }
    public set stepLength(value: number) {
        this._stepLength = value;
    }

    private _spaceFriction: number = 0.1;
    public get spaceFriction(): number {
        return this._spaceFriction;
    }
    public set spaceFriction(value: number) {
        this._spaceFriction = value;
    }

    private run: boolean = false;
    public get isRun(): boolean {
        return this.run;
    }

    private _dispUnitsDiv: number = 1;
    public get dispUnitsDiv(): number {
        return this._dispUnitsDiv;
    }
    public set dispUnitsDiv(value: number) {
        this._dispUnitsDiv = value;
        for (let myPlanet of this.planets) {
            myPlanet.dispUnitsDiv = value;
        }
        for (let myCross of this.crosses) {
            myCross.dispUnitsDiv = value;
        }
        this.messenger.notify(SolarisMessenger.SYST_CHANGED, undefined);
    }
    
    private _dispPosCor2RotCenter: Vector = new Vector(0,0,0);
    public get dispPosCor2RotCenter(): Vector {
        return this._dispPosCor2RotCenter;
    }
    public set dispPosCor2RotCenter(value: Vector) {
        this._dispPosCor2RotCenter = value;
        for (let myPlanet of this.planets) {
            myPlanet.dispPosCor2RotCenter = value;
        }
        for (let myCross of this.crosses) {
            myCross.dispPosCor2RotCenter = value;
        }
    }
    
    private _dispPosCor2View: Vector = new Vector(0,0,0);
    public get dispPosCor2View(): Vector {
        return this._dispPosCor2View;
    }
    public set dispPosCor2View(value: Vector) {
        this._dispPosCor2View = value;
        for (let myPlanet of this.planets) {
            myPlanet.dispPosCor2View = value;
        }
        for (let myCross of this.crosses) {
            myCross.dispPosCor2View = value;
        }
    }
    
    private _dispAngleCorrection: Vector = new Vector(0,0,0);
    public get dispAngCor(): Vector {
        return this._dispAngleCorrection;
    }
    public set dispAngCor(value: Vector) {
        this._dispAngleCorrection = value;
        for (let myPlanet of this.planets) {
            myPlanet.dispAnCor = value;
        }
        for (let myCross of this.crosses) {
            myCross.dispAnCor = value;
        }
    }

    public constructor() {
        this.messenger = MessengerFactory.getMessenger("solaris");
    }

    public remPlanet(planet: Planet) {
        let index = 0;
        for (let myPlanet of this.planets) {
            if (planet === myPlanet) {
                this.planets.splice(index, 1);
                return;
            }
            index++;
        }
    }
    public addMassPlanet(name: string, color: string, mass: number, positon: Vector): Planet {
        let planet = new Planet();
        planet.name = name;
        planet.strokeColor = color;
        planet.dispUnitsDiv = this.dispUnitsDiv;
        planet.dispPosCor2View = this.dispPosCor2View;
        planet.dispPosition = positon;
        planet.mass = mass;
        planet.dispRadius = this.radiusFromMass(mass);
        this.planets.push(planet);
        return planet;
    }
    public addCross(positon: Vector, size: number): void {
        let cross = new Cross();
        cross.dispUnitsDiv = this.dispUnitsDiv;
        cross.dispSize = size;
        cross.dispPosCor2View = this.dispPosCor2View;
        cross.dispPosition = positon;
        this.crosses.push(cross);
    }

    public step(): void {
        for(let planet of this.planets) {
            planet.havePosUpdated = false;
        }

        for(let i = 0; i < this.planets.length; i++) {
            let planet = this.planets[i];
            this.updateForceVectorByMasAndDist(planet);
        }
        for(let planet of this.planets) {
            this.movePlanetAccordingToForceVector(planet, this.stepLength);
        }
        this.messenger.notify(SolarisMessenger.PLANETS_UPDATE, undefined);
        if (this.run) {
            setTimeout(() => {
                this.step();
            }, this.refreshRate);
        }
    }

    public runSystem(): void {
        if (this.run) return;
        this.run = true;
        setTimeout(() => {
            this.step();
        }, this.refreshRate);
    }
    public stopSystem(): void {
        this.run = false;
    }

    private radiusFromMass(mass: number): number {
        return Math.ceil(Math.sqrt(mass)*this.radiusDivMassPow2);
    }
    private colorFromRadius(radius: number): string {
        let c = Math.min(Math.max(radius - 10) * 255 / 30, 255);
        c = Math.round(c);
        return "rgb("+c+","+c+","+"0)";
    }

    private collapsePlanets(planetToKeep: Planet, planet2: Planet) {
        let mass = planetToKeep.mass + planet2.mass;

        let momentum1 = Vector.from(planetToKeep.oldVelocityVector).multiplyScalar(planetToKeep.mass);
        let momentum2 = Vector.from(planet2.oldVelocityVector).multiplyScalar(planet2.mass);
        let sumMom = momentum1.addVector(momentum2);
        let v = Vector.from(sumMom).divideScalar(mass);
        planetToKeep.oldVelocityVector = v;

        let pos1 = Vector.from(planetToKeep.position).multiplyScalar(planetToKeep.mass);
        let pos2 = Vector.from(planet2.position).multiplyScalar(planet2.mass);
        let sumPos = pos1.addVector(pos2);
        let newPosition = sumPos.divideScalar(mass);
        planetToKeep.position = newPosition;

        planetToKeep.mass = mass;
        let radius = this.radiusFromMass(mass);
        planetToKeep.dispRadius = radius;
        planetToKeep.strokeColor = this.colorFromRadius(radius);
        this.remPlanet(planet2);
    }

    private updateActFdMVector(planet: Planet, otherPlanet: Planet): void {
        //f=g*m1*m2/r^2
        //g=6.674×10^−11
        let otherDirection = Vector.from(otherPlanet.position).substractVector(planet.position);
        let dist = otherDirection.size();
        if (dist <= planet.diameter+otherPlanet.diameter) {
            this.collapsePlanets(planet, otherPlanet);
        } else {
            let force = otherPlanet.mass * this.G / Math.pow(dist, 2);
            planet.newFdMVector.addVector(otherDirection.divideScalar(dist).multiplyScalar(force));
        }
    }
    
    private movePlanetAccordingToForceVector(planet: Planet, time: number) {
        //a=f/m
        let a = planet.newFdMVector; //a
        let na = a.substractVector(Vector.from(planet.oldVelocityVector).multiplyScalar(this._spaceFriction*0.0001))

        planet.oldVelocityVector.addVector(na.multiplyScalar(time));
        let posAppx = Vector.from(planet.oldVelocityVector).multiplyScalar(time);
        planet.position = planet.position.addVector(posAppx);
    }

    private updateForceVectorByMasAndDist(planet: Planet): void {
        planet.newFdMVector = new Vector(0, 0, 0);
        for (let i = 0; i < this.planets.length; i++) {
            let otherPlanet = this.planets[i];
            if (planet == otherPlanet) continue;
            this.updateActFdMVector(planet, otherPlanet);
        }
    }

    public log() {
        console.log("planets {");
        this.planets.forEach((planet)=>{
            planet.log();
        });
        console.log("}");
    }
}