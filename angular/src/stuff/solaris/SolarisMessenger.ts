import { Messenger } from 'src/stuff/messenger/Messenger';
import { MessengerFactory } from 'src/stuff/messenger/MessengerFactory';

export class SolarisMessenger {
    private static messenger: Messenger;
    public static readonly SYST_CHANGED: string = "systemChanged";
    public static readonly PLANETS_UPDATE: string = "planetsUpdate";

    public constructor() {
        if (SolarisMessenger.messenger === undefined) {
            SolarisMessenger.messenger = MessengerFactory.getMessenger("solaris");
        }
    }

    public subscribe(message: string, update: (o: any) => void) {
        SolarisMessenger.messenger.subscribe(message, update);
    }
    public unsubscribe(update: () => void) {
        SolarisMessenger.messenger.unsubscribe(update);
    }
    public notify(message: string, data: any) {
        SolarisMessenger.messenger.notify(message, data);
    }
}