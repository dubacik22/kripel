import { Vector } from './Vector';
//f=g*m1*m2/r^2
//g=6.674×10^−11
//f=m*a
export class Planet {
    public dispUnitsDiv: number = 1;
    public dispPosCor2RotCenter: Vector = new Vector(0,0,0);
    public dispPosCor2View: Vector = new Vector(0,0,0);
    public dispAnCor: Vector = new Vector(0,0,0);

    public name: string;
    public havePosUpdated: boolean = false;
    public strokeColor: string;
    public newFdMVector: Vector = new Vector(0,0,0);
    public oldVelocityVector: Vector = new Vector(0,0,0);

    public outerRadius: number = 0;

    public position: Vector = new Vector(0, 0, 0);
    public get dispPosition(): Vector {
        return Planet.getDispFromRealVector(Vector.from(this.position), this.dispUnitsDiv, this.dispPosCor2RotCenter,
               this.dispAnCor, this.dispPosCor2View);
    }
    public set dispPosition(value: Vector) {
        this.position = Planet.getRealFromDispVector(Vector.from(value), this.dispPosCor2View, this.dispAnCor,
                        this.dispPosCor2RotCenter, this.dispUnitsDiv);
    }
    public static getRealFromDispVector(ofV: Vector, dispPosCor2View: Vector, dispAngCor: Vector,
        dispPosCor2RotCenter: Vector, dispUnitsDiv: number): Vector {
        ofV.substractVector(dispPosCor2View);
        ofV.rotateBackVector(dispAngCor);
        ofV.addVector(dispPosCor2View);
        ofV.substractVector(dispPosCor2RotCenter);
        ofV.multiplyScalar(dispUnitsDiv);
        return ofV;
    }
    public static getDispFromRealVector(ofV: Vector, dispUnitsDiv: number, dispPosCor2RotCenter: Vector,
        dispAngCor: Vector, dispPosCor2View: Vector): Vector {
        ofV.divideScalar(dispUnitsDiv);
        ofV.addVector(dispPosCor2RotCenter);
        ofV.substractVector(dispPosCor2View);
        ofV.rotateVector(dispAngCor);
        ofV.addVector(dispPosCor2View);
        return ofV;
    }

    private _radius: number = NaN;
    public get radius(): number {
        return this._radius;
    }
    public set radius(value: number) {
        if (this._radius == value) return;
        this._radius = value;
        this.diameter = value*2;
    }
    private _diameter: number = NaN;
    public get diameter(): number {
        return this._diameter;
    }
    public set diameter(value: number) {
        if (this._diameter == value) return;
        this._diameter = value;
        this.radius = value/2;
        this.dispDiameter = Math.ceil(value / this.dispUnitsDiv);
    }
    public mass: number;

    private _dispRadius: number = NaN;
    public get dispRadius(): number {
        return this._dispRadius;
    }
    public set dispRadius(value: number) {
        if (this._dispRadius === value) return;
        if (isNaN(this._dispRadius) && isNaN(value)) return;
        this._dispRadius = value;
        this.dispDiameter = value*2;
    }
    private _dispDiameter: number = NaN;
    public get dispDiameter(): number {
        return this._dispDiameter;
    }
    public set dispDiameter(value: number) {
        if (this._dispDiameter === value) return;
        if (isNaN(this._dispDiameter) && isNaN(value)) return;
        this._dispDiameter = value;
        this.dispRadius = value / 2;
        this.diameter = value * this.dispUnitsDiv;
    }

    public logStr(prefix: string): string {
        var ret = prefix+this.name;
        ret += " pos:"+this.position.logStr("");
        ret += " r:"+this.radius+" d:"+this.diameter;
        ret += " dpos:"+this.dispPosition.logStr("");
        ret += " dr:"+this.dispRadius+" dd:"+this.dispDiameter;
        return ret;
    }
    public log(): void {
        console.log(this.logStr(""));
    }
}