import { Vector } from './Vector';
import { Planet } from './Planet';
//f=g*m1*m2/r^2
//g=6.674×10^−11
//f=m*a
export class Cross {
    public dispUnitsDiv: number = 1;
    public dispPosCor2RotCenter: Vector = new Vector(0,0,0);
    public dispPosCor2View: Vector = new Vector(0,0,0);
    public dispAnCor: Vector = new Vector(0,0,0);

    public size: number = 10;
    public position: Vector = new Vector(0, 0, 0);

    public get dispPosition(): Vector {
        return Planet.getDispFromRealVector(Vector.from(this.position), this.dispUnitsDiv, this.dispPosCor2RotCenter,
               this.dispAnCor, this.dispPosCor2View);
    }
    public set dispPosition(value: Vector) {
        this.position = Planet.getRealFromDispVector(Vector.from(value), this.dispPosCor2View, this.dispAnCor,
                        this.dispPosCor2RotCenter, this.dispUnitsDiv);
    }
    public get dispSize(): number {
        return this.size / this.dispUnitsDiv;
    }
    public set dispSize(value: number) {
        this.size = value * this.dispUnitsDiv;
    }

    public get svgPointsH(): string {
        let sizeDiv2 = this.size / 2;
        let upperPnt = Vector.from(this.position).shift(0, sizeDiv2, 0);
        let lowerPnt = Vector.from(this.position).shift(0, -sizeDiv2, 0);

        upperPnt = Planet.getDispFromRealVector(upperPnt, this.dispUnitsDiv, this.dispPosCor2RotCenter,
                   this.dispAnCor, this.dispPosCor2View);
        lowerPnt = Planet.getDispFromRealVector(lowerPnt, this.dispUnitsDiv, this.dispPosCor2RotCenter,
                   this.dispAnCor, this.dispPosCor2View);

        return upperPnt.x + " " + upperPnt.y + " " + lowerPnt.x + " " + lowerPnt.y;
    }
    public get svgPointsW(): string {
        let sizeDiv2 = this.size / 2;
        let leftPnt = Vector.from(this.position).shift(-sizeDiv2, 0, 0);
        let rightPnt = Vector.from(this.position).shift(sizeDiv2, 0, 0);
        
        leftPnt = Planet.getDispFromRealVector(leftPnt, this.dispUnitsDiv, this.dispPosCor2RotCenter,
                  this.dispAnCor, this.dispPosCor2View);
        rightPnt = Planet.getDispFromRealVector(rightPnt, this.dispUnitsDiv, this.dispPosCor2RotCenter,
                  this.dispAnCor, this.dispPosCor2View);
        
        return leftPnt.x + " " + leftPnt.y + " " + rightPnt.x + " " + rightPnt.y;
    }
    public get svgPointsD(): string {
        let sizeDiv2 = this.size / 2;
        let frontPnt = Vector.from(this.position).shift(0, 0, -sizeDiv2);
        let backPnt = Vector.from(this.position).shift(0, 0, sizeDiv2);

        frontPnt = Planet.getDispFromRealVector(frontPnt, this.dispUnitsDiv, this.dispPosCor2RotCenter,
                   this.dispAnCor, this.dispPosCor2View);
        backPnt = Planet.getDispFromRealVector(backPnt, this.dispUnitsDiv, this.dispPosCor2RotCenter,
                   this.dispAnCor, this.dispPosCor2View);
        
        return frontPnt.x + " " + frontPnt.y + " " + backPnt.x + " " + backPnt.y;
    }
}