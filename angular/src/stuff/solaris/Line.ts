import { Vector } from './Vector';

export class Line {
    public slope: number;
    public offset: number;

    public constructor(slope: number, offset: number) {
        this.slope = slope;
        this.offset = offset;
    }

    public getPerpendicular(inPoint: Vector): Line {
        var newSlope = -1 / this.slope;
        return new Line(newSlope, inPoint.y - newSlope * inPoint.x);
    }

    public getPointsInDistanceFrom(point: Vector, distance: number): Vector[] {
        var p: Vector[] = [];
        var p1 = this.getPointFarerFrom(point, distance);
        var p2 = this.getPointNeaerFrom(point, distance);
        //var dp1x = p1.x - point.x;
        //var dp2x = p2.x - point.x;
        //var dp1y = p1.y - point.y;
        //var dp2y = p2.y - point.y;
        // if ((dp1x > 0) && (dp1y > 0)) {
            p.push(p1);
            p.push(p2);
        // } else {
        //     p.push(p2);
        //     p.push(p1);
        // }
        return p;
    }
    
    public getPointFarerFrom(point: Vector, distance: number): Vector {
        var xDistance = distance * Math.sqrt(1 / (1 + Math.pow(this.slope, 2)));
        var yDistance = this.slope * xDistance;
        return new Vector(point.x + xDistance, point.y + yDistance, 0);
    }
    
    public getPointNeaerFrom(point: Vector, distance: number): Vector {
        var xDistance = distance * Math.sqrt(1 / (1 + Math.pow(this.slope, 2)));
        var yDistance = this.slope * xDistance;
        return new Vector(point.x - xDistance, point.y - yDistance, 0);
    }

    public static fromPoints(p1:Vector, p2:Vector): Line {
        var newSlope = (p2.y - p1.y) / (p2.x - p1.x);
        var ret = new Line(newSlope, p1.y - newSlope*p1.x);
        //var ret = new Line(newSlope, -(newSlope * p1.x) / p1.y);
        return ret;
    }
    
    public log(): void {
        console.log(this.slope+"a+"+this.offset);
    }
}