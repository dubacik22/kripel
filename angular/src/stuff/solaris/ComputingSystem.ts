export enum ComputingSystem {
    ByMasAndForcesAndPos,
    ByRotationAndVelocity,
    Static
}