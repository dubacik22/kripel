export class Vector{
    public x: number;
    public y: number;
    public z: number;

    public static from(what: Vector): Vector {
        if (what === undefined)  return new Vector(NaN, NaN, NaN);
        return new Vector(what.x, what.y, what.z);
    }
    public constructor(x: number, y: number, z: number) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public divideScalar(what: number): Vector {
        this.x /= what;
        this.y /= what;
        this.z /= what;
        return this;
    }
    public multiplyScalar(what: number): Vector {
        this.x *= what;
        this.y *= what;
        this.z *= what;
        return this;
    }
    public multiplyVector(what: Vector): Vector {
        this.x = (this.y * what.z) - (this.z * what.y);
        this.y = (this.z * what.x) - (this.x * what.z);
        this.x = (this.x * what.y) - (this.y * what.x);
        return this;
    }
    public pow2(): Vector {
        this.x = this.x*this.x;
        this.y = this.y*this.y;
        this.z = this.z*this.z;
        return this;
    }
    public sqrt(): Vector {
        this.x = Math.sqrt(this.x)
        this.y = Math.sqrt(this.y)
        this.z = Math.sqrt(this.z)
        return this;
    }
    public addVector(what: Vector): Vector {
        this.x += what.x;
        this.y += what.y;
        this.z += what.z;
        return this;
    }
    public substractVector(what: Vector): Vector {
        this.x -= what.x;
        this.y -= what.y;
        this.z -= what.z;
        return this;
    }
    public whatMinusMeVector(what: Vector): Vector {
        this.x = what.x - this.x;
        this.y = what.y - this.y;
        this.z = what.z - this.z;
        return this;
    }
    public mean(what: Vector): Vector {
        this.x = what.x - this.x;
        this.y = what.y - this.y;
        this.z = what.z - this.z;
        return this;
    }
    public isZero(): boolean {
        if (this.x !== 0) return false;
        if (this.y !== 0) return false;
        if (this.z !== 0) return false;
    }
    public size(): number {
        return Math.sqrt(Math.pow(this.x, 2)+Math.pow(this.y, 2)+Math.pow(this.z, 2));
    }
    public rotateVector(angle: Vector): Vector {
        return this.rotate(angle.x, angle.y, angle.z);
    }
    public rotateBackVector(angle: Vector): Vector {
        return this.rotateBack(angle.x, angle.y, angle.z);
    }
    public rotate(angleX: number, angleY: number, angleZ: number): Vector {
        var x: number;
        var y: number;
        var z: number;

        y = Math.cos(angleX)*this.y-Math.sin(angleX)*this.z;
        z = Math.sin(angleX)*this.y+Math.cos(angleX)*this.z;

        this.y = y;
        this.z = z;

        x = Math.cos(angleY)*this.x+Math.sin(angleY)*this.z;
        z = -Math.sin(angleY)*this.x+Math.cos(angleY)*this.z;

        this.x = x;
        this.z = z;

        var x = Math.cos(angleZ)*this.x-Math.sin(angleZ)*this.y;
        var y = Math.sin(angleZ)*this.x+Math.cos(angleZ)*this.y;

        this.x = x;
        this.y = y;

        return this;
    }
    public rotateBack(angleX: number, angleY: number, angleZ: number): Vector {
        var x: number;
        var y: number;
        var z: number;

        var x = Math.cos(-angleZ)*this.x-Math.sin(-angleZ)*this.y;
        var y = Math.sin(-angleZ)*this.x+Math.cos(-angleZ)*this.y;

        this.x = x;
        this.y = y;

        x = Math.cos(-angleY)*this.x+Math.sin(-angleY)*this.z;
        z = -Math.sin(-angleY)*this.x+Math.cos(-angleY)*this.z;

        this.x = x;
        this.z = z;

        y = Math.cos(-angleX)*this.y-Math.sin(-angleX)*this.z;
        z = Math.sin(-angleX)*this.y+Math.cos(-angleX)*this.z;

        this.y = y;
        this.z = z;

        return this;
    }
    public rotateDeg(angleX: number, angleY: number, angleZ: number): Vector {
        angleX = angleX/360*2*Math.PI;
        angleY = angleY/360*2*Math.PI;
        angleZ = angleZ/360*2*Math.PI;

        this.rotate(angleX, angleY, angleZ);

        return this;
    }
    public shift(x: number, y: number, z: number): Vector {
        this.x += x;
        this.y += y;
        this.z += z;
        return this;
    }

    public static equals(v1: Vector, v2: Vector): boolean {
        if (v1 === v2) return true;
        if ((v1 === null) || (v2 === null)) return false;
        if (v1.x !== v2.x) return false;
        if (v1.y !== v2.y) return false;
        if (v1.z !== v2.z) return false;
        return true;
    }
    public logStr(prefix: string): string {
        let x = Math.round(this.x*1000)/1000;
        let y = Math.round(this.y*1000)/1000;
        let z = Math.round(this.z*1000)/1000;
        return prefix+"["+x+", "+y+", "+z+"]";
    }
    public log(): void {
        console.log(this.logStr(""));
    }
    public logWStr(prefix: string): void {
        console.log(this.logStr(prefix));
    }
}