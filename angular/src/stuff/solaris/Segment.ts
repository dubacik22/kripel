import { Vector } from './Vector';

export class Segment{
    public p1: Vector;
    public p2: Vector;

    public constructor(p1: Vector, p2: Vector) {
        this.p1 = p1;
        this.p2 = p2;
    }

    public getLength(): number {
        var p = Vector.from(this.p1);
        p.substractVector(this.p2);
        return p.size();
    }

    public extend(p: Vector, len: number) {
        var xGrow = this.p2.x - this.p1.x;
        var yGrow = this.p2.y - this.p1.y;
        var myLen = this.getLength();
        var xDivAdd = xGrow / myLen;
        var yDivAdd = yGrow / myLen;
        var xAdd = len * xDivAdd;
        var yAdd = len * yDivAdd;
        if (p === this.p1) {
            this.p1.x -= xAdd;
            this.p1.y -= yAdd;
        } else {
            this.p2.x += xAdd;
            this.p2.y += yAdd;
        }
    }

    // Given three colinear points p, q, r, the function checks if 
    // point q lies on line segment 'pr' 
    public static onSegment(p: Vector, q: Vector, r: Vector): boolean {
        if (q.x <= Math.max(p.x, r.x) && q.x >= Math.min(p.x, r.x) && 
            q.y <= Math.max(p.y, r.y) && q.y >= Math.min(p.y, r.y)) 
        return true;
        return false;
    }

    // To find orientation of ordered triplet (p, q, r). 
    // The function returns following values 
    // 0 --> p, q and r are colinear 
    // 1 --> Clockwise 
    // 2 --> Counterclockwise 
    public static orientation(p: Vector, q: Vector, r: Vector): number {
        // See https://www.geeksforgeeks.org/orientation-3-ordered-points/
        // for details of below formula.
        var val = (q.y - p.y) * (r.x - q.x) -
                (q.x - p.x) * (r.y - q.y);
        if (val == 0) return 0; // colinear
        return (val > 0)? 1: 2; // clock or counterclock wise
    }

    // function that returns true if line segment 'p1q1' 
    // and 'p2q2' intersect. 
    public static doIntersect(p1: Vector, q1: Vector, p2: Vector, q2: Vector): boolean
    {
        // Find the four orientations needed for general and
        // special cases
        var o1 = Segment.orientation(p1, q1, p2);
        var o2 = Segment.orientation(p1, q1, q2);
        var o3 = Segment.orientation(p2, q2, p1);
        var o4 = Segment.orientation(p2, q2, q1);

        // General case
        if (o1 != o2 && o3 != o4) return true;

        // Special Cases
        // p1, q1 and p2 are colinear and p2 lies on segment p1q1
        if (o1 == 0 && Segment.onSegment(p1, p2, q1)) return true;
    
        // p1, q1 and q2 are colinear and q2 lies on segment p1q1
        if (o2 == 0 && Segment.onSegment(p1, q2, q1)) return true;
    
        // p2, q2 and p1 are colinear and p1 lies on segment p2q2
        if (o3 == 0 && Segment.onSegment(p2, p1, q2)) return true; 

        // p2, q2 and q1 are colinear and q1 lies on segment p2q2
        if (o4 == 0 && Segment.onSegment(p2, q1, q2)) return true;  

        return false; // Doesn't fall in any of the above cases
    }

    public log(): void {
        console.log(this.p1.logStr("")+" - "+this.p2.logStr(""))
    }
}