import { Component, ChangeDetectorRef, AfterViewInit, OnDestroy, Input, ViewChildren, QueryList, ElementRef, HostListener, ViewChild } from '@angular/core';
import { PlanetSystem } from './PlanetSystem';
import { Planet } from './Planet';
import { Vector } from './Vector';
import { SolarisMessenger } from './SolarisMessenger';
import 'hammerjs';

@Component({
  selector: 'planet-system',
  templateUrl: './planet-system.component.html',
  styleUrls: ['./planet-system.component.css'],
})
export class PlanetSystemComponent implements AfterViewInit, OnDestroy {
  private oldPointerPos: Vector;
  private oldDispUnitsDiv: number;
  private moveStarted: boolean;
  private messenger: SolarisMessenger = new SolarisMessenger();
  
  private getHolderCenter(): Vector {
    let holderDim: ClientRect | DOMRect = this.svgHolder.getBoundingClientRect();
    return new Vector(holderDim.width / 2, holderDim.height / 2, 0);
  }
  private refreshPlanetSystemsHolderCenter() {
    if (this.planetSystem === undefined) return;
    let holderCenter = this.getHolderCenter();
    if (!Vector.equals(holderCenter, this.planetSystem.dispPosCor2View)) {
      //this.planetSystem.dispPosCor2RotCenter.substractVector(this.planetSystem.dispPosCor2View).addVector(holderCenter);
      this.planetSystem.dispPosCor2View = holderCenter;
    }
  }
  
  @ViewChild('svgHolder') svgHolderRef: ElementRef;
  private get svgHolder(): HTMLElement {
    return this.svgHolderRef.nativeElement;
  }
  
  private _height: number;
  public get height(): number {
    return this._height;
  }
  @Input('height')
  public set height(value: number) {
    if (this._height == value) return;
    this._height = value * 1;
    this.detectChanges();
  }

  private _width: number;
  public get width(): number {
    return this._width;
  }
  @Input('width')
  public set width(value: number) {
    if (this._width == value) return;
    this._width = value * 1;
    this.detectChanges();
  }

  private _planetSystem: PlanetSystem;
  public get planetSystem(): PlanetSystem {
    return this._planetSystem;
  }
  @Input('planetSystem')
  public set planetSystem(value: PlanetSystem) {
    if (this._planetSystem == value) return;
    this._planetSystem = value;
    this.initPlanetSystem(this._planetSystem);
    this.detectChanges();
  }

  private _stepLength: number;
  public get stepLength(): number {
    return this._stepLength;
  }
  @Input('stepLength')
  public set stepLength(value: number) {
    this._stepLength = value;
    if (this.planetSystem == null) return;
    if (this.planetSystem.stepLength == value) return;
    this.planetSystem.stepLength = value * 1;
    this.detectChanges();
  }

  private _refreshRate: number;
  public get refreshRate(): number {
    return this._refreshRate;
  }
  @Input('refreshRate')
  public set refreshRate(value: number) {
    this._refreshRate = value;
    this.detectChanges();
    if (this.planetSystem == null) return;
    if (this.planetSystem.refreshRate == value) return;
    this.planetSystem.refreshRate = value;
  }
  private myPlanetSystemUpdate: (o: any) => void = (o: any) => {
    this.detectChanges();
  }

  private _spaceFriction: number;
  public get spaceFriction(): number {
    return this._spaceFriction;
  }
  @Input('spaceFriction')
  public set spaceFriction(value: number) {
    this._spaceFriction = value;
    this.detectChanges();
    if (this.planetSystem == null) return;
    if (this.planetSystem.spaceFriction == value) return;
    this.planetSystem.spaceFriction = value;
  }

  private _drag: boolean;
  public get drag(): boolean {
    return this._drag;
  }
  @Input('drag')
  public set drag(value: boolean) {
    this._drag = value;
    this.detectChanges();
  }

  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }

  constructor(private chDref: ChangeDetectorRef) {
  }

  public ngAfterViewInit() {
    this.chDref.detach();
    this.messenger.subscribe(SolarisMessenger.SYST_CHANGED, this.myPlanetSystemUpdate);
    this.messenger.subscribe(SolarisMessenger.PLANETS_UPDATE, this.myPlanetSystemUpdate);
  }

  public ngOnDestroy() {}

  private initPlanetSystem(planetSystem: PlanetSystem) {
    if (planetSystem === undefined) return;
    planetSystem.stepLength = this.stepLength;
    planetSystem.refreshRate = this.refreshRate;
    planetSystem.spaceFriction = this.spaceFriction;
  }

  private startMove(startPointerPos: Vector, startPinchDistance: number) {
    //startPointerPos.logWStr("start move")
    //this.oldPointerPos = startPointerPos;
    this.oldPointerPos = undefined;
    if (this.planetSystem === undefined) {
      this.oldDispUnitsDiv = 1;
      return;
    }
    this.oldDispUnitsDiv = this.planetSystem.dispUnitsDiv;
    this.moveStarted = false;

    this.refreshPlanetSystemsHolderCenter();
  }

  private moveView(move: Vector, center: Vector) {
    if (this.planetSystem === undefined) {
      return;
    }
    this.planetSystem.dispPosCor2RotCenter.logWStr("dpp")
    let newDispPosCor2RotCenter = Vector.from(this.planetSystem.dispPosCor2RotCenter).addVector(move);
    this.planetSystem.dispPosCor2RotCenter = newDispPosCor2RotCenter;
    this.messenger.notify(SolarisMessenger.SYST_CHANGED, undefined);
  }

  private rotateView(switchedRotate: Vector, center: Vector) {
    if (this.planetSystem === undefined) {
      return;
    }
    let rotation = new Vector(-switchedRotate.y, switchedRotate.x, switchedRotate.z);
    let newRotate = Vector.from(this.planetSystem.dispAngCor).addVector(rotation);
    this.planetSystem.dispAngCor = newRotate;
    this.planetSystem.dispPosCor2RotCenter.addVector(this.planetSystem.dispPosCor2View).substractVector(center);
    //this.planetSystem.dispPosCor2RotCenter.addVector(center);
    this.messenger.notify(SolarisMessenger.SYST_CHANGED, undefined);
  }

  private zoomView(ammount: number, mouseDispPos: Vector) {
    let to = this.planetSystem.dispUnitsDiv * ammount;
    this.zoomViewTo(to, mouseDispPos);
  }

  private zoomViewTo(to: number, mouseDispPos: Vector) {
    if (this.planetSystem === undefined) {
      return;
    }
    let oldPosCor2View = this.planetSystem.dispPosCor2View;
    let oldPosCor2RotCenter = this.planetSystem.dispPosCor2RotCenter;
    let oldAngCor = this.planetSystem.dispAngCor;
    let oldMousePos = Planet.getRealFromDispVector(Vector.from(mouseDispPos), oldPosCor2View, oldAngCor,
                      oldPosCor2RotCenter, this.planetSystem.dispUnitsDiv)

    this.planetSystem.dispUnitsDiv = to;

    let newDiv = this.planetSystem.dispUnitsDiv;
    let newMousePos = Planet.getRealFromDispVector(Vector.from(mouseDispPos), oldPosCor2View, oldAngCor,
                      oldPosCor2RotCenter, newDiv)

    let mouseDist = newMousePos.substractVector(oldMousePos);
    let mouseDispDist = mouseDist.divideScalar(newDiv);

    this.planetSystem.dispPosCor2RotCenter = oldPosCor2RotCenter.addVector(mouseDispDist);
    this.messenger.notify(SolarisMessenger.SYST_CHANGED, undefined);
  }

  private pauseOnMouseClickIfNotMove(): void {
    if (this.moveStarted) {
      return;
    }
    this.pause();
  }

  public pause(): void {
    if (this.planetSystem === undefined) {
      return;
    }
    if (this.planetSystem.isRun) {
      this.planetSystem.stopSystem();
    } else {
      this.planetSystem.runSystem();
    }
  }

  @HostListener('contextmenu', ['$event'])
  onRightClick(event) {
    event.preventDefault();
  }

  public mouseUp(event: MouseEvent) {
    this.pauseOnMouseClickIfNotMove();
  }

  public mouseWheel(event: WheelEvent) {
    if (this.planetSystem === undefined) {
      return;
    }
    this.startMove(new Vector(0,0,0), 1);

    let holderDim: ClientRect | DOMRect = this.svgHolder.getBoundingClientRect();
    let holderPos = new Vector(holderDim.left, holderDim.top, 0);
    let mousePos = new Vector(event.clientX, event.clientY, 0).substractVector(holderPos);
    this.zoomView((event.deltaY < 0) ? 1/1.1 : 1.1, mousePos);
  }

  public onPanStart(event) {
    let pointerPos = new Vector(event.center.x, event.center.y, 0);
    this.startMove(pointerPos, event.distance);
  }
  public onPanMove(event) {
    let pointerPos = new Vector(event.center.x, event.center.y, 0);
    if (this.oldPointerPos === undefined) {
      this.oldPointerPos = pointerPos;
      return;
    }
    let moveVect = Vector.from(pointerPos).substractVector(this.oldPointerPos);
    this.oldPointerPos = pointerPos;
    let holderDim: ClientRect | DOMRect = this.svgHolder.getBoundingClientRect();
    let holderCenter = new Vector(holderDim.width / 2, holderDim.height / 2, 0);

    if (!this.moveStarted || (moveVect.size() > 0)) {
      this.moveStarted = true;
    }
    if (!this.drag) {
      let holderDim: ClientRect | DOMRect = this.svgHolder.getBoundingClientRect();
      let holderCenter = new Vector(holderDim.width / 2, holderDim.height / 2, 0);
      moveVect.divideScalar(100);
      this.rotateView(moveVect, holderCenter);
    } else {
      moveVect.logWStr("pan")
      this.moveView(moveVect, holderCenter);
    }
  }
  public onPanEnd(event) {
    this.pauseOnMouseClickIfNotMove();
  }

  public onPinchStart(event) {
    let pointerPos = new Vector(event.center.x, event.center.y, 0);
    this.startMove(pointerPos, event.distance);
  }
  public onPinchMove(event: any) {
    let mousePos = new Vector(event.center.x, event.center.y, 0);
    if (this.oldPointerPos === undefined) {
      this.oldPointerPos = mousePos;
      return;
    }
    let moveVect = Vector.from(mousePos).substractVector(this.oldPointerPos);
    this.oldPointerPos = mousePos;
    let holderDim: ClientRect | DOMRect = this.svgHolder.getBoundingClientRect();
    let holderCenter = new Vector(holderDim.width / 2, holderDim.height / 2, 0);

    if (!this.moveStarted || (moveVect.size() > 0)) {
      this.moveStarted = true;
    }
    this.moveView(moveVect, holderCenter);
  }
  public onPinchEnd(event) {
  }
  public onPinchIn(event) {
    if (this.planetSystem === undefined) {
      return;
    }
    let mousePos = new Vector(event.center.x, event.center.y, 0);
    let to = (1/event.scale) * this.oldDispUnitsDiv;
    let holderDim: ClientRect | DOMRect = this.svgHolder.getBoundingClientRect();
    let holderPos = new Vector(holderDim.left, holderDim.top, 0);
    let mouseDispPos = mousePos.substractVector(holderPos);
    this.zoomViewTo(to, mouseDispPos);
  }
  public onPinchOut(event) {
    if (this.planetSystem === undefined) {
      return;
    }
    let mousePos = new Vector(event.center.x, event.center.y, 0);
    let to = (1/event.scale) * this.oldDispUnitsDiv;
    let holderDim: ClientRect | DOMRect = this.svgHolder.getBoundingClientRect();
    let holderPos = new Vector(holderDim.left, holderDim.top, 0);
    let mouseDispPos = mousePos.substractVector(holderPos);
    this.zoomViewTo(to, mouseDispPos);
  }
}
