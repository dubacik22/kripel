import { EventEmitter } from '@angular/core';

export interface iNotifyPropertyChanged {
  propertyChange: EventEmitter <string>;
}