import { Component, 
  Input, Output, 
  EventEmitter, ElementRef, 
  AfterViewInit, ViewChild, 
  ChangeDetectorRef} from '@angular/core';
import { style, animate, AnimationPlayer, AnimationBuilder, AnimationMetadata } from '@angular/animations';
import { MenuItem } from './MenuItem';
import { InnerMenuItem } from './InnerMenuItem';
import { Rect } from './Rect';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
})
export class MenuComponent implements AfterViewInit {
  public static readonly activeClassName: string = 'active';
  private _menus: MenuItem[] = [];
  @Input() public get menus(): MenuItem[] {
    return this._menus;
  }
  public set menus(value: MenuItem[]) {
    this._menus = value;
    this.innerMenus = [];
    if (!value) return;
    value.forEach((item: MenuItem) => {
      let newItem: InnerMenuItem = new InnerMenuItem();
      newItem.consumeItem(item);
      if (newItem.whenToDoActive) {
        newItem.whenUrlActiveArray = newItem.whenToDoActive.split(',');
        for (let i: number = 0; i < newItem.whenUrlActiveArray.length; i++) {
          newItem.whenUrlActiveArray[i] = newItem.whenUrlActiveArray[i].trim();
        }
      } else {
        newItem.whenUrlActiveArray = [newItem.whenActiveToDo];
      }
      this.innerMenus.push(newItem);
    });
    if (this._toDo) this.activateButton(this._toDo);
    this.detectChanges();
  }
  public innerMenus: InnerMenuItem[];
  @Output('userToDoClicked') userToDoClicked: EventEmitter <string> = new EventEmitter();
  @Output('toDoChange') toDoChange: EventEmitter <string> = new EventEmitter();
  private _toDo: string;
  @Input() public get toDo(): string {
    return this._toDo;
  }
  public set toDo(value: string) {
    if (value == this._toDo) return;
    this._toDo = value;
    this.activateButton(value);
    this.detectChanges();
  }
  private _compact: boolean = undefined;
  @Input()
  public set compact(value: boolean) {
    if (value === this._compact) return;
    this._compact = value;
    this.doStuffOnSetCompact();
    this.detectChanges();
  }
  public get compact(): boolean{
    return this._compact;
  }
  @Output('compacted')
  public compacted: EventEmitter <void> = new EventEmitter();
  private doStuffOnSetCompact() {
    if (this._compact) {
      this.doAnimOfCompactizingMenu();
      this.setCompactInternal();
    } else {
      this.doAnimOfNonCompactizingMenu();
      this.setNonCompactInternal();
    }
    this.compacted.emit();
    this.detectChanges();
  }
  private setCompactInternal() {
    this.showElement(this.menuButton);
    this.menuVisible = false;
    this.compactSubClass = MenuComponent.compactSubClassCompact;
    this.hideElement(this.menuUlWrapper);
    this.hideElement(this.menuBkg);
  }
  private setNonCompactInternal() {
    this.hideElement(this.menuButton);
    this.menuVisible = true;
    this.compactSubClass = MenuComponent.compactSubClassNonCompact;
    this.showElement(this.menuUlWrapper);
    this.hideElement(this.menuBkg);
  }
  public static readonly compactSubClassCompact: string = 'compact';
  public static readonly compactSubClassNonCompact: string = 'nonCompact';
  public compactSubClass: string = MenuComponent.compactSubClassNonCompact;

  @Output('onStart') onStart: EventEmitter <string> = new EventEmitter();
  @ViewChild('menuButtonWrapperStrip') menuButtonWrapperStripRef: ElementRef;
  private get menuButtonWrapperStrip(): HTMLElement{
    return this.menuButtonWrapperStripRef.nativeElement;
  }
  @ViewChild('menuCompactizeStrip') menuCompactizeStripRef: ElementRef;
  private get menuCompactizeStrip(): HTMLElement{
    return this.menuCompactizeStripRef.nativeElement;
  }
  @ViewChild('menuButton') menuButtonRef: ElementRef;
  private get menuButton(): HTMLElement{
    return this.menuButtonRef.nativeElement;
  }
  @ViewChild('menuUlWrapper') menuUlWrapperRef: ElementRef;
  private get menuUlWrapper(): HTMLElement{
    return this.menuUlWrapperRef.nativeElement;
  }
  @ViewChild('menuBkg') menuBkgRef: ElementRef;
  private get menuBkg(): HTMLElement{
    return this.menuBkgRef.nativeElement;
  }
  constructor(private animationBuilder: AnimationBuilder,
    private chDref: ChangeDetectorRef
    ) {
  }
  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  ngAfterViewInit() {
    this.chDref.detach();
  }
  private activateButton(which: string) {
    if (!which) {
      this.goDefault();
    } else {
      if (!this.innerMenus) return;
      if (this.innerMenus.length == 0) return;
      this.innerMenus.forEach((item: InnerMenuItem) => {
        if (item.whenUrlActiveArray.find((value: string) => { return value == which;})) {
          item.activeSubClass = 'active';
        } else { item.activeSubClass = ''; }
      });
    }
  }
  private goDefault() {
    let defaultItem: InnerMenuItem = this.findDefaultItem()
    if (!defaultItem) return;
    this.toDo = defaultItem.whenActiveToDo;
    this.toDoChange.emit(defaultItem.whenActiveToDo);
  }
  private findDefaultItem(): InnerMenuItem{
    let defaultMenu: InnerMenuItem = undefined;
    if (this.innerMenus.length == 0) return defaultMenu;
    defaultMenu = this.innerMenus.find((actMenu: InnerMenuItem) => {
      return actMenu.default;
    });
    return defaultMenu;
  }
  private menuVisible: boolean = true;
  public toggleMenuWrapper() {
    this.menuVisible = !this.menuVisible;
    if (this.menuVisible) {
      this.doAnimOfShowMenuWrapper(() => {
        this.showElement(this.menuUlWrapper);
      })
      this.showElement(this.menuBkg);
    } else {
      this.doAnimOfHideMenuWrapper();
      this.hideElement(this.menuBkg);
    }
  }
  private setFixedPos(fixedStyle: CSSStyleDeclaration, rect: ClientRect|Rect, minus: boolean) {
    let top: number;
    let height: number;
    let left: number;
    let width: number;
    if (minus) {
      top = (rect.top) ? (rect.top+5) : 0;
      height = (rect.height) ? (rect.height-10) : 0;
      left = (rect.left) ? (rect.left+5) : 0;
      width = (rect.width) ? (rect.width-10) : 0;
    } else {
      top = (rect.top) ? (rect.top) : 0;
      height = (rect.height) ? (rect.height) : 0;
      left = (rect.left) ? (rect.left) : 0;
      width = (rect.width) ? (rect.width) : 0;
    }
    fixedStyle.top = top + 'px';
    fixedStyle.height= height + 'px';
    fixedStyle.left = left + 'px';
    fixedStyle.width = width + 'px';
  }
  private createAnimStripMeta(rect: ClientRect|Rect, time: number, minus: boolean, opacity: number): AnimationMetadata[] {
    let top: number;
    let height: number;
    let left: number;
    let width: number;
    if (minus) {
      top = (rect.top) ? (rect.top+5) : 0;
      height = (rect.height) ? (rect.height-10) : 0;
      left = (rect.left) ? (rect.left+5) : 0;
      width = (rect.width) ? (rect.width-10) : 0;
    } else {
      top = (rect.top) ? (rect.top) : 0;
      height = (rect.height) ? (rect.height) : 0;
      left = (rect.left) ? (rect.left) : 0;
      width = (rect.width) ? (rect.width) : 0;
    }

    let ret: AnimationMetadata[]=[
      animate(time + 'ms ease', style({
        top: top + 'px',
        height: height + 'px',
        left: left + 'px',
        width: width + 'px',
        opacity: opacity
      })),
    ];
    return ret;
  }
  private stripPlayer: AnimationPlayer;
  private doAnimOfShowMenuWrapper(whatNext?: ()=>any) {
    this.showElement(this.menuUlWrapper);
    this.addClass(this.menuUlWrapper,'visibilityHidden');
    
    this.setFixedPos(this.menuButtonWrapperStrip.style, this.menuButton.getBoundingClientRect(), true);
    this.menuButtonWrapperStrip.style.opacity='0';
    this.showElement(this.menuButtonWrapperStrip);

    if (this.stripPlayer) this.stripPlayer.destroy();
    let targetRect: Rect = this.menuUlWrapper.getBoundingClientRect();
    let meta: AnimationMetadata[] = this.createAnimStripMeta(targetRect, 400, false, 1);
    const factory = this.animationBuilder.build(meta);
    this.stripPlayer = factory.create(this.menuButtonWrapperStrip);
    this.stripPlayer.play();
    this.stripPlayer.onDone(() => {
      this.hideElement(this.menuButtonWrapperStrip);
      this.removeClass(this.menuUlWrapper,'visibilityHidden');
      if (whatNext) whatNext();
    });
  }
  private doAnimOfHideMenuWrapper(whatNext?:()=>any) {
    this.setFixedPos(this.menuButtonWrapperStrip.style,this.menuUlWrapper.getBoundingClientRect(),false);
    this.hideElement(this.menuUlWrapper);
    this.menuButtonWrapperStrip.style.opacity='1';
    this.showElement(this.menuButtonWrapperStrip);

    if (this.stripPlayer) this.stripPlayer.destroy();
    
    let targetRect: Rect = this.menuButton.getBoundingClientRect();
    let meta: AnimationMetadata[] = this.createAnimStripMeta(targetRect, 400, true, 0);
    const factory = this.animationBuilder.build(meta);
    this.stripPlayer = factory.create(this.menuButtonWrapperStrip);
    this.stripPlayer.play();
    this.stripPlayer.onDone(() => {
      this.hideElement(this.menuButtonWrapperStrip);
      if (whatNext) whatNext();
    });
  }
  private doAnimOfCompactizingMenu(whatNext?: ()=>any) {
    this.setFixedPos(this.menuCompactizeStrip.style, this.menuUlWrapper.getBoundingClientRect(), false);
    this.menuCompactizeStrip.style.opacity = '1';
    this.showElement(this.menuCompactizeStrip);

    if (this.stripPlayer) this.stripPlayer.destroy();
    let targetRect: Rect = new Rect(0, 50, window.innerWidth-60, 40);
    let meta: AnimationMetadata[] = this.createAnimStripMeta(targetRect, 800, true, 0);
    const factory = this.animationBuilder.build(meta);
    this.stripPlayer = factory.create(this.menuCompactizeStrip);
    this.stripPlayer.play();
    this.stripPlayer.onDone(() => {
      this.hideElement(this.menuCompactizeStrip);
      if (whatNext) whatNext();
    });
  }
  private doAnimOfNonCompactizingMenu(whatNext?: ()=>any) {
    let initRect: Rect = new Rect(0, window.innerHeight, window.innerWidth, 0);
    this.setFixedPos(this.menuCompactizeStrip.style, initRect, false);
    this.menuCompactizeStrip.style.opacity = '0';
    this.showElement(this.menuCompactizeStrip);

    if (this.stripPlayer) this.stripPlayer.destroy();
    let targetRect: Rect = new Rect(0, window.innerHeight, window.innerWidth, 0);
    let meta: AnimationMetadata[] = this.createAnimStripMeta(targetRect, 500, true, 1);
    const factory = this.animationBuilder.build(meta);
    this.stripPlayer = factory.create(this.menuCompactizeStrip);
    this.stripPlayer.play();
    this.stripPlayer.onDone(() => {
      this.hideElement(this.menuCompactizeStrip);
      if (whatNext) whatNext();
    });
  }
  private showElement(element: HTMLElement) {
    this.removeClass(element, 'displayNoneImportant');
  }
  private hideElement(element: HTMLElement) {
    this.addClass(element, 'displayNoneImportant');
  }
  private addClass(element: HTMLElement, className: string) {
    element.classList.add(className);
  }
  private removeClass(element: HTMLElement, className: string) {
    element.classList.remove(className);
  }
  menuItemClicked(item: InnerMenuItem) {
    let toDoClicked: string = item.whenActiveToDo;
    this.userToDoClicked.emit(toDoClicked);
    this.activateButton(toDoClicked);
    this.toDoChange.emit(toDoClicked);
    if (this.compact) this.toggleMenuWrapper();
    this.detectChanges();
  }
}