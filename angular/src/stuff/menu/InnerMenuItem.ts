import { MenuItem } from './MenuItem';

export class InnerMenuItem extends MenuItem{
  activeSubClass: string = '';
  miniSubclass: string = '';
  whenUrlActiveArray: string[];

  consumeItem(item: MenuItem) {
    super.copyOf(item);
    if (item.mini) this.miniSubclass = 'mini';
  }
}