export class MenuItem{
  displayName: string;
  whenActiveToDo: string; //ak je aktivne, tak nastavime toto todo
  whenToDoActive: string; //ak je nastavene toto todo, tk sme aktivny (ak chcem viac toto, tak oddelim ciarkou)
  scope: any;
  default: boolean = false;
  mini: boolean = false;

  copyOf(other: MenuItem) {
    this.displayName = other.displayName;
    this.whenActiveToDo = other.whenActiveToDo;
    this.whenToDoActive = other.whenToDoActive;
    this.scope = other.scope;
    this.default = other.default;
  }
}