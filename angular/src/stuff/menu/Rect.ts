export class Rect {
  top: number;
  height: number;
  left: number;
  width: number;
  
  constructor(top: number, height: number, left: number, width: number) {
    this.top = top;
    this.height = height;
    this.left = left;
    this.width = width;
  }
}