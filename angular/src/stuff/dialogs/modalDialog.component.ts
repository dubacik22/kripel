import { Component, ViewChild, ElementRef, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { ButtonConf } from './res/ButtonConf';

@Component({
  selector: 'modalDialog',
  templateUrl: './modalDialog.component.html',
  styleUrls: ['./modalDialog.component.css','../styles.css']
})
export class DialogBoxComponent implements AfterViewInit {
  @ViewChild('table') table: ElementRef;
  public text: string = '';
  public visible: boolean = false;
  public buttons: ButtonConf[] = [];
  private static instance: DialogBoxComponent;

  private changeDetectionInProgress: boolean = false;
  private needChangeDetectionRunWhileInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) {
      this.needChangeDetectionRunWhileInProgress = true;
      return;
    }
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
    if (this.needChangeDetectionRunWhileInProgress) {
      this.needChangeDetectionRunWhileInProgress = false;
      this.detectChanges();
    }
  }
  constructor(private chDref: ChangeDetectorRef) {
    DialogBoxComponent.instance = this;
  }
  ngAfterViewInit() {
    this.chDref.detach();
  }
  public static setText(text: string) {
    DialogBoxComponent.instance.text = text;
  }
  public static setButtons(buttons: ButtonConf[]) {
    DialogBoxComponent.instance.buttons = buttons;
  }
  public static set(text: string, buttons: ButtonConf[]) {
    DialogBoxComponent.instance.text = text;
    DialogBoxComponent.setButtons(buttons);
  }
  public static show() {
    DialogBoxComponent.instance.show();
  }
  public static showJustInfo(info: string) {
    DialogBoxComponent.set(info ,[
      new ButtonConf('ok',(n: string) => {}),
    ]);
    DialogBoxComponent.show();
  }
  public static hide() {
    DialogBoxComponent.instance.hide();
  }

  public buttonClicked(btn: ButtonConf) {
    this.hide();
    if (btn.func != undefined) {
      btn.func(btn.content);
    }
  }
  public hide(){
    this.visible = false;
    this.detectChanges();
  }
  private show(){
    this.visible = true;
   // setTimeout(() => {this.table.nativeElement.focus()}, 1000);
    this.detectChanges();
  }
  public bkgClicked(event: MouseEvent) {
    this.hide();
  }
  public fgClicked(event: MouseEvent) {
    event.stopPropagation();
  }
}