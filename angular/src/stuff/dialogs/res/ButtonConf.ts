export class ButtonConf {
    content: string = '';
    style: any;
    func: (name: string) => any;
    constructor(name: string, func?: (name: string) => any) {
      this.content = name;
      this.func = func;
    }
  }