import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { SvgCircleModule, SvgLineModule, SvgPolygonModule, SvgPolylineModule, SvgTextModule, SvgPathModule, SvgEllipseModule } from 'angular-svg'
import { AttractToCursorDirective } from './directives/attract-to-cursor.directive';
import { ScrollableStylesDirective } from './directives/scrollable-styles.directive';
import { KindaPerspectiveDirective } from './directives/kinda-perspective.directive';
import { KindaPerspectiveComponent } from './kinda-perspective/kinda-perspective.component';
import { ScrollChngDirDirective } from './directives/scroll-chng-dir.directive';
import { FixedScrollableDirective } from './directives/fixed-scrollable.directive';
import { SlideMenuDirective } from './directives/slide-menu.directive';
import { DialogBoxComponent } from './dialogs/modalDialog.component';
import { GalleryComponent } from './gallery/gallery.component';
import { MenuComponent } from './menu/menu.component';
import { TButton } from './tbutton/tbutton.component';
import { NodeFinder } from './rich-text-box/res/NodeFinder';
import { NodeCreator } from './rich-text-box/res/NodeCreator';
import { NodeStringer } from './rich-text-box/res/NodeStringer';
import { RichTextBoxComponent } from './rich-text-box/rich-text-box.component'
import { AddCommentComponent } from './addComment/addComment.component';
import { Communicator as MessageCommunicator } from './addComment/res/Communicator';
import { ColorDotsGameComponent } from './color-dots/app-color-dots-game.component';
import { ColorDotsRowComponent } from './color-dots/res/color-dots-row/color-dots-row.component';
import { ColorDotComponent } from './color-dots/res/color-dot/color-dot.component';
import { PaletteComponent } from './color-dots/res/palette/palette.component';
import { ColorDotsRowMatchComponent } from './color-dots/res/color-dots-row-match/color-dots-row-match.component';
import { DoodleCanvasComponent } from './doodle-canvas/doodle-canvas.component';
import { Communicator as doodleCommunicator } from './doodle-canvas/res/Communicator'
import { PlanetSystemComponent } from './solaris/planet-system.component';
import { MathExt } from './MathExt';

@NgModule({
  exports: [
    SlideMenuDirective,
    FixedScrollableDirective,
    AttractToCursorDirective,
    ScrollableStylesDirective,
    KindaPerspectiveDirective,
    KindaPerspectiveComponent,
    ScrollChngDirDirective,
    DialogBoxComponent,
    GalleryComponent,
    MenuComponent,
    TButton,
    RichTextBoxComponent,
    AddCommentComponent,
    ColorDotsGameComponent,
    DoodleCanvasComponent,
    PlanetSystemComponent,

    //Session,
    //MyRouting,
  ],
  declarations: [
    SlideMenuDirective,
    FixedScrollableDirective,
    AttractToCursorDirective,
    ScrollableStylesDirective,
    KindaPerspectiveDirective,
    KindaPerspectiveComponent,
    ScrollChngDirDirective,
    DialogBoxComponent,
    GalleryComponent,
    MenuComponent,
    TButton,
    RichTextBoxComponent,
    AddCommentComponent,
    ColorDotsGameComponent,
    ColorDotsRowComponent,
    ColorDotsRowMatchComponent,
    PaletteComponent,
    ColorDotComponent,
    DoodleCanvasComponent,
    PlanetSystemComponent,
    //Session,
    //MyRouting,
  ],
  providers: [
    NodeFinder,
    NodeCreator,
    NodeStringer,
    MessageCommunicator,
    doodleCommunicator,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    SvgCircleModule, SvgLineModule, SvgPolygonModule, SvgPolylineModule, SvgTextModule, SvgPathModule, SvgEllipseModule
  ]
})
export class StuffModule { }
