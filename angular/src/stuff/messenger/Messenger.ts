export class Messenger{
    
    private listeners: Listener[] = [];

    public subscribe(message: string, update: (o: any) => void) {
        var listener = new Listener(message, update);
        this.listeners.push(listener);
    }
    public unsubscribe(update: () => void) {
        for (var i = 0; i<this.listeners.length; i++) {
            if (this.listeners[i].isMyListener(update)) {
                this.listeners.splice(i);
            }
        }
    }
    public notify(message: string, data: any) {
        for (var listener of this.listeners) {
            listener.tryNotify(message, data);
        }
    }
}

class Listener {
    private message: string;
    private listener: (o: any) => void;

    public constructor(message: string, listener: (o: any) => void) {
        this.message = message;
        this.listener = listener;
    }
    public tryNotify(message: string, data: any) {
        if (message != this.message) {
            return;
        }
        this.listener(data);
    }
    public isMyListener(listener: (o: any) => void) {
        return listener === this.listener;
    }
}