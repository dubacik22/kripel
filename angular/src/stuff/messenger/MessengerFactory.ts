import { Messenger } from './Messenger';

export class MessengerFactory {
    private static messengers: Messenger[] = [];
    private static names: string[] = [];

    public static getMessenger(name: string): Messenger {
        var index = this.names.indexOf(name);
        if (index >= 0) {
            return this.messengers[index];
        }
        var newMessenger = new Messenger();
        this.names.push(name);
        this.messengers.push(newMessenger);
        return newMessenger;
    }
}