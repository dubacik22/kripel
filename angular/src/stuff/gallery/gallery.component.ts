import { Component, Input, Output, EventEmitter, ViewChild, ElementRef, ChangeDetectorRef, HostListener, AfterViewInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AnimationMetadata, style, animate, AnimationPlayer, AnimationBuilder, AnimationFactory } from '@angular/animations';
import { ResizeSensor } from 'css-element-queries';
import { GalleryPath } from './res/GalleryPath';
import { GalleryPicture } from './res/GalleryPicture';
import { Size } from './res/Size';
import { GalleryImg } from './res/GalleryImg';

@Component({
  selector: 'gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements AfterViewInit, OnDestroy {
  public static readonly actveSubClass: string = 'active';
  public static readonly nonActveSubClass: string = 'nonActive';
  public static readonly displayNoneClassName: string = 'displayNoneImportant';
  private resizeSensor: ResizeSensor;
  @Input() public thumbBackground: string;
  private readonly enlargedImgSizeCompact: number = 1;
  private readonly enlargedImgSizeNonCompact: number = 0.85;
  private readonly enlargedImgTopCompact:string = '50%';
  private readonly enlargedImgTopNonCompact:string = '44%';
  public gThumbHeight: number = 0.1;
  public enlargedImgSize: number = this.enlargedImgSizeNonCompact;
  public enlargedImgTop: string = this.enlargedImgTopNonCompact;
  private _compact: boolean = false;
  @Input() public get compact(): boolean {
    return this._compact;
  }
  public set compact(value: boolean) {
    if (this._compact == value) return;
    this._compact = value;
    if (value) {
      this.enlargedImgSize = this.enlargedImgSizeCompact;
      this.enlargedImgTop = this.enlargedImgTopCompact;
      this.globalGThumbsIndex = this.showedIndex;
    } else {
      this.enlargedImgSize = this.enlargedImgSizeNonCompact;
      this.enlargedImgTop = this.enlargedImgTopNonCompact;
    }

    if (this.prevImg) this.setNewWinSizeOfEnlargedImg(this.prevImg);
    if (this.thisImg) this.setNewWinSizeOfEnlargedImg(this.thisImg);
    if (this.nextImg) this.setNewWinSizeOfEnlargedImg(this.nextImg);
  }
  @Input() public imgMarginPx: number;

  private _galleryScale: number = 1;
  @Input() public set galleryScale(value: number) {
    if (this._galleryScale == value) return;
    this._galleryScale = value;
    this.divide();
    this.detectChanges();
  }
  public get galleryScale(): number {
    return this._galleryScale;
  }

  @Output('galleryNameChange') galleryNameChange: EventEmitter <string> = new EventEmitter();
  private _galleryName: string = '';
  @Input() public get galleryName(): string {
    return this._galleryName;
  }
  public set galleryName(value: string) {
    if (this._galleryName == value) return;
    this._galleryName = value;
    if (this._openedImage) {
      this.afterGalleryOpenToDoStack.push(() => { this.openImgName(this._openedImage); });
    }
    this.openGallery(value);
  }

  @Output('openedImageChange') openedImageChange: EventEmitter <string> = new EventEmitter();
  private _openedImage: string = '';
  @Input() public get openedImage(): string {
    return this._openedImage;
  }
  public set openedImage(value: string) {
    if (this._openedImage == value) return;
    this._openedImage = value;
    this.openImgName(value);
  }
  @Output('pathChange') pathChange: EventEmitter <GalleryPath> = new EventEmitter();
  @ViewChild('galleryWrapper') galleryWrapperRef: ElementRef;
  private get galleryWrapper():HTMLElement{
    return this.galleryWrapperRef.nativeElement;
  }
  private _imgWrapperSubclass: string = GalleryComponent.displayNoneClassName;
  public get imgWrapperSubclass(): string {
    return this._imgWrapperSubclass;
  }
  public set imgWrapperSubclass(value: string) {
    if (value == this._imgWrapperSubclass) return;
    this._imgWrapperSubclass = value;
    this.detectChanges();
  }

  @ViewChild('imgHolder') imgHolderRef: ElementRef;
  private get imgHolder():HTMLElement{
    return this.imgHolderRef.nativeElement;
  }
  
  galleryData: GalleryPicture[] = [];

  galleryPictRows: GalleryPicture[][] = [];

  private _width: number = 0;
  public get width(): number{
    return this._width;
  }
  public set width(value: number){
    if (value == this._width) return;
    this._width = value;
    if (value != 0) this.divide();
    this.detectChanges();
  }

  public leftButtonVisible: boolean = true;
  public rightButtonVisible: boolean = true;
  private nextImg: HTMLElement;
  private thisImg: HTMLElement;
  private prevImg: HTMLElement;
  private nextPlayer: AnimationPlayer;
  private thisPlayer: AnimationPlayer;
  private prevPlayer: AnimationPlayer;
  private prevPlayerDestroyed: boolean = false;
  private nextPlayerDestroyed: boolean = false;
  private prevAnimPlay: boolean = false;
  private nextAnimPlay: boolean = false;

  private _showedIndex: number;
  private get showedIndex(): number {
    return this._showedIndex;
  }
  private set showedIndex(value: number){
    if (this._showedIndex == value) return;
    this.setShowedIndex(value);
    this.leftButtonVisible = (value != 0);
    this.rightButtonVisible = (value != this.galleryData.length-1);
  }
  private setShowedIndex(value: number) {
    this._showedIndex = value;
  }
  private _gThumbsCnt: number = 0;
  private get gThumbsCnt(): number {
    return this._gThumbsCnt;
  }
  private set gThumbsCnt(value: number) {
    value = Math.floor(value);
    if (!(value%2)) value--;
    if (value < 3) value = 0;
    if (this._gThumbsCnt == value) return;
    this.setGThumbCnt(value);
    if (this.compact) return;
    let picts: GalleryPicture[] = this.getRawGThumbs();
    this.setGThumbs(picts);
    if (this.compact) return;
    this._localGThumbsIndex = this.getLocalFromGlobalIndex(this._globalGThumbsIndex);
    this.setLocalGThumbsIndex(this._localGThumbsIndex);
    this.setGThumbsSize(this._gThumbsSize);
  }
  private setGThumbCnt(value: number) {
    this._gThumbsCnt = value;
  }
  private getLeftCnt(): number {
    let gtc:number = this.galleryData.length;
    if (gtc > this._gThumbsCnt) {
      gtc = this._gThumbsCnt;
    }
    let gtcDiv2:number = gtc/2;
    let halfThumbs: number = Math.floor(gtcDiv2);
    return halfThumbs;
  }
  private getRightCnt(): number {
    let gtc:number = this.galleryData.length;
    if (gtc>this._gThumbsCnt) {
      gtc = this._gThumbsCnt;
    }
    let gtcDiv2:number = gtc/2;
    let halfThumbs: number = Math.floor(gtcDiv2);
    let rightCnt: number = (gtcDiv2!=halfThumbs) ? halfThumbs : halfThumbs-1;
    return rightCnt;
  }
  private getRawGThumbs(): GalleryPicture[] {
    let fdl:number = this.galleryData.length;

    let leftCnt: number = this.getLeftCnt();
    let rightCnt: number = this.getRightCnt();
    let halfThumbs: number = leftCnt;

    if (this._globalGThumbsIndex<leftCnt) {
      rightCnt += leftCnt-this._globalGThumbsIndex;
      leftCnt = this._globalGThumbsIndex;
    }

    if (this._globalGThumbsIndex>fdl-halfThumbs-1) {
      leftCnt += rightCnt-(fdl-this._globalGThumbsIndex-1);
      rightCnt = fdl-this._globalGThumbsIndex-1;
    }
    let picts: GalleryPicture[] = this.getRange(this.galleryData,this._globalGThumbsIndex,leftCnt,rightCnt);
    return picts;
  }
  private _gThumbs: GalleryPicture[];
  public get gThumbs(): GalleryPicture[]{
    return this._gThumbs;
  }
  public set gThumbs(value: GalleryPicture[]){
    if (GalleryPicture.isArraysSame(this._gThumbs, value)) return;
    this.setGThumbs(value);
    if (this.compact) return;
    this.setLocalGThumbsIndex(this._localGThumbsIndex);
    this.setGThumbsSize(this._gThumbsSize);
  }
  private setGThumbs(value: GalleryPicture[]){
    this._gThumbs = value;
  }

  private _gThumbsSize: number;
  private get gThumbSize(): number {
    return this._gThumbsSize;
  }
  private set gThumbSize(value: number) {
    if (this._gThumbsSize == value) return;
    this.setGThumbsSize(value);
  }
  private setGThumbsSize(value: number) {
    if (this._gThumbs && !this.compact) {
      let nSize: Size = new Size();
      nSize.h = value;
      nSize.w = value;
      this._gThumbs.forEach((pict:GalleryPicture) => {
        if (!pict) return;

        let newSize:Size = this.setSizeToAllmostFit(nSize, pict.getOrigSize());
        pict.preview_thumb.imgSize.copyOf(newSize);
        pict.preview_thumb.holderSize.h = value;
        pict.preview_thumb.holderSize.w = value;
      });
    }
    this._gThumbsSize = value;
  }
  private _localGThumbsIndex: number;
  private get localGThumbIndex(): number {
    return this._localGThumbsIndex;
  }
  private set localGThumbIndex(value: number) {
    if (value == this._localGThumbsIndex) return;
    if (this.compact) return;
    this.setLocalGThumbsIndex(value);
  }
  private setLocalGThumbsIndex(value:number) {
    let i: number = 0;
    if (this._gThumbs) {
      this._gThumbs.forEach((pict:GalleryPicture) => {
        if (pict) pict.activeSubclass = (i == value) ? GalleryComponent.actveSubClass:GalleryComponent.nonActveSubClass;
        i++;
      });
    }
    this._localGThumbsIndex = value;
  }
  private _globalGThumbsIndex: number;
  private get globalGThumbsIndex(): number {
    return this._globalGThumbsIndex;
  }
  private set globalGThumbsIndex(value: number) {
    if (value == this._globalGThumbsIndex) return;
    this.setGlobalGThumbsIndex(value);
    if (this.compact) return;
    let picts:GalleryPicture[] = this.getRawGThumbs();
    this.setGThumbs(picts);
    this._localGThumbsIndex = this.getLocalFromGlobalIndex(this._globalGThumbsIndex);
    this.setLocalGThumbsIndex(this._localGThumbsIndex);
    this.setGThumbsSize(this._gThumbsSize);
  }
  private setGlobalGThumbsIndex(value: number) {
    this._globalGThumbsIndex = value;
  }
  private getLocalFromGlobalIndex(globalIndex: number): number {
    let ret: number;
    let rightCnt: number = this.getRightCnt();
    let leftCnt: number = this.getLeftCnt();
    if (leftCnt > globalIndex) {
      ret = globalIndex;
    } else if (globalIndex > this.galleryData.length-rightCnt-1) {
      let distanceFromEnd: number = this.galleryData.length-globalIndex-1;
      ret = rightCnt + leftCnt - distanceFromEnd;
    } else {
      ret = leftCnt;
    }
    return ret;
  }
  private updatePath() {
    let path: GalleryPath = new GalleryPath();
    if (this._galleryName) path.gallery = this._galleryName;
    else path.gallery = undefined;
    if (this._showedIndex != -1) {
      let img: GalleryPicture = this.galleryData[this._showedIndex];
      this._openedImage = img.name;
      path.img = img.name;
    } else {
      this._openedImage = undefined;
    }
    this.openedImageChange.emit(this._openedImage);
    this.pathChange.emit(path);
  }

  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private http: HttpClient, private animationBuilder: AnimationBuilder,
    private chDref: ChangeDetectorRef) {
  }
  public ngAfterViewInit() {
    this.chDref.detach();
    this.windowResized();
    this.onGalleryWrapperResized();
    this.resizeSensor = new ResizeSensor(this.galleryWrapper, () => this.onGalleryWrapperResized());
  }

  public ngOnDestroy() {
    this.resizeSensor.detach();
  }

  public keyUp(event: KeyboardEvent) {
    switch (event.key) {
    case 'ArrowLeft':
      this.showPrevImg();
      break;
    case 'ArrowRight':
      this.showNextImg();
      break;
    default:
      break; 
    }
  }

  private setNewWinSizeOfEnlargedImg(img: HTMLElement) {
    if (!img) return;
    let formerSize: Size = new Size();
    formerSize.h = parseFloat(img.style.height.slice(0,-2));
    formerSize.w = parseFloat(img.style.width.slice(0,-2));
    if (formerSize.isAnyNaN()) return;
    this.setSizeOfEnlargedImgToWin(img, formerSize);
  }
  private openingGallery: boolean = false;
  private afterGalleryOpenToDoStack: (() => void)[]=[];
  private openGallery(name: string) {
    this.openingGallery = true;
    this.galleryData = [];
    if (name) {
      let mainPath: string = './assets/imgs/gallery/'+name;
      this.http.get(mainPath+'/data.json').subscribe((data:[]) => {
        data.forEach((item) => {
          let gp: GalleryPicture = new GalleryPicture(mainPath);
          gp.loadFromJson(item);
          this.galleryData.push(gp);
        });
        if (this.width != 0) {
          this.divide();
        }
        this.openingGallery = false;
        this.afterGalleryOpenToDoStack.forEach((item: ()=>void) => {
          item();
        });
        this.afterGalleryOpenToDoStack = [];
        this.detectChanges();
      });
    } else {
      this.galleryData = [];
      this.galleryPictRows = [];
      this.detectChanges();
    }
    this.showedIndex = -1;
    this.updatePath();
  }
  private openImgName(imgName: string) {
    if (this.openingGallery && imgName) {
      this.afterGalleryOpenToDoStack.push(() => { this.openImgName(imgName); });
      return;
    }
    if (imgName) {
      let img: GalleryPicture = this.galleryData.find((img: GalleryPicture) => {
        return img.name == imgName;
      });
      if (!img) return;
      this.openImg(img);
    } else {
      this.openImg(undefined);
    }
  }
  private openImg(img: GalleryPicture) {
    if (img) {
      this.showedIndex = this.galleryData.indexOf(img);
      if (this.showedIndex < 0) return;
      this.loadImgs(this.galleryData, this.showedIndex);
      this.globalGThumbsIndex = this.showedIndex;
      this.showImgWrapper();
    } else {
      this.hideImgWrapper();
    }
    this.updatePath();
    this.detectChanges();
  }
  public openImgFromGThumb(img: GalleryPicture) {
    this.showedIndex = this.galleryData.indexOf(img);
    if (this.showedIndex < 0) return;

    if (this.prevPlayer) this.prevPlayer.destroy();
    const factory = this.animationBuilder.build(this.disappearOutMeta());
    this.prevPlayer = factory.create(this.thisImg);
    let toRem: HTMLElement = this.thisImg;
    this.prevPlayer.play();
    this.prevPlayer.onDone(() => {
      toRem.remove();
    });
    this.loadImgs(this.galleryData, this.showedIndex);
    this.globalGThumbsIndex = this.showedIndex;
    this.detectChanges();
    this.updatePath();
  }
  private divide() {
    this.galleryPictRows = [];
    let maxWidth: number = this.width;
    let rowWidth: number = 0;
    let row: GalleryPicture[] = [];
    let dimDiv: number = 2.5 / this._galleryScale;
    let numOfPictsInRow: number = 0;
    this.galleryData.forEach((pict: GalleryPicture) => {
      let actImg: GalleryImg = pict.getThumbImg();
      let actImgW: number = Math.floor(actImg.size.w/dimDiv);
      
      let rowWidthPlus1: number = rowWidth+actImgW;
      let tryAddToRow: boolean = false;
      switch (numOfPictsInRow+1) {
        case 1:
          if ((rowWidthPlus1/maxWidth) < 1.6) tryAddToRow = true;
          break;
        case 2:
          if ((rowWidthPlus1/maxWidth) < 1.2) tryAddToRow = true;
          break;
        default:
          if (rowWidthPlus1 <= maxWidth) tryAddToRow = true;
          break;
      }

      if (tryAddToRow) {
        row.push(pict);
        rowWidth += actImgW;
        numOfPictsInRow++;
      } else {
        this.galleryPictRows.push(row);
        row = [];
        rowWidth = actImgW;
        row.push(pict);
        numOfPictsInRow = 0;
      }
    });
    if (row.length > 0) this.galleryPictRows.push(row);
    for(let i: number = 0; i < this.galleryPictRows.length; i++){
      row = this.galleryPictRows[i];
      rowWidth = 0;
      row.forEach((pict: GalleryPicture) => {
        rowWidth += pict.getThumbImg().size.w/dimDiv;
      });
      let mult: number = (maxWidth-2)/rowWidth;
      if ((i >= this.galleryPictRows.length-1) && (mult > 1.5)) mult = 1;
      mult = mult/dimDiv;
      row.forEach((pict: GalleryPicture) => {
        let img: GalleryImg = pict.getThumbImg();
        img.holderSize.h = img.size.h*mult;
        img.holderSize.w = img.size.w*mult;
        img.imgSize.h = Math.floor(img.holderSize.h-(2*this.imgMarginPx));
        img.imgSize.w = Math.floor(img.holderSize.w-(2*this.imgMarginPx));
      });
    }
  }
  private getRange(from: GalleryPicture[], where: number, leftSize: number, rightSize: number): GalleryPicture[] {
    let ret: GalleryPicture[] = [];
    
    let maxHIndex: number = where+rightSize;
    let minHIndex: number = where-leftSize;
    let maxIndex: number = Math.min(from.length-1, maxHIndex);
    let minIndex: number = Math.max(0,minHIndex);
    if (minHIndex != minIndex) {
      for(let i: number = 0; i < -minHIndex; i++) {
        ret.push(undefined);
      }
    }
    for(let i: number = minIndex; i <= maxIndex; i++) {
      ret.push(from[i]);
    }
    if (maxHIndex != maxIndex) {
      for(let i: number = maxIndex; i < maxHIndex; i++) {
        ret.push(undefined);
      }
    }
    return ret;
  }
  private _loadedPicts: GalleryPicture[];
  private loadImgs(from: GalleryPicture[], activeIndex: number) {
    let picts: GalleryPicture[] = this.getRange(from, activeIndex, 1, 1);
    if (GalleryPicture.isArraysSame(picts, this._loadedPicts)) return;
    this._loadedPicts = picts;
    let ih: number = window.innerHeight;
    if (this.thisImg) this.thisImg.remove();
    this.thisImg = this.createImg(this._loadedPicts[1].getForHeight(ih).path,this._loadedPicts[1].getOrigSize());
    this.thisImg.style.transform = 'translate(-50%,-50%)';

    if (this._loadedPicts[0]) this.prevImg = this.createImg(this._loadedPicts[0].getForHeight(ih).path, this._loadedPicts[0].getOrigSize());
    else this.prevImg = undefined;
    if (this._loadedPicts[2]) this.nextImg = this.createImg(this._loadedPicts[2].getForHeight(ih).path, this._loadedPicts[2].getOrigSize());
    else this.nextImg = undefined;

    this.imgHolder.appendChild(this.thisImg);
    if (this.thisPlayer) this.thisPlayer.destroy();
    const factory = this.animationBuilder.build(this.appearInMeta());
    this.thisPlayer = factory.create(this.thisImg);
    this.thisPlayer.onDone(() => {this.focus(this.thisImg);});
    this.thisPlayer.play();
  }
  private focus(component: HTMLElement) {
    component.focus();
  }
  private setSizeToCompletelyFit(maxSize: Size, origSize: Size){
    let ih: number = origSize.h;
    let iw: number = origSize.w;
    let imgAspect=iw/ih;
    let wAspect=maxSize.w/maxSize.h;
    if (wAspect>imgAspect) {
      ih = maxSize.h;
      iw = ih*imgAspect;
    } else {
      iw = maxSize.w;
      ih = iw/imgAspect;
    }
    let ret: Size = new Size();
    ret.h=ih;
    ret.w=iw;
    return ret;
  }
  private setSizeToAllmostFit(maxSize: Size, origSize: Size){
    let ih: number = origSize.h;
    let iw: number = origSize.w;
    let imgAspect=iw/ih;
    let wAspect=maxSize.w/maxSize.h;
    if (wAspect<imgAspect) {
      ih = maxSize.h;
      iw = ih*imgAspect;
    } else {
      iw = maxSize.w;
      ih = iw/imgAspect;
    }
    let ret: Size = new Size();
    ret.h=ih;
    ret.w=iw;
    return ret;
  }
  private setSizeOfEnlargedImgToWin(img: HTMLElement, origSize: Size) {
    let maxSize: Size = new Size();
    maxSize.w = window.innerWidth;
    maxSize.h = window.innerHeight;
    maxSize.h = maxSize.h*this.enlargedImgSize;
    let newSize: Size = this.setSizeToCompletelyFit(maxSize, origSize);
    
    img.style.height = newSize.h+'px';
    img.style.width = newSize.w+'px';

    img.style.transform='translate(-50%,-50%)';
    img.style.left = '50%';
    img.style.top = this.enlargedImgTop;
  }

  private beforeNextAnim() {
    let ih: number = window.innerHeight;
    let nextIndex: number = this.showedIndex+1;
    this.prevImg = this.thisImg;
    this.thisImg = this.nextImg;
    if (nextIndex < this.galleryData.length) {
      let nextPct: GalleryPicture = this.galleryData[nextIndex];
      this.nextImg = this.createImg(nextPct.getForHeight(ih).path, nextPct.getOrigSize());
    } else {
      this.nextImg = undefined;
    }
    this.imgHolder.appendChild(this.thisImg);
  }
  private beforeNextNonAnim() {
    this.prevImg['src'] = this.thisImg['src'];
    this.prevImg.onload = () => {
      this.setSizeOfEnlargedImgToWin(this.prevImg, this.galleryData[this.showedIndex-1].getOrigSize());
      this.prevImg.onload = undefined;
    }
    this.thisImg['src'] = this.nextImg['src'];
    this.thisImg.onload = () => {
      this.setSizeOfEnlargedImgToWin(this.thisImg, this.galleryData[this.showedIndex].getOrigSize());
      this.thisImg.onload = undefined;
    }

    let ih: number = window.innerHeight;
    let nextIndex: number = this.showedIndex+1;
    if (nextIndex < this.galleryData.length) {
      let nextPct: GalleryPicture = this.galleryData[nextIndex];
      this.nextImg = this.createImg(nextPct.getForHeight(ih).path, nextPct.getOrigSize());
    } else {
      this.nextImg = undefined;
    }
  }
  private beforePrevAnim() {
    let ih: number = window.innerHeight;
    let prevIndex: number = this.showedIndex-1;
    this.nextImg = this.thisImg;
    this.thisImg = this.prevImg;
    if (prevIndex >= 0) {
      let prevPct: GalleryPicture = this.galleryData[prevIndex];
      this.prevImg = this.createImg(prevPct.getForHeight(ih).path, prevPct.getOrigSize());
    } else {
      this.prevImg = undefined;
    }
    this.imgHolder.appendChild(this.thisImg);
  }
  private beforePrevNonAnim() {
    this.nextImg['src'] = this.thisImg['src'];
    this.nextImg.onload = () => {
      this.setSizeOfEnlargedImgToWin(this.nextImg, this.galleryData[this.showedIndex+1].getOrigSize());
      this.nextImg.onload = undefined;
    }
    this.thisImg['src'] = this.prevImg['src'];
    this.thisImg.onload = () => {
      this.setSizeOfEnlargedImgToWin(this.thisImg, this.galleryData[this.showedIndex].getOrigSize());
      this.thisImg.onload = undefined;
    }
    let ih: number = window.innerHeight;
    let prevIndex: number = this.showedIndex-1;
    if (prevIndex >= 0) {
      let prevPct: GalleryPicture = this.galleryData[prevIndex];
      this.prevImg = this.createImg(prevPct.getForHeight(ih).path, prevPct.getOrigSize());
    } else {
      this.prevImg = undefined;
    }
  }
  private nextPlayerCreateNPlay(factory: AnimationFactory, animatedElement: HTMLElement) {
    this.nextPlayer = factory.create(animatedElement);
    this.nextPlayer.onDone(() => {this.focus(animatedElement);});
    this.nextPlayer.play();
    this.nextAnimPlay = true;
    this.nextPlayerDestroyed = false;
  }
  private prevPlayerCreateNPlay(factory: AnimationFactory, animatedElement: HTMLElement){
    this.prevPlayer = factory.create(animatedElement);
    this.prevPlayer.play();
    this.prevAnimPlay = true;
    this.prevPlayerDestroyed = false;
  }
  private nextPlayerDestroy() {
    this.nextPlayer.destroy();
    this.nextPlayerDestroyed = true;
  }
  private prevPlayerDestroy() {
    this.prevPlayer.destroy();
    this.prevPlayerDestroyed = true;
  }
  private animateInImg(meta: AnimationMetadata[]) {
    const inFactory: AnimationFactory = this.animationBuilder.build(meta);
    this.nextPlayerCreateNPlay(inFactory, this.thisImg);
    this.nextPlayer.onDone(() => {
      this.nextAnimPlay = false;
      //this.nextPlayerDestroy();
      this.thisImg.style.transform = 'translate(-50%,-50%)';
    });
  }
  private animateOutImg(which: HTMLElement, meta: AnimationMetadata[]) {
    const outFactory: AnimationFactory = this.animationBuilder.build(meta);
    this.prevPlayerCreateNPlay(outFactory, which);
    this.prevPlayer.onDone(() => {
      //this.prevPlayerDestroy();
      this.prevAnimPlay = false;
      which.remove();
      which.style.transform = '';
    });
  }

  public showNextImg() {
    if (!this.nextImg) return;
    this.showedIndex++;

    let picts: GalleryPicture[] = this.getRange(this.galleryData, this.showedIndex, 1, 1);
    if (GalleryPicture.isArraysSame(picts, this._loadedPicts)) return;
    this._loadedPicts = picts;

    this.globalGThumbsIndex = this.showedIndex;
    if (this.prevAnimPlay || this.nextAnimPlay) {
      this.beforeNextNonAnim();
    } else {
      this.beforeNextAnim();
      if (this.prevPlayer && !this.prevPlayerDestroyed) this.prevPlayerDestroy();
      if (this.nextPlayer && !this.nextPlayerDestroyed) this.nextPlayerDestroy();
      this.animateInImg(this.slideInRightMeta());
      //this.prevImg.remove()
      this.animateOutImg(this.prevImg, this.slideOutLeftMeta());
    }
    this.detectChanges();
    this.updatePath();
  }
  public showPrevImg() {
    if (!this.prevImg) return;
    this.showedIndex--;

    let picts: GalleryPicture[] = this.getRange(this.galleryData, this.showedIndex, 1, 1);
    if (GalleryPicture.isArraysSame(picts, this._loadedPicts)) return;
    this._loadedPicts = picts;

    this.globalGThumbsIndex = this.showedIndex;
    if (this.prevAnimPlay || this.nextAnimPlay) {
      this.beforePrevNonAnim();
    } else {
      this.beforePrevAnim();
      if (this.prevPlayer && !this.prevPlayerDestroyed) this.prevPlayerDestroy();
      if (this.nextPlayer && !this.nextPlayerDestroyed) this.nextPlayerDestroy();

      this.animateInImg(this.slideInLeftMeta());
      //this.nextImg.remove()
      this.animateOutImg(this.nextImg, this.slideOutRightMeta())
    }
    this.detectChanges();
    this.updatePath();
  }
  
  private createImg(src:string, origWH: Size): HTMLElement {
    let ret: HTMLElement;
    ret = document.createElement('IMG');
    ret['src'] = src;
    ret.onload = () => {
      this.setSizeOfEnlargedImgToWin(ret, origWH);
      ret.onload = undefined;
    };
    ret.style.position = 'absolute';
    ret.tabIndex = 0;
    ret.classList.add('outlineNone');
    //ret.onkeyup = (event: KeyboardEvent) => {
    //  this.keyUp(event);
    //};
    return ret;
  }
  public hideImgWrapper() {
    this.showedIndex = -1;
    this._loadedPicts = [];
    this.updatePath();
    //this.removePath();
    if (this.thisImg) this.thisImg.remove();
    this.imgWrapperSubclass = GalleryComponent.displayNoneClassName;
  }
  private showImgWrapper() {
    this.imgWrapperSubclass = '';
  }
  @HostListener("window:resize", [])
  public windowResized() {
    this.gThumbSize = window.innerHeight*this.gThumbHeight;

    let nCnt: number = window.innerWidth/(this.gThumbSize+20);
    this.gThumbsCnt = nCnt-1;

    if (this.prevImg) this.setNewWinSizeOfEnlargedImg(this.prevImg);
    if (this.thisImg) this.setNewWinSizeOfEnlargedImg(this.thisImg);
    if (this.nextImg) this.setNewWinSizeOfEnlargedImg(this.nextImg);
  }
  private onGalleryWrapperResized() {
    this.width = this.galleryWrapper.clientWidth;
  }

  private slideInRightMeta(): AnimationMetadata[] {
    let transformBegin: string = 'translate(50%,-50%)';
    let transformFinal: string = 'translate(-50%,-50%)';
    let ret = [
      style({ transform: transformBegin, opacity: 0 }),
      animate('500ms ease', style({ transform: transformFinal, opacity: 1 })),
    ];
    return ret;
  }
  private slideInLeftMeta(): AnimationMetadata[] {
    let transformBegin: string = 'translate(-150%,-50%)';
    let transformFinal: string = 'translate(-50%,-50%)';
    let ret = [
      style({ transform: transformBegin, opacity: 0 }),
      animate('500ms ease', style({ transform: transformFinal, opacity: 1 })),
    ];
    return ret;
  }
  private slideOutRightMeta(): AnimationMetadata[] {
    let transformBegin: string = 'translate(-50%,-50%)';
    let transformFinal: string = 'translate(50%,-50%)';
    let ret = [
      style({ transform: transformBegin, opacity: 1 }),
      animate('500ms ease', style({ transform: transformFinal, opacity: 0 })),
    ];
    return ret;
  }
  private slideOutLeftMeta():AnimationMetadata[] {
    let transformBegin: string = 'translate(-50%,-50%)';
    let transformFinal: string = 'translate(-150%,-50%)';
    let ret = [
      style({ transform: transformBegin, opacity: 1 }),
      animate('500ms ease', style({ transform: transformFinal, opacity: 0 })),
    ];
    return ret;
  }
  private appearInMeta(): AnimationMetadata[] {
    let ret = [
      style({ opacity: 0 }),
      animate('600ms ease', style({ opacity: 1 }))
    ];
    return ret;
  }
  private disappearOutMeta(): AnimationMetadata[] {
    let ret = [
      style({ opacity: 1 }),
      animate('600ms ease', style({ opacity: 0 }))
    ];
    return ret;
  }
}