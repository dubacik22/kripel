export class GalleryPath {
    gallery: string;
    img: string;
    public set(gallery: string, img: string) {
      this.gallery = gallery;
      this.img = img;
    }
    public static isSame(one: GalleryPath, twoo: GalleryPath): boolean {
      if (!one && !twoo) return true;
      if (!one || !twoo) return false;
      if (one.gallery != twoo.gallery) return false;
      if (one.img != twoo.img) return false;
      return true;
    }
  }