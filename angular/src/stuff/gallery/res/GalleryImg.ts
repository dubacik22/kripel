import { Size } from './Size';

export class GalleryImg {
    mainPath: string;
    path: string = '';
    size: Size = new Size();
    holderSize: Size = new Size();
    imgSize: Size = new Size();
    borderSize: number = 0;
  
    constructor(path: string) {
      if (path) this.mainPath = path;
      else this.mainPath = '';
    }
    public loadFromJson(jsonObj) {
      this.path=jsonObj.path;
      
      console.log(this.path);
      this.size.w=jsonObj.width;
      this.size.h=jsonObj.height;
    }
    public isSame(other: GalleryImg): boolean {
      if (!other) return false;
      if (other === this) return true;
      if (this.path != other.path) return false;
      if (this.size.isSame(other.size)) return false;
      if (this.holderSize.isSame(other.holderSize)) return false;
      if (this.imgSize.isSame(other.imgSize)) return false;
      if (this.borderSize != other.borderSize) return false;
      return true;
    }
  }