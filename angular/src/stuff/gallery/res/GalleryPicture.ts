import { GalleryImg } from "./GalleryImg";
import { Size } from './Size';

export class GalleryPicture {
    name: string;
    date: string;
    mainPath: string;
    dominantColor: string;
    raw: GalleryImg;
    preview_s: GalleryImg;
    preview_xxs: GalleryImg;
    preview_thumb: GalleryImg;
  
    activeSubclass: string = '';
  
    constructor(path: string) {
      this.mainPath = path + '/';
    }
    public loadFromJson(jsonObj) {
      this.name = jsonObj.name;
      
      console.log(this.name);
      this.preview_s = new GalleryImg(this.mainPath);
      this.preview_s.loadFromJson(jsonObj.preview_s);
  
      this.raw = new GalleryImg(this.mainPath);
      this.raw.loadFromJson(jsonObj.raw);
  
      this.preview_xxs = new GalleryImg(this.mainPath);
      this.preview_xxs.loadFromJson(jsonObj.preview_xxs);
  
      this.preview_thumb = new GalleryImg(this.mainPath);
      this.preview_thumb.loadFromJson(jsonObj.preview_xxs);
    }
    public getThumbImg(): GalleryImg {
      return this.preview_xxs;
    }
    public getOrigSize(): Size {
      let ret: Size = new Size();
      ret.w = this.raw.size.w;
      ret.h = this.raw.size.h;
      return ret;
    }
    public getForHeight(height: number): GalleryImg {
      return this.preview_s;
    }
    public isSame(other: GalleryPicture): boolean {
      if (!other) return false;
      if (other === this) return true;
      if (this.name != other.name) return false;
      if (this.date != other.date) return false;
      if (this.dominantColor!=other.dominantColor) return false;
  
      if (!this.preview_s.isSame(other.preview_s)) return false;
      if (!this.preview_xxs.isSame(other.preview_xxs)) return false;
      if (!this.preview_thumb.isSame(other.preview_thumb)) return false;
      return true;
    }
    public static isArraysSame(ar1: GalleryPicture[], ar2: GalleryPicture[]): boolean {
      if (!ar1 && !ar2) return true;
      if (!ar1 || !ar2) return false;
      let l1: number = ar1.length;
      let l2: number = ar2.length;
      if (l1 != l2) return false;
      for (let i: number = 0; i < l1; i++) {
        let iar1: GalleryPicture = ar1[i];
        let iar2: GalleryPicture = ar2[i];
        if (iar1 || iar2) {
          if (!iar1 || !iar2) return false;
          if (!iar1.isSame(iar2)) return false;
        }
      }
      return true;
    }
  }