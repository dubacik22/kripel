export class Size {
    public w: number=0;
    public h: number=0;
    public isAnyNaN() {
      return (isNaN(this.w) || isNaN(this.h));
    }
    public copyOf(other: Size) {
      this.w = other.w;
      this.h = other.h;
    }
    public isSame(other: Size): boolean {
      if (!other) return false;
      if (other === this) return true;
      if (this.w != other.w) return false;
      if (this.h != other.h) return false;
      return true;
    }
  }