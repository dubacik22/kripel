import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColorDotsGameComponent } from './app-color-dots-game.component';

describe('AppColorDotsGameComponent', () => {
  let component: ColorDotsGameComponent;
  let fixture: ComponentFixture<ColorDotsGameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColorDotsGameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColorDotsGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
