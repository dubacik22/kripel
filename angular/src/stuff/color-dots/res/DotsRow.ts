import { Dot } from './Dot';
import { Palette } from './Pallette';
import { iNotifyPropertyChanged } from 'src/stuff/iNotifyProperyChanged';
import { EventEmitter } from '@angular/core';

export class DotsRow implements iNotifyPropertyChanged {
    public propertyChange: EventEmitter <string> = new EventEmitter();
    private _dots: Dot[] = [];
    public get dots(): Dot[] {
        return this._dots;
    }
    private _found: Dot[] = [];
    public get found(): Dot[] {
        return this._found;
    }
    private innerSetFound(value: Dot[]){
        this._found = value;
        this.propertyChange.emit('found');
    }
    public get dotsCnt():number {
        return this._dotsCnt;
    }
    public get colorsCnt():number {
        return this._colorsCnt;
    }
    constructor(private _dotsCnt: number, private _colorsCnt: number) {
        for (let i: number = 0; i < _dotsCnt; i++) {
            this._dots.push(new Dot(undefined));
        }
    }
    private haveDotInDots(dot: FoundDot, where: FoundDot[]): boolean {
        if (where.find((value: FoundDot) => {
            return ((dot.color == value.color) && (dot.position == value.position));
        })) return true;
        else return false;
    }
    private haveColorInDots(dot: FoundDot, where: FoundDot[]): boolean {
        if (where.find((value: FoundDot) => {
            return (dot.color == value.color);
        })) return true;
        else return false;
    }
    private writeDots(dts: FoundDot[]) {
        let ret: string = '';
        dts.forEach((dt:FoundDot) => {
            ret+=dt.color+'('+dt.position+')'
        });
    }
    public check(mainDots: DotsRow) {
        let foundExact: FoundDot[] = [];
        for (let i: number = 0; i < mainDots._dots.length; i++) {
            let mainDot: Dot = mainDots._dots[i];
            let userDot: Dot = this._dots[i];
            if (mainDot.color == userDot.color) {
                foundExact.push(FoundDot.get(userDot, i));
            }
        }
        let foundAnywhere: FoundDot[] = [];
        for (let color: number = 0; color < this._colorsCnt; color++) {
            let userDots: FoundDot[] = [];
            this._dots.forEach((userDot: Dot, userIndex: number) => {
                let userDotFoundDot: FoundDot = FoundDot.get(userDot, userIndex);
                if ((color == userDot.color) && !this.haveDotInDots(userDotFoundDot, foundExact))
                    userDots.push(userDotFoundDot);
            });
            if (userDots.length <= 0) continue;
            this.writeDots(userDots)
            let mainDotsExactColor: FoundDot[] = [];
            mainDots._dots.forEach((mainDot: Dot, mainIndex: number) => {
                let mainDotFoundDot: FoundDot = FoundDot.get(mainDot, mainIndex);
                if ((color == mainDot.color) && !this.haveDotInDots(mainDotFoundDot, foundExact))
                    mainDotsExactColor.push(mainDotFoundDot);
            });
            this.writeDots(mainDotsExactColor)
            let addInThisColor: number = Math.min(mainDotsExactColor.length, userDots.length);
            for (let i: number = 0; i < addInThisColor; i++) {
                foundAnywhere.push(undefined);
            }
        }
        
        let found: Dot[] = [];
        foundExact.forEach((haveFound: FoundDot) => {
            found.push(new Dot(Palette.getFoudPlaceAndColor()));
        });
        foundAnywhere.forEach((haveFound: FoundDot) => {
            found.push(new Dot(Palette.getFoundOnlyColor()));
        });
        for (let i: number = found.length; i < mainDots.dots.length; i++) {
            found.push(new Dot(undefined));
        }
        this.innerSetFound(found);
    }
    public setRandomColors() {
        let oldColors: Palette[] = [];
        this.dots.forEach((dot: Dot) => {
            let newColor: Palette;
            let oldColor: Palette;
            let tryIteration: number = 0;
            do {
                newColor = Palette.randomPaletteColor(this.colorsCnt);
                oldColor = oldColors.find((color: Palette) => {
                    return Palette.isSame(color, newColor);
                });
                tryIteration++;

            } while (oldColor && (tryIteration < 15));
            oldColors.push(newColor);
            dot.forceColor(newColor);
        });
    }
}
class FoundDot {
    public color: number;
    public position: number;
    
    public static get(dot: Dot, position: number): FoundDot {
        let ret: FoundDot = new FoundDot();
        ret.color = dot.color;
        ret.position = position;
        return ret;
    }
}