import { Component, Input, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { Dot } from '../Dot';
import { Subscription } from 'rxjs';
import { iNotifyPropertyChanged } from 'src/stuff/iNotifyProperyChanged';
import { ButtonConf } from 'src/stuff/dialogs/res/ButtonConf';
import { DialogBoxComponent } from 'src/stuff/dialogs/modalDialog.component';

@Component({
  selector: 'color-dot',
  templateUrl: './color-dot.component.html',
  styleUrls: ['./color-dot.component.css']
})
export class ColorDotComponent implements AfterViewInit {
  private _buttonColors: string[] = [
    'green',
    'yellow',
    'purple',
    'brown',
    'blue',
    'grey',
    'pink',
    'magenta',
    'lime',
    'orange',
    'olive',
    'sienna',
    'PowderBlue',
    'teal',
    'tomato',
    'oldlace',
  ];
  private matchColors: string[] = [
    'red',
    'white'
  ];
  @Input()
  public matchResult: boolean = false;
  private buttonColors: string[] = [];
  private _backgroundColor: string;
  public get backgroundColor() {
    return this._backgroundColor;
  }
  public set backgroundColor(value: string) {
    if (this._backgroundColor == value) return;
    this._backgroundColor = value;
    this.detectChanges();
  }
  private _dotSizeInPercent: number = 12;
  @Input()
  public get dotSizeInPercent(): number {
    return this._dotSizeInPercent;
  }
  public set dotSizeInPercent(value: number) {
    if (this._dotSizeInPercent == value) return;
    this._dotSizeInPercent = value;
    this.detectChanges();
  }

  private contextSubscribtion: Subscription
  private _context: Dot;
  @Input()
  public get context(): Dot {
    return this._context;
  }
  public set context(value: Dot) {
    if (Dot.isSame(this._context, value)) return;
    this.unlistenTo(this.contextSubscribtion);
    this._context = value;
    this.listenTo(this._context, () => {
      this.setColor(this._context.color);
    });
    this.setColor(this._context.color);
  }
  private setColor(index: number) {
    this.backgroundColor = (this.matchResult) ? this.matchColors[index] : this.buttonColors[index];
  }
  @Input()
  public colorsCnt: number;

  private _editableColor: boolean;
  @Input()
  public get editableColor(): boolean {
    return this._editableColor;
  }
  public set editableColor(value: boolean) {
    if (this._editableColor == value) return;
    this._editableColor = value;
  }

  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private chDref: ChangeDetectorRef) {
    this._buttonColors.forEach((color: string) => {
      this.buttonColors.push('radial-gradient('+color+',black)');
    });
  }

  public ngAfterViewInit() {
    this.chDref.detach();
  }
  setColorIndex(colorNum: number) {
    this._context.color = colorNum;
  }
  clicked() {
    if (!this._editableColor) return;
    let buttons: ButtonConf[] = [];
    for (let i: number = 0; i < this.colorsCnt; i++) {
      let button: ButtonConf;
      button = new ButtonConf('');
      button.content = '';
      button.style = {
        'height': '40px',
        'width': '40px',
        'padding': '0',
        'margin-left': '10px',
        'margin-right': '10px',
        'background-color': this._buttonColors[i]
      };
      button.func = (n: string) => {this.setColorIndex(i);}//+Palette.paletteOffset);}
      buttons.push(button);
    }
    DialogBoxComponent.set('aka farba?', buttons);
    DialogBoxComponent.show();
  }
  unlistenTo(subscribtion: Subscription) {
    if (subscribtion) subscribtion.unsubscribe();
  }
  listenTo(to: iNotifyPropertyChanged, toDo: (propName: string) => void): Subscription {
      if (to) return to.propertyChange.subscribe(toDo);
      return undefined;
  }
}
