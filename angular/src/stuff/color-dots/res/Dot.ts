import { Palette } from './Pallette';
import { iNotifyPropertyChanged } from 'src/stuff/iNotifyProperyChanged';
import { EventEmitter } from '@angular/core';

export class Dot implements iNotifyPropertyChanged {
    public propertyChange: EventEmitter <string> = new EventEmitter();
    private _color: number;
    public get color(): number {
        return this._color;
    }
    public set color(value: number) {
        if (this._color == value) return;
        this._color = value;
        this.propertyChange.emit('color');
    }

    constructor(colorPallette: Palette) {
        this.forceColor(colorPallette);
    }
    public forceColor(colorPallette: Palette) {
        if (!colorPallette) return;
        this._color = colorPallette.actColor;
        this.propertyChange.emit('color');
    }

    public static isSame(one: Dot, twoo: Dot): boolean {
        if (!one && !twoo) return true;
        if (!one || !twoo) return false;
        return one.isSame(twoo);
    }
    public isSame(other: Dot): boolean {
        if (this.color != other.color) return false;
        return true;
    }
}