export class Palette {
    //public static readonly paletteOffset: number = 2;
    private _actColor: number;
    public get actColor(): number {
        return this._actColor;
    }
    public static getColor(numberOfColor: number): Palette {
        let ret: Palette = new Palette();
        ret._actColor = numberOfColor;
        return ret;
    }
    public static getFoudPlaceAndColor(): Palette {
        let ret: Palette = new Palette();
        ret._actColor = 0;
        return ret;
    }
    public static getFoundOnlyColor(): Palette {
        let ret: Palette = new Palette();
        ret._actColor = 1;
        return ret;
    }
    public static randomPaletteColor(colorsCnt: number): Palette {
        let randNum: number = Math.random();
        let ret: Palette = new Palette();
        ret._actColor = Math.round(randNum*(colorsCnt-1));//+this.paletteOffset;
        return ret;
    }
    public static isSame(one: Palette, twoo: Palette): boolean {
        if (!one && !twoo) return true;
        if (!one || !twoo) return false;
        return one.isSame(twoo);
    }
    public isSame(other: Palette): boolean {
        if (this.actColor != other.actColor) return false;
        return true;
    }
}