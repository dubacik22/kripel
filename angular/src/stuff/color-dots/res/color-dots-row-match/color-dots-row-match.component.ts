import { Component, Input, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { DotsRow } from '../DotsRow';
import { Subscription } from 'rxjs';
import { iNotifyPropertyChanged } from 'src/stuff/iNotifyProperyChanged';
import { Dot } from '../Dot';

@Component({
  selector: 'color-dots-row-match',
  templateUrl: './color-dots-row-match.component.html',
  styleUrls: ['./color-dots-row-match.component.css']
})
export class ColorDotsRowMatchComponent implements AfterViewInit {
  private _dotsCnt: number;
  @Input()
  public get dotsCnt(): number {
    return this._dotsCnt;
  }
  public set dotsCnt(value: number) {
    this._dotsCnt = value;
    this.detectChanges();
  }

  public dots1: Dot[] = [];
  public dots2: Dot[] = [];

  private contextSubscribtion: Subscription
  private _context: DotsRow;
  @Input()
  public get context(): DotsRow {
    return this._context;
  }
  public set context(value: DotsRow) {
    //if (DotsRow.isSame(this._context, value)) return;
    this.unlistenTo(this.contextSubscribtion);
    this._context = value;
    this.contextSubscribtion = this.listenTo(this._context, () => {
      this.resetDotSizeInPercent();
      this.refreshResultsDots();
    });
    this.resetDotSizeInPercent();
    this.refreshResultsDots();
    this.detectChanges();
  }
  public _dotSizeInPercent: number;
  public get dotSizeInPercent(): number {
    return this._dotSizeInPercent;
  }
  public set dotSizeInPercent(value: number) {
    if (this._dotSizeInPercent == value) return;
    this._dotSizeInPercent = value;
    this.detectChanges();
  }
  private resetDotSizeInPercent() {
    if (!this._context) return;
    this.dotSizeInPercent = 100 / Math.ceil(this._context.dotsCnt/2) - 10;
  }
  private refreshResultsDots() {
    if (!this._context) return;
    let resCnt: number = this._context.found.length;
    let half: number = Math.ceil(resCnt / 2);
    if ((half == 1) && (resCnt <= 3)) half = 0;
    this.dots2 = this._context.found.slice(0, half);
    this.dots1 = this._context.found.slice(half, resCnt);
  }
  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private chDref: ChangeDetectorRef) { }

  ngAfterViewInit() {
    this.chDref.detach();
  }

  private unlistenTo(subscribtion: Subscription) {
    if (subscribtion) subscribtion.unsubscribe();
  }
  private listenTo(to: iNotifyPropertyChanged, toDo: (propName: string) => void): Subscription {
      if (to) return to.propertyChange.subscribe(toDo);
      return undefined;
  }
}
