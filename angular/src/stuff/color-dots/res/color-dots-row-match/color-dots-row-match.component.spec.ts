import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColorDotsRowMatchComponent } from './color-dots-row-match.component';

describe('ColorDotsRowMatchComponent', () => {
  let component: ColorDotsRowMatchComponent;
  let fixture: ComponentFixture<ColorDotsRowMatchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColorDotsRowMatchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColorDotsRowMatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
