import { Component, Input, ChangeDetectorRef, AfterViewInit } from '@angular/core';
@Component({
  selector: 'palette',
  templateUrl: './palette.component.html',
  styleUrls: ['./palette.component.css']
})
export class PaletteComponent implements AfterViewInit {
  private _dotsCnt:number;
  @Input()
  public get dotsCnt(): number {
    return this._dotsCnt;
  }
  public set dotsCnt(value: number) {
    this._dotsCnt = value;
    this.detectChanges();
  }
  
  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private chDref: ChangeDetectorRef) { }

  ngAfterViewInit() {
    this.chDref.detach();
  }
}
