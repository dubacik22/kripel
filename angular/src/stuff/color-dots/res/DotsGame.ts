import { DotsRow } from './DotsRow';
import { iNotifyPropertyChanged } from 'src/stuff/iNotifyProperyChanged';
import { EventEmitter } from '@angular/core';

export class DotsGame implements iNotifyPropertyChanged {
    public propertyChange: EventEmitter <string> = new EventEmitter();
    private _userRows: DotsRow[] = [];
    public get userRows(): DotsRow[] {
        return this._userRows;
    }
    private _dotsCnt: number = 4;
    public get dotsCnt(): number {
        return this._dotsCnt;
    }
    public set dotsCnt(value: number) {
        if (this._dotsCnt == value) return;
        if (value <= 1) return;
        this._dotsCnt = value;
        this.resetCnt();
    }
    private _colorsCnt: number = 6;
    public get colorsCnt(): number {
        return this._colorsCnt;
    }
    public set colorsCnt(value: number) {
        if (this._colorsCnt == value) return;
        if (value <= 1) return;
        this._colorsCnt = value;
        this.resetCnt();
    }
    public _myColors: DotsRow;
    public get myColors(): DotsRow {
        return this._myColors;
    }
    private _actuallyAddedRow: DotsRow = new DotsRow(this._dotsCnt, this._colorsCnt);
    public get actuallyAddedRow(): DotsRow {
        return this._actuallyAddedRow;
    }
    private setActuallyAddedRow(value: DotsRow) {
        this._actuallyAddedRow = value;
        this.propertyChange.emit('actuallyAddedRow');
    }
    constructor() {
        this._myColors = new DotsRow(this._dotsCnt, this._colorsCnt);
        this._myColors.setRandomColors();
        this.propertyChange.emit('myColors');
    }
    
    private resetCnt() {
        this._userRows = [];
        this._myColors = new DotsRow(this._dotsCnt, this._colorsCnt);
        this._myColors.setRandomColors();
        this.propertyChange.emit('myColors');
        this.setActuallyAddedRow(new DotsRow(this._dotsCnt, this._colorsCnt));
        this.propertyChange.emit('userRows');
    }

    public checkAndAddActuallyAdded() {
        this.actuallyAddedRow.check(this.myColors);
        this._userRows.push(this.actuallyAddedRow);
        this.setActuallyAddedRow(new DotsRow(this._dotsCnt, this._colorsCnt));
        this.propertyChange.emit('userRows');
    }
}