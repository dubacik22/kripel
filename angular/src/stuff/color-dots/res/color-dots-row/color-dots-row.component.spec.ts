import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColorDotsRowComponent } from './color-dots-row.component';

describe('ColorDotsRowComponent', () => {
  let component: ColorDotsRowComponent;
  let fixture: ComponentFixture<ColorDotsRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColorDotsRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColorDotsRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
