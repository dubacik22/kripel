import { Component, Input, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { DotsRow } from '../DotsRow';
import { Subscription } from 'rxjs';
import { iNotifyPropertyChanged } from 'src/stuff/iNotifyProperyChanged';

@Component({
  selector: 'color-dots-row',
  templateUrl: './color-dots-row.component.html',
  styleUrls: ['./color-dots-row.component.css']
})
export class ColorDotsRowComponent implements AfterViewInit {
  private _dotsCnt: number;
  @Input()
  public get dotsCnt(): number {
    return this._dotsCnt;
  }
  public set dotsCnt(value: number) {
    this._dotsCnt = value;
    this.detectChanges();
  }

  private _readonly: boolean;
  @Input()
  public get readonly(): boolean {
    return this._readonly;
  }
  public set readonly(value: boolean) {
    this._readonly = value;
    this.detectChanges();
  }

  private _editableColor: boolean;
  @Input()
  public get editableColor(): boolean {
    return this._editableColor;
  }
  public set editableColor(value: boolean) {
    if (this._editableColor == value) return;
    this._editableColor = value;
  }

  private contextSubscribtion: Subscription
  private _context: DotsRow;
  @Input()
  public get context(): DotsRow {
    return this._context;
  }
  public set context(value: DotsRow) {
    //if (DotsRow.isSame(this._context, value)) return;
    this.unlistenTo(this.contextSubscribtion);
    this._context = value;
    this.listenTo(this._context, () => {
      this.resetDotSizeInPercent();
    });
    this.resetDotSizeInPercent();
    this.detectChanges();
  }
  public _dotSizeInPercent: number;
  public get dotSizeInPercent(): number {
    return this._dotSizeInPercent;
  }
  public set dotSizeInPercent(value: number) {
    if (this._dotSizeInPercent == value) return;
    this._dotSizeInPercent = value;
    this.detectChanges();
  }
  public resetDotSizeInPercent() {
    if (!this._context) return;
    this.dotSizeInPercent = 100 / this._context.dotsCnt;
  }
  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private chDref: ChangeDetectorRef) { }

  ngAfterViewInit() {
    this.chDref.detach();
  }

  unlistenTo(subscribtion: Subscription) {
    if (subscribtion) subscribtion.unsubscribe();
  }
  listenTo(to: iNotifyPropertyChanged, toDo: (propName: string) => void): Subscription {
      if (to) return to.propertyChange.subscribe(toDo);
      return undefined;
  }
}
