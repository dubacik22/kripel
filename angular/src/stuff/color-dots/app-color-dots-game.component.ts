import { Component, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { DotsGame } from './res/DotsGame';
import { Subscription } from 'rxjs';
import { iNotifyPropertyChanged } from 'src/stuff/iNotifyProperyChanged';

@Component({
  selector: 'app-color-dots-game',
  templateUrl: './app-color-dots-game.component.html',
  styleUrls: ['./app-color-dots-game.component.css']
})
export class ColorDotsGameComponent implements AfterViewInit {

  public get dotsCnt(): number {
    return this.game.dotsCnt;
  }
  public set dotsCnt(value: number) {
    this.game.dotsCnt = value;
    this.detectChanges();
  }
  public get colorsCnt(): number {
    return this.game.colorsCnt;
  }
  public set colorsCnt(value: number) {
    this.game.colorsCnt = value;
    this.detectChanges();
  }

  public showMyColors: boolean = false;
  public game: DotsGame = new DotsGame();

  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private chDref: ChangeDetectorRef) { 
    this.listenTo(this.game, (propertyName: string) => {
      this.detectChanges();
    });
  }

  ngAfterViewInit() {
    this.chDref.detach();
    /*this.game.actuallyAddedRow.setRandomColors();
    this.game.checkAndAddActuallyAdded();
    this.game.actuallyAddedRow.setRandomColors();
    this.game.checkAndAddActuallyAdded();*/

  }

  add(){
    this.game.checkAndAddActuallyAdded();
  }
  toggleMyColors() {
    this.showMyColors = !this.showMyColors;
    this.detectChanges();
  }
  unlistenTo(subscribtion: Subscription) {
    if (subscribtion) subscribtion.unsubscribe();
  }
  listenTo(to: iNotifyPropertyChanged, toDo: (propName: string) => void): Subscription {
      if (to) return to.propertyChange.subscribe(toDo);
      return undefined;
  }
}
