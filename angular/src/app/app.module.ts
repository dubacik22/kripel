import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { NgModel } from '@angular/forms';
import { AppComponent } from './app.component';
import { AboutComponent } from './app-about/about.component';
import { ProjectsComponent } from './app-projectos/projects.component';
import { LosAngularosComponent } from './app-losAngularos/losAngularos.component';
import { LoginComponent } from './app-login/login.component';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SudokuModule } from './projects/sudoku/sudoku.module';
import { TesterModule } from './projects/tester/tester.module';
import { GalleryComponent as AppGalleryComponent } from './app-gallery/gallery.component';
import { Communicator as loginCommunicator } from './app-login/res/Communicator';
import { StuffModule } from 'src/stuff/stuff.module';
import { Session } from 'src/stuff/injectables/Session';
import { MyRouting } from 'src/stuff/injectables/MyRouting';
import { Paths } from 'src/stuff/injectables/Paths';
import { AppDoodlingComponent } from './projects/app-doodling/app-doodling.component';
import { AppAboutPageComponent } from './projects/about-page/app-about-page.component';
import { AppAboutFixedScrollableComponent } from './projects/about-page/fixedScrollable/about-fixedScrollable.component';
import { AppAboutScrollChangeComponent } from './projects/about-page/scrollChange/about-scrollChange.component';
import { AppAboutKindaPerspectiveDirectiveComponent } from './projects/about-page/kindaPerspectiveDirective/about-kindaPerspectiveDirective.component';
import { AppAboutKindaPerspectiveComponentComponent } from './projects/about-page/kindaPerspectiveComponent/about-kindaPerspectiveComponent.component';
import { AppAboutSlideMenuComponent } from './projects/about-page/slideMenu/about-slideMenu.component';
import { AppRubiksCubeComponent } from './projects/app-rubiks-cube/app-rubiks-cube.component';
import { RubiksCubeComponent } from './projects/rubiks-cube/rubiks-cube.component';
import { RubiksSubCubeComponent } from './projects/rubiks-cube/res/rubiks-sub-cube/rubiks-sub-cube.component';
import { AppSolarisComponent } from './projects/app-solaris/app-solaris.component';
import { SvgLineModule, SvgPathModule } from 'angular-svg';
import { DotNetteComponent } from './app-dotnette/dotNette.component';
import { EKComponent } from './projects/EK/EK.component'
import { EKEquationComponent } from './projects/EKEquation/EKEquation.component'
import { EKPropertyComponent } from './projects/EKProperty/EKProperty.component'
import { EKIKalkulatorowaciaKlassaComponent } from './projects/EKIKalkulatorowaciaKlassa/EKIKalkulatorowaciaKlassa.component'
import { EquationerComponent } from './projects/equationer/equationer.component'
import { ObservableDictionaryComponent } from './projects/observableDictionary/observableDictionary.component'
import { GrafosComponent } from './projects/app-grafos/grafos.component';
import { LimitsComponent } from './projects/app-grafos/res/limits/limits.component';
import { Communicator as grafosCommunicator } from './projects/app-grafos/res/Communicator';
import { NgxEchartsModule } from 'ngx-echarts';
import * as echarts from 'echarts';
import { Ng5SliderModule } from 'ng5-slider';

export function chartModule(){
  import('echarts'); // or import('./path-to-my-custom-echarts')
}

@NgModule({
  exports: [
  ],
  declarations: [
    AppComponent,
    AboutComponent,
    ProjectsComponent,
    LosAngularosComponent,
    LoginComponent,
    AppGalleryComponent,

    AppAboutFixedScrollableComponent,
    AppAboutScrollChangeComponent,
    AppAboutKindaPerspectiveDirectiveComponent,
    AppAboutKindaPerspectiveComponentComponent,
    AppAboutSlideMenuComponent,

    AppRubiksCubeComponent,
    RubiksCubeComponent,
    RubiksSubCubeComponent,

    AppSolarisComponent,

    AppDoodlingComponent,
    AppAboutPageComponent,
    
    DotNetteComponent,
    EKComponent,
    EKEquationComponent,
    EKPropertyComponent,
    EKIKalkulatorowaciaKlassaComponent,
    EquationerComponent,
    ObservableDictionaryComponent,

    GrafosComponent,
    LimitsComponent,
  ],
  imports: [
    FormsModule,
    SudokuModule,
    TesterModule,
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([]),
    StuffModule,

    SvgLineModule,
    SvgPathModule,
    NgxEchartsModule,
    Ng5SliderModule,
  ],
  providers: [
    NgModel,
    MyRouting,
    Paths,
    Session,
    loginCommunicator,
    grafosCommunicator,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
