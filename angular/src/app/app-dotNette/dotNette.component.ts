import { Component, Input, ChangeDetectorRef, AfterViewInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Strings } from 'src/app/Strings';

@Component({
  selector: 'app-dotNette',
  templateUrl: './dotNette.component.html',
  styleUrls: ['./dotNette.component.css'],
})
export class DotNetteComponent implements AfterViewInit, OnDestroy {
  public get projectsUrl(): string {
    return Strings.projectsUrl;
  }

  private _compact: boolean;
  @Input()
  public get compact(): boolean{
    return this._compact;
  }
  public set compact(value: boolean) {
    if (value == this._compact) return;
    this._compact=value;
    this.detectChanges();
  }

  private _ultraCompact: boolean;
  @Input()
  public get ultraCompact(): boolean{
    return this._ultraCompact;
  }
  public set ultraCompact(value: boolean) {
    if (value == this._ultraCompact) return;
    this._ultraCompact=value;
    this.detectChanges();
  }

  private _toDo: string;
  @Output('toDoChange') toDoChange: EventEmitter <string> = new EventEmitter();
  @Input() public get toDo(): string {
    return this._toDo;
  }
  public set toDo(value: string) {
    if (value == this._toDo) return;
    this._toDo = value;
    this.toDoChange.emit(this._toDo);
    this.detectChanges();
  }
  public get dotNetteUrl(): string { return Strings.dotNetteUrl; }
  public get EKUrl(): string { return Strings.EKUrl; }
  public get EKEquationUrl(): string { return Strings.EKEquationUrl; }
  public get EKPropertyUrl(): string { return Strings.EKPropertyUrl; }
  public get equationerUrl(): string { return Strings.equationerUrl; }
  public get observableDictionaryUrl(): string { return Strings.observableDictionaryUrl; }

  public get isEK(): boolean {
    return DotNetteComponent.isEK(this.toDo);
  }

  private _compactSubClass:string;
  @Input()
  public get compactSubClass():string{
    return this._compactSubClass;
  }
  public set compactSubClass(value:string) {
    if (value == this._compactSubClass) return;
    this._compactSubClass = value;
    this.setCompact();
  }
  private _scdStart: number;
  @Input()
  public get scdStart(): number {
    return this._scdStart;
  }
  public set scdStart(value: number) {
    if (this._scdStart == value) return;
    this._scdStart = value;
    this.detectChanges();
  }
  private _scdEnd: number;
  @Input()
  public get scdEnd(): number {
    return this._scdEnd;
  }
  public set scdEnd(value: number) {
    if (this._scdEnd == value) return;
    this._scdEnd = value;
    this.detectChanges();
  }

  private readonly cdRight: string = 'šťuk vpravo';
  private readonly cdDown: string = 'šťuk dole';
  public clickDirection: string = this.cdRight;
  private readonly cdFixedWidth: any = {width: '550px'};
  private readonly cdPercentWidth: any = {width: '90%'};
  public colorDotsWidth: any = this.cdFixedWidth;

  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }

  constructor(private chDref: ChangeDetectorRef) {}
  
  public ngAfterViewInit() {
    this.chDref.detach();
  }
  public ngOnDestroy() {
  }
  public showEK() {
    this.toDo = this.EKUrl;
    window.scrollTo(0, 0);
  }
  public showEquationer() {
    this.toDo = this.equationerUrl;
    window.scrollTo(0, 0);
  }
  public showObservableDictionary() {
    this.toDo = this.observableDictionaryUrl;
    window.scrollTo(0, 0);
  }

  private setCompact() {
    if (this._compactSubClass == Strings.compactSubClassUltraCompact) {
      this.clickDirection = this.cdDown;
      this.colorDotsWidth = this.cdPercentWidth;
    } else if (this._compactSubClass == Strings.compactSubClassCompact) {
      this.clickDirection = this.cdRight;
      this.colorDotsWidth = this.cdFixedWidth;
    } else {
      this.clickDirection = this.cdRight;
      this.colorDotsWidth = this.cdFixedWidth;
    }
    this.detectChanges();
  }
  
  public static isEK(toDo: string) {
    if (toDo == Strings.EKUrl) return true;
    if (toDo == Strings.EKEquationUrl) return true;
    if (toDo == Strings.EKPropertyUrl) return true;
    if (toDo == Strings.EKIKalkulatorowaciaKlassaUrl) return true;
    return false;
  }
}