import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DotNetteComponent } from './dotNette.component';

describe('DotNetteComponent', () => {
  let component: DotNetteComponent;
  let fixture: ComponentFixture<DotNetteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DotNetteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DotNetteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
