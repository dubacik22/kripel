export class Strings {

    public static readonly toDoQueryParamName = 'toDo';
    public static readonly aboutUrl: string = 'about';
    public static readonly projectsUrl: string = 'projects';
    public static readonly losAngularosUrl: string = 'losAngularos';
    public static readonly dotNetteUrl: string = 'dotNette';
    public static readonly aboutPageUrl: string = 'aboutPage';
    public static readonly sudokuUrl: string = 'sudoku';
    public static readonly testerUrl: string = 'tester';
    public static readonly colorDotsUrl: string = 'colorDots';
    public static readonly doodlingUrl: string = 'doodling';
    public static readonly rubiksUrl: string = 'rubiks';
    public static readonly grafosUrl: string = 'grafos';
    public static readonly solarisUrl: string = 'solaris';
    public static readonly galleryUrl: string = 'gallery';
    public static readonly EKUrl: string = 'EK';
    public static readonly EKEquationUrl: string = 'EKEquation';
    public static readonly EKPropertyUrl: string = 'EKProperty';
    public static readonly EKIKalkulatorowaciaKlassaUrl: string = 'EKIKalkulatorowaciaKlassa';
    
    public static readonly fixedScrollableUrl: string = 'fixedScrollable';
    public static readonly scrollChangeDirUrl: string = 'scrollChangeDir';
    public static readonly kindaPerspectiveDirectiveUrl: string = 'kindaPerspectiveDirective';
    public static readonly kindaPerspectiveComponentUrl: string = 'kindaPerspectiveComponent';
    public static readonly slideMenuUrl: string = 'slideMenu';
    public static readonly aboutToDoQueryParamName: string = 'aboutToDo';
    
    public static readonly equationerUrl: string = 'equationer';
    public static readonly observableDictionaryUrl: string ='observableDictionary';
    public static readonly rtbUrl: string = 'rtb';
    public static readonly loginUrl: string = 'login';
    public static readonly loginOthersUrl: string = 'loginOthers';

    public static readonly compactSubClassNonCompact: string = 'nonCompact';
    public static readonly compactSubClassCompact: string = 'compact';
    public static readonly compactSubClassUltraCompact: string = 'ultraCompact';
    public static readonly loggedNoone: string = 'jogin';
}
