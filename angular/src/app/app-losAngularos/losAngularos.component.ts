import { Component, Input, ChangeDetectorRef, AfterViewInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Strings } from 'src/app/Strings';

@Component({
  selector: 'app-losAngularos',
  templateUrl: './losAngularos.component.html',
  styleUrls: ['./losAngularos.component.css'],
})
export class LosAngularosComponent implements AfterViewInit, OnDestroy {
  public get projectsUrl(): string {
    return Strings.projectsUrl;
  }

  private _compact: boolean;
  @Input()
  public get compact(): boolean{
    return this._compact;
  }
  public set compact(value: boolean) {
    if (value == this._compact) return;
    this._compact=value;
    this.detectChanges();
  }

  private _ultraCompact: boolean;
  @Input()
  public get ultraCompact(): boolean{
    return this._ultraCompact;
  }
  public set ultraCompact(value: boolean) {
    if (value == this._ultraCompact) return;
    this._ultraCompact=value;
    this.detectChanges();
  }

  private _toDo: string;
  @Output('toDoChange') toDoChange: EventEmitter <string> = new EventEmitter();
  @Input() public get toDo(): string {
    return this._toDo;
  }
  public set toDo(value: string) {
    if (value == this._toDo) return;
    this._toDo = value;
    this.toDoChange.emit(this._toDo);
    this.detectChanges();
  }
  public get losAngularosUrl(): string { return Strings.losAngularosUrl; }
  public get aboutPageUrl(): string { return Strings.aboutPageUrl; }
  public get sudokuUrl(): string { return Strings.sudokuUrl; }
  public get testerUrl(): string { return Strings.testerUrl; }
  public get colorDotsUrl(): string { return Strings.colorDotsUrl; }
  public get doodlingUrl(): string { return Strings.doodlingUrl; }
  public get rubiksUrl(): string { return Strings.rubiksUrl; }
  public get grafosUrl(): string { return Strings.grafosUrl; }
  public get solarisUrl(): string { return Strings.solarisUrl; }

  private _compactSubClass:string;
  @Input()
  public get compactSubClass():string{
    return this._compactSubClass;
  }
  public set compactSubClass(value:string) {
    if (value == this._compactSubClass) return;
    this._compactSubClass=value;
    this.setCompact();
  }
  private _scdStart: number;
  @Input()
  public get scdStart(): number {
    return this._scdStart;
  }
  public set scdStart(value: number) {
    if (this._scdStart == value) return;
    this._scdStart = value;
    this.detectChanges();
  }
  private _scdEnd: number;
  @Input()
  public get scdEnd(): number {
    return this._scdEnd;
  }
  public set scdEnd(value: number) {
    if (this._scdEnd == value) return;
    this._scdEnd = value;
    this.detectChanges();
  }

  private readonly cdRight: string = 'šťuk vpravo';
  private readonly cdDown: string = 'šťuk dole';
  public clickDirection: string = this.cdRight;
  private readonly cdFixedWidth: any = {width: '550px'};
  private readonly cdPercentWidth: any = {width: '90%'};
  public colorDotsWidth: any = this.cdFixedWidth;

  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }

  constructor(private chDref: ChangeDetectorRef) {}
  
  public ngAfterViewInit() {
    this.chDref.detach();
  }
  public ngOnDestroy() {
  }
  public showAboutPage() {
    this.toDo = this.aboutPageUrl;
    window.scrollTo(0, 0);
  }
  public showSudoku() {
    this.toDo = this.sudokuUrl;
    window.scrollTo(0, 0);
  }
  public showTester() {
    this.toDo = this.testerUrl;
    window.scrollTo(0, 0);
  }
  public showColorDots() {
    this.toDo = this.colorDotsUrl;
    window.scrollTo(0, 0);
  }
  public showDoodling() {
    this.toDo = this.doodlingUrl;
    window.scrollTo(0, 0);
  }
  public showRubiks() {
    this.toDo = this.rubiksUrl;
    window.scrollTo(0, 0);
  }
  public showGrafos() {
    this.toDo = this.grafosUrl;
    window.scrollTo(0, 0);
  }
  public showSolaris() {
    this.toDo = this.solarisUrl;
    window.scrollTo(0, 0);
  }

  private setCompact() {
    if (this._compactSubClass == Strings.compactSubClassUltraCompact) {
      this.clickDirection = this.cdDown;
      this.colorDotsWidth = this.cdPercentWidth;
    } else if (this._compactSubClass == Strings.compactSubClassCompact) {
      this.clickDirection = this.cdRight;
      this.colorDotsWidth = this.cdFixedWidth;
    } else {
      this.clickDirection = this.cdRight;
      this.colorDotsWidth = this.cdFixedWidth;
    }
    this.detectChanges();
  }
}