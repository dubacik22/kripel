import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LosAngularosComponent } from './losAngularos.component';

describe('LosAngularosComponent', () => {
  let component: LosAngularosComponent;
  let fixture: ComponentFixture<LosAngularosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LosAngularosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LosAngularosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
