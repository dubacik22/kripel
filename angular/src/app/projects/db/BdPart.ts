export class BdPart {
    public myPart: number;
    public exponent: number;

    protected plus(plus: BdPart) {
        this.myPart += plus.myPart;
    }
}