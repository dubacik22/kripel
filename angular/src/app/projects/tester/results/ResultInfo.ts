import { TestClass2SaveResults } from '../resources/SavedClassesResults';

export class ResultInfo{
    result: TestClass2SaveResults;
    id: string;
    testId: string;
    testName: string;
    userName: string;
}