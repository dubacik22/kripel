import { Component, ChangeDetectorRef, AfterViewInit, OnDestroy } from '@angular/core';
import { Session } from 'src/stuff/injectables/Session';
import { User } from 'src/app/app-login/res/User';
import { MyRouting } from 'src/stuff/injectables/MyRouting';
import { TesterComponent } from '../tester.component';
import { ResultsInfo } from './ResultsInfo';
import { ResultInfo } from './ResultInfo';
import { Saver } from '../resources/Saver';
import { TestClass } from '../resources/ComponentClassesCommon';
import { TestHolder } from '../resources/TestHolder';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css','../styles.css']
})
export class ResultsComponent implements AfterViewInit, OnDestroy {
  public actAddedResult: string;
  public showedResults: ResultsInfo;
  public resultsGroups: ResultsInfo[] = [];
  public actResults: ResultInfo[] = [];
  private changeDetectionInProgress: boolean = false;

  public get test(): TestClass {
    return <TestClass>this.testHolder.test;
  }
  public set test(value: TestClass) {
    this.testHolder.test = value;
    this.detectChanges();
  }

  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  private testSubscribtion: Subscription;
  constructor(private saver: Saver, private router: MyRouting, private testHolder: TestHolder,
    private chDref: ChangeDetectorRef, private session: Session) {}

  ngAfterViewInit() {
    this.chDref.detach();
    this.refreshResults();
    this.detectChanges();

    this.testSubscribtion = this.testHolder.test$.subscribe((test: TestClass) => {
      this.detectChanges();
    });
  }
  ngOnDestroy() {
    this.testSubscribtion.unsubscribe();
  }

  private refreshResults() {
    this.saver.getAllResultsInfo(this.session.getItem(User.userIdPropName), (resultsGroups: ResultsInfo[]) => {
      this.initResults(resultsGroups);
    });
  }
  private initResults(resultsGroups: ResultsInfo[]) {
    if (!resultsGroups) return;
    this.resultsGroups = [];
    resultsGroups.forEach((gettedResults: ResultsInfo) => {
      this.resultsGroups.push(gettedResults);
    });
    this.detectChanges();
  }
  public addResultsName(resultsName: string) {
    if (!this.actAddedResult) return;
    if (this.resultsGroups && (this.resultsGroups.length > 0)) {
      let existingResults: ResultsInfo = this.resultsGroups.find((results: ResultsInfo) => {
        return (results.name == this.actAddedResult);
      });
      if (existingResults) return;
    }
    this.saver.saveResultsInfo(this.session.getItem(User.userIdPropName), resultsName, (success: boolean) => {
      if (success) {
        this.refreshResults();
        this.hideResult();
      }
    });
  }
  public goResults(results: ResultsInfo) {
    this.showedResults = results;
    this.actAddedResult = results.name;
    this.router.insertQueryS(TesterComponent.resultsGroupQueryParamName, results.id);
    this.router.unlistenOnce();
    this.router.insert();
    this.showResults(results)
    this.detectChanges();
  }
  private showResults(results: ResultsInfo) {
    this.saver.getResults(results.id, (results: ResultInfo[]) => {
      this.actResults = results;
      this.detectChanges();
    })
  }
  public goResult(result: ResultInfo) {
    this.saver.loadTest(result.testId, (test:TestClass) => {
      test.insertAnswers(result.result);
      test.enabled = false;
    });
  }
  private hideResult() {
    this.showedResults = undefined;
    this.actAddedResult = undefined;
    this.router.remQuery(TesterComponent.resultsGroupQueryParamName);
    this.router.unlistenOnce();
    this.router.insert();
    this.test = undefined;
    this.detectChanges();
  }
  public remResults(results: ResultsInfo) {
    this.saver.remWholeResults(results.id, () => {
      this.refreshResults();
      this.hideResult();
    });
  }
}