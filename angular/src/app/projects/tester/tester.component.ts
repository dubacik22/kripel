import { Component,
  ChangeDetectorRef, AfterViewInit, OnDestroy } from '@angular/core';
import { MyRouting, routerListener } from 'src/stuff/injectables/MyRouting';
import { Saver } from './resources/Saver';
import { TestInfo } from './resources/TestInfo';
import { Session } from 'src/stuff/injectables/Session';
import { DialogBoxComponent } from 'src/stuff/dialogs/modalDialog.component';
import { TestHolder } from './resources/TestHolder';

@Component({
  selector: 'app-tester',
  templateUrl: './tester.component.html',
  styleUrls: ['./tester.component.css','./styles.css']
})
export class TesterComponent implements AfterViewInit , OnDestroy {
  public static readonly testIdQueryParamName = 'test';
  public static readonly toDoQueryParamName = 'tester';
  public static readonly resultsGroupQueryParamName = 'resultsGroup';
  public static readonly testingUrl = 'test';
  public static readonly creatingUrl = 'create';
  public static readonly linkingUrl = 'link';
  public static readonly resultsUrl = 'results';
  public static readonly nothingUrl = 'choose';
  public get testingUrl(): string {
    return TesterComponent.testingUrl;
  }
  public get creatingUrl(): string {
    return TesterComponent.creatingUrl;
  }
  public get linkingUrl(): string {
    return TesterComponent.linkingUrl;
  }
  public get resultsUrl(): string {
    return TesterComponent.resultsUrl;
  }
  public get nothingUrl(): string {
    return TesterComponent.nothingUrl;
  }
  public tests: TestInfo[] = [];
  
  private _toDo:string = TesterComponent.nothingUrl;
  public get toDo(): string {
    return this._toDo;
  }
  public set toDo(value :string) {
    if (this._toDo == value) return;
    this._toDo = value;
    this.detectChanges();
  }

  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private chDref: ChangeDetectorRef, private router: MyRouting,
    private saver: Saver, private testHolder: TestHolder,
    public session: Session) {}

  public ngAfterViewInit() {
    this.saver.createTables();
    this.chDref.detach();
    this.chRQLambda = this.router.onChangeRouteQueries((query: Map<string, string>) => {
      this.takeCareOfQuery(query);
    });
    this.refreshTests();
  }
  private initTests(gettedTests: TestInfo[]) {
    if (gettedTests) {
      this.tests = gettedTests;
      this.detectChanges();
    } else {
      DialogBoxComponent.showJustInfo('však tu nič nieje');
    }
  }
  private chRQLambda: routerListener;
  public ngOnDestroy() {
    this.chRQLambda.unsubscribe();
  }
  private takeCareOfQuery(pathQueries: Map<string, string>) {
    if (pathQueries.has(TesterComponent.toDoQueryParamName)) {
      let toDo: string;
      toDo = pathQueries.get(TesterComponent.toDoQueryParamName);
      if (!toDo) toDo = TesterComponent.nothingUrl;
      this.toDo = toDo;
      this.detectChanges();
    } else {
      this.toDo = TesterComponent.nothingUrl;
    }
  }
  public refreshTests() {
    if (this.session.isUberAdmin()) {
      this.saver.doWithAllTests((tests: TestInfo[]) => {
        this.initTests(tests);
      });
    } else {
      this.saver.doWithAvailableTests((tests: TestInfo[]) => {
        this.initTests(tests);
      });
    }
  }
  public decideAgain() {
    this.testHolder.test = undefined;
    this.toDo = TesterComponent.nothingUrl;
    this.router.remQuery(TesterComponent.toDoQueryParamName);
    this.router.remQuery(TesterComponent.testIdQueryParamName);
    this.router.remQuery(TesterComponent.resultsGroupQueryParamName);
    this.router.unlistenOnce();
    this.router.insert();
  }
  public goTest(testInfo: TestInfo) {
    this.toDo = TesterComponent.testingUrl;
    this.router.insertQueryS(TesterComponent.toDoQueryParamName, TesterComponent.testingUrl);
    this.router.insertQueryS(TesterComponent.testIdQueryParamName, testInfo.id);
    this.router.remQuery(TesterComponent.resultsGroupQueryParamName);
    this.router.unlistenOnce();
    this.router.insert();
    this.saver.loadTest(testInfo.id);
  }
  public goCreate() {
    this.toDo = TesterComponent.creatingUrl;
    this.router.insertQueryS(TesterComponent.toDoQueryParamName, TesterComponent.creatingUrl);
    this.router.unlistenOnce();
    this.router.insert();
  }
  public goLink() {
    this.toDo = TesterComponent.linkingUrl;
    this.router.insertQueryS(TesterComponent.toDoQueryParamName, TesterComponent.linkingUrl);
    this.router.unlistenOnce();
    this.router.insert();
  }
  public goResults() {
    this.toDo = TesterComponent.resultsUrl;
    this.router.insertQueryS(TesterComponent.toDoQueryParamName, TesterComponent.resultsUrl);
    this.router.unlistenOnce();
    this.router.insert();
  }
}
