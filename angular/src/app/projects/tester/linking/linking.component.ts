import { Component, ChangeDetectorRef, AfterViewInit, OnDestroy } from '@angular/core';
import { Saver } from '../resources/Saver';
import { TestInfo as TestInfo } from '../resources/TestInfo';
import { Session } from 'src/stuff/injectables/Session';
import { User } from 'src/app/app-login/res/User';
import { DialogBoxComponent } from 'src/stuff/dialogs/modalDialog.component';
import { ResultsInfo } from '../results/ResultsInfo';
import { MyRouting } from 'src/stuff/injectables/MyRouting';
import { TesterComponent } from '../tester.component';

@Component({
  selector: 'app-linking',
  templateUrl: './linking.component.html',
  styleUrls: ['./linking.component.css','../styles.css']
})
export class LinkingComponent implements AfterViewInit, OnDestroy {
  public tests: TestInfo[] = [];
  public resultsGroups: ResultsInfo[] = [];
  public resultsGroup: string = '';

  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private saver: Saver, private router: MyRouting,
    private chDref: ChangeDetectorRef, private session: Session) {}

  ngAfterViewInit() {
    this.chDref.detach();
    this.refreshTests();
    this.refreshResultsInfos();
    this.detectChanges();
  }
  ngOnDestroy() {
  }

  private refreshTests() {
    this.saver.doWithAvailableTests((tests: TestInfo[]) => {
      this.initTests(tests);
    });
  }
  private refreshResultsInfos() {
    this.saver.getAllResultsInfo(this.session.getItem(User.userIdPropName), (resultsInfos: ResultsInfo[]) => {
      this.initResultsInfos(resultsInfos);
    });
  }
  private initTests(gettedTests: TestInfo[]) {
    if (!gettedTests) {
      this.tests = [];
      return;
    }
    this.tests = gettedTests;
    this.detectChanges();
  }
  private initResultsInfos(gettedResultsInfos: ResultsInfo[]) {
    if (!gettedResultsInfos) {
      this.resultsGroups = [];
      return;
    }
    this.resultsGroups = gettedResultsInfos;
    this.detectChanges();
  }
  public finish() {
    let ids: string = '';
    this.tests.forEach((item: TestInfo) => {
      if (item.goToLink) ids += item.id + ',';
    });
    ids = ids.substring(0,ids.length-1);
    let path: string = window.location.href;
    path = this.addToPath(path, TesterComponent.toDoQueryParamName, 'test');
    path = this.addToPath(path, TesterComponent.testIdQueryParamName, ids);
    if (this.resultsGroup) path = this.addToPath(path, TesterComponent.resultsGroupQueryParamName, this.resultsGroup);

    DialogBoxComponent.showJustInfo(path);
    //this.router.clearAll();
    //this.router.insertQueryS('tester', 'test');
    //this.router.insertQueryS('test', ids);
    //this.router.insertQueryS('resultsGroup', this.resultsGroup);
    //this.router.insert();
  }
  private addToPath(path: string, what: string, whatValue: string) {
    if (!whatValue) return path;
    let start: number = path.indexOf(what+'=');
    if (start >= 0) path = this.removeFromPath(path, what);
    let qmi: number = path.indexOf('?');
    let ret: string;
    if (qmi < 0) ret = path+'?';
    else ret = path+'&';
    ret += what+'='+whatValue;
    return ret;
  }
  private removeFromPath(path: string, what: string): string {
    let start: number = path.indexOf(what+'=');
    if (start < 0) return path;
    if (path.substr(start-1,1) != '?') start--;
    let finish: number = path.indexOf('&', start+what.length);
    if (finish < 0) finish = path.length;
    else finish++;
    let beg: string = path.substring(0, start);
    let end: string = path.substring(finish);
    let ret: string = beg+end;
    if (ret.substr(ret.length-1, 1) == '?') ret = ret.substr(0, ret.length-1);
    return ret;
  }
}
