import { Component, OnInit, Input, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { TestClass, TestItemClass } from '../../resources/ComponentClassesCommon';
import { TestHolder } from 'src/app/projects/tester/resources/TestHolder';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-testing-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css','../../styles.css']
})
export class Test implements OnInit, OnDestroy {
  private _divideItems: boolean = true;
  public get divideItems(): boolean {
    return this._divideItems;
  }
  @Input() public set divideItems(value: boolean) {
    if (this._divideItems == value) return;
    this._divideItems = value;
    this.detectChanges();
  }
  public testItem: TestItemClass;
  public actTestItemIndex: number = 0;
  public get test(): TestClass {
    return <TestClass>this.testHolder.test;
  }
  @Input() public set test(value: TestClass) {
    this.setActQuestion();
  }
  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  private testSubscribtion: Subscription;
  constructor(private testHolder:TestHolder, private chDref: ChangeDetectorRef) {
    this.testSubscribtion = this.testHolder.test$.subscribe((test: TestClass) => {
      this.actTestItemIndex = 0;
      this.setActQuestion();
    });
  }

  ngOnInit() {
    this.actTestItemIndex=0;
    this.setActQuestion();
  }
  ngOnDestroy() {
    this.testSubscribtion.unsubscribe();
  }

  setActQuestion(){
    if (!this.test) return;
    this.testItem = this.test.items[this.actTestItemIndex];
  }
  havePrev():boolean{
    return this.actTestItemIndex>0;
  }
  haveNext():boolean{
    if (this.test==undefined) return false;
    return this.actTestItemIndex<this.test.items.length-1;
  }
  prevQuestion(){
    if (this.havePrev()) this.actTestItemIndex--;
    this.setActQuestion();
    this.detectChanges();
  }
  nextQuestion(){
    if (this.haveNext()) this.actTestItemIndex++;
    this.setActQuestion();
    this.detectChanges();
  }
}
