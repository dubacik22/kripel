import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Test } from './test.component';

describe('GrouppedScalesComponent', () => {
  let component: Test;
  let fixture: ComponentFixture<Test>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Test ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Test);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
