import { Component, Input, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { TestClass as TestClassBase, QuestionClass } from '../../resources/ComponentClassesCommon';
import { Subscription } from 'rxjs';
import { iNotifyPropertyChanged } from 'src/stuff/iNotifyProperyChanged';

@Component({
  selector: 'app-test-answers-cnt-weights',
  templateUrl: './answers-cnt-weights.component.html',
  styleUrls: ['./answers-cnt-weights.component.css','../../styles.css']
})
export class AnswersCntWeightsComponent implements AfterViewInit {
  private _context: QuestionClass;
  @Input() public get context(): QuestionClass {
    return this._context;
  }
  public set context(value: QuestionClass) {
    this.unlistenTo(this.contextSubscribtion);
    if (this._context == value) return;
    this._context = value;
    this.contextSubscribtion = this.listenTo(this._test, (propName: string) => {this.propertyChanged(propName);});
    this.detectChanges();
  }
  private contextSubscribtion: Subscription;

  private _test: TestClassBase;
  @Input() public get test(): TestClassBase {
    return this._test;
  }
  public set test(value: TestClassBase) {
    this.unlistenTo(this.testSubscribtion);
    if (this._test == value) return;
    this._test = value;
    this.testSubscribtion = this.listenTo(this._test, (propName: string) => {this.propertyChanged(propName);});
    this.detectChanges();
  }
  private testSubscribtion: Subscription;

  public propertyChanged(propName: string) {
    this.detectChanges();
  }
  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private chDref: ChangeDetectorRef) { }

  ngAfterViewInit() {
  }

  unlistenTo(subscribtion: Subscription) {
    if (subscribtion) subscribtion.unsubscribe();
  }
  listenTo(to: iNotifyPropertyChanged, toDo: (propName: string) => void): Subscription {
      if (to) return to.propertyChange.subscribe(toDo);
      return undefined;
  }
}
