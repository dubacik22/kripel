import { Component, ChangeDetectorRef, AfterViewInit, OnDestroy } from '@angular/core';
import { TestClass, Timer } from '../resources/ComponentClassesCommon';
//import { TestClass } from 'src/app/resources/TestClasses';
import { Saver } from '../resources/Saver';
import { TestHolder } from '../resources/TestHolder';
import { Subscription } from 'rxjs';
import { TesterComponent } from '../tester.component';
import { MyRouting, routerListener } from 'src/stuff/injectables/MyRouting';
import { DialogBoxComponent } from 'src/stuff/dialogs/modalDialog.component';

@Component({
  selector: 'app-testing',
  templateUrl: './testing.component.html',
  styleUrls: ['./testing.component.css','../styles.css']
})
export class TestingComponent implements AfterViewInit, OnDestroy {
  public identificator: string;
  private resultGroup: string;
  private _testsIdsToOpen: string[] = [];
  public get testsIdsToOpen(): string[] {
    return this._testsIdsToOpen;
  }
  public set testsIdsToOpen(value: string[]) {
    this._testsIdsToOpen = value;
    this.startOpeningTestNumber(this.testNumber);

  }
  public param: string[] = [];
  private _testNumber: number = 0;
  private get testNumber(): number {
    return this._testNumber;
  }
  private set testNumber(value: number) {
    if (value == this._testNumber) return;
    this._testNumber = value;
    this.startOpeningTestNumber(this.testNumber);
  }

  private startTime: number;
  private timerRunning: boolean = false;
  private _timerCounter: number = 0;
  private get timerCounter(): number {
    return this._timerCounter;
  }
  private set timerCounter(value: number) {    
    if (value == this._timerCounter) return;
    this._timerCounter = value;
    this.detectChanges();
  }
  private timerRef: any;

  public get test(): TestClass{
    return this.testHolder.test;
  }

  get typeOfTest(): number {
    if (this.test === undefined) return 0;
    return this.test.thisTestType;
  }
  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  private testSubscribtion: Subscription;
  constructor(private router: MyRouting, private saver: Saver,
    private chDref: ChangeDetectorRef, private testHolder:TestHolder) {}
    
  ngAfterViewInit() {
    this.chDref.detach();
    this.testSubscribtion = this.testHolder.test$.subscribe((test: TestClass) => {
      if (test) {
        if (test.timer.use) this.showTimingInfo = true;
        if (!test.anonym && !this.identificator) this.showNameInfo = true;
      }
      this.detectChanges();
    });
    this.chRQLambda = this.router.onChangeRouteQueries((query: Map<string, string>) => {
      this.takeCareOfQuery(query);
    });
    if (this.router.haveSentQuerie(TesterComponent.resultsGroupQueryParamName))
      this.resultGroup = this.router.getSentQuerie(TesterComponent.resultsGroupQueryParamName);
    else
      this.resultGroup = '';
  }
  private chRQLambda: routerListener;
  ngOnDestroy() {
    this.testSubscribtion.unsubscribe();
    clearInterval(this.timerRef);
    this.chRQLambda.unsubscribe();
  }
  private takeCareOfQuery(pathQueries: Map<string, string>) {
    if (pathQueries.has(TesterComponent.testIdQueryParamName)) {
      let idString: string = pathQueries.get(TesterComponent.testIdQueryParamName);
      let ids: string[] = idString.split(',');
      for (let i: number = 0; i < ids.length; i++) {
        ids[i] = ids[i].trim();
      }
      this.testsIdsToOpen = ids;
      
      this.detectChanges();
    }

  }
  public showTimingInfo: boolean = false;
  public showNameInfo: boolean = false;
  private startOpeningTestNumber(index: number) {
    if (index >= this._testsIdsToOpen.length) return;
    let testId: string = this.testsIdsToOpen[index];
    this.saver.loadTest(testId);
  }

  canOpenTestNumber(index: number):boolean {
    if (index < 0) return false;
    if (index >= this.testsIdsToOpen.length) return false;
    return true;
  }

  startOrResetTimer() {
    if (!this.test.timer.use) return;
    this.startTime = Date.now() - this.test.timer.runnedTimeInMs;
    if (!this.timerRunning) {
      this.timerRunning = true;
      this.timerRef = setInterval(() => {
        this.afterTime();
      }, 1000);
    }
    this.onTimerStart();
  }
  private endTestIfNeed() {
    if (this.test.timer.type == Timer.countdownType) {
      if (this.test.timer.runnedTimeInMs >= this.test.timer.maxTime) {
        this.test.enabled = false;
        this.timerCounter = 0;
        this.clearTimer();
      }
    }
  }
  public afterTime() {
    this.test.timer.runnedTimeInMs = Date.now() - this.startTime;
    this.endTestIfNeed();
    if (!this.test.enabled) return;
    if (this.test.timer.type == Timer.countdownType) {
      this.timerCounter = this.test.timer.maxTime - this.test.timer.runnedTimeInMs + 10;
    } else {
      this.timerCounter = this.test.timer.runnedTimeInMs + 10;
    }
  }
  private onTimerStart() {
    if (this.test.timer.type == Timer.countdownType) {
      this.timerCounter = this.test.timer.maxTime - this.test.timer.runnedTimeInMs;
    } else {
      this.timerCounter = this.test.timer.runnedTimeInMs;
    }
  }

  clearTimer() {
    this.timerRunning = false;
    clearInterval(this.timerRef);
  }
  tryRunTest() {
    if (this.showTimingInfo || this.showNameInfo) return;
    this.startOrResetTimer();
    this.detectChanges();
  }
  
  public hideNameInfo() {
    if (this.identificator) this.showNameInfo = false;
    this.tryRunTest();
    this.detectChanges();
  }
  public hideTimingInfo() {
    this.showTimingInfo = false;
    this.tryRunTest();
    this.detectChanges();
  }
  goPrev() {
    if (this.testNumber > 0) this.testNumber--;
    this.saveTime();
    if (this.canOpenTestNumber(this.testNumber)) {
      this.startOpeningTestNumber(this.testNumber);
    } else {
      this.clearTimer();
    }
    this.detectChanges();
  }
  goNext() {
    if (this.testNumber < this.testsIdsToOpen.length) this.testNumber++;
    this.saveTime();
    this.sendTestResult();
    if (this.canOpenTestNumber(this.testNumber)) {
      this.startOpeningTestNumber(this.testNumber);
    } else {
      this.clearTimer();
    }
    this.detectChanges();
  }
  finish() {
    this.saveTime();
    this.sendTestResult();
  }

  isOpenedTestFirstTest(): boolean {
    if (this.testNumber == 0) return false;
    return true;
  }
  isOpenedTestLastTest(): boolean {
    if (this.testNumber >= this.testsIdsToOpen.length-1) return true;
    return false;
  }

  private saveTime() {
    if (this.timerRunning) {
      this.test.timer.runnedTimeInMs = this._timerCounter;
    }
  }
  private sendTestResult() {
    if (!this.resultGroup) return;
    let identificator: string;
    if (this.identificator) identificator = this.identificator;
    else identificator = 'anonym';
    this.saver.saveWholeResult(identificator, this.resultGroup, () => {
      DialogBoxComponent.showJustInfo('ok');
    });
  }
}
