import { Injectable } from '@angular/core';
import { OffsetNode } from './OffsetNode';
import { ImgBoxComponent } from '../../img-box/img-box.component';
import { ImgGetter } from '../../../resources/ImgGetter';
@Injectable()
export class NodeFinder{
    public parentNode:Node;
    private end:number;
    private offset:number;
  
    constructor(private getter:ImgGetter){
    }
    public getTextNode(start:number,end:number,offset:number):OffsetNode{
        let ret:OffsetNode=new OffsetNode();
        ret.offsetInNode=start;
        this.end=end;
        this.offset=offset;
        if (this.offset<0) return undefined;
        ret.numberOfCharsBeforeNode=0;
        ret.numCharsFromBegToEnd=end-start;
        ret.node=this.innerGetTextNode(this.parentNode,ret);
        if (ret.node) return ret;
        return undefined;
    }
    public getTextNodes(start:number,end:number):OffsetNode[]{
        let foundNode:OffsetNode=undefined;
        let i:number=0;
        let ret:OffsetNode[]=[];
        do{
            foundNode=this.getTextNode(start,end,i);
            if (foundNode){
                ret.push(foundNode);
            }
            i++;
        }while(foundNode!=undefined)
        if (ret.length>0) return ret;
        return undefined;
    }
    public trim(ons:OffsetNode[]):OffsetNode[]{
        let ret:OffsetNode[]=[];
        ons.forEach((on:OffsetNode)=>{
            if ((on.numCharsFromBegToEnd-on.offsetInNode)!=0) ret.push(on);
        });
        return ret;
    }
    private innerGetTextNode(value:Node,on:OffsetNode):Node{
        if (value.hasChildNodes()){
            for(let i:number=0;i<value.childNodes.length;i++){
                let childNode:Node=value.childNodes[i];
                if (this.end<0) return undefined;
                if (childNode.nodeType==childNode.TEXT_NODE){
                    let tl:number=childNode.textContent.length;
                    if (on.offsetInNode<=tl){
                        if (this.offset==0) {
                            on.numCharsFromBegToEnd=Math.min(this.end,tl);
                            return childNode;
                        }
                        on.numCharsFromBegToEnd-=tl;
                        this.offset--;
                        on.offsetInNode=0;
                    } else {
                        on.offsetInNode-=tl;
                    }
                    on.numberOfCharsBeforeNode+=tl;
                    this.end-=tl;
                }
                let ret:Node=this.innerGetTextNode(childNode,on);
                if (ret) return ret;
            }
        }
        return undefined;
    }
    public forE(whatToDo:(imgBox:ImgBoxComponent)=>any){
        return this.innerForE(this.parentNode,whatToDo);
    }
    private innerForE(value:Node,whatToDo:(imgBox:ImgBoxComponent)=>any){
        if (value.nodeName=='APP-IMG-BOX') {
            let imgBox:ImgBoxComponent=this.getter.getFromDom(<HTMLElement>value);
            if (imgBox) whatToDo(imgBox)
        } else if (value.hasChildNodes()){
            for(let i:number=0;i<value.childNodes.length;i++){
                let childNode:Node=value.childNodes[i];
                this.innerForE(childNode,whatToDo);
            }
        }
    }
    public showNode():string{
        return this.innerShowNode(this.parentNode,0);
    }
    public innerShowNode(value:Node,deep:number):string{
        let ret:string=' '+deep+': <'+value.nodeName;
        if (value.hasChildNodes()){
            for(let i:number=0;i<value.childNodes.length;i++){
                let childNode:Node=value.childNodes[i];
                ret+=this.innerShowNode(childNode,deep+1);
            }
        } else if (value.nodeType==value.TEXT_NODE) ret+='<t: '+value.textContent+'>';
        else if (value.nodeName=='IMG') {
            let aval:any=value;
            ret+=' src='+aval.src;
            ret+=' w='+aval.width;
            ret+=' h='+aval.height;
        }
        ret+='>'
        return ret;
    }
}