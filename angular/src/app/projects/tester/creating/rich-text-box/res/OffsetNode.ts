
export class OffsetNode {
    public node:Node;
    public offsetInNode:number;
    public numberOfCharsBeforeNode:number;
    public numCharsFromBegToEnd:number;
}