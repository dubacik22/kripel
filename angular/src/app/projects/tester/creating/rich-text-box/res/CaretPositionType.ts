export class CaretPositionType {
    public start:number=0;
    public end:number=0;
    public startN:Node=undefined;
    public endN:Node=undefined;
}