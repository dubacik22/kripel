import { Injectable } from '@angular/core';
import { dividedNode } from './DividedNode';
import { NodeFinder } from './NodeFinder';
import { OffsetNode } from './OffsetNode';

@Injectable()
export class NodeDivider{
    public deepestNode:Node;
  
    constructor(
        private finder:NodeFinder,
    ){}
    private insertAfter(parentNode:Node,after:Node,what:Node){
        if (after){
          let snode:Node=after.nextSibling;
          if (snode) parentNode.insertBefore(what,snode);
          else parentNode.appendChild(what);
        } else {
          let fc:Node=parentNode.firstChild;
          if (fc) parentNode.insertBefore(what,fc);
          else parentNode.appendChild(what);
        }
    }
    private divideToDeepest(from:Node,howFarFromDeepest?:number):Node{
        let parent:Node=from.parentNode;
        for(let i:number=0;i<howFarFromDeepest;i++){
            parent=parent.parentNode;
        }
        if (from.parentNode.parentElement==this.deepestNode) return from;
        if (from==from.parentNode.lastChild) {
            return this.divideToDeepest(from.parentNode);
        }
        let i:number=this.indexOfChild(from)+1;
        let newParentSibling:Node;

        newParentSibling=from.parentNode.cloneNode(false);
        this.insertAfter(from.parentNode.parentNode,from.parentNode,newParentSibling);
        while(from.parentNode.childNodes.length>i) newParentSibling.appendChild(from.parentNode.childNodes.item(i));
        return this.divideToDeepest(from.parentNode);
    }
    private indexOfChild(node:Node):number{
        let parent:Node=node.parentNode;
        for(let i=0;i<parent.childNodes.length;i++){
            if (parent.childNodes.item(i)==node) return i;
        }
        return -1;
    }
    public divide(position:number,howFarFromDeepest?:number):dividedNode{
        let ret:dividedNode=new dividedNode();
        ret.node=this.deepestNode;
        //afterNode by mal byt nod druhej vrstvy (hned po deepestNode)
        //ret.node bude deepestNode
        if (position==0) {
            for(let i:number=0;i<howFarFromDeepest;i++){
                ret.node=ret.node.lastChild;
            }
            ret.nodeBefore=undefined;
            return ret;
        }
        let strValue:string=this.deepestNode.textContent;
        if (position==strValue.length) {
            for(let i:number=0;i<howFarFromDeepest;i++){
                ret.node=ret.node.lastChild;
            }
            ret.nodeBefore=ret.node.lastChild;
            return ret;
        }
        let on:OffsetNode=this.finder.getTextNode(position,position,0);
        ret.nodeBefore=on.node;
        if (on.offsetInNode==on.node.textContent.length) {
            ret.nodeBefore=this.divideToDeepest(on.node,howFarFromDeepest)
            ret.node=ret.nodeBefore.parentNode;
            return ret;
        }
        let startStr:string=on.node.textContent.substring(0,on.offsetInNode);
        let endStr:string=on.node.textContent.substring(on.offsetInNode,ret.node.textContent.length);
        on.node.textContent=startStr;
        let nextSibling:Node=document.createTextNode(endStr);
        this.insertAfter(on.node.parentNode,on.node,nextSibling);
        ret.nodeBefore=this.divideToDeepest(on.node,howFarFromDeepest);
        ret.node=ret.nodeBefore.parentNode;
        return ret;
    }
}