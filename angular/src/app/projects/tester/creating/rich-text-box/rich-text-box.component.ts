import { Component, ViewChild, ElementRef, Input, Output, EventEmitter, OnInit,
   ViewChildren, QueryList, OnDestroy, AfterViewInit } from '@angular/core';
import { GlyphsBase } from './res/GyphsBase';
import { CreatedNode } from './res/CreatedNode';
import { OffsetNode } from './res/OffsetNode';
import { NodeFinder } from './res/NodeFinder';
import { NodeDivider } from './res/NodeDivider';

import { dividedNode } from './res/DividedNode';
import { CaretPositionType } from './res/CaretPositionType';
import { NodeCreator } from './res/NodeCreator';
import { NodeStringer, stringRepresentationOfNode } from './res/NodeStringer';
import { ImgBoxComponent } from '../img-box/img-box.component';
import { creationReturn } from '../../resources/ImgCreator';
import { RichTextBoxGetter } from '../../resources/RichTextBoxGetter';
import { ImgGetter } from '../../resources/ImgGetter';
//import * as _ from 'lodash';


@Component({
  selector: 'app-rich-text-box',
  templateUrl: './rich-text-box.component.html',
  styleUrls: ['./rich-text-box.component.css','../../styles.css']
})
export class RichTextBoxComponent implements OnInit, OnDestroy{
  @ViewChild('wrapper') wrapperRef:ElementRef;
  public get wrapper():HTMLElement{
    return this.wrapperRef.nativeElement;
  };
  @ViewChild('imgSelector') imgSelectorRef:ElementRef;
  public get imgSelector():HTMLElement{
    return this.imgSelectorRef.nativeElement;
  };
  @ViewChild('valHolder') valHolderRef:ElementRef;
  public get valHolder():HTMLElement{
    return this.valHolderRef.nativeElement;
  };
  @ViewChildren(ImgBoxComponent) imgs: QueryList<ImgBoxComponent>;
  private specialKeys: Array<string> = [ 'Alt' ,'Backspace', 'Tab', 'End', 'Home', 'Shift', 'Delete', 'ArrowLeft', 'ArrowRight', 'ArrowDown', 'ArrowUp'];
  private _text:string='';
  private divider:NodeDivider;
  @Input()
  public get text():string{
    return this._text;
  }
  public set text(value:string) {
    if (value==undefined) value='';
    if (value==this._text) return;
    let node:Node=this.creator.createNodes(value,this.readonly); //pri starte testovania sa vola toto pri vkladani textu z jsonu. preto readonly=true
    let nodes:NodeListOf<ChildNode> = node.childNodes;
    this.removeAll();
    let vh:Node=this.valHolderRef.nativeElement;
    while(nodes.length>0) {
      let child:Node=nodes.item(0);
      vh.appendChild(child);
    }
    this._text=this.getTextFromValueHolder();
  }
  public buttonsVisible:boolean=true;
  @Input()
  public get readonly():boolean{
    let ce:string=this.valHolder.contentEditable;
    if (ce.toLowerCase()=='true') return false;
    else return true;
  }
  public set readonly(value:boolean) {
    if (this.finder.parentNode) {
      this.finder.forE((img:ImgBoxComponent)=>{
        img.readonly=value;
      });
    }
    if (value) {
      this.valHolder.contentEditable='false';
      this.buttonsVisible=false;
      this.unselectable=true;
    } else {
      this.valHolder.contentEditable='true';
      this.buttonsVisible=true;
      this.unselectable=false;
    }
  }
  @Input()
  public get unselectable():boolean{
    let ce:string=this.valHolder['unselectable'];
    if (ce.toLowerCase()=='on') return true;
    else return false;
  }
  public set unselectable(value:boolean) {
    this.valHolder.removeEventListener('mousedown',this.checkSelection);
    if (value) {
      this.valHolder.addEventListener('mousedown', this.checkSelection);
      document.getSelection().removeAllRanges();
      this.valHolder.style.cursor='default';
    } else {
      this.valHolder.style.cursor='text';
    }
  }
  @Output('textChange') textChange: EventEmitter <string> = new EventEmitter();
  constructor(
  //  private imgCreator:imgCreatorAndGetter,
  //  textBoxGetter:richTextBoxGetter,
    private creator:NodeCreator,
    private stringer:NodeStringer,
    private finder:NodeFinder,
    private richTextBoxGetter:RichTextBoxGetter,
    private imgGetter:ImgGetter
  ) {
    richTextBoxGetter.created(this); //memory leak
  }
  ngOnInit() {
    this.finder.parentNode=this.valHolder;
    this.divider=new NodeDivider(this.finder);
    let holder=this.valHolder;
    this.divider.deepestNode=holder;

    this.finder.forE((img:ImgBoxComponent)=>{
      img.readonly=this.readonly;
    });
  }
  ngOnDestroy() {
    this.richTextBoxGetter.remove(this);
  }
  divide() {
    console.log(this.imgGetter.count())
  }
  show() {
    console.log('show')
    console.log(this.valHolder)
    /*
    console.log(this.imgs)
    this.imgs.forEach((img:ImgBoxComponent,index:number)=>{
      console.log('a')
    });*/
  }
  removeBrAround(caretPosition:CaretPositionType) {
    let spanNode:Node;
    if (caretPosition===undefined) return;
    if (caretPosition.startN===undefined) return;
    if (caretPosition.endN===undefined) return;
    if (caretPosition.endN.nodeName=='SPAN') {
      console.log('looking at span')
      spanNode=caretPosition.endN;
    } else if (caretPosition.endN.parentNode.nodeName=='SPAN') {
      console.log('looking atchildren of  span')
      spanNode=caretPosition.endN.parentNode;
    }
    if (spanNode && spanNode.lastChild && spanNode.lastChild.nodeName=='BR') {
      if (spanNode.textContent.length==0) spanNode.parentNode.removeChild(spanNode);
      else spanNode.removeChild(spanNode.lastChild)
    }
  }
  public checkSelection(event:MouseEvent) {
    event.preventDefault();
  }
  
  private getTextFromValueHolder():string{
    return this.stringer.toString(this.valHolder);
  }
  public textChanged() {
    this._text=this.getTextFromValueHolder();
    this.textChange.emit(this._text);
  }
  private static insertAfter(parentNode:Node,after:Node,what:Node) {
    if (after) {
      let snode:Node=after.nextSibling;
      if (snode) parentNode.insertBefore(what,snode);
      else parentNode.appendChild(what);
    } else {
      let fc:Node=parentNode.firstChild;
      if (fc) parentNode.insertBefore(what,fc);
      else parentNode.appendChild(what);
    }
  }

  private static insertNewNodes(newNodes:CreatedNode,where:Node,after:Node) {
    newNodes.allNodes.forEach((newNode:Node)=>{
      RichTextBoxComponent.insertAfter(where,after,newNode);
      after=newNode;
    });
  }
  private addGlyph(tnode:OffsetNode,which:string,wantAdd:boolean) {
    if (!wantAdd) return;
    let pnode:Node=tnode.node.parentNode;
    let tNodeT:string=tnode.node.textContent;
    let newT:string;
    let newNodes:CreatedNode;
    let lastT:string='';

    tnode.node.textContent=tNodeT.substr(0,tnode.offsetInNode);
    newT=tNodeT.substring(tnode.offsetInNode,tnode.numCharsFromBegToEnd);
    lastT=tNodeT.substring(tnode.numCharsFromBegToEnd,tNodeT.length);

    let created:creationReturn=this.creator.createElementFromString(which,false);
    newNodes=new CreatedNode(created.domElement);

    RichTextBoxComponent.insertNewNodes(newNodes,pnode,tnode.node);
    let newTnode:Node=document.createTextNode(newT);
    newNodes.mainNode.appendChild(newTnode);
    if (tnode.offsetInNode==0) pnode.removeChild(tnode.node);
    if (lastT) {
      RichTextBoxComponent.insertAfter(pnode,newNodes.allNodes[newNodes.allNodes.length-1],document.createTextNode(lastT));
    }
  }
  private remGlyph(tnode:OffsetNode,which:string,nodeToRem:Node,wantRem:boolean) {
    if (!wantRem) return;
    let pnode:Node=nodeToRem.parentNode;
    while(nodeToRem.childNodes.length>0) {
      pnode.insertBefore(nodeToRem.firstChild,nodeToRem);
    }
    pnode.removeChild(nodeToRem);
    let origStart:number=tnode.numberOfCharsBeforeNode+tnode.offsetInNode;
    let origEnd:number=tnode.numberOfCharsBeforeNode+tnode.numCharsFromBegToEnd;
    let wholeStart:number=tnode.numberOfCharsBeforeNode;
    let wholeLen:number=tnode.numberOfCharsBeforeNode+tnode.node.textContent.length;
    if (wholeStart!=origStart) this.insertGlyph(wholeStart,origStart,which);
    if (origEnd!=wholeLen) this.insertGlyph(origEnd,wholeLen,which);
  }
  private getNode(pnode:Node,which:stringRepresentationOfNode):Node{
    do{
      let thisTags:stringRepresentationOfNode=this.stringer.getTagsForNode(pnode);
      if (which.isSame(thisTags)) return pnode;
      pnode=pnode.parentNode;
    }while(pnode!==this.valHolder);
    return undefined;
  }
  private willAdd(start:number,end:number,which:stringRepresentationOfNode):boolean{
    let have:number=0;
    let dontHave:number=0;
    let nodes:OffsetNode[]=this.finder.getTextNodes(start,end);
    nodes=this.finder.trim(nodes);
    nodes.forEach((on:OffsetNode)=>{
      let pnode:Node=on.node.parentNode;
      let haveIt:boolean;
      do{
        let thisTags:stringRepresentationOfNode=this.stringer.getTagsForNode(pnode);
        haveIt=which.isSame(thisTags);
        if (haveIt) break;
        pnode=pnode.parentNode;
      }while(pnode!==this.valHolder);
      let len:number=on.node.textContent.substr(on.offsetInNode,on.numCharsFromBegToEnd).length;
      if (haveIt) have+=len;
      else dontHave+=len;
    });
    if (dontHave>=have) return true;
    return false;
  }
  private insertGlyph(start:number,end:number,which:string) {
    if (start>end) return undefined;
    let ret:CaretPositionType=new CaretPositionType();
    let whichStrRepr:stringRepresentationOfNode=this.stringer.getTagsForNode(this.creator.createElementFromString(which,false).domElement);
    let wantAdd:boolean=this.willAdd(start,end,whichStrRepr);
    let tnodes:OffsetNode[];
    tnodes=this.finder.getTextNodes(start,end);
    tnodes=this.finder.trim(tnodes);
    tnodes.forEach((tnode:OffsetNode)=>{
      let nodeToRem:Node=this.getNode(tnode.node,whichStrRepr);
      if (nodeToRem) {
        this.remGlyph(tnode,which,nodeToRem,!wantAdd);
      } else {
        this.addGlyph(tnode,which,wantAdd);
      }
    });
    this.textChanged();
    return ret;
  }
  insertAttribute(where:HTMLElement,what:string,value:string) {
    let at:Attr=document.createAttribute(what);
    at.textContent=value;
    where.attributes.setNamedItem(at);
  }
  private insertImg(where:number, fileName:string, fullReader:FileReader, fileToSave:File) {
    let dnode:dividedNode=this.divider.divide(where,1);
    let props:Map<string,string>=new Map();
    props['src']=fullReader.result.toString();
    props['startingHeight']=100;
    props['whatToSave']=fileToSave;
    props['name']=fileName;
    let newNodes:CreatedNode;
    let created:creationReturn=this.creator.createElementfromNameAndMapAttrs(NodeCreator.imgNodeName,props,false);

    newNodes=new CreatedNode(created.domElement);
    let nodeBefore:Node=dnode.nodeBefore;
    let wasLast:boolean=(dnode.node.lastChild==dnode.nodeBefore);
    //(dnode.nodeBefore==dnode.nodeBefore.parentNode.lastChild);
    //(dnode.node.lastChild===dnode.nodeBefore);
    
    newNodes.allNodes.forEach((newNode:Node)=>{
      RichTextBoxComponent.insertAfter(dnode.node,nodeBefore,newNode);
      nodeBefore=newNode;
    });
    if (wasLast) {
      let span:Node=document.createElement('SPAN');
      span.appendChild(document.createTextNode(' '));
      span.appendChild(document.createElement('BR'));
      RichTextBoxComponent.insertAfter(dnode.node,nodeBefore,span);
    }
    this.textChanged();
    //console.log('tc in '+this.valHolder.nativeElement.textContent)*/
  }
  private forEachImg(whatToDo:()=>void){
    this.valHolder.childNodes.forEach((child:HTMLElement)=>{
      child
    });
  }
  private removeAll() {
    while(this.valHolder.childNodes.length>0) {
      this.valHolder.removeChild(this.valHolder.firstChild);
    }
  }

  private getRange() {
    let sel = document.getSelection();
    if (sel.rangeCount === 0) return;
    return sel.getRangeAt(0);
  }
  onKeyUp(event: KeyboardEvent) {
    let valHolder:Node=this.valHolder;
    if (valHolder.childNodes.length==0) {
      let span:Node=document.createElement('SPAN');
      let div:Node=document.createElement('DIV');
      div.appendChild(span);
      valHolder.appendChild(div);
    }
    valHolder.childNodes.forEach((valHolderChild:Node)=>{
      if (valHolderChild.nodeName!=='DIV') {
        let div:Node=document.createElement('DIV');
        let span:Node=document.createElement('SPAN');
        div.appendChild(span);
        valHolder.insertBefore(div,valHolderChild);
        span.appendChild(valHolderChild);
      } else {
        valHolderChild.childNodes.forEach((valHolderChildChild:Node)=>{
          if (valHolderChildChild.nodeName!=='SPAN') {
            let span:Node=document.createElement('SPAN');
            valHolderChild.insertBefore(span,valHolderChildChild);
            span.appendChild(valHolderChildChild);
          }
        });
      }
    });
    this.textChanged();
  }
  onKeyDown(event: KeyboardEvent) {
    let valHolder:Node=this.valHolder;
    //vh.firstChild.insertBefore(document.createElement('span'),vh.firstChild.firstChild)
    if (event.ctrlKey) {
      let ma:number = GlyphsBase.marksAbr.indexOf(event.key);
      if (ma<0) return;

      event.preventDefault();

      let caretPosition:CaretPositionType=this.getCaretOffsetFrom0();
      if (caretPosition.start>caretPosition.end) return;
      caretPosition=this.insertGlyph(caretPosition.start,caretPosition.end,GlyphsBase.marks[ma]);
      this.setCaretOffsetFrom0(caretPosition);
    }
  //  if (event.key=='Enter') {
  //    let cp:caretPositionType=this.getCaretOffsetFrom0();
  //    console.log(cp.start)
  //    this.divider.divide(cp.start);
  //    event.preventDefault();
  //  }
    if (this.specialKeys.indexOf(event.key)>=0) return;
  }
  
  onMouseDown(event) {
    let caretPosition:CaretPositionType=this.getCaretOffsetFrom0();
  }
  onMouseUp(event) {
    let caretPosition:CaretPositionType=this.getCaretOffsetFrom0();
  }

  private getCaretOffsetFrom0():CaretPositionType {
    let element:Node=<Node>this.valHolder;
    let ret:CaretPositionType = new CaretPositionType();
    let range = this.getRange();
    if (!range) {
      ret.start=0;
      ret.end=0;
      return ret;
    }
    let caretRange:Range = range.cloneRange();
    caretRange.selectNodeContents(element);
    ret.startN=range.startContainer;
    let startO:number=range.startOffset;
    caretRange.setEnd(ret.startN, startO);
    ret.start = caretRange.toString().length;
    ret.endN=range.endContainer;
    let endO:number=range.endOffset;
    caretRange.setEnd(ret.endN, endO);
    ret.end = caretRange.toString().length;
    return ret;
  }
  private setCaretOffsetFrom0(caretPosition:CaretPositionType) {
    let range = document.createRange();
    let sel = window.getSelection();
    let al:number=(<Node>this.valHolder).textContent.length;
    if (caretPosition.start==al) {
      let on:OffsetNode=this.finder.getTextNode(caretPosition.start-1,caretPosition.end-1,0);
      let nl:number=on.node.textContent.length;
      range.setStart(on.node,nl);
      range.setEnd(on.node,nl);
    } else {
      let ons:OffsetNode[]=this.finder.getTextNodes(caretPosition.start,caretPosition.end);
      range.setStart(ons[0].node,ons[0].offsetInNode);
      let lastON:OffsetNode;
      let length:number=caretPosition.end-caretPosition.start+ons[0].offsetInNode;
      ons.forEach((on:OffsetNode)=>{
        length-=on.node.textContent.length;
        lastON=on;
      });
      length+=lastON.node.textContent.length;
      range.setEnd(lastON.node,length);
    }
    
    sel.removeAllRanges();
    sel.addRange(range);
  }

  addBoldGlyph() {
    let caretPosition:CaretPositionType=this.getCaretOffsetFrom0();
    if (caretPosition.start>caretPosition.end) return;
    this.insertGlyph(caretPosition.start,caretPosition.end,'B');
    this.setCaretOffsetFrom0(caretPosition);
  }
  addItalicGlyph() {
    let caretPosition:CaretPositionType=this.getCaretOffsetFrom0();
    if (caretPosition.start>caretPosition.end) return;
    this.insertGlyph(caretPosition.start,caretPosition.end,'I');
    this.setCaretOffsetFrom0(caretPosition);
  }
  addSubGlyph() {
    let caretPosition:CaretPositionType=this.getCaretOffsetFrom0();
    if (caretPosition.start>caretPosition.end) return;
    this.insertGlyph(caretPosition.start,caretPosition.end,'SUB');
    this.setCaretOffsetFrom0(caretPosition);
  }
  addSupGlyph() {
    let caretPosition:CaretPositionType=this.getCaretOffsetFrom0();
    if (caretPosition.start>caretPosition.end) return;
    this.insertGlyph(caretPosition.start,caretPosition.end,'SUP');
    this.setCaretOffsetFrom0(caretPosition);
  }
  addBiggerGlyph() {
    let caretPosition:CaretPositionType=this.getCaretOffsetFrom0();
    if (caretPosition.start>caretPosition.end) return;
    this.insertGlyph(caretPosition.start,caretPosition.end,'BIG');
    this.setCaretOffsetFrom0(caretPosition);
  }
  addSmallerGlyph() {
    let caretPosition:CaretPositionType=this.getCaretOffsetFrom0();
    if (caretPosition.start>caretPosition.end) return;
    this.insertGlyph(caretPosition.start,caretPosition.end,'SMALL');
    this.setCaretOffsetFrom0(caretPosition);
  }
  addRedGlyph() {
    let caretPosition:CaretPositionType=this.getCaretOffsetFrom0();
    if (caretPosition.start>caretPosition.end) return;
    this.insertGlyph(caretPosition.start,caretPosition.end,'SPAN style=\'color:red\'');
    this.setCaretOffsetFrom0(caretPosition);
  }
  addGreenGlyph() {
    let caretPosition:CaretPositionType=this.getCaretOffsetFrom0();
    if (caretPosition.start>caretPosition.end) return;
    this.insertGlyph(caretPosition.start,caretPosition.end,'SPAN style=\'color:green\'');
    this.setCaretOffsetFrom0(caretPosition);
  }
  addImg() {
    let caretPosition:CaretPositionType=this.getCaretOffsetFrom0();
    if (caretPosition.startN===undefined) return;
    if (caretPosition.endN===undefined) return;
    if (caretPosition.start>caretPosition.end+1) return;
    this.imgSelector.click();
    //this.setCaretCharacterOffsetWithin(caretPosition);
    //caretPosition=this.insertGlyph(caretPosition.start,caretPosition.end,'B');
  }
  openImg(event:Event) {
    let fileName = (<any>event.target).files[0].name;
    //alert('The file "' + fileName + '" has been selected.' );
    let filelist:FileList=(<any>event.target).files;
    if (filelist.length === 0) return;
  
    //Saver.saveImg(filelist);

    let reader = new FileReader();
    reader.readAsDataURL(filelist[0]);
    console.log(filelist[0].name)
    reader.onload = (_event) => {
      let caretPosition:CaretPositionType=this.getCaretOffsetFrom0();
      this.removeBrAround(caretPosition);
      this.insertImg(caretPosition.start, fileName, reader, filelist[0]); //files[0].name
    }
  }
}