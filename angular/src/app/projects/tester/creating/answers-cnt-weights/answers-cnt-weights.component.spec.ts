import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnswersCntWeightsComponent } from './answers-cnt-weights.component';

describe('AnswersCntWeightsComponent', () => {
  let component: AnswersCntWeightsComponent;
  let fixture: ComponentFixture<AnswersCntWeightsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnswersCntWeightsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnswersCntWeightsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
