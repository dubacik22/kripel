import { Component, OnInit, Input, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { AnswersWrapper } from '../../resources/ComponentClassesCommon';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-create-answers-cnt-weights',
  templateUrl: './answers-cnt-weights.component.html',
  styleUrls: ['./answers-cnt-weights.component.css','../../styles.css']
})
export class AnswersCntWeightsComponent implements OnInit, AfterViewInit {
  private contentPropListener: Subscription;
  private valuePropListener: Subscription;
  private _context: AnswersWrapper;
  @Input()
  public get context(): AnswersWrapper {
    return this._context;
  }
  public set context(value: AnswersWrapper) {
    if (this.contentPropListener) this.contentPropListener.unsubscribe();
    if (this.valuePropListener) this.valuePropListener.unsubscribe();
    if (AnswersWrapper.isSame(value, this._context)) return;
    this._context = value;

    if (this._context) {
      this.contentPropListener = this._context.propertyChange.subscribe((valueName: string) => {
        this.contextPropertyChanged(valueName);
      });
    }
    this.detectChanges();
  }

  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private chDref: ChangeDetectorRef) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
   // this.chDref.detach();
  }

  contextPropertyChanged(valueName: string) {
    this.detectChanges();
  }
}
