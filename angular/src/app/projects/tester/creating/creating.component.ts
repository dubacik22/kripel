import { Component, OnInit, ChangeDetectorRef, AfterViewInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { TestClass } from '../resources/ComponentClassesCommon';
import { Saver } from '../resources/Saver';
import { TestHolder } from '../resources/TestHolder';
import { DialogBoxComponent } from 'src/stuff/dialogs/modalDialog.component';
import { ButtonConf } from 'src/stuff/dialogs/res/ButtonConf';
import { Subscription } from 'rxjs';
import { Session } from 'src/stuff/injectables/Session';
import { TestInfo } from '../resources/TestInfo';

@Component({
  selector: 'app-creating',
  templateUrl: './creating.component.html',
  styleUrls: ['./creating.component.css','../styles.css']
})

export class CreatingComponent implements AfterViewInit, OnDestroy {
  @Output('testsChanged') testsChanged: EventEmitter <void> = new EventEmitter();
  public set test(value:TestClass){
    this.testHolder.test = value;
    this.detectChanges();
  }
  public get test():TestClass{
    return this.testHolder.test;
  }

  get typeOfTest():number {
    if (this.testHolder.test===undefined) return 0;
    return this.test.thisTestType;
  }

  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  private testSubscribtion: Subscription;
  constructor(private saver: Saver, private testHolder: TestHolder,
    private chDref: ChangeDetectorRef, private session: Session) {}

  ngAfterViewInit() {
    this.chDref.detach();
    this.chDref.reattach();
    this.testHolder.test = undefined;
    this.testSubscribtion = this.testHolder.test$.subscribe((test: TestClass) => {
      this.detectChanges();
    });
  }
  ngOnDestroy() {
    this.testSubscribtion.unsubscribe();
  }

  createTest(event) {
    let test = new TestClass();
    this.test = test;
  }

  saveTest() {
    if (this.testHolder.test) {
      this.saver.checkIfExists(() => {
        DialogBoxComponent.set('test už existuje. otvoriť test ktorý existuje, alebo prepísat test na servri?',[
          new ButtonConf('otvoriť',(n: string) => {this.saver.loadTest(this.test.testId);}),
          new ButtonConf('prepisať',(n: string) => {this.saver.updateTest(() => {
            DialogBoxComponent.showJustInfo('uložené');
            this.testsChanged.emit();
          });}),
          new ButtonConf('zrušit')
        ]);
        DialogBoxComponent.show();
      }, () => {
        this.saver.saveWholeTest(() => {
          DialogBoxComponent.showJustInfo('uložené');
          this.testsChanged.emit();
        });
      });
    }
  }
  private getTestNamesButtons(tests: TestInfo[], toDo: (test: TestInfo, n: string) => void): ButtonConf[] {
    let buttons: ButtonConf[] = [];
    tests.forEach((test: TestInfo) => {
      let btn: ButtonConf = new ButtonConf(test.name, (n: string) => {
        toDo(test, n);
      });
      if (test.my) btn.style = {'background-color': '#404040cf'};
      else if (test.erare) btn.style = {'background-color': '#4e5041ef'};
      else if (test.foreign) btn.style = {'background-color': '#504141cf'};
      buttons.push(btn);
    });
    return buttons;
  }
  private createNShowOpenDialog(tests: TestInfo[]) {
    if (tests && (tests.length > 0)) {
      let buttons: ButtonConf[] = this.getTestNamesButtons(tests, (test: TestInfo, n: string) => {
        this.saver.loadTest(test.id);
      });
      DialogBoxComponent.set('aky test otvorit?', buttons);
      DialogBoxComponent.show();
    } else {
      DialogBoxComponent.showJustInfo('však tu nič nieje');
    }
  }
  public loadTest() {
    if (this.session.isUberAdmin()) {
      this.saver.doWithAllTests((tests: TestInfo[]) => {
        this.createNShowOpenDialog(tests);
      });
    } else {
      this.saver.doWithMyTests((tests: TestInfo[]) => {
        this.createNShowOpenDialog(tests);
      });
    }
  }
  private afterTestRemoved(id: string) {
    if (this.test) {
      if (id == this.test.testId) {
        this.testHolder.test = undefined;
      }
    }
    this.testsChanged.emit();
  }
  private createNShowRemoveDialog(testInfos: TestInfo[]) {
    if (testInfos) {
      let buttons: ButtonConf[] = this.getTestNamesButtons(testInfos, (test: TestInfo, n: string) => {
        this.saver.removeTest(test.id, (removedId: string)=>{
          this.afterTestRemoved(removedId);
        });
      });
      DialogBoxComponent.set('aky test vymazat?', buttons);
      DialogBoxComponent.show();
    } else {
      DialogBoxComponent.showJustInfo('však tu nič nieje');
    }
  }
  public removeTest() {
    if (this.session.isUberAdmin()) {
      this.saver.doWithAllTests((testInfos: TestInfo[]) => {
        this.createNShowRemoveDialog(testInfos);
      });
    } else {
      this.saver.doWithMyTests((testInfos: TestInfo[]) => {
        this.createNShowRemoveDialog(testInfos);
      });
    }
  }
}
