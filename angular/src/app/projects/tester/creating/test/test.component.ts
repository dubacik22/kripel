import { Component, ChangeDetectorRef, AfterViewInit, OnDestroy } from '@angular/core';
import { TestClass, QuestionClass, DescriptionClass, DimensionClass } from '../../resources/ComponentClassesCommon';
import { slideInOut, rotate } from '../../resources/Animations'
import { TestItemClass, AnswersWrapper } from '../../resources/ComponentClassesCommon';
import { TestHolder } from 'src/app/projects/tester/resources/TestHolder';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-creating-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css','../../styles.css'],
  animations: [
    slideInOut,
    rotate
  ]
})

export class Test implements AfterViewInit, OnDestroy {

  public get test(): TestClass{
    return <TestClass>this.testHolder.test;
  }
  public DimensionsVisible: boolean;
  public TempAnswersVisible: boolean;
  private _tempAnswers: AnswersWrapper = new AnswersWrapper(this.test, false);
  public get TempAnswers(): AnswersWrapper {
    return this._tempAnswers;
  }
  public set TempAnswers(value: AnswersWrapper) {
    if (AnswersWrapper.isSame(value, this._tempAnswers)) return;
    this._tempAnswers = value;
    this.detectChanges();
  }
  private _tempDimesion: string = '';
  public get TempDimesion(): string {
    return this._tempDimesion
  }
  public set TempDimesion(value: string) {
    if (this._tempDimesion == value) return;
    this._tempDimesion = value;
    this.detectChanges();
  }
  public addTempAnswerButtonText: string = Test.addTempAnswerButtonPlusText;
  static readonly addTempAnswerButtonOkText:string = 'ok';
  static readonly addTempAnswerButtonPlusText:string = '+';

  public get name(): string {
    return this.test.name;
  }
  public set name(value: string) {
    if (this.test.name == value) return;
    this.test.name = value;
    this.detectChanges();
  }
  public get private(): boolean {
    return this.test.private;
  }
  public set private(value: boolean) {
    if (this.test.private == value) return;
    this.test.private = value;
    this.detectChanges();
  }
  public get anonym(): boolean {
    return this.test.anonym;
  }
  public set anonym(value: boolean) {
    if (this.test.anonym == value) return;
    this.test.anonym = value;
    this.detectChanges();
  }
  public get timerUse(): boolean {
    return this.test.timer.use;
  }
  public set timerUse(value: boolean) {
    if (this.test.timer.use == value) return;
    this.test.timer.use = value;
    this.detectChanges();
  }
  public get timerType(): string {
    return this.test.timer.type ;
  }
  public set timerType(value: string) {
    if (this.test.timer.type == value) return;
    this.test.timer.type = value;
    this.detectChanges();
  }
  public get timerTimeInMin(): number {
    return this.test.timer.maxTime/60/1000;
  }
  public set timerTimeInMin(value: number) {
    this.test.timer.maxTime = value*60*1000;
  }
  public contentOfTest: string = '';

  private changeDetectionInProgress: boolean = false;
  public detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  private testSubscribtion: Subscription;
  constructor(private testHolder:TestHolder, private chDref: ChangeDetectorRef) {}
  ngAfterViewInit() {
    this.chDref.detach();
    this.testSubscribtion = this.testHolder.test$.subscribe((test: TestClass) => {
      this._tempAnswers = new AnswersWrapper(this.test, false);
      this._tempDimesion = '';
      this.detectChanges();
    });
  }
  ngOnDestroy() {
    this.testSubscribtion.unsubscribe();
  }
  public addQuestion(afterItem: TestItemClass) {
    let arrayIndex: number;
    if (afterItem == undefined) {
      arrayIndex = -1;
    } else {
      arrayIndex = this.Item2ArrayIndex(afterItem);
      if (!this.haveArrayIndex(arrayIndex)) return;
    }
    let myQuest:QuestionClass = <QuestionClass>this.test.createItem(QuestionClass.ItemType);
    this.test.items.splice(arrayIndex+1,0,myQuest);
    myQuest.dimension=this.test.getNoneDimension();
    this.detectChanges();
  }

  public addDescription(afterItem: TestItemClass) {
    let arrayIndex:number;

    if (afterItem == undefined) {
      arrayIndex = -1;
    } else {
      arrayIndex = this.Item2ArrayIndex(afterItem);
      if (!this.haveArrayIndex(arrayIndex)) return;
    }
    let myDesc:DescriptionClass=<DescriptionClass>this.test.createItem(DescriptionClass.ItemType);
    this.test.items.splice(arrayIndex+1,0,myDesc);
    this.detectChanges();
  }

  public removeItem(item: TestItemClass) {
    let i = this.Item2ArrayIndex(item);
    if (i < 0) return;
    this.test.items.splice(i, 1);
    this.detectChanges();
  }

  public moveItemUp(item: TestItemClass) {
    let index = this.Item2ArrayIndex(item);
    if (!this.haveArrayIndex(index)) return;
    let insertIndex = index - 1;
    if (!this.haveArrayIndex(insertIndex)) return;
    this.moveItem(index, insertIndex);
    this.detectChanges();
  }
  private moveItem(from: number, to: number){
    let fromItem: TestItemClass = this.test.items[from];
    let toItem: TestItemClass = this.test.items[to];
    this.test.items[from] = toItem;
    this.test.items[to] = fromItem;
  }
  public moveItemDown(item: TestItemClass) {
    let index = this.Item2ArrayIndex(item);
    if (!this.haveArrayIndex(index)) return;
    let insertIndex = index + 1;
    if (!this.haveArrayIndex(insertIndex)) return;
    this.moveItem(index, insertIndex);
    this.detectChanges();
  }
  public addTempDimension() {
    let newDimId:number = 0;
    if (this.test.dimensions.length !== 0) {
      newDimId = this.test.dimensions[this.test.dimensions.length-1].id+1;
    }
    let name:string = this.TempDimesion;
    this.TempDimesion = "";
    if (name == '') return;
    for (let i = 0; i < this.test.dimensions.length; i++) {
      if (this.test.dimensions[i].name == name) return;
    }
    let newDimension = new DimensionClass();
    newDimension.name = name;
    newDimension.id = newDimId;
    this.test.dimensions.push(newDimension);
    this.detectChanges();
  }
  public remTempDimension(index: number) {
    this.test.dimensions.splice(index, 1);
    this.detectChanges();
  }
  public addTempDimensionOnEnter(event) {
    if (event.keyCode == 13) {
      this.addTempDimension();
    }
  }
  public addTempAnswers() {
    if (this.addTempAnswerButtonText == Test.addTempAnswerButtonPlusText) {
      if (this._tempAnswers == null) return;
      if (this._tempAnswers.numberOfAnswers <= 0) return;
      this._tempAnswers.test = this.test;
      this.test.addTempAnswers(this._tempAnswers);
      this.TempAnswers = new AnswersWrapper(this.test, false);
    } else {
      this.addTempAnswerButtonText = Test.addTempAnswerButtonPlusText;
      this.TempAnswers = new AnswersWrapper(this.test, false);
    }
    this.detectChanges();
  }
  public remTempAnswer(index: number) {
    this.test.remTempAnswers(index);
    this.addTempAnswerButtonText=Test.addTempAnswerButtonPlusText;
    this.detectChanges();
  }
  public insertTempAnswer(tempAnswers:AnswersWrapper){
    this.TempAnswers = tempAnswers;
    this.addTempAnswerButtonText = Test.addTempAnswerButtonOkText;
    this.detectChanges();
  }
  public insertContent() {
    let test = JSON.parse(this.contentOfTest);
    this.test.generate4Use(test);
    this.detectChanges();
  }
  private haveArrayIndex(index:number):boolean{
    if (index < 0) return false;
    if (index >= this.test.items.length) return false;
    return true;
  }
  private Item2ArrayIndex(item: TestItemClass): number {
    if (!item) return -1;
    for (let i: number = 0; i < this.test.items.length; i++){
      if (item === this.test.items[i]) return i;
    }
    return -1;
  }

  public detailsVisible2rotation(visible: boolean): string {
    return (visible)?'down':'up';
  }
  public back(){
    this.testHolder.test = undefined;
  }
}
