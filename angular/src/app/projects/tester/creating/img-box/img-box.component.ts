import { Component, ViewChild, ElementRef, OnInit, Input, OnDestroy } from '@angular/core';
import { ImgGetter } from '../../resources/ImgGetter';

@Component({
  selector: 'app-img-box',
  templateUrl: './img-box.component.html',
  styleUrls: ['./img-box.component.css','../../styles.css']
})
export class ImgBoxComponent implements OnInit, OnDestroy {
  public index:number=0;
  @ViewChild('wrapper') wrapperRef:ElementRef;
  public get wrapper(){
    return this.wrapperRef.nativeElement;
  }
  public name:string='';
  public whatToSave:File=undefined;
  constructor(private imgGetter:ImgGetter) {
  }
  private _startingHeight:number=undefined;
  @Input()
  public get startingHeight():number{
    return this._startingHeight;
  };
  public set startingHeight(value:number){
    this._startingHeight=value;
    this.resetHW();
    this.setHW();
  };
  private _startingWidth:number=undefined;
  @Input()
  public get startingWidth():number{
    return this._startingWidth;
  };
  public set startingWidth(value:number){
    this._startingWidth=value;
    this.resetHW();
    this.setHW();
  };
  public height:number;
  public width:number;

  private firstX:number=-1;
  private firstY:number=-1;
  private oldX:number=-1;
  private oldY:number=-1;
  private horizontal:boolean=false;
  private inverseHorizontal:boolean=false;
  private vertical:boolean=false;
  private inverseVertical:boolean=false;
  private keepAspect:boolean=false;

  @Input()
  public readonly:boolean;

  @ViewChild('imgElement') imgElement:ElementRef;
  @ViewChild('box8') box8:ElementRef;
  @ViewChild('box7') box7:ElementRef;
  @ViewChild('box6') box6:ElementRef;
  @ViewChild('box5') box5:ElementRef;
  @ViewChild('box4') box4:ElementRef;
  @ViewChild('box3') box3:ElementRef;
  @ViewChild('box2') box2:ElementRef;
  @ViewChild('box1') box1:ElementRef;
  @Input()
  public get src():string{
    return this.imgElement.nativeElement.src;
  }
  public set src(value:string){
    this.setSrc(value);
  }

  ngOnDestroy(){
    this.imgGetter.remove(this);
  }

  ngOnInit() { }
  onLoadSrc(reader:FileReader){
    this.setSrc(reader.result.toString());
  }
  private setSrc(value:string){
    this.imgElement.nativeElement.src=value;
    this.imgElement.nativeElement.onload=()=>{
      this.resetHW();
      this.setHW();
    }
  }

  setHW(){
    let img=this.imgElement.nativeElement;
    img.width=this.width;
    img.height=this.height;
  }
  resetHW(){
    if ((this._startingHeight)&&(this._startingWidth)){
      this.height=this._startingHeight;
      this.width=this._startingWidth;
    } else {
      let img=this.imgElement.nativeElement;
      let ratio:number=img.naturalHeight/img.naturalWidth;
      this.height=this._startingHeight;
      this.width=this.height/ratio;
    }
  }

  private haveOpposite(){
    if ((this.firstX<0)||(this.firstY<0)) return false;
    return true;
  }
  private saveFirstPos(evt:PointerEvent){
    this.firstX=evt.clientX;
    this.firstY=evt.clientY;
    this.oldX=this.firstX;
    this.oldY=this.firstY;
  }
  private clearAll(){
    this.firstX=-1;
    this.firstY=-1;
    this.horizontal=false;
    this.inverseHorizontal=false;
    this.vertical=false;
    this.inverseVertical=false;
    this.keepAspect=false;
  }
  calcDiag(x:number,y:number):number{
    return Math.sqrt((x*x)+(y*y));
  }
  private addIncrementToWH(increment:number){
    let oldDiag:number=this.calcDiag(this.width,this.height);
    let newDiag:number=oldDiag+increment;
    this.width=this.width/oldDiag*newDiag;
    this.height=this.height/oldDiag*newDiag;
  }
  private resizeImg(newX:number,newY:number){
    if (!this.haveOpposite()) return;

    let addX:number=newX-this.oldX;
    let addY:number=newY-this.oldY;
    if (this.keepAspect){
      let increment:number=this.calcDiag(addX,addY);
      let ratio:number=this.height/this.width;
      let xltz:boolean=addX<0;
      let yltz:boolean=addY<0;
      if ((addX==0)||(addY==0)) return;
      if (this.inverseHorizontal && !this.inverseVertical){
        if (!xltz && !yltz) increment=-increment;
        if (yltz!=xltz) { // ak sa pohybujem v druhom a strvrtom kvartali
          if (yltz&&!xltz){ //druhy kvartal
            increment=addX;
            this.width+=increment;
            this.height=this.width*ratio;
          }
          if (!yltz&&xltz){ //stvrty kvartal
            increment=addY;
            this.height+=increment;
            this.width=this.height/ratio;
          }
        } else {
          this.addIncrementToWH(increment);
        }
      }
      if (!this.inverseHorizontal && !this.inverseVertical){
        if (xltz && !yltz) increment=-increment;
        if (yltz==xltz) {
          if (yltz&&xltz){ //druhy kvartal
            increment=addY;
            this.height+=increment;
            this.width=this.height/ratio;
          }
          if (!yltz&&!xltz){ //stvrty kvartal
            increment=addX;
            this.width+=increment;
            this.height=this.width*ratio;
          }
        } else {
          this.addIncrementToWH(increment);
        }
      }
      if (!this.inverseHorizontal && this.inverseVertical){
        if (xltz && yltz) increment=-increment;
        if (yltz!=xltz) { // ak sa pohybujem v druhom a strvrtom kvartali
          if (yltz&&!xltz){ //druhy kvartal
            increment=addX;
            this.width+=increment;
            this.height=this.width*ratio;
          }
          if (!yltz&&xltz){ //stvrty kvartal
            increment=addY;
            this.height+=increment;
            this.width=this.height/ratio;
          }
        } else {
          this.addIncrementToWH(increment);
        }
      }
      if (this.inverseHorizontal && this.inverseVertical){
        if (!xltz && yltz) increment=-increment;
        if (yltz==xltz) {
          if (yltz&&xltz){ //druhy kvartal
            increment=addX;
            this.width+=increment;
            this.height=this.width*ratio;
          }
          if (!yltz&&!xltz){ //stvrty kvartal
            increment=addY;
            this.height+=increment;
            this.width=this.height/ratio;
          }
        } else {
          this.addIncrementToWH(increment);
        }
      }
    } else {
      if (this.horizontal){
        if (this.inverseHorizontal) this.width=this.width-addX;
        else this.width=this.width+addX;
      }
      if (this.vertical){
        if (this.inverseVertical) this.height=this.height+addY;
        else this.height=this.height-addY;
      }
    }
    this.oldX=newX;
    this.oldY=newY;
    this.setHW();
  }
  boxClicked(evt:PointerEvent,settings:string,thisNum:number){
    this.saveFirstPos(evt);
    if (settings.indexOf('h')>=0){
      this.horizontal=true;
      if (settings.indexOf('-h')>=0) this.inverseHorizontal=true;
    }
    if (settings.indexOf('v')>=0){
      this.vertical=true;
      if (settings.indexOf('-v')>=0) this.inverseVertical=true;
    }
    if (settings.indexOf('ka')>=0) this.keepAspect=true;
    
    (<any>evt.srcElement).setPointerCapture(evt.pointerId);
  }
  boxMove(evt:PointerEvent){
    if (!this.haveOpposite()) return;
    this.resizeImg(evt.clientX,evt.clientY);
  }
  boxReleased(evt:PointerEvent){
    this.clearAll();
    (<any>evt.srcElement).releasePointerCapture(evt.pointerId)
  }
}
