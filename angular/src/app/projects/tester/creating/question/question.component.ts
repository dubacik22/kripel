import { Component, ChangeDetectorRef, AfterViewInit, Input } from '@angular/core';
import { QuestionClass, DimensionClass, AnswersWrapper, TestClass } from '../../resources/ComponentClassesCommon'
import { slideInOut, rotate } from '../../resources/Animations'
import { iNotifyPropertyChanged } from 'src/stuff/iNotifyProperyChanged';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-creating-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css','../../styles.css'],
  animations: [
    rotate
  ]
})

export class QuestionComponent implements AfterViewInit {
  private _quest: QuestionClass;
  @Input()
  public get quest(): QuestionClass {
    return this._quest;
  }
  public set quest(value: QuestionClass) {
    this.unlistenTo(this.questionSubscribtion);
    this.unlistenTo(this.answersSubscribtion);
    this._quest = value;
    this.questionSubscribtion = this.listenTo(this._quest, (propName: string) => {this.questPropertyChanged(propName);});
    this.answersSubscribtion = this.listenTo(this._quest.answers, (propName: string) => {this.answersPropertyChanged(propName);});
    this.detectChanges();
  }
  private questionSubscribtion: Subscription;
  private answersSubscribtion: Subscription;

  private _dimensions: DimensionClass[];
  @Input()
  public get dimensions(): DimensionClass[] {
    return this._dimensions;
  }
  public set dimensions(value: DimensionClass[]) {
    this._dimensions = value;
  }

  private _answersTemplate: AnswersWrapper
  @Input()
  public get answersTemplate(): AnswersWrapper {
    return this._answersTemplate;
  }
  public set answersTemplate(value: AnswersWrapper) {
    this.setAnswersTemplate(value);
    this.setQuestionAnswers(value);
  }
  setAnswersTemplate(value: AnswersWrapper) {
    if (!AnswersWrapper.isSame(value, this._answersTemplate)) {
      this._answersTemplate = value;
    }
  }
  setQuestionAnswers(value: AnswersWrapper) {
    if (this.quest && this.quest.answers && value) {
      if (!AnswersWrapper.isSame(value, this.quest.answers)) {
        this.quest.answers.generate4Use(value);
      }
    }
  }
  
  private _tempAnswers: AnswersWrapper[];
  @Input()
  public get tempAnswers(): AnswersWrapper[] {
    return this._tempAnswers;
  }
  public set tempAnswers(value: AnswersWrapper[]) {
    this._tempAnswers = value;
    this.findAnswersTemplate();
  }

  private _test: TestClass;
  @Input()
  public get test(): TestClass {
    return this._test;
  }
  public set test(value: TestClass) {
    this.unlistenTo(this.testSubscribtion);
    this._test = value;
    this.testSubscribtion = this.listenTo(this._test, (propName: string) => {this.testPropertyChanged(propName);});
    this.findAnswersTemplate();
  }
  private testSubscribtion: Subscription;

  private changeDetectionInProgress: boolean = false;
  public detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }

  constructor(private chDref: ChangeDetectorRef) {}

  ngAfterViewInit() {
    this.chDref.detach();
  }

  public questPropertyChanged(propName: string) {
    if (propName == 'answers') {
      this.unlistenTo(this.answersSubscribtion);
      this.answersSubscribtion = this.listenTo(this._quest.answers, (propName: string) => {this.answersPropertyChanged(propName);});
    }
  }
  public answersPropertyChanged(propName: string) {
    let tempAnswers: AnswersWrapper = this._test.findTempAnswers(this._quest.answers);
    if (tempAnswers) {
      this.setAnswersTemplate(tempAnswers);
    } else {
      this.setAnswersTemplate(undefined);
    }
    this.detectChanges();
  }
  public testPropertyChanged(propName: string) {
    if (propName == 'tempAnswers') {
      this.findAnswersTemplate();
      this.detectChanges();
    }
  }
  private findAnswersTemplate() {
    if (!this._answersTemplate && this._test && this._quest) {
      let tempAnswers: AnswersWrapper = this._test.findTempAnswers(this._quest.answers);
      this.setAnswersTemplate(tempAnswers);
    }
  }
  public detailsVisible2rotation(visible: boolean): string {
    return (visible)?'down':'up';
  }
  unlistenTo(subscribtion: Subscription) {
    if (subscribtion) subscribtion.unsubscribe();
  }
  listenTo(to: iNotifyPropertyChanged, toDo: (propName: string) => void): Subscription {
      if (to) return to.propertyChange.subscribe(toDo);
      return undefined;
  }
}
