import { ComponentFactoryResolver, ApplicationRef, Injector, EmbeddedViewRef, ComponentFactory, ComponentRef, Injectable, ViewContainerRef } from '@angular/core';
import { ImgBoxComponent } from "../creating/img-box/img-box.component";

@Injectable()
export class ImgCreator {
    private imgFactory: ComponentFactory<ImgBoxComponent>;

    constructor(
      componentFactoryResolver: ComponentFactoryResolver,
      private appRef: ApplicationRef,
      private injector: Injector
    ) {
        this.imgFactory = componentFactoryResolver.resolveComponentFactory(ImgBoxComponent);
    }
    public create(): creationReturn{
        let imgComponent: ComponentRef<ImgBoxComponent> = this.imgFactory.create(this.injector);
        this.appRef.attachView(imgComponent.hostView);
        let domElement: HTMLElement = (imgComponent.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
        let ret: creationReturn = new creationReturn();
        
        ret.component = imgComponent.instance;
        ret.domElement = domElement;
        return ret;
    }
}
export class creationReturn{
    domElement: HTMLElement;
    component:ImgBoxComponent;
}