export class TestClass2SaveResults {
    public items: TestItem2SaveResults[] = [];
}

export abstract class TestItem2SaveResults {
    public ItemType: string;
}
export class Description2SaveResults extends TestItem2SaveResults {
}
export class Question2SaveResults extends TestItem2SaveResults {
    public answerWeight: number;
}