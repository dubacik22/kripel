import { TestClass2Save, TestItem2Save, Description2Save, Question2Save, Answer2Save, Answers2Save, Dimension2Save, Timer2Save } from './SavedClassesCommon';
import { TestClass2SaveResults, TestItem2SaveResults, Description2SaveResults, Question2SaveResults } from './SavedClassesResults'
import { EventEmitter } from '@angular/core';
import { iNotifyPropertyChanged } from 'src/stuff/iNotifyProperyChanged';
import { Subscription } from 'rxjs';
import { SaveHelper } from './SaveHelper';


export class TestClass implements iNotifyPropertyChanged {
    public propertyChange: EventEmitter <string> = new EventEmitter();

    public name: string;
    public private: boolean;
    public anonym: boolean = true;
    public testId: string;
    public items: TestItemClass[] = [];
    public timer: Timer = new Timer();
    public thisTestType: number = 0;
    private _enabled: boolean = true;
    public get enabled(): boolean {
        return this._enabled;
    }
    public set enabled(value: boolean) {
        if (this._enabled == value) return;
        this._enabled = value;
        this.propertyChange.emit('enabled');
    }
    private _tempAnswers: AnswersWrapper[] = [];
    public get tempAnswers(): AnswersWrapper[]{
        return this._tempAnswers;
    }
    public set tempAnswers(value: AnswersWrapper[]) {
        this._tempAnswers = value;
        this.propertyChange.emit('tempAnswers');
    }
    public dimensions: DimensionClass[] = [];

    public get4Save(): TestClass2Save {
        let ret: TestClass2Save = new TestClass2Save();
        ret.name = this.name;
        ret.private = SaveHelper.bool2String(this.private);
        ret.anonym = SaveHelper.bool2String(this.anonym);
        ret.origName = this.testId;
        ret.thisTestType = this.thisTestType;
        //ret.enabled = this.enabled;
        ret.timer = this.timer.get4Save();
        this.items.forEach((item: TestItemClass) => {
            let item2Save: TestItem2Save = item.get4Save();
            ret.items.push(item2Save);
        });
        this.tempAnswers.forEach((answers: AnswersWrapper) => {
            let answs: Answers2Save = answers.get4Save();
            ret.tempAnswers.push(answs);
        });
        this.dimensions.forEach((dimension: DimensionClass) => {
            let dim: Dimension2Save = dimension.get4Save();
            ret.dimensions.push(dim);
        });
        return ret;
    }

    public getResults4Save(): TestClass2SaveResults {
        let ret: TestClass2SaveResults = new TestClass2SaveResults();
        this.items.forEach((item: TestItemClass) => {
            let item2Save: TestItem2SaveResults = item.getResults4Save();
            ret.items.push(item2Save);
        });
        return ret;
    }

    public generate4Use(savedTest: TestClass2Save) {
        this.name = savedTest.name;
        this.anonym = SaveHelper.string2Bool(savedTest.anonym);
        this.private = SaveHelper.string2Bool(savedTest.private);
        this.testId = savedTest.origName;
        this.thisTestType = savedTest.thisTestType;
        this.enabled = true;//savedTest.enabled;
        this.timer.generate4Use(savedTest.timer);
        this.getOrCreateDimension(DimensionClass.noneDimensionId, DimensionClass.noneDimensionName);
        this.getOrCreateDimension(DimensionClass.onlyGroupDimensionId, DimensionClass.onlyGroupDimensionName);
        if (savedTest.dimensions) {
            for (let i: number = 0; i < savedTest.dimensions.length; i++) {
                if (DimensionClass.isMyDim(savedTest.dimensions[i].id)) continue;
                this.dimensions[i]=new DimensionClass();
                this.dimensions[i].generate4Use(savedTest.dimensions[i]);
            }
        }
        if (savedTest.tempAnswers) {
            for (let i: number = 0; i < savedTest.tempAnswers.length; i++) {
                this.tempAnswers[i] = new AnswersWrapper(this, false);
                this.tempAnswers[i].generate4Use(savedTest.tempAnswers[i]);
            }
        }
        if (savedTest.items) {
            for(let i: number = 0; i < savedTest.items.length; i++) {
                this.items[i] = this.createNewTestItem(savedTest.items[i].ItemType);
                this.items[i].generate4Use(savedTest.items[i], this);
            }
        }
    }
    public createItem(type: string): TestItemClass {
        let item: TestItemClass = this.createNewTestItem(type);
        return item;
    }
    
    private createNewTestItem(type: string): TestItemClass {
        if (type == DescriptionClass.ItemType) return new DescriptionClass();
        if (type == QuestionClass.ItemType) return new QuestionClass(this);
    }
    public getNoneDimension(): DimensionClass {
        return this.getOrCreateDimension(DimensionClass.noneDimensionId, DimensionClass.noneDimensionName);
    }

    private getOrCreateDimension(id: number, name: string): DimensionClass {
        let ret: DimensionClass = this.getDimensionById(id);
        if (ret != undefined) return ret;
        ret = new DimensionClass();
        ret.id = id;
        ret.name = name;
        this.dimensions.push(ret);
        return ret;
    }
    private getDimensionById(id: number): DimensionClass {
        if (this.dimensions == undefined) return undefined;
        for (let i: number = 0; i < this.dimensions.length; i++) {
            let ret: DimensionClass = this.dimensions[i];
            if (ret.id == id) return ret;
        }
        return undefined;
    }
    public findTempAnswers(answers: AnswersWrapper): AnswersWrapper {
        let found: AnswersWrapper = this.tempAnswers.find((val: AnswersWrapper, index: number,obj: AnswersWrapper[]) => {
            return answers.templateStr === val.templateStr;
        });
        return found;
    }
    public insertAnswers(answers: TestClass2SaveResults) {
        let maxItem: number = Math.min(answers.items.length, this.items.length);
        for (let i: number = 0; i < maxItem; i++) {
            if ((this.items[i].ItemType == QuestionClass.ItemType) && (answers.items[i].ItemType == QuestionClass.ItemType)) {
                let question: QuestionClass = <QuestionClass>this.items[i];
                let insertingQuestion: Question2SaveResults = <Question2SaveResults>answers.items[i];
                let insertingAnswer: AnswerClass;
                if (insertingQuestion) {
                    let foundAnswer: AnswerClass = question.answers.answers.find((answer: AnswerClass) => {
                        return (answer.weight == insertingQuestion.answerWeight);
                    });
                    insertingAnswer = foundAnswer;
                }
                question.answer = insertingAnswer;
            }
        }
    }
    public removeAnswers() {
        this.items.forEach((item: TestItemClass) => {
            if (item.ItemType == QuestionClass.ItemType) {
                let question: QuestionClass = <QuestionClass>item;
                question.answer = undefined;
            }
        });
    }
    public addTempAnswers(answers: AnswersWrapper) {
        this.tempAnswers.push(answers);
        this.propertyChange.emit('tempAnswers');
    }
    public remTempAnswers(index: number) {
        this.tempAnswers.splice(index, 1);
        this.propertyChange.emit('tempAnswers');
    }
}

export abstract class TestItemClass implements iNotifyPropertyChanged {
    public propertyChange: EventEmitter <string> = new EventEmitter();

    public ItemType: string;
    public description: string;
    public newLetter: boolean;

    public generate4Use(savedItem: any, test: TestClass) {
        this.description = savedItem.description;
        this.newLetter = savedItem.newLetter;
    }
    public saveSuper(item: TestItem2Save) {
        item.ItemType = this.ItemType;
        item.description = this.description;
        item.newLetter = this.newLetter;
    }
    public resultsSaveSuper(item: TestItem2SaveResults) {
        item.ItemType = this.ItemType;
    }
    public abstract get4Save(): TestItem2Save;
    public abstract getResults4Save(): TestItem2SaveResults;
}
export class DescriptionClass extends TestItemClass {
    public readonly ItemType = "desc";
    public static readonly ItemType = "desc";

    public generate4Use(savedItem: any, test: TestClass) {
        super.generate4Use(savedItem,test);
    }
    public get4Save(): Description2Save {
        let ret: Description2Save = new Description2Save();
        super.saveSuper(ret);
        return ret;
    }
    public getResults4Save(): Description2SaveResults {
        let ret: Description2SaveResults = new Description2SaveResults();
        this.resultsSaveSuper(ret);
        return ret;
    }
}
export class QuestionClass extends TestItemClass {
    public questionDetailsVisible: boolean = false;
    public dimension: DimensionClass = new DimensionClass();
    private _answers: AnswersWrapper;
    public get answers(): AnswersWrapper {
        return this._answers;
    }
    public set answers(value: AnswersWrapper) {
        this._answers = value;
    }
    private _answer: AnswerClass = undefined;
    public get answer(): AnswerClass {
        return this._answer;
    }
    public set answer(value: AnswerClass) {
        if (AnswerClass.isSame(this._answer, value)) return;
        this._answer = value;
        this.propertyChange.emit('answer');
    }

    public readonly ItemType = "quest";
    public static readonly ItemType = "quest";
    unlistenTo(subscribtion: Subscription) {
        if (subscribtion) subscribtion.unsubscribe();
    }
    listenTo(to: iNotifyPropertyChanged, toDo: (propName: string) => void): Subscription {
        if (to) return to.propertyChange.subscribe(toDo);
        return undefined;
    }

    constructor(private test: TestClass) {
        super();
        this.answers = new AnswersWrapper(this.test, true);
    }
    public get4Save(): Question2Save {
        let ret: Question2Save = new Question2Save();
        super.saveSuper(ret);
        ret.answers = this.answers.get4Save();
        ret.questionDetailsVisible = this.questionDetailsVisible;
        ret.dimension = this.dimension.get4Save();
        return ret;
    }
    public getResults4Save(): Question2SaveResults {
        let ret: Question2SaveResults = new Question2SaveResults();
        this.resultsSaveSuper(ret);
        if (this.answer) ret.answerWeight = this.answer.weight;
        return ret;
    }
    public generate4Use(savedItem: any, test: TestClass) {
        super.generate4Use(savedItem,test);
        this.questionDetailsVisible = false;//savedItem.questionDetailsVisible;
        this.test = test;
        this.findAndSetAnswers(savedItem.answers, test.tempAnswers);
        this.findAndSetDimension(savedItem.dimension, test.dimensions);
    }
    private findAndSetDimension(dimensionToFind: any, dims: DimensionClass[]) {
        if (dimensionToFind == undefined) {
            this.dimension = this.test.getNoneDimension();
            return;
        }
        let tempDim: DimensionClass = new DimensionClass();
        tempDim.generate4Use(dimensionToFind);
        let found: DimensionClass = dims.find((val: DimensionClass) => {
            return DimensionClass.isSame(tempDim,val);
        });

        if (found !== undefined) {
            this.dimension = found;
        } else {
            this.dimension = tempDim;
        }
    }
    private findAndSetAnswers(answersToFind: any, answs: AnswersWrapper[]) {
        let tempWrapper: AnswersWrapper = new AnswersWrapper(this.test, true);

        tempWrapper.generate4Use(answersToFind);
        let found: AnswersWrapper = answs.find((val: AnswersWrapper) => {
            return tempWrapper.templateStr === val.templateStr;
        });
        if (found !== undefined) {
            this.answers.generate4Use(found);
        } else {
            this.answers.generate4Use(tempWrapper);
        }
    }
}

export class AnswersWrapper implements iNotifyPropertyChanged {
    public propertyChange: EventEmitter <string> = new EventEmitter();

    private _leftMostSign: string = '';
    public get leftMostSign(): string {
        return this._leftMostSign;
    }
    public set leftMostSign(value: string) {
        if (this._leftMostSign == value) return;
        this._leftMostSign = value;
        this.editedWrapper();
        this.propertyChange.emit('leftMostSign');
    }
    private _rightMostSign: string = '';
    public get rightMostSign(): string {
        return this._rightMostSign;
    }
    public set rightMostSign(value: string) {
        if (this._rightMostSign == value) return;
        this._rightMostSign = value;
        this.editedWrapper();
        this.propertyChange.emit('rightMostSign');
    }
    private _answerWeightsVisible: boolean = false;
    public get answerWeightsVisible(): boolean{
        return this._answerWeightsVisible;
    }
    public set answerWeightsVisible(value: boolean) {
        if (this._answerWeightsVisible == value) return;
        this._answerWeightsVisible = value;
        this.editedWrapper();
        this.propertyChange.emit('answerWeightsVisible');
    }

    private get _numberOfAnswers(): number {
        return this.answers.length;
    }
    private set _numberOfAnswers(value: number) {
        while (this.answers.length > value) this.answers.splice(this.answers.length - 1, 1);
        while (this.answers.length < value) this.answers.splice(this.answers.length, 0, new AnswerClass(this));
    }
    public get numberOfAnswers(): number {
        return this._numberOfAnswers;
    }
    public set numberOfAnswers(value: number) {
        if (this._numberOfAnswers == value) return;
        this._numberOfAnswers = value;
        this.editedWrapper();
        this.propertyChange.emit('numberOfAnswers');
    }
    public answers: AnswerClass[] = [];
    constructor(public test: TestClass, private isInQuestion: boolean){}
//zabezpecit: ked nastavim template a neskor ho zmenim, musia sa zmetit vsetky pouzitia templatu
//ked nasavim template a potom zmenim hodnotu jeho pouzitia, tak sa toto pouzitie rozviaze s templatom
    public get4Save(): Answers2Save {
        let ret: Answers2Save = new Answers2Save();
        ret.leftMostSign = encodeURI(this._leftMostSign);
        ret.rightMostSign = encodeURI(this._rightMostSign);
        ret.answerWeightsVisible = this._answerWeightsVisible;
        this.answers.forEach((answer: AnswerClass) => {
            let answer2s: Answer2Save = answer.get4Save();
            ret.answers.push(answer2s);
        });
        return ret;
    }
    public editedWeight() {
        this.editedWrapper();
        this.propertyChange.emit('weights');
    }
    public editedWrapper() {
        if (!this.isInQuestion) {
            let oldTempStr: string = this._templateStr;
            let newTempStr: string = this.templateStr;

            this.test.items.forEach((item: TestItemClass) => {
                if (item.ItemType !== QuestionClass.ItemType) return;
                let actTempStr: string = (<QuestionClass>item).answers.templateStr;
                if ((actTempStr !== newTempStr) && (actTempStr != oldTempStr)) return;
                (<QuestionClass>item).answers.generate4Use(this);
            });
        }
        this._templateStr = this.templateStr;
    }
    public generate4Use(savedAnswers: any) {
        if (savedAnswers == this) return;
        this._leftMostSign = decodeURI(savedAnswers.leftMostSign);
        this._rightMostSign = decodeURI(savedAnswers.rightMostSign);
        this._answerWeightsVisible = savedAnswers.answerWeightsVisible;

        this._numberOfAnswers = savedAnswers.numberOfAnswers;
        for(let i = 0; i < savedAnswers.answers.length; i++) {
            this.answers[i] = new AnswerClass(this);
            this.answers[i].generate4Use(savedAnswers.answers[i]);
        }
        this.propertyChange.emit('answers');
    }
    private _templateStr: string;
    public get templateStr(): string {
        let ret: string = this.leftMostSign + " (";
        if (!this._answerWeightsVisible) ret += '[';
        this.answers.forEach((answer: AnswerClass) => {
            ret += answer.weight + " ";
        })
        ret = ret.substring(0, ret.length - 1);
        if (!this._answerWeightsVisible) ret += ']';
        ret += ") " + this.rightMostSign;
        return ret;
    }

    public ascWeights() {
        let val = 1;
        for (let i = 0; i < this.answers.length; i++) {
           this.answers[i].weight = val++;
        }
        this.propertyChange.emit('answers');
    }
    public descWeights() {
        let val = this.answers.length;
        for (let i = 0; i < this.answers.length; i++) {
          this.answers[i].weight = val--;
        }
        this.propertyChange.emit('answers');
    }
    public ascSawWeights() {
        let val = -Math.round(this.answers.length / 2) + 1;
        for (let i = 0; i < this.answers.length; i++) {
          this.answers[i].weight = val++;
        }
        this.propertyChange.emit('answers');
    }
    public descSawWeights() {
        let val = Math.round(this.answers.length / 2) - 1;
        for (let i = 0; i < this.answers.length; i++) {
          this.answers[i].weight = val--;
        }
        this.propertyChange.emit('answers');
    }
    public static isSame(one: AnswersWrapper, twoo: AnswersWrapper): boolean {
        if (!one && !twoo) return true;
        if (!one || !twoo) return false;
        if (one.templateStr != twoo.templateStr) return false;
        if (one._answerWeightsVisible != twoo._answerWeightsVisible) return false;
        return true;
    }
}

export class AnswerClass {
    private _weight: number;
    public get weight(): number{
        return this._weight;
    }
    public set weight(value: number){
        this._weight = value;
        this.wrapper.editedWeight();
    }

    constructor(private wrapper: AnswersWrapper) {}
    public copyOf(copy: AnswerClass) {
        this.weight = copy.weight;
    }
    public get4Save() {
        let ret: Answer2Save = new Answer2Save();
        ret.weight = this._weight;
        return ret;
    }
    public generate4Use(answer: any) {
        this._weight = answer.weight;
    }
    public static isSame(one: AnswerClass, twoo: AnswerClass): boolean {
        if (!one && !twoo) return true;
        if (!one || !twoo) return false;
        if (one.weight != twoo.weight) return false;
        if (one.wrapper != twoo.wrapper) return false;
        return true;
    }
}
export class DimensionClass {
    public static readonly noneDimensionName: string = '-';
    public static readonly noneDimensionId: number = -1;
    public static readonly onlyGroupDimensionName: string = 'ovplyvňuje skupinu';
    public static readonly onlyGroupDimensionId: number = 0;
    public name: string;
    public id: number;
    
    public generate4Use(savedDimension: any) {
        this.id = savedDimension.id;
        this.name = savedDimension.name;
        if (this.id == DimensionClass.onlyGroupDimensionId) this.name = DimensionClass.onlyGroupDimensionName;
        else if (this.id == DimensionClass.noneDimensionId) this.name = DimensionClass.noneDimensionName;
    }
    public get4Save(): Dimension2Save {
        let ret: Dimension2Save = new Dimension2Save();
        ret.id = this.id;
        ret.name = this.name;
        return ret;
    }
    public isMyDim(): boolean {
        return DimensionClass.isMyDim(this.id);
    }
    public static isMyDim(id: number): boolean {
        if (id == DimensionClass.noneDimensionId) return true;
        if (id == DimensionClass.onlyGroupDimensionId) return true;
        return false;
    }
    static isSame(one: DimensionClass, twoo: DimensionClass) {
        return one.id == twoo.id;
    }
}

export class Timer {
    public use: boolean;

    static readonly timerType: string = "timer";
    public get timerType(): string{
        return Timer.timerType;
    }
    static readonly countdownType: string = "countdown";
    public get countdownType(): string{
        return Timer.countdownType;
    }
    public type: string = Timer.countdownType;

    public maxTime: number = 20;
    public runnedTimeInMs: number = 0;
    public startingItemIndex: number = 0;

    public get4Save(): Timer2Save{
        let ret: Timer2Save = new Timer2Save();
        ret.use = this.use;
        ret.type = this.type;
        ret.maxTime = this.maxTime;
        ret.startingItemIndex = this.startingItemIndex;
        return ret;
    }
    public generate4Use(timer: any) {
        this.type = timer.type;
        this.use = timer.use;
        this.maxTime = timer.maxTime;
        this.startingItemIndex = timer.startingItemIndex;
    }
}