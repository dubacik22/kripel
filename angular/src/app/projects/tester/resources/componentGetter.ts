export class componentGetter<T>{
    private list:T[]=[];

    constructor() {
    }
    public get(index:number):T{
        if (index<0) return undefined;
        if (index>=this.list.length) return undefined;
        return this.list[index];
    }
    public remove(what:T){
        let index:number=this.list.indexOf(what);
        this.removeIndex(index)
    }
    public removeIndex(index:number){
        this.list.splice(index,1);
    }
    public created(what:T){
        this.list.push(what);
    }
    public count():number{
        return this.list.length;
    }
}