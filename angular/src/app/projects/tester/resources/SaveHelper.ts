export class SaveHelper{
  private static readonly True: string = 'true';
  private static readonly False: string = 'false';

  public static bool2String(value: boolean): string {
    return (value) ? this.True : this.False;
  }
  public static string2Bool(value: string): boolean {
    return (value == 'true') ? true : false;
  }
}