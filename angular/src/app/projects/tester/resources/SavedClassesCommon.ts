export class TestClass2Save {
    public name: string;
    public private: string;
    public anonym: string;
    public origName: string;
    public items: TestItem2Save[] = [];
    public timer: Timer2Save = new Timer2Save();
    public thisTestType: number = 0;
    public enabled: boolean = true;
    public static readonly testType: number = 1;
    public tempAnswers: Answers2Save[] = [];
    public dimensions: Dimension2Save[] = [];
}

export abstract class TestItem2Save {
    public ItemType: string;
    public description: string;
    public newLetter: boolean;
}
export class Description2Save extends TestItem2Save {
}
export class Question2Save extends TestItem2Save {
    public questionDetailsVisible: boolean = false;
    public dimension: Dimension2Save = new Dimension2Save();
    public answers: Answers2Save = new Answers2Save();
}

export class Answers2Save {
    public leftMostSign: string;
    public rightMostSign: string;
    public answerWeightsVisible: boolean = false;
    public answers: Answer2Save[] = [];
}

export class Answer2Save {
    public weight: number;
}
export class Dimension2Save {
    public name: string;
    public id: number;
}

export class Timer2Save {
    public use: boolean;

    static readonly timerType: string = "timer";
    static readonly countdownType: string = "countdown";
    public type: string = Timer2Save.countdownType;

    public maxTime: number = 20;
    public runnedTimeInMs:number = 0;
    public startingItemIndex: number = 0;

}