import { TestClass } from './ComponentClassesCommon'
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Paths } from 'src/stuff/injectables/Paths';

@Injectable()
export class Communicator {
    public verbose: boolean = false;
    private get uploadImg(): string{
        return ''//this.paths.phpServer+'/uploadImg.php'
    }
    private get duplicateImgsToFolderUrl(): string{
        return ''//this.paths.phpServer+'/duplicateImgsToFolder.php'
    }
    constructor(private http: HttpClient, private paths: Paths){
    }
    
    private static createTestDataToGetMyNames(userId: string, session: string): any {
        let ret = {
            'toDo': 'tester_readIdsNames',
            'session': session
        }
        return ret;
    }
    private static createTestDataToGetErareNames(userId: string, session: string): any {
        let ret = {
            'toDo': 'tester_readErareNames',
            'session': session
        }
        return ret;
    }
    private static createTestDataToGetForeignNames(userId: string, session: string): any {
        let ret = {
            'toDo': 'tester_readForeignNames',
            'session': session
        }
        return ret;
    }
    private static createTestDataToGetAllResultsInfo(userId: string, session): any {
        let ret = {
            'toDo': 'tester_getResultsInfo',
            'userId': userId,
            'session': session,
        }
        return ret;
    }
    private static createTestDataToReadResults(resultsId: string, session: string): any {
        let ret = {
            'toDo': 'tester_readResults',
            'resultsId': resultsId,
            'session': session,
        }
        return ret;
    }
    private static createTestDataToSaveResultsInfo(userId: string, name: string, session: string): any {
        let ret = {
            'toDo': 'tester_saveResultsInfo',
            'userId': userId,
            'name': name,
            'session': session,
        }
        return ret;
    }
    private static createTestDataToRemWholeResults(resultsId: string, session: string): any {
        let ret = {
            'toDo': 'tester_remResults',
            'resultsId': resultsId,
            'session': session,
        }
        return ret;
    }
    private static createTestDataToSaveTestResult(userName: string, gesultsGroupId: string, test: TestClass): any{
        let results: string = JSON.stringify(test.getResults4Save());
        let ret = {
            'toDo': 'tester_saveResult',
            'results': results,
            'groupId': gesultsGroupId,
            'testId': test.testId,
            'userName': userName,
        }
        return ret;
    }
    private static createTestDataToLoadTestById(testId: string, session: string): any {
        let ret = {
            'toDo': 'tester_loadById',
            'testId': testId,
            'session': session,
        }
        return ret;
    }
    private static createTestDataToRemoveTest(testId: string, session: string): any {
        let ret = {
            'toDo': 'tester_remove',
            'testId': testId,
            'session': session,
        }
        return ret;
    }
    private static createTestDataToSaveTest(userId: string, test: TestClass, privateTest: boolean, session: string): any{
        let testData: string = JSON.stringify(test.get4Save());
        let ret = {
            'toDo': 'tester_save',
            'content': testData,
            'name': test.name,
            'userId': userId,
            'private': privateTest,
            'session': session,
        }
        return ret;
    }
    private static createTestDataToUpdateTest(test: TestClass, privateTest: boolean, session: string): any{
        let testData: string = JSON.stringify(test.get4Save());
        let ret = {
            'toDo': 'tester_update',
            'name': test.name,
            'content': testData,
            'testId': test.testId,
            'private': privateTest,
            'session': session,
        }
        return ret;
    }
    private static createCreateTablesData(session: string): any{
        let ret = {
            'toDo': 'tester_createTables',
            'session': session,
        }
        return ret;
    }
    private static createDuplicateImgData(imgs:string[],from:string,to:string):any{
        let ret = {
            'imgs' : imgs,
            'from' : from,
            'to' : to
        }
        return ret;
    }
    private static createImgData(imgs: File[], path: string): any {
        let ret = new FormData();
        ret.append('path', path);
        for (let i = 0; i < imgs.length; i++) {
            let file = imgs[i];
            ret.append('files[]', file);
        }
        return ret;
    }
    
    public createTables(session: string, whatNext?: () => void) {
        let sentData = Communicator.createCreateTablesData(session);
        if (this.verbose) {
            console.log('We sent to server createTables:');
            console.log(sentData);
        }
        this.http.post(this.paths.phpCommunicator, sentData, {responseType:'json'}).subscribe((retData: any) => {
            if (this.verbose) {
                console.log('Server returned at createTables:');
                console.log(retData);
            }
            if (retData && retData.success && whatNext) {
                whatNext();
            }
        });
    }
    public copyImgs(imgs: string[], from: string, to: string, whatNext?: (success:boolean) => void) {
        if (imgs==undefined) return;
        if (imgs.length===0) return;
        let sentData=Communicator.createDuplicateImgData(imgs,from,to);
        if (this.verbose) {
            console.log("We sent to server copyImgs:");
            console.log(sentData);
            console.log('start')
        }
        this.http.post(this.duplicateImgsToFolderUrl,sentData,{responseType:'text'}).subscribe(retData => {
            if (this.verbose) {
                console.log("Server returned at copyImgs:");
                console.log(retData);
            }
            let receivedData:any = JSON.parse(retData);
            if (whatNext) whatNext(receivedData.success);
        });

    }
    public saveImgs(imgs: File[], path: string, whatNext?: (success: boolean) => void) {
        if (imgs == undefined) return;
        if (imgs.length === 0) return;
        let sentData = Communicator.createImgData(imgs, path);
        if (this.verbose) {
            console.log("We sent to server saveImgs:"+imgs.length+' '+path);
            console.log(sentData);
        }
        this.http.post(this.uploadImg,sentData,{responseType: 'text'}).subscribe(retData => {
            if (this.verbose) {
                console.log("Server returned at saveImgs:");
                console.log(retData);
            }
            let receivedData:any = JSON.parse(retData);
            if (whatNext) whatNext(receivedData.success);
        });
    }
    public updateTest(test: TestClass, session: string, whatNext?: () => void) {
        if (test == undefined) return;
        if (!test.name) return;
        let sentData = Communicator.createTestDataToUpdateTest(test, test.private, session);
        if (this.verbose) {
            console.log("We sent to server udateTest:");
            console.log(sentData);
        }
        this.http.post(this.paths.phpCommunicator, sentData, {responseType: 'json'}).subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at udateTest:");
                console.log(retData);
            }
            if (retData.success && whatNext) whatNext();
        });
    }
    public saveTest(userId: string, test: TestClass, session: string, whatNext?: () => void) {
        if (test == undefined) return;
        if (!test.name) return;
        let sentData = Communicator.createTestDataToSaveTest(userId, test, test.private, session);
        if (this.verbose) {
            console.log("We sent to server saveTest:");
            console.log(sentData);
        }
        this.http.post(this.paths.phpCommunicator, sentData, {responseType: 'json'}).subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at saveTest:");
                console.log(retData);
            }
            if (retData.success && whatNext) whatNext();
        });
    }
    public saveResult(userName: string, resultsGroupId: string, test: TestClass, whatNext?: () => void) {
        if (test == undefined) return;
        if (!test.name) return;
        let sentData = Communicator.createTestDataToSaveTestResult(userName, resultsGroupId, test);
        if (this.verbose) {
            console.log("We sent to server saveResults:");
            console.log(sentData);
        }
        this.http.post(this.paths.phpCommunicator, sentData, {responseType: 'json'}).subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at saveResults:");
                console.log(retData);
            }
            if (retData.success && whatNext) whatNext();
        });
    }
    public loadTestById(id: string, session: string, whatNext?: (test: any) => void) {
        let sentData = Communicator.createTestDataToLoadTestById(id, session);
        if (this.verbose) {
            console.log("We sent to server loadTest:");
            console.log(sentData);
        }
        this.http.post(this.paths.phpCommunicator, sentData, {responseType: 'json'}).subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at loadTest:");
                console.log(retData);
            }
            if (retData.success && whatNext) whatNext(retData);
        });
    }
    public remTest(testId: string, session: string, whatNext?: (removedId: string) => void) {
        let sentData = Communicator.createTestDataToRemoveTest(testId, session);
        if (this.verbose) {
            console.log("We sent to server remTest:");
            console.log(sentData);
        }
        this.http.post(this.paths.phpCommunicator, sentData, {responseType: 'json'}).subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at remTest:");
                console.log(retData);
            }
            if (whatNext) whatNext(testId);
        });
    }
    public getMyTestNames(userId: string, session: string, whatNext?: (tests:any) => void) {
        if (!session) {
            whatNext(undefined);
            return;
        }
        let sentData = Communicator.createTestDataToGetMyNames(userId, session);
        if (this.verbose) {
            console.log('We sent to server getMyTestNames:');
            console.log(sentData);
        }
        this.http.post(this.paths.phpCommunicator, sentData, {responseType:'json'}).subscribe((retData: any) => {
            if (this.verbose) {
                console.log('Server returned at getMyTestNames:');
                console.log(retData);
            }
            if (whatNext) whatNext(retData);
        });
    }
    public getErareTestNames(userId: string, session: string, whatNext?: (tests: any) => void) {
        let sentData = Communicator.createTestDataToGetErareNames(userId, session);
        if (this.verbose) {
            console.log('We sent to server getAvailableTestNames:');
            console.log(sentData);
        }
        this.http.post(this.paths.phpCommunicator, sentData, {responseType:'json'}).subscribe((retData: any) => {
            if (this.verbose) {
                console.log('Server returned at getAvailableTestNames:');
                console.log(retData);
            }
            if (whatNext) whatNext(retData);
        });
    }
    public getForeignTestNames(userId: string, session: string, whatNext?: (tests: any) => void) {
        if (!session) {
            whatNext(undefined);
            return;
        }
        let sentData = Communicator.createTestDataToGetForeignNames(userId, session);
        if (this.verbose) {
            console.log('We sent to server getForeignTestNames:');
            console.log(sentData);
        }
        this.http.post(this.paths.phpCommunicator, sentData, {responseType:'json'}).subscribe((retData: any) => {
            if (this.verbose) {
                console.log('Server returned at getForeignTestNames:');
                console.log(retData);
            }
            if (whatNext) whatNext(retData);
        });
    }
    public getAllResultsInfo(userId: string, session: string, whatNext?: (retData: any) => void) {
        let sentData = Communicator.createTestDataToGetAllResultsInfo(userId, session);
        if (this.verbose) {
            console.log('We sent to server getAllResults:');
            console.log(sentData);
        }
        this.http.post(this.paths.phpCommunicator, sentData, {responseType:'json'}).subscribe((retData: any) => {
            if (this.verbose) {
                console.log('Server returned at getAllResults:');
                console.log(retData);
            }
            if (whatNext) whatNext(retData);
        });
    }
    public saveResultsInfo(userId: string, name: string, session: string, whatNext?: (retData: boolean) => void) {
        let sentData = Communicator.createTestDataToSaveResultsInfo(userId, name, session);
        if (this.verbose) {
            console.log('We sent to server saveResultsInfo:');
            console.log(sentData);
        }
        this.http.post(this.paths.phpCommunicator, sentData, {responseType:'json'}).subscribe((retData: any) => {
            if (this.verbose) {
                console.log('Server returned at saveResultsInfo:');
                console.log(retData);
            }
            if (whatNext) whatNext(retData);
        });
    }
    public remWholeResults(resultsId: string, session: string, whatNext?: () => void) {
        let sentData = Communicator.createTestDataToRemWholeResults(resultsId, session);
        if (this.verbose) {
            console.log('We sent to server saveResultsName:');
            console.log(sentData);
        }
        this.http.post(this.paths.phpCommunicator, sentData, {responseType:'json'}).subscribe((retData: any) => {
            if (this.verbose) {
                console.log('Server returned at saveResultsName:');
                console.log(retData);
            }
            if (whatNext) whatNext();
        });
    }
    public getResults(resultsId: string, session: string, whatNext?: (result: any[]) => void) {
        let sentData = Communicator.createTestDataToReadResults(resultsId, session);
        if (this.verbose) {
            console.log('We sent to server getResult:');
            console.log(sentData);
        }
        this.http.post(this.paths.phpCommunicator, sentData, {responseType:'json'}).subscribe((retData: any) => {
            if (this.verbose) {
                console.log('Server returned at getResult:');
                console.log(retData);
            }
            if (whatNext) whatNext(retData);
        });
    }
}
