import { TestClass } from "./ComponentClassesCommon";
import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs';
@Injectable()
export class TestHolder{
    private _test: TestClass;
    private testSource = new BehaviorSubject<TestClass>(this._test);
    public test$ = this.testSource.asObservable();

    public set test(value: TestClass){
        if (this._test == value) return;
        this._test = value;
        this.testSource.next(this._test);
    }
    public get test(): TestClass{
        return this._test;
    }
}