import { Injectable } from '@angular/core';
import { ImgBoxComponent } from "../creating/img-box/img-box.component";
import { creationReturn } from './ImgCreator';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ImgGetter{
    private list:ImgBoxComponent[]=[];
    private lastIndex:number=-1;
    constructor(private http:HttpClient){
    }
    public getFile(path:string,whatToDo:(data)=>any){
        this.http.get(path).subscribe((data:any) => {
            let blob = new Blob([data]);
            whatToDo(blob);
        });
    }
    public getIndex(ofWhat:ImgBoxComponent):number{
        return this.list.indexOf(ofWhat);
    }
    public get(index:number):ImgBoxComponent{
        this.clearFromOld();
        if (index<0) return undefined;
        if (this.list.length==0) return undefined;
        for(let i:number=0;i<this.list.length;i++){
            let actItem:ImgBoxComponent=this.list[i];
            if (actItem.index==index) return actItem;
        }
        return undefined;
    }
    public remove(what:ImgBoxComponent){
        let index:number=this.list.indexOf(what);
        this.removeIndex(index)
    }
    public removeIndex(index:number){
        this.list.splice(index,1);
    }
    public created(what:creationReturn){
        this.lastIndex++;
        what.component.index=this.lastIndex;
        this.insertAttribute(what.domElement,'index',this.lastIndex.toString());
        this.list.push(what.component);
        //nodeCreator.insertAttribute(ret.domElement,'index',''+this.imgCreator.getIndex(ret.component))
        //ret.domElement['index']='gvwthe'//this.imgCreator.getIndex(ret.component);
    }
    private insertAttribute(where:HTMLElement,what:string,value:string){
        let at:Attr=document.createAttribute(what);
        at.textContent=value;
        where.attributes.setNamedItem(at);
    }
    public count():number{
        this.clearFromOld();
        return this.list.length;
    }
    public getFromDom(element:HTMLElement):ImgBoxComponent{
        let index:number=parseInt(element.getAttribute('index'));
        return this.get(index);
    }
    public clearFromOld(){
        for(let i:number=0;i<this.list.length;i++){
            let item:ImgBoxComponent=this.list[i];
            if (item.wrapper){ //ked vytvaram img tak existuje component ale este nie jeho template.
                if (!item.wrapper.parentNode.parentNode || !item.wrapper.parentNode.parentNode.parentNode){
                    //samotny wrapper je v app-img-box component a pri stlaceni tlacitka (del, backspace, ctrl+x)
                    //sa dava do spanu. takze holder je az treti rodic v poradi.
                    this.list.splice(i,1);
                    i--;
                }
            }
        }
    }
}