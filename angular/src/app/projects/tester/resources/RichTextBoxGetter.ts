import { Injectable } from "@angular/core";
import { RichTextBoxComponent } from "../creating/rich-text-box/rich-text-box.component";
@Injectable()
export class RichTextBoxGetter{
    private list:RichTextBoxComponent[]=[];

    constructor() {
    }
    public get(index:number):RichTextBoxComponent{
        if (index<0) return undefined;
        if (index>=this.list.length) return undefined;
        return this.list[index];
    }
    public remove(what:RichTextBoxComponent){
        let index:number=this.list.indexOf(what);
        this.removeIndex(index)
    }
    public removeIndex(index:number){
        this.list.splice(index,1);
    }
    public created(what:RichTextBoxComponent){
        this.list.push(what);
    }
    public count():number{
        return this.list.length;
    }
}