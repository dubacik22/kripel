
import { Communicator } from './Communicator';
import { TestClass } from './ComponentClassesCommon';
import { ImgBoxComponent } from '../creating/img-box/img-box.component';
import { ImgGetter } from './ImgGetter';
import { Injectable } from '@angular/core';
import { TestHolder } from './TestHolder';
import { RichTextBoxGetter } from './RichTextBoxGetter';
import { RichTextBoxComponent } from '../creating/rich-text-box/rich-text-box.component';
import { Paths } from 'src/stuff/injectables/Paths';
import { Session } from 'src/stuff/injectables/Session';
import { User } from 'src/app/app-login/res/User';
import { TestInfo } from './TestInfo';
import { ResultsInfo } from '../results/ResultsInfo';
import { ResultInfo } from '../results/ResultInfo';

@Injectable()
export class Saver {
  public tests: TestClass[]=[];

  //private loadTestFromJSONString(name: string, content: string) {
    //let test = JSON.parse(content);
    //this.loadTestFromJSONObj(name, test);
  //}
  private loadTestFromJSONObj(testId: string, jsonObj: any): TestClass {
    let test: TestClass = new TestClass();
    test.generate4Use(jsonObj);
    test.testId = testId;
    if (!this.tests.includes(test)) this.tests.push(test);
    this.testHolder.test = test;
    return test;
  }
  constructor(private communicator: Communicator,
    private imgGetter: ImgGetter,
    private testHolder: TestHolder,
    private richTextBoxGetter: RichTextBoxGetter,
    private paths: Paths,
    private session: Session) {
      communicator.verbose = false;
  } 
  
  public createTables(whatNext?: () => void) {
    this.communicator.createTables(this.session.getEncryptedSession(), () => {
      if (whatNext) whatNext();
    });
   }

   public loadTest(id: string, whatNext?: (loadedTest: TestClass) => void) {
    this.communicator.loadTestById(id, this.session.getEncryptedSession(), (retData: any) => {
      if (!retData.test) return;
      let test: TestClass = this.loadTestFromJSONObj(id, retData.test);
      if (whatNext) whatNext(test);
    });
   }
  
  public removeTest(id: string, whatNext?: (removedId: string) => void) {
    this.communicator.remTest(id, this.session.getEncryptedSession(), whatNext)
  }
  public updateTest(whatNext?: () => void) {
    if (!this.testHolder.test) return;
    let test: TestClass = this.testHolder.test;

    for(let i: number = 0; i < this.richTextBoxGetter.count(); i++) {
      let rtb: RichTextBoxComponent = this.richTextBoxGetter.get(i);
      rtb.textChanged();
    }
    let imgsToSaveFiles: File[] = [];
    let imgsToSaveNames: string[] = [];
    let imgsToCopy: string[] = [];
    for(let i: number = 0; i < this.imgGetter.count(); i++) {
      let img: ImgBoxComponent = this.imgGetter.get(i);
      if (!img.whatToSave) {
        if (imgsToCopy.indexOf(img.name) < 0) imgsToCopy.push(img.name);
      } else {
        if (imgsToSaveNames.indexOf(img.name) < 0) {
          imgsToSaveFiles.push(img.whatToSave);
          imgsToSaveNames.push(img.name);
        }
      }
    }
    this.communicator.updateTest(test, this.session.getEncryptedSession(), () => {
      if (imgsToSaveFiles.length>0) {
        this.communicator.saveImgs(imgsToSaveFiles,this.paths.mainPath,whatNext);
      } else {
        if (whatNext) whatNext();
      }
    });
  }
  public saveWholeTest(whatNext?: () => any) {
    if (!this.testHolder.test) return;
    let test: TestClass = this.testHolder.test;
    
    for (let i: number = 0; i < this.richTextBoxGetter.count(); i++) {
      let rtb:RichTextBoxComponent=this.richTextBoxGetter.get(i);
      rtb.textChanged();
    }
    let imgList:File[]=[];
    for (let i: number = 0; i < this.imgGetter.count(); i++) {
      let img: ImgBoxComponent = this.imgGetter.get(i);
      if (!img.whatToSave) console.log('no image to save '+img.name)
      else imgList.push(img.whatToSave);
    }
    this.communicator.saveTest(this.session.getItem(User.userIdPropName), test, this.session.getEncryptedSession(), () => {
      if (imgList.length > 0) {
        this.communicator.saveImgs(imgList, this.paths.mainPath, whatNext);
      } else {
        if (whatNext) whatNext();
      }
    });
  }
  public saveWholeResult(userName: string, resultsGroupId: string, whatNext?: () => any) {
    if (!this.testHolder.test) return;
    let test: TestClass = this.testHolder.test;
    
    this.communicator.saveResult(userName, resultsGroupId, test, () => {
      if (whatNext) whatNext();
    });
  }
  public readAllResults() {

  }
  public checkIfExists(ifYes?: () => void, ifNo?: () => void) {
    if (!this.testHolder.test) return;
    let what: string = this.testHolder.test.name;
    this.getMyTestNames(this.session.getItem(User.userIdPropName), this.session.getEncryptedSession(), (items: TestInfo[])=>{
      if (items) {
        let found = items.find((value: TestInfo) => {
          return (value.name == what);
        });
        if (found){
          if (ifYes) ifYes();
        } else {
          if (ifNo) ifNo();
        }
      } else {
        ifNo();
      }
    });
  }
  private getForeignTestNames(userId: string, session: string, whatNext?: (names: TestInfo[]) => void) {
    this.communicator.getForeignTestNames(userId, session, (retData: any) => {
      let gettedTests: TestInfo[] = [];
      if (retData && retData.tests) retData.tests.forEach((gettedTest: any) => {
          let test = new TestInfo();
          test.name = gettedTest.name;
          test.id = gettedTest.id;
          test.foreign = true;
          gettedTests.push(test);
      });
      if (whatNext) whatNext(gettedTests);
    });
  }
  private getErareTestNames(userId: string, session: string, whatNext?: (names: TestInfo[]) => void) {
    this.communicator.getErareTestNames(userId, session, (retData: any) => {
      let gettedTests: TestInfo[] = [];
      if (retData && retData.tests) retData.tests.forEach((gettedTest: any) => {
          let test = new TestInfo();
          test.name = gettedTest.name;
          test.id = gettedTest.id;
          test.erare = true;
          gettedTests.push(test);
      });
      if (whatNext) whatNext(gettedTests);
    });
  }
  private getMyTestNames(userId: string, session: string, whatNext?: (names: TestInfo[]) => void) {
    this.communicator.getMyTestNames(userId, session, (retData: any) => {
      let gettedTests: TestInfo[] = [];
      if (retData && retData.tests) retData.tests.forEach((gettedTest: any) => {
          let test = new TestInfo();
          test.name = gettedTest.name;
          test.id = gettedTest.id;
          test.my = true;
          gettedTests.push(test);
      });
      if (whatNext) whatNext(gettedTests);
    });
  }
  public doWithMyTests(what: (tesiInfo: TestInfo[]) => void) {
    this.getMyTestNames(this.session.getItem(User.userIdPropName), this.session.getEncryptedSession(), (myTests: TestInfo[]) => {
      if (what) what(myTests);
    });
  }
  public doWithAvailableTests(what: (tesiInfo: TestInfo[]) => void) {
    this.getMyTestNames(this.session.getItem(User.userIdPropName), this.session.getEncryptedSession(), (myTests: TestInfo[]) => {
      this.getErareTestNames(this.session.getItem(User.userIdPropName), this.session.getEncryptedSession(), (erareTests: TestInfo[]) => {
        let tests: TestInfo[] = myTests.concat(erareTests);
        if (what) what(tests);
      });
    });
  }
  public doWithAllTests(what: (tesiInfo: TestInfo[]) => void) {
    this.getMyTestNames(this.session.getItem(User.userIdPropName), this.session.getEncryptedSession(), (myTests: TestInfo[]) => {
      this.getErareTestNames(this.session.getItem(User.userIdPropName), this.session.getEncryptedSession(), (erareTests: TestInfo[]) => {
        erareTests = myTests.concat(erareTests);
        this.getForeignTestNames(this.session.getItem(User.userIdPropName), this.session.getEncryptedSession(), (restTests: TestInfo[]) => {
          let tests: TestInfo[] = erareTests.concat(restTests);
          if (what) what(tests);
        });
      });
    });
  }
  public getAllResultsInfo(userId: string, whatNext?: (resultsInfos: ResultsInfo[]) => void) {
    this.communicator.getAllResultsInfo(userId, this.session.getEncryptedSession(), (retData: any) => {
      let resultsGroups: ResultsInfo[] = [];
        if (retData.results) {
          retData.results.forEach((gettedResults: any) => {
            let resultsGroup: ResultsInfo = new ResultsInfo();
            resultsGroup.id = gettedResults.id;
            resultsGroup.name = gettedResults.name;
            resultsGroups.push(resultsGroup);
          });
        }
      if (whatNext) whatNext(resultsGroups);
    });
  }
  public saveResultsInfo(userId: string, name: string, whatNext?: (success: boolean) => void) {
    this.communicator.saveResultsInfo(userId, name, this.session.getEncryptedSession(), (retData: any) => {
      if (whatNext) whatNext(retData.success);
    });
  }
  public remWholeResults(resultsId: string, whatNext?: () => void) {
    this.communicator.remWholeResults(resultsId, this.session.getEncryptedSession(), () => {
      if (whatNext) whatNext();
    });
  }
  public getResults(resultsId: string, whatNext?: (result: ResultInfo[]) => void) {
    this.communicator.getResults(resultsId, this.session.getEncryptedSession(), (retData: any) => {
      let results: ResultInfo[] = [];
      if (retData.results) {
        retData.results.forEach((gettedResult: any) => {
          let result: ResultInfo = new ResultInfo();
          result.id = gettedResult.id;
          result.testId = gettedResult.testId;
          result.result = gettedResult.result;
          result.testName = gettedResult.testName;
          result.userName = gettedResult.userName;
          results.push(result);
        });
      }
      if (whatNext) whatNext(results);
    });
  }
}
