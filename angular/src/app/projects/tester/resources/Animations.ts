import { trigger, transition, animate, style, state } from '@angular/animations'

export const slideInOut = trigger('slideInOut', [
    transition(':enter', [
        style({opacity: 0, transform: 'translateY(-100%) scaleY(0)'}),
        animate('200ms ease-in', style({opacity: 1, transform: 'translateY(0%) scaleY(1)'}))
    ]),
    transition(':leave', [
        animate('200ms ease-in', style({opacity: 0, transform: 'translateY(-100%) scaleY(0)'}))
    ])
]);

export const rotate = trigger('rotate', [
    state('up', style({
      transform: 'rotate(0deg)'
    })),
    state('right', style({
      transform: 'rotate(90deg)'
    })),
    state('down', style({
      transform: 'rotate(180deg)'
    })),
    state('left', style({
      transform: 'rotate(270deg)'
    })),
    transition('* => up', animate('200ms ease-in-out')),
    transition('* => right', animate('200ms ease-in-out')),
    transition('* => down', animate('200ms ease-in-out')),
    transition('* => left', animate('200ms ease-in-out'))
]);