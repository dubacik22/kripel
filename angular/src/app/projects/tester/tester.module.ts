import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestingComponent } from './testing/testing.component';
import { CreatingComponent } from './creating/creating.component';
import { QuestionComponent } from './creating/question/question.component';
import { ImgBoxComponent } from './creating/img-box/img-box.component';
import { EndTestComponent } from './testing/end-test/end-test.component';
import { RichTextBoxComponent } from './creating/rich-text-box/rich-text-box.component';
import { Test as TestCreator } from './creating/test/test.component';
import { AnswersCntWeightsComponent as CreateAnswersCntWeightsComponent } from './creating/answers-cnt-weights/answers-cnt-weights.component';
import { Test as TestTester } from './testing/test/test.component';
import { AnswersCntWeightsComponent as TestAnswersCntWeightsComponent } from './testing/answers-cnt-weights/answers-cnt-weights.component';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TestHolder } from './resources/TestHolder';
import { ImgCreator } from './resources/ImgCreator';
import { ImgGetter } from './resources/ImgGetter';
import { RichTextBoxGetter } from './resources/RichTextBoxGetter';
import { NodeDivider } from './creating/rich-text-box/res/NodeDivider';
import { NodeFinder } from './creating/rich-text-box/res/NodeFinder';
import { NodeCreator } from './creating/rich-text-box/res/NodeCreator';
import { NodeStringer } from './creating/rich-text-box/res/NodeStringer';
import { Saver } from './resources/Saver';
import { Communicator } from './resources/Communicator';
import { TesterComponent } from './tester.component';
import { StuffModule } from 'src/stuff/stuff.module';
import { Session } from 'src/stuff/injectables/Session';
import { Paths } from 'src/stuff/injectables/Paths';
import { LinkingComponent } from './linking/linking.component';
import { ResultsComponent } from './results/results.component';

@NgModule({
  declarations: [
    TesterComponent,
    TestingComponent,
    CreatingComponent,
    LinkingComponent,
    ResultsComponent,
    QuestionComponent,
    TestCreator,
    CreateAnswersCntWeightsComponent,
    TestTester,
    TestAnswersCntWeightsComponent,
    RichTextBoxComponent,
    ImgBoxComponent,
    EndTestComponent,
  ],
  entryComponents: [
    ImgBoxComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    CommonModule,
    StuffModule,
  ],
  providers: [
    TestHolder,
    ImgCreator,
    ImgGetter,
    RichTextBoxGetter,
    HttpClient,
    NodeDivider,
    NodeFinder,
    NodeCreator,
    NodeStringer,
    Saver,
    Paths,
    Session,
    Communicator,
    //ViewContainerRef,
   // dialogInjectable
  ],
  bootstrap: [TesterComponent],
  exports: [TesterComponent]
})
export class TesterModule { }
