import { Component, OnInit, EventEmitter, Output, Input, ViewChild, ElementRef } from '@angular/core';
import { creatingArgs } from '../resources/creatingArgs'
import { changeArgs } from '../resources/changeArgs'
import { errEnum } from '../resources/errEnum';
import { tuple } from '../resources/tuple';
import { SudokuComponent } from '../sudoku.component';

@Component({
  selector: 'app-number',
  templateUrl: './number.component.html',
  styleUrls: ['./number.component.css', '../styles.css']
})
export class NumberComponent implements OnInit {
  private app:SudokuComponent=undefined;
  public set App(value:SudokuComponent){
    if (this.app==undefined) this.app=value;
  }
  public get App():SudokuComponent{
    return this.app;
  }
  @ViewChild('numberHolder') numberHolderRef: ElementRef;
  private get numberHolder():HTMLElement{
    return this.numberHolderRef.nativeElement;
  }
  private get number():number{
    let bindedText:string=this.bindedText;
    if (!bindedText) return -1;
    return parseInt(bindedText);
  }
  verbose: boolean = false;
  text: any = "";
  text2: string = "";
  private _bindedText: string = '';
  textColor: string;
  backgroundImage: string;
  readonlyInput: boolean=false;
  private _readonlyColor: boolean;
  _canhave: number[][] = [];
  cannave: string;
  wantHave: number[] = [];
  set hopeWillHave(value: number[]) {
    this._hopeWillHave = value;
  }
  get hopeWillHave(): number[] {
    if (this._hopeWillHave == undefined) return [];
    return this._hopeWillHave;
  }
  private _hopeWillHave: number[];
  
  set bindedText(value: string) {
    if (value != this._bindedText) {
      this.removeAllWantHave();
      let arg: tuple<changeArgs,creatingArgs> = new tuple<changeArgs,creatingArgs>();
      let chngArgs: changeArgs = new changeArgs();
      let createArgs: creatingArgs = new creatingArgs();
      chngArgs.oldVal = this._bindedText;
      chngArgs.newVal = value;
      this._bindedText = value;
      this.numHolder.nativeElement.value = value;
      createArgs.col = this.thiscol;
      createArgs.row = this.thisrow;
      createArgs.text = this._bindedText;
      createArgs.number = this;
      arg.one = chngArgs;
      arg.twoo = createArgs;
      this.onValidate.emit(arg);
      if (!this.app.BuildGame) {
        this.unselectEachFriend();
        if (this._bindedText) this.selectFriends();
      }
    }
  }
  get bindedText(): string {
    return this._bindedText;//this.thiscol + " " + this.thisrow
  }

  wantHaveStringLength:number=0;
  private _wantHaveString:string='';
  set wantHaveItem(value: string){
    let item:String=new String(value);
    if (value===null){
      this.wantHaveStringLength=0;
      item='';
    }
    if (this.wantHaveStringLength<item.length) {
      this.wantHaveStringLength=item.length;
      item=item.substr(item.length-1);
      this.addOrRemWantHave(parseInt(item.toString()));
    } else {
      this.wantHaveStringLength=item.length;
    }
    this._wantHaveString=value;
    if (item!='') {
      this.bindedText='';
    }
  }
  get wantHaveItem():string{
    return this._wantHaveString;
  }
  

  public focused:boolean;
  @Input() thisrow: number;
  @Input() thiscol: number;

  @Output('onCreate') onCreate: EventEmitter <creatingArgs> = new EventEmitter();
  @Output('onValidate') onValidate: EventEmitter <tuple<changeArgs,creatingArgs>> = new EventEmitter();
  @Output('onArrowPressed') onArrowPressed: EventEmitter <creatingArgs> = new EventEmitter();

  private regex: RegExp = new RegExp(/^[1-9]{1}$/g);
  private specialKeys: Array<string> = [ 'Backspace', 'Tab', 'End', 'Home', 'Delete'];
  public static arrowKeys: Array<string> = [ 'ArrowDown', 'ArrowUp', 'ArrowLeft', 'ArrowRight'];
  @ViewChild('numholder') numHolder:ElementRef;
  @ViewChild('wthholder') wthHolder:ElementRef;

 // get disabled():boolean{
 //   return this._disabled;
 // }
 // set disabled(value: boolean){
 //   this._disabled=value;
 // }

  refreshCannave(){
    let ret:string="";
    this.hopeWillHave.forEach(element => {
      ret+=element.toString();
    });
    this.cannave=ret;
  }

  newStyle={};
  private _problem: errEnum=errEnum.ok;
  set problem(value: errEnum) {
    this._problem=value;
    this.resetProblemColor();
  }
  get problem():errEnum{
    return this._problem;
  }
  addErr(err: errEnum){
    if (this.problem>err) return;
    this.problem=err;
    return;
  }
  removeErr(){
    this.problem=errEnum.ok;
  }
  setStyle() {
    
  }
  set ReadonlyColor(value:boolean){
    this._readonlyColor=value;
    this.resetReadonlyColor();
  }
  get ReadonlyColor():boolean{
    return this._readonlyColor;
  }
  set ReadonlyInput(value:boolean){
    this.readonlyInput=value;
    this.ReadonlyColor=value;
  }
  get ReadonlyInput():boolean{
    return this.readonlyInput;
  }
  resetReadonlyColor(){
    if (this._readonlyColor) {
      this.addClass(this.numberHolder,'readonly');
    }
    else
    {
      this.removeClass(this.numberHolder,'readonly');
    }
  }

  resetProblemColor(){
    if (this._problem==errEnum.ok) {
      this.removeClass(this.numberHolder,'err');
    } else if (this._problem==errEnum.problem) {
      this.backgroundImage='blue';
    } else if (this._problem==errEnum.err) {
      this.addClass(this.numberHolder,'err');
    }
  }

  ngOnInit() {
    this.resetReadonlyColor();
    let args = new creatingArgs();
    args.number = this;
    args.col = this.thiscol;
    args.row = this.thisrow;
    this.onCreate.emit(args);
  }
  focusFunction(){
    if (this.bindedText) this.selectFriends();
    else this.unselectEachFriend();
  }
  focusOutFunction(){
    this.app.numbers.forEach((num:NumberComponent)=>{
      num.friendNotSelected();
    });
  }
  private unselectEachFriend(){
    this.app.numbers.forEach((num:NumberComponent)=>{
      num.friendNotSelected();
    });
  }
  private selectFriends(){
    let selectedNum:number=this.number;
    this.app.numbers.forEach((num:NumberComponent)=>{
      if (num.number==selectedNum){
        num.friendSelected();
      } else {
        num.friendNotSelected();
      }
    });
  }
  private friendSelected(){
    this.addClass(this.numberHolder,'friendSelected');
  }
  private friendNotSelected(){
    this.removeClass(this.numberHolder,'friendSelected');
  }
  
  addOrRemWantHave(value:number){
    let wh:number[]=this.wantHave;
    let iOfValue:number=this.wantHave.indexOf(value);
    if (iOfValue>=0){
      wh.splice(iOfValue,1);
      this.wantHave=wh;
    } else {
      wh.push(value);
      this.wantHave=wh;
    }
  }
  removeAllWantHave(){
    this.wantHave=[];
  }
  ngAfterViewInit() {
  }

  isArrowKey(key:string):boolean{
    for(let i:number=0;i<NumberComponent.arrowKeys.length;i++){
      if (NumberComponent.arrowKeys[i]==key) return true;
    }
    return false;
  }
  wthFocusOut(){
    this.wantHaveItem='';
  }
  navigationCheck(event: KeyboardEvent){
    if (this.isArrowKey(event.key)){
      let args = new creatingArgs();
      args.number = this;
      args.col = this.thiscol;
      args.row = this.thisrow;

      args.text = event.key;
      
      this.onArrowPressed.emit(args);
    }
  }
  onWantHaveKeydown(event: KeyboardEvent){
    this.navigationCheck(event);
  }
  onNumKeydown(event: KeyboardEvent) {
    this.navigationCheck(event);
    if (this.specialKeys.indexOf(event.key) !== -1) {
      return;
    }
    let text=this.numHolder.nativeElement.value;
    let next: string = event.key;
    if (!String(next).match(this.regex)) {
      event.preventDefault();
      return;
    }
    if (!this.app.WantToHave){
      if ((text.length==1) && (!this.readonlyInput)) {
        this.numHolder.nativeElement.value='';
      }
    }
  }
  onScroll(event:WheelEvent){
    event.preventDefault();
  }
  private addClass(element:HTMLElement,className:string){
    element.classList.add(className);
  }
  private removeClass(element:HTMLElement,className:string){
    element.classList.remove(className);
  }
}
