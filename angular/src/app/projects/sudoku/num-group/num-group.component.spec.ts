import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumGroupComponent } from './num-group.component';

describe('NumGroupComponent', () => {
  let component: NumGroupComponent;
  let fixture: ComponentFixture<NumGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
