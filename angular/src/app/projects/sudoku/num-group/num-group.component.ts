import { Component,Input,Output,EventEmitter  } from '@angular/core';
import { creatingArgs } from '../resources/creatingArgs'
import { changeArgs } from '../resources/changeArgs'
import { tuple } from '../resources/tuple'

@Component({
  selector: 'app-num-group',
  templateUrl: './num-group.component.html',
  styleUrls: ['./num-group.component.css', '../styles.css'],
  outputs: ['validate']
})
export class NumGroupComponent {
  @Input() row: number;
  @Input() col: number;
  @Output('onCreate') onCreate: EventEmitter <creatingArgs> = new EventEmitter();
  @Output('onValidate') onValidate: EventEmitter <tuple<changeArgs,creatingArgs>> = new EventEmitter();
  @Output('onArrowPressed') onArrowPressed: EventEmitter <creatingArgs> = new EventEmitter();

  constructor() { }

  onNumCreate(args: creatingArgs){
    this.onCreate.emit(args);
  }

  onNumValidate(args: tuple<changeArgs,creatingArgs>){
    this.onValidate.emit(args);
  }

  onArrowPressedInNum(keyArgs:creatingArgs){
    this.onArrowPressed.emit(keyArgs);
  }
}