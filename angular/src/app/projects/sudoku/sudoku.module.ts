import { NgModule } from '@angular/core';
import { SudokuComponent } from './sudoku.component';
import { NumberComponent } from './number/number.component';
import { NumGroupComponent } from './num-group/num-group.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    SudokuComponent,
    NumberComponent,
    NumGroupComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [SudokuComponent],
  exports: [SudokuComponent]
})
export class SudokuModule { }
