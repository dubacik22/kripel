import { NumberComponent } from '../number/number.component';
import { groupType } from './groupType';
import { GroupWithNum } from './GroupWithNum'
import { arrays } from './arrays';

export class cubeGroupWithNum extends GroupWithNum {
  saveMyNumber(number: NumberComponent) {
    if (number==undefined) return;
    if (arrays.isInArrayNumberComponent(number,this.myNumbers)) return;
    if ((Math.floor(number.thisrow/3)==Math.floor(this.row/3)) && (Math.floor(number.thiscol/3)==Math.floor(this.col/3))) this.myNumbers.push(number);
  }
  public getType():groupType{
    return groupType.cube;
  }

  protected getGroupRowFromCellRow(row:number){
    return cubeGroupWithNum.getGroupRowFromCellRow(row);
  }
  protected getGroupColFromCellCol(col:number){
    return cubeGroupWithNum.getGroupColFromCellCol(col);
  }
  static getGroupRowFromCellRow(cellRow:number):number{
    return Math.floor(cellRow/3);
  }
  static getGroupColFromCellCol(cellCol:number):number{
    return Math.floor(cellCol/3);
  }
}
