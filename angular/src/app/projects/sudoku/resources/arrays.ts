import { NumberComponent } from "../number/number.component"
import { NumCnt } from "./NumCnt"

export class arrays{
  static timerCounter:number=0;
  static saveTime() {
    if (this.timerCounter==0) this.timerCounter = Date.now()/1000;
    else this.timerCounter = Date.now()/1000 - this.timerCounter;
  }

  static duplicateNumberArray(what:number[]):number[]{
    let ret:number[]=[];
    what.forEach(num=>{
      ret.push(num);
    });
    return ret;
  }
  static getLastItem(array:any[]):any{
    let item:any = array[array.length-1];
    return item;
  }
  static getFirstItemAndRemove(array:any[]):any{
    let item:any = array.splice(0,1)[0];
    return item;
  }
    static duplicateArray(what:any[]):any[]{
      let ret:any[]=[];
      what.forEach(num=>{
        ret.push(num);
      });
      return ret;
    }
    static reverseNumberArray(arr:number[]):number[]{
      if (arr==undefined) return undefined;
      let ret:number[]=[];
      for(let i=arr.length-1;i>=0;i--){
        ret.push(arr[i]);
      }
      return ret;
    }
    public static isInArrayNumber(arr:number[],num:number):boolean{
      for(let i=0;i<arr.length;i++){
        if (arr[i]==num) return true;
      }
      return false;
    }
    public static isEachNumsInArrayNumber(arr:number[],nums:number[]):boolean{
      for(let i=0;i<nums.length;i++){
        if (!arrays.isInArrayNumber(arr,nums[i])) return false;
      }
      return true;
    }
    public static isAtLeastOneFromNumsInArrayNumber(arr:number[],nums:number[]):boolean{
      for(let i=0;i<nums.length;i++){
        if (arrays.isInArrayNumber(arr,nums[i])) return true;
      }
      return false;
    }
    static isInArrayNumberComponent(what:NumberComponent,array:NumberComponent[]):boolean{
      if (array==undefined) return false;
      for(let i=0;i<array.length;i++){
        if (array[i]==what) return true;
      }
      return false;
    }
    static haveSuccession(arr:number[],succesion:number[]):boolean{
      if (succesion.length==0) return false;
      for(let i=0;i<succesion.length;i++){
        let suc:number=succesion[i];
        if (!arrays.isInArrayNumber(arr,suc)) return false;
      }
      return true;
    }
      static haveSuccessionNumberComponent(arr:NumberComponent[],succesion:NumberComponent[]):boolean{
        if (succesion.length==0) return false;
        for(let i=0;i<succesion.length;i++){
          let suc:NumberComponent=succesion[i];
          if (!arrays.isInArrayNumberComponent(suc,arr)) return false;
        }
        return true;
      }
    static areFieldsSame(nums1:number[],nums2:number[]):boolean{
      if (nums1==nums2) return true;
      if ((nums1==undefined)||(nums2==undefined)) return false;
      if (nums1.length!=nums2.length) return false;
      if (nums1.length==0) return true;

      let n:number=0;
      for(let i:number=0;i<nums1.length;i++){
        let c1:number=nums1[i];
        for(let j:number=0;j<nums2.length;j++){
          let c2:number=nums2[j];
          if (c1==c2) {
            n++;
            break;
          }
        }
      }
      if (n==nums1.length) return true;
      return false;
    }

    static isArraysOfNumberComponentSame(nums1:NumberComponent[],nums2:NumberComponent[]){
      if (nums1==nums2) return true;
      if ((nums1==undefined)||(nums2==undefined)) return false;
      if (nums1.length!=nums2.length) return false;
      if (nums1.length==0) return true;
        
      let n:number=0;
      for(let i:number=0;i<nums1.length;i++){
        let c1:NumberComponent=nums1[i];
        for(let j:number=0;j<nums2.length;j++){
          let c2:NumberComponent=nums2[j];
          if (c1==c2) {
            n++;
            break;
          }
        }
      }
      if (n==nums1.length) return true;
      return false;
    }
  static isArraysOfAllNumberComponentSame(nums:NumberComponent[][]):boolean{
    if (nums==undefined) return false;
    if (nums.length<=1) return false;
    let isSame:boolean=true;
    let numComps:NumberComponent[]=null;
    for(let i:number=0;i<nums.length;i++){
      if (numComps==null) numComps=nums[i];
      else{
        if (numComps!=nums[i]) isSame=false;
      }
    }
    if (isSame) return true;
    for(let i:number=0;i<nums.length;i++){
      if (nums[i]==undefined) return false;
    }
    let len:number=null;
    for(let i:number=0;i<nums.length;i++){
      if (len==null) len=nums[i].length;
      else{
        if (len!=nums[i].length) return false;
      }
    }
    if (nums[0].length==0) return true;

    let mem:NumberComponent=null;
    for(let i:number=0;i<nums[0].length;i++){
      mem=nums[0][i];
      for(let j:number=1;j<nums.length;j++){
        numComps=nums[j];
        isSame=false;
        for(let k:number=0;k<nums[0].length;k++){
          if (mem===numComps[k]){
            isSame=true;
            break;
          }
        }
        if (!isSame) return false;
      }
    }
    return true;
  }
  static createSameField(field1:number[],field2:number[]):number[]{
    let ret:number[]=[];
    field1.forEach(num1=>{
      field2.forEach(num2=>{
        if (num1==num2) ret.push(num1);
      });
    });
    return ret;
  }
  static turnBackwards(arr:number[]){
    let mem:number;
    let back:number=arr.length-1;
    let front:number=0;
    while(back-front>1){
      mem=arr[front];
      arr[front]=arr[back];
      arr[back]=mem;

      front++;
      back--;
    }
  }
  static descending(arr:number[]):boolean{
    if (arr==undefined) return false;
    if (arr!=undefined){
      let oldNum:number=0;
      let num:number;
      for(let i:number=0;i<arr.length;i++){
        num=arr[i];
        if (oldNum>num){
          return false;
        }
        oldNum=num;
      }
    }
    return true;
  }
  static isNumbersSame(arr:number[]):boolean{
    let mem=undefined;
    for(let i:number=0;i<arr.length;i++){
      let element:number=arr[i];
      if (mem==undefined) mem=element;
      else{
        if (mem==element) return true;
      }
    }
    return false;
  }
  static isAnyNumbersSame(arr:number[]):boolean{
    if (arr===undefined) return false;
    if (arr===null) return false;
    for(let i:number=0;i<arr.length;i++){
      let mem:number=arr[i];
      for(let j:number=0;j<arr.length;j++){
        if (i==j)continue;
        let element:number=arr[j];
        if (mem==element) return true;
      }
    }
    return false;
  }
  static tosstring(field: NumberComponent[]):string{
    let ret:string="";
    field.forEach((pf)=>{
      ret+=pf.thisrow.toString()+','+pf.thiscol.toString()+'-';
    });
    return ret;
  }
  static toString(field:NumCnt[]):string
  {
    let ret:string="";
    field.forEach((pf)=>{
      ret+=pf.num.toString()+','+pf.cnt.toString()+'-';
    });
    return ret;
  }
  static tostring(field:number[]):string
  {
    let ret:string="";
    if (field===undefined) return 'undef';
    if (field===null) return 'null';
    field.forEach((el)=>{
      ret+=el.toString();
    });
    return ret;
  }
}