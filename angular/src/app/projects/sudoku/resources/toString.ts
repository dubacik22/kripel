import {NumCnt} from './NumCnt'

export class toString{
static NumCntFtoString(field:NumCnt[]):string
{
  let ret:string="";
  field.forEach((pf)=>{
    ret+=pf.num.toString()+','+pf.cnt.toString()+'-';
  });
  return ret;
}
static NumFtostring(field:number[]):string
{
  let ret:string;
  field.forEach((pf)=>{
    ret+=pf.toString();
  });
  return ret;
}
}