import { NumberComponent } from '../number/number.component';
import { groupType } from './groupType';
import { GroupWithNum } from './GroupWithNum';
import { arrays } from './arrays';

export class verticalGroupWithNum extends GroupWithNum {
  saveMyNumber(number: NumberComponent) {
    if (number==undefined) return;
    if (arrays.isInArrayNumberComponent(number,this.myNumbers)) return;
    if (number.thiscol==this.col) this.myNumbers.push(number);
  }
  public getType():groupType{
    return groupType.vertical;
  }
  static getGroupRowFromCellRow(cellRow:number):number{
    return 0;
  }
  static getGroupColFromCellCol(cellCol:number):number{
    return cellCol;
  }
}
