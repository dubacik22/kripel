import { SudokuComponent } from '../sudoku.component'
import { NumberComponent } from '../number/number.component';
;import { brutForceResultType } from './brutForceResultType';
import { arrays } from './arrays';
import { solverClass } from './solverClass';

export class brutForceResultStackType{
    public stack:brutForceResultType[]=[];
    constructor(private app:SudokuComponent){
    }
    
    undoLast():brutForceResultType{
      if (this.stack.length==0) return undefined;
      let step:brutForceResultType=arrays.getLastItem(this.stack);
      if (step===undefined) return undefined;
      if (step.oldCells===undefined) return step;
      for(let i:number=this.app.numbers.length-1;i>=0;i--){
        if (step.oldCells[i]!=='') {
          this.app.numbers[i].bindedText=step.oldCells[i];
          this.app.numbers[i].hopeWillHave=[];
        } else {
          this.app.numbers[i].bindedText='';
          this.app.numbers[i].hopeWillHave=step.oldHwh.pop();
        }
      }
      return step;
    }
    saveNew():brutForceResultType{
      let step:brutForceResultType=arrays.getLastItem(this.stack);
      for(let i:number=0;i<this.app.numbers.length;i++){
        if (this.app.numbers[i].bindedText) step.oldCells[i]=this.app.numbers[i].bindedText;
        else {
          step.oldCells[i]='';
          step.oldHwh.push(arrays.duplicateNumberArray(this.app.numbers[i].hopeWillHave))
        }
      }
  
      return step;
    }
    createNew():brutForceResultType{
      let step:brutForceResultType=new brutForceResultType();
      this.stack.push(step);
  
      step.hwhl=2
      step.i=0;
      step.addedHwhI=-1;
  
      return step;
    }
    doBFStep(step:brutForceResultType,solver:solverClass){
      step.addedHwhI++;
      for(let hwhl:number=step.hwhl;hwhl<=this.app.cols;hwhl++){
        for(;step.i<this.app.numbers.length;step.i++){
          let element:NumberComponent=this.app.numbers[step.i];
          if (element.bindedText) continue;
          if (element.hopeWillHave.length==hwhl){
            if (step.addedHwhI<element.hopeWillHave.length){
              element.bindedText=element.hopeWillHave[step.addedHwhI].toString();
              solver.transformed.push(element);
              return step;
            }
          }
          step.addedHwhI=0;
        }
        step.i=0;
      }
    }
  }