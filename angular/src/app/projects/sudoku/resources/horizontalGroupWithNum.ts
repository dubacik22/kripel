import { NumberComponent } from '../number/number.component';
import { groupType } from './groupType';
import { GroupWithNum } from './GroupWithNum'
import { arrays } from './arrays';

export class horizontalGroupWithNum extends GroupWithNum {
  saveMyNumber(number: NumberComponent) {
    if (number==undefined) return;
    if (arrays.isInArrayNumberComponent(number,this.myNumbers)) return;
    if (number.thisrow==this.row) this.myNumbers.push(number);
  }
  public getType():groupType{
    return groupType.horizontal;
  }
  static getGroupRowFromCellRow(cellRow:number):number{
    return cellRow;
  }
  static getGroupColFromCellCol(cellCol:number):number{
    return 0;
  }
}
