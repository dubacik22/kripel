import { NumberComponent } from '../number/number.component'

export class creatingArgs {
    row: number;
    col: number;
    text: string;
    number: NumberComponent;
    mode: number;

    public static createDummyArqs(number:NumberComponent): creatingArgs{
        let args:creatingArgs = new creatingArgs();
        args.number=number;
        args.row=number.thisrow;
        args.col=number.thiscol;
        args.mode=0;
        return args
    }
}