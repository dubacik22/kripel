import { NumberComponent } from "../number/number.component";
import { groupType } from "./groupType";
import { SudokuComponent } from "../sudoku.component";

export class nums{
  nums:NumberComponent[]=[];
  num:number;
  private _haveFixRow:boolean;
  public get haveFixRow():boolean{
    return this._haveFixRow;
  }
  private _haveFixCol:boolean;
  public get haveFixCol():boolean{
    return this._haveFixCol;
  }

  constructor(private app:SudokuComponent){
    //this.nums=arrays.duplicateArray(nums);
  }

  isOneWay():boolean{
    if (this.nums.length<=1) {
      this._haveFixRow=false;
      this._haveFixCol=false;
      return false; //alebo true
    }
    this._haveFixRow=true;
    this._haveFixCol=true;
    for(let i=1;i<this.nums.length;i++){
      if (this.get(0).thisrow!=this.get(i).thisrow) this._haveFixRow=false;
      if (this.nums[0].thiscol!=this.get(i).thiscol) this._haveFixCol=false;
      if (!this._haveFixRow && !this._haveFixCol) return false;
    }
    return true;
  }


  getAndRemove(i:number):NumberComponent{
    let ret:NumberComponent;
    if (i>=this.nums.length) return undefined;
    ret=this.nums[i];
    let index=this.nums.indexOf(ret);
    this.nums.splice(index,1);
    return ret;
  }
  get(i:number):NumberComponent{
    if (i>=this.nums.length) return undefined;
    return this.nums[i];
  }
  size():number{
    return this.nums.length;
  }
  removeI(i:number){
    if (i>=this.nums.length) return undefined;
    this.nums.splice(i,1);
  }
  remove(what:NumberComponent){
    let index:number=this.nums.indexOf(what);;
    this.nums.splice(index,1);
  }
  isOneOrDoubleWay():isDoubleReturnType{
    let ret:isDoubleReturnType=new isDoubleReturnType();
    if (this.nums.length<2) return ret;
    ret.freeRow=this.missingCoord(groupType.horizontal);
    ret.freeCol=this.missingCoord(groupType.vertical);
    return ret;
  }
  haveCoord(haveCoord:number,which:groupType):boolean{
    for(let i=0;i<this.nums.length;i++)
    {
      let element:NumberComponent=this.nums[i];
      let coord:number;
      coord=(which==groupType.horizontal)?element.thisrow:element.thiscol;
      if (coord%this.app.maxCubeCoord==haveCoord%this.app.maxCubeCoord) return true;
    }
    return false;
  }
  missingCoord(which: groupType):number{
    let maxCoord:number=3;
    let dontHaveCoords:number[]=[];
    for(let possibleCoord:number=0;possibleCoord<maxCoord;possibleCoord++){
      if (!this.haveCoord(possibleCoord,which)) dontHaveCoords.push(possibleCoord);
    }
    if (dontHaveCoords.length>=1) return dontHaveCoords[0];
    return -1;
  }
  clear(){
    this.nums=[];
    this._haveFixCol=false;
    this._haveFixRow=false;
  }
}
export class isDoubleReturnType{
  freeRow:number=-1;
  freeCol:number=-1;
  get isHorizontal():boolean{
    return this.freeRow>=0;
  }
  get isVertical():boolean{
    return this.freeCol>=0;
  }
}
