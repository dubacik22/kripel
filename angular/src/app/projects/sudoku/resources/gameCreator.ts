import { tuple } from "./tuple";
import { SudokuComponent } from '../sudoku.component';
import { solverClass } from './solverClass';
import { errEnum } from "./errEnum";
import { NumberComponent } from "../number/number.component";

export class gameCreator{
  private app:SudokuComponent;
  game:number[][]=[];
  lastAddedCoord:tuple<number,number>=new tuple<number,number>();

  constructor(app:SudokuComponent){
      this.app=app;
      this.createEmptyGame(this.game);
  }
  createEmptyGame(where:number[][]){
    for(let i:number=0;i<this.app.cols;i++){
      where[i]=[];
      for(let j:number=0;j<this.app.cols;j++){
        where[i][j]=-1;
      }
    }
  }
  getRandom(max:number){
    return Math.floor(Math.random()*max);
  }
  insertNextNewNumToSolution(){
    do{
      this.lastAddedCoord.one=this.getRandom(this.app.cols);
      this.lastAddedCoord.twoo=this.getRandom(this.app.cols);
    }while(this.game[this.lastAddedCoord.one][this.lastAddedCoord.twoo]>0);
    this.game[this.lastAddedCoord.one][this.lastAddedCoord.twoo]=this.getRandom(this.app.cols-1)+1;
  }
  private insertNextOldNumToSolution(){
      let num:NumberComponent;
    do{
      this.lastAddedCoord.one=this.getRandom(this.app.cols);
      this.lastAddedCoord.twoo=this.getRandom(this.app.cols);
      num=this.app.getNumber(this.lastAddedCoord.one,this.lastAddedCoord.twoo);
    }while(num.readonlyInput);
    this.game[this.lastAddedCoord.one][this.lastAddedCoord.twoo]=parseInt(num.bindedText);
  }
  private insertGame(){
    this.app.clearAllClicked();
    this.app.BuildGame=true;
    //this.app.q1=''
    for(let i:number=0;i<this.app.cols;i++){
      for(let j:number=0;j<this.app.cols;j++){
        if (this.game[i][j]>0) {
          this.app.getNumber(i,j).bindedText=this.game[i][j].toString();
        }
      }
    }
    this.app.BuildGame=false;
  }

  haveErr():boolean{
    for(let i:number=0;i<this.app.numbers.length;i++){
      let element:NumberComponent=this.app.numbers[i];
      if (element.problem==errEnum.err) return true;
    }
    return false;
  }
  trySolveWithoutBrutForce(){
    let solver:solverClass = new solverClass(this.app);
    solver.solveWthoutBrutForce();
  }
  trySolveWithBrutForce(){
    let solver:solverClass = new solverClass(this.app);
    solver.solveWithBrutForce();
  }
  trySolveEasyMode(){
    let solver:solverClass = new solverClass(this.app);
    solver.solveEasyMode();
  }
  remLastNum(){
    if (this.lastAddedCoord==undefined) return;
    this.game[this.lastAddedCoord.one][this.lastAddedCoord.twoo]=-1;
  }
  createSnapshotGame():number[][]{
    let ret:number[][]=[];
    this.createEmptyGame(ret);
    for(let i:number=0;i<this.app.cols;i++){
      for(let j:number=0;j<this.app.cols;j++){
        let actNum:NumberComponent=this.app.getNumber(i,j);
        if (actNum.bindedText) ret[j][i]=parseInt(actNum.bindedText);
        else ret[j][i]=-1;
      }
    }
    return ret;
  }
  restoreSnapshotGame(snapshot:number[][]){
    if (snapshot==undefined) return;
    for(let i:number=0;i<this.app.cols;i++){
      for(let j:number=0;j<this.app.cols;j++){
        let actNum:NumberComponent=this.app.getNumber(j,i);
        let actSnapshotNum:number=snapshot[i][j];
        if (actSnapshotNum>0) actNum.bindedText=actSnapshotNum.toString();
        else actNum.bindedText='';
      }
    }
  }
  create(difficulty:number){
    this.createEmptyGame(this.game);
    let i:number=0;
    do{
      this.insertNextNewNumToSolution();
      this.insertGame();
      this.trySolveEasyMode();
      if (this.haveErr()) {
        this.remLastNum();
      } else {
        if (i>=10) break;
        i++;
      }
    }while(!this.app.win);
    if (this.app.win) {
      this.app.clearClicked();
      return;
    }
    this.trySolveWithBrutForce();
    let s:number[][]=this.createSnapshotGame();
    do{
      this.restoreSnapshotGame(s);
      this.insertNextOldNumToSolution();
      this.insertGame();
      if (difficulty==0){
        this.trySolveEasyMode();
      } else if (difficulty==1){
        this.trySolveWithoutBrutForce();
      } else {
        this.trySolveWithBrutForce();
      }
      if (this.haveErr()) {
        this.remLastNum();
      }
    }while(!this.app.win);
    this.app.clearClicked();
    //*/
  }
}