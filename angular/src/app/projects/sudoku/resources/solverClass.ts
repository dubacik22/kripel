import {NumberComponent} from '../number/number.component'
import {SudokuComponent} from '../sudoku.component'
import { groupType } from './groupType'
import { nums, isDoubleReturnType, } from './Nums'
import { NumCnt } from './NumCnt'
import { toString } from './toString'
import { arrays } from './arrays'
import { cubeGroupWithNum } from './cubeGroupWithNum';
import { GroupWithNum } from './GroupWithNum';
import { brutForceResultType } from './brutForceResultType';
import { brutForceResultStackType } from './brutForceResultStackType';

export class solverClass{
  private hopeWillHaveChanged:boolean=false;
  public hopeWillHaveChangedToBindedText:boolean=true;
  
  constructor(private app:SudokuComponent){
    this.undoBrutForceStack=new brutForceResultStackType(this.app);
  }
  solveWithBrutForce(){
    this.solve(true);
  }
  solveWthoutBrutForce(){
    this.solve(false);
  }
  solveEasyMode(){
    do{
      this.hopeWillHaveChanged=false;
      if (this.hopeWillHaveChangedToBindedText){
        this.createCanHave();
        this.unitCanHaveAndMakeHopeWillHave();
      }
      this.hopeWillHaveChangedToBindedText=false;
      
      this.removeOtherThanWholeNthExistenceOfNNumbers();
    //  this.removeNextToNthExistenceInCube();
    //  this.remOtherThan4ExistenceInCube();
    //  this.purifyExistenceOfNNumbers();
    //  this.removeOtherThanXNumsInSquare();
      

  //    this.makeStillFromSingleInGroup();
      this.makeSillFromSingle();
      this.clearTranformed();
    }while(this.hopeWillHaveChanged || this.hopeWillHaveChangedToBindedText);

    this.updateCannave();
  }
  solve(useBrutForce:boolean){
    this.solveEasyMode();

    this.app.bfInfo='';
    do{
      this.hopeWillHaveChanged=false;
      if (this.hopeWillHaveChangedToBindedText){
        this.createCanHave();
        this.unitCanHaveAndMakeHopeWillHave();
      }
      this.hopeWillHaveChangedToBindedText=false;
      
    //  this.removeOtherThanWholeNthExistenceOfNNumbers();
      this.removeNextToNthExistenceInCube();
      this.remOtherThan4ExistenceInCube();
      this.purifyExistenceOfNNumbers();
      this.removeOtherThanXNumsInSquare();
      
      if (useBrutForce && !this.hopeWillHaveChanged && !this.app.win){
        this.app.bfInfo='brut force';
        if (this.brutForce()){
          return;
        }
      }

      this.makeStillFromSingleInGroup();
      this.makeSillFromSingle();
      this.clearTranformed();
    }while(this.hopeWillHaveChanged || this.hopeWillHaveChangedToBindedText);

    this.updateCannave();
  }
  private updateCannave(){
    this.app.numbers.forEach(element=>{
      element.refreshCannave();
    });
  }
  private createCanHaveHorizontal(){
    let needHaveField:number[];
    let needHave:number;
    for(let numberIndex:number=0;numberIndex<this.app.numbers.length;numberIndex++){
      needHaveField=this.createNeedHaveField();
      let element:NumberComponent=this.app.numbers[numberIndex];

      if (!element.bindedText)
      {
        let row:number=element.thisrow;
        for(let col:number=0; col<this.app.cols; col++)
        {
          let number:NumberComponent=this.app.getNumber(row,col);
          for(needHave=this.app.cols; needHave>0; needHave--){
            if (number.bindedText)
            if ((needHave).toString()==number.bindedText){
              let index:number=needHaveField.indexOf(needHave);
              needHaveField.splice(index,1);
              break;
            }
          }
        }
        element._canhave[groupType.horizontal]=needHaveField;
      }
    }
  }

  private createCanHaveVertical(){
    let needHaveField:number[];
    let needHave:number;
  
    for(let numberIndex:number=0; numberIndex<this.app.numbers.length; numberIndex++){
      needHaveField=this.createNeedHaveField();
          
      let element:NumberComponent=this.app.numbers[numberIndex];
  
      if (!element.bindedText)
      {
        let col:number=element.thiscol;
        for(let row:number=0; row<this.app.cols; row++)
        {
          let number:NumberComponent=this.app.getNumber(row,col);
          for(needHave=this.app.cols; needHave>0; needHave--){
            if ((needHave).toString()==number.bindedText){
              let index:number=needHaveField.indexOf(needHave);
              needHaveField.splice(index,1);
              break;
            }
          }
        }
        element._canhave[groupType.vertical]=needHaveField;
      }
    }
  }
  private createCanHaveCube(){
    let needHaveField:number[];
    let needHave:number;
   
    for(let numberIndex:number=0; numberIndex<this.app.numbers.length; numberIndex++){
      needHaveField=this.createNeedHaveField();
        
      let element:NumberComponent=this.app.numbers[numberIndex];
  
      if (!element.bindedText)
      {
        for(let col=Math.floor(element.thiscol/3)*3; col<Math.floor(element.thiscol/3)*3+3; col++)
        {
          for(let row=Math.floor(element.thisrow/3)*3; row<Math.floor(element.thisrow/3)*3+3; row++)
          {
            let number=this.app.getNumber(row,col);
            for(needHave=this.app.cols; needHave>0; needHave--){
              if ((needHave).toString()==number.bindedText){
                let index:number=needHaveField.indexOf(needHave);
  
                needHaveField.splice(index,1);
                break;
              }
            }
          }
        }
        element._canhave[groupType.cube]=needHaveField;
      }
    }
  }
  private createCanHave(){
    this.createCanHaveHorizontal();
    this.createCanHaveVertical();
    this.createCanHaveCube();
  }
  private createNeedHaveField():number[]{
    let ret:number[]=[];
    for(let x:number=0;x<this.app.cols;x++){
      ret.push(x+1);
    }
    return ret;
  }
  
  private unitCanHaveAndMakeHopeWillHave(){
    let reallyCanhave:number[]=undefined;
    let hopeWillHave:number[];
    this.app.numbers.forEach(element=>{
      if (!element.bindedText){
        reallyCanhave=undefined;
       
        element._canhave.forEach(canhave=>{
  
          if (reallyCanhave==undefined){
            reallyCanhave=canhave;
          }
          else
          {
            reallyCanhave=arrays.createSameField(canhave,reallyCanhave);
          }
        });
        element.hopeWillHave=reallyCanhave;
        this.hopeWillHaveChanged=true;
      }
    });
  }
  private areHopeWillHaveSame(num1:NumberComponent,num2:NumberComponent):boolean{
    if (num1==num2) return true;
    if ((num1==undefined)||(num2==undefined)) return false;

    return arrays.areFieldsSame(num1.hopeWillHave,num2.hopeWillHave)
  }

  private getNextNum(row:number,col:number,offset:number,dir:groupType):NumberComponent{
    let nextRow=row;
    let nextCol=col;
    if (dir==groupType.horizontal){
      nextCol+=offset;
      nextCol=nextCol%this.app.cols;
    }else if (dir==groupType.vertical){
      nextRow+=offset;
      nextRow=nextRow%this.app.cols;
    }else if (dir==groupType.cube){
      let groupRow:number=Math.floor(row/this.app.maxCubeCoord);
      let groupCol:number=Math.floor(col/this.app.maxCubeCoord);
      let rowAddition:number=row%this.app.maxCubeCoord;
      let colAddition:number=col%this.app.maxCubeCoord;

      nextCol=groupCol*this.app.maxCubeCoord + ((colAddition+offset)%this.app.maxCubeCoord);
      nextRow=groupRow*this.app.maxCubeCoord + (rowAddition+Math.floor((offset+colAddition)/this.app.maxCubeCoord))%this.app.maxCubeCoord;
    }
    return this.app.getNumber(nextRow,nextCol)
  }

  private haveSpaceOneWay(where:cubeGroupWithNum, num:number):isDoubleReturnType{
    let n:nums=new nums(this.app);
    where.myNumbers.forEach(element=>{
      if (arrays.isInArrayNumber(element.hopeWillHave,num)) n.nums.push(element);
    });
    return n.isOneOrDoubleWay();
  }
  private haveHwhInBindedTextInRectangle(row:number,col:number,hwh:number,which:groupType):boolean{
    let stringHwh:string=hwh.toString();
    for(let i=0;i<this.app.maxCubeCoord;i++){
      let cube2=(which==groupType.horizontal)?this.app.getCubeInHorizontal(row,col,i):this.app.getCubeInVertical(row,col,i);
      for(let j:number=0;j<cube2.myNumbers.length;j++){
        let element:NumberComponent=cube2.myNumbers[j];
        if (element.bindedText==stringHwh){
          return true;
        }
      }
    }
    return false;
  }
  private remOtherThan4ExistenceInCube(){
    let ignoreHorizontal=false;
    let ignoreVertical=false;
    let ret:boolean=true;
    for(let i=0;i<this.app.maxCubeCoord;i++){
      for(let j=0;j<this.app.maxCubeCoord;j++){
        let cube:cubeGroupWithNum=this.app.getCube(i,j);
        //pre kazdu kocku
        for(let hwh=1;hwh<=this.app.cols;hwh++){
          //pre kazde jedno cislo ktore v nej moze byt

          ignoreHorizontal=this.haveHwhInBindedTextInRectangle(i,j,hwh,groupType.horizontal);
          ignoreVertical=this.haveHwhInBindedTextInRectangle(i,j,hwh,groupType.vertical);
          if (ignoreHorizontal && ignoreVertical){
           continue;
          }
          let cube2:cubeGroupWithNum;
          let isDoubleRet:isDoubleReturnType=this.haveSpaceOneWay(cube,hwh);
          let isDoubleRet2:isDoubleReturnType=new isDoubleReturnType();
          let nci:number;
          if (isDoubleRet.isHorizontal && !ignoreHorizontal){
            for(let nci2=1;nci2<this.app.maxCubeCoord;nci2++){
              cube2=this.app.getCubeInHorizontal(i,j,nci2);
              isDoubleRet2=this.haveSpaceOneWay(cube2,hwh);
              if (isDoubleRet2.freeRow==isDoubleRet.freeRow){
                nci=nci2;
                break;
              }
            }
            if (isDoubleRet2.freeRow==isDoubleRet.freeRow){
              for(let nci2=1;nci2<this.app.maxCubeCoord;nci2++){
                if (nci2!=nci){
                  cube2=this.app.getCubeInHorizontal(i,j,nci2);
                  cube2.myNumbers.forEach(element=>{
                    if (element.thisrow%this.app.maxCubeCoord!=isDoubleRet.freeRow){
                      this.removeHopeWillHaveButIgnore(hwh,element,undefined);
                    }
                  });
                }
              }
            }
          }
          else if (isDoubleRet.isVertical && !ignoreVertical){
            for(let nci2=1;nci2<this.app.maxCubeCoord;nci2++){
              cube2=this.app.getCubeInVertical(i,j,nci2);
              isDoubleRet2=this.haveSpaceOneWay(cube2,hwh);
              if (isDoubleRet2.freeCol==isDoubleRet.freeCol){
                nci=nci2;
                break;
              }
            }
            if (isDoubleRet2.freeCol==isDoubleRet.freeCol){
              for(let nci2=1;nci2<this.app.maxCubeCoord;nci2++){
                if (nci2!=nci){
                  cube2=this.app.getCubeInVertical(i,j,nci2);
                  cube2.myNumbers.forEach(element=>{
                    if (element.thiscol%this.app.maxCubeCoord!=isDoubleRet.freeCol){
                      this.removeHopeWillHaveButIgnore(hwh,element,undefined);
                    }
                  });
                }
              }
            }
          }
        }
      }
    }
  }


  private removeNextToNthExistenceInCube(){
    let numbers:nums=new nums(this.app);
    for(let i=0;i<this.app.numbers.length;i++){
      let num:NumberComponent=this.app.numbers[i];
      if (!num.bindedText){
        let hopeWillHave:number[]=num.hopeWillHave;
        for(let j=0;j<hopeWillHave.length;j++){
          let thisHopeWillHave:number=hopeWillHave[j];
          numbers.num=thisHopeWillHave;
          let offset:number=0;
          let nextCube:NumberComponent;
          let cntHwh:number=0;
          do{
            offset++;
            nextCube=this.getNextNum(num.thisrow,num.thiscol,offset,groupType.cube);
            if (!nextCube.bindedText && arrays.isInArrayNumber(nextCube.hopeWillHave,thisHopeWillHave)) {
              numbers.nums.push(nextCube);
            }
          }while(nextCube!=num);

          if (numbers.isOneWay()){
            if (numbers.haveFixRow){
              let nextNum:NumberComponent;
              let offset1:number=0;
              do{
                offset1++;
                nextNum=this.getNextNum(num.thisrow,num.thiscol,offset1,groupType.horizontal);
                if (!nextNum.bindedText){
                  this.removeHopeWillHaveButIgnore(thisHopeWillHave,nextNum,numbers.nums);
                }
              }while(nextNum!=num)
            }
            if (numbers.haveFixCol){
              let nextNum:NumberComponent;
              let offset1:number=0;
              do{
                offset1++;
                nextNum=this.getNextNum(num.thisrow,num.thiscol,offset1,groupType.vertical);
                if (!nextNum.bindedText){
                  this.removeHopeWillHaveButIgnore(thisHopeWillHave,nextNum,numbers.nums);
                }
                }while(nextNum!=num)
            }
          }
          numbers.clear();
        }
      }
    }
  }
  
  private removeHopeWillHaveButIgnore(remHopeWillHave:number,where:NumberComponent,ignore:NumberComponent[]){
    if (where.bindedText) return;
    if (arrays.isInArrayNumberComponent(where,ignore)) return;
    let order:number=where.hopeWillHave.indexOf(remHopeWillHave);
    if (order>=0){
      where.hopeWillHave.splice(order,1);
      this.hopeWillHaveChanged=true;
    }
  }
  private leaveHopeWillHave(where:NumberComponent,ignore:number[]){
    if (arrays.haveSuccession(where.hopeWillHave,ignore) && (where.hopeWillHave.length!=ignore.length)){
      where.hopeWillHave=arrays.duplicateNumberArray(ignore);
      this.hopeWillHaveChanged=true;
    }
  }
  private removeOtherThanWholeNthExistenceOfNNumbers(){
    groupType.values().forEach(gt => {
      this.removeOtherThanWholeNExistenceOfNNumbers(groupType[gt]);
    });
  }
  private removeOtherThanWholeNExistenceOfNNumbers(gt:groupType){
    let offset:number=0;
    let cnt:number;
    let len:number;
    let sameNums:NumberComponent[]=[];
    let toRem:number[]=[];
    let nextNum:NumberComponent;
    this.app.numbers.forEach(num=>{
      sameNums=[];
      sameNums.push(num);

      if (!num.bindedText){
        len=num.hopeWillHave.length;
        cnt=1;
        offset=0;
        do{
          offset++;
          nextNum=this.getNextNum(num.thisrow,num.thiscol,offset,gt);
          if ((num!=nextNum)&&(this.areHopeWillHaveSame(num,nextNum))){
            cnt++;
            sameNums.push(nextNum);
          }
        }while(num!=nextNum);
        if (cnt==len){
          toRem=arrays.duplicateNumberArray(num.hopeWillHave);
          offset=0;
          do{
            offset++;
            nextNum=this.getNextNum(num.thisrow,num.thiscol,offset,gt);
            if ((num!=nextNum) && (!nextNum.bindedText) && (undefined==sameNums.find((element,index,array)=>{return element==nextNum;})))
            {
              this.removeCurrentPartOfSuccession(nextNum,toRem);
            }
          }while(num!=nextNum);
        }
      }
    });
  }
  private removeCurrentPartOfSuccession(numC:NumberComponent,succesion:number[]){
    let hopeWillHave:number[]=numC.hopeWillHave;
    let i=0;
    let removed:boolean;
    while(i<hopeWillHave.length){
      let num:number=hopeWillHave[i];
      for(let j=0;j<succesion.length;j++){
        let suc:number=succesion[j];
        removed=false;
        if (suc==num) {
          hopeWillHave.splice(i,1);
          this.hopeWillHaveChanged=true;
          removed=true;
          break;
        }
      }
      if (!removed) i++;
    }
  }
  private findCellsWithAllHwh(element:NumberComponent,hwh:number[],gt:groupType):NumberComponent[]{
    let ret:NumberComponent[]=[];
    let offset:number=1;
    let nextNum:NumberComponent;
    do{
      nextNum=this.getNextNum(element.thisrow,element.thiscol,offset,gt);
      if (arrays.isEachNumsInArrayNumber(nextNum.hopeWillHave,hwh)) ret.push(nextNum);
      offset++;
    }while(element!=nextNum);
    return ret;
  }
  private findCellsWithWhateverFromHwh(element:NumberComponent,hwh:number[],gt:groupType):NumberComponent[]{
    let ret:NumberComponent[]=[];
    let offset:number=1;
    let nextNum:NumberComponent;
    do{
      nextNum=this.getNextNum(element.thisrow,element.thiscol,offset,gt);
      if (arrays.isAtLeastOneFromNumsInArrayNumber(nextNum.hopeWillHave,hwh)) ret.push(nextNum);
      offset++;
    }while(element!=nextNum);
    return ret;
  }

  private purifyExistenceOfNNumbers(){
    for(let i:number=0;i<this.app.cols;i++){
      this.purifyExistenceOfNNumbersForDirectionAndElement(groupType.horizontal,this.app.getNumber(i,0));
      this.purifyExistenceOfNNumbersForDirectionAndElement(groupType.vertical,this.app.getNumber(0,i));
    }
    for(let i:number=0;i<this.app.maxCubeCoord;i++){
      for(let j:number=0;j<this.app.maxCubeCoord;j++){
        this.purifyExistenceOfNNumbersForDirectionAndElement(groupType.cube,this.app.getNumber(i*this.app.maxCubeCoord,j*this.app.maxCubeCoord));
      }
    }
    this.app.numbers.forEach(element=>{
      element.verbose=true;
    });
  }
  private addIncrementToAscendingNumberArray(ret:number[],increment:number):boolean{
    while (increment>0){
      if (ret[ret.length-1]+increment>this.app.cols){
        increment-=this.app.cols-ret[ret.length-1]+1;

        let j:number=1;
        while(ret[ret.length-j-1]==(this.app.cols-j)){
          j++;
        }
        if (j==ret.length) return false;
        ret[ret.length-j-1]+=1;
        for(let i=j;i>0;i--){
          ret[ret.length-i]=ret[ret.length-i-1]+1;
        }
      }
      else
      {
        ret[ret.length-1]+=increment;
        increment=0;
      }
    }
    return true;
  }
  private createSequence(cnt:number,offset:number):number[]{
    if (cnt>this.app.cols) return undefined;
    let ret:number[]=[];
    for(let n=0;n<cnt;n++){
      ret[n]=n+1;
    }
    if (!this.addIncrementToAscendingNumberArray(ret,offset)) return undefined;
    
    return ret;
  }
  private timesOfExistenceOfNumber(num:number,element:NumberComponent,dir:groupType):number{
    let actelement:NumberComponent=undefined;
    let actOffset:number=1;
    let existence:number=0;
    do{
      actelement=this.getNextNum(element.thisrow,element.thiscol,actOffset,dir)
      if (arrays.isInArrayNumber(actelement.hopeWillHave,num)){
        existence++;
      }
      actOffset++;
    }while(element!=actelement);
    return existence;
  }

  private purifyExistenceOfNNumbersForDirectionAndElement(dir:groupType,element:NumberComponent){
    let numToRem:NumberComponent=undefined;
    let offsetToRem:number=0;
    let cellsWithAllHwh:NumberComponent[]=[];
    let cellsWithAtLeastOneFromHwh:NumberComponent[]=[];
    let nums:number[]=[];
    let offset:number=1;
    let space:number=0;
    arrays.saveTime();
    do{
      numToRem=this.getNextNum(element.thisrow,element.thiscol,offset,dir);
      if (!numToRem.bindedText){
        space++;
      }
      offset++;
    }while(element!=numToRem);

    let timess:number[]=[];
    let cnt:number=0;
    for(let i=1;i<=this.app.cols;i++){
      let times:number=this.timesOfExistenceOfNumber(i,element,dir);
      if (arrays.isInArrayNumber(timess,times) || (times==0)) continue;
      timess.push(times);
      cnt=times;
      offset=0;
      let sequenceOfHwh:number[]=undefined;
      let firstSequenceOfHwh:number[]=null;
      let asc:boolean=true;
      while(arrays.tostring(sequenceOfHwh)!=arrays.tostring(firstSequenceOfHwh)){
        if (sequenceOfHwh!=undefined) {
          cellsWithAllHwh=this.findCellsWithAllHwh(element,sequenceOfHwh,dir);
          cellsWithAtLeastOneFromHwh=this.findCellsWithWhateverFromHwh(element,sequenceOfHwh,dir);

          if (cellsWithAllHwh.length==cnt && cellsWithAtLeastOneFromHwh.length===cnt){
            offsetToRem=1;
            do{
              numToRem=this.getNextNum(element.thisrow,element.thiscol,offsetToRem,dir)
              if (arrays.isInArrayNumberComponent(numToRem,cellsWithAllHwh)){
                this.leaveHopeWillHave(numToRem,sequenceOfHwh);
              }
              offsetToRem++;
            }while(element!=numToRem);
          }
        }
        if (firstSequenceOfHwh===null) firstSequenceOfHwh=sequenceOfHwh;
        sequenceOfHwh=this.createSequence(cnt,offset);
        offset++;
      }
    }
  }

  private findNumsWitPartOfHwhInDir(findNum:number,dir:groupType,offsetCell:number):NumberComponent[]{
    let ret:NumberComponent[]=[];
    let dim:number=0;
    let y:number=0;
    dim=offsetCell%this.app.cols;
    let group:GroupWithNum=this.app.getGroupFromCellCoord(dim,dim,dir);
    for(let cn:number=0;cn<group.myNumbers.length;cn++){
      let numC:NumberComponent=group.myNumbers[cn];
      if (arrays.isInArrayNumber(numC.hopeWillHave,findNum)) ret.push(numC);
    }
    return ret;
  }
  private isInGroupInHave(num:number,dir:groupType,dim:number){
    let group:GroupWithNum=this.app.getGroupFromCellCoord(dim,dim,dir);
    let stringNum:string=num.toString();
    for(let i=0;i<group.myNumbers.length;i++){
      if (group.myNumbers[i].bindedText===stringNum) return true;
    }
    return false;
  }

  private haveDimInNumerComponentArray(arrs:NumberComponent[][],dim:number,which:groupType){
    for(let cs:number=0; cs<arrs.length; cs++){
      let dimToCompare:number;
      if (which==groupType.horizontal){
        dimToCompare=arrs[cs][0].thisrow;
      } else if (which==groupType.vertical){
        dimToCompare=arrs[cs][0].thiscol;
      }
      if (dimToCompare==dim) return true;
    }
    return false;
  }
  private alreadyHaveEachDim(arrs:NumberComponent[][],arr:NumberComponent[],which:groupType){
    let haveDim:boolean;
    for(let ai1:number=0;ai1<arr.length;ai1++){
      haveDim=false;
      let origDim:number;
      if (which==groupType.horizontal){
        origDim=arrs[0][ai1].thisrow;
      } else if (which==groupType.vertical){
        origDim=arrs[0][ai1].thiscol;
      }
      for(let ai0:number=0;ai0<arrs[0].length;ai0++){
        let dimToCompare:number;
        if (which==groupType.horizontal){
          dimToCompare=arr[ai0].thisrow;
        } else if (which==groupType.vertical){
          dimToCompare=arr[ai0].thiscol;
        }
        if (origDim===dimToCompare){
          haveDim=true;
          break; 
        }
      }
      if (!haveDim) return false;
    }
    return true;
  }
  private removeOtherThanXNumsInSquare(){
    this.removeOtherThanXNumsInSquareInDir(groupType.horizontal);
    this.removeOtherThanXNumsInSquareInDir(groupType.vertical);
  }
  
  private removeOtherThanXNumsInSquareInDir(dir:groupType){
    let oppositeDir:groupType;
    if (dir==groupType.horizontal){
      oppositeDir=groupType.vertical;
    } else if (dir==groupType.vertical){
      oppositeDir=groupType.horizontal;
    }
    for(let num=1;num<=this.app.cols;num++){
      //cez vsetky cisla
      for(let dim=0;dim<this.app.cols;dim++){

        let cellsWithHwhArr:NumberComponent[][]=[];
        let lastDim:number;
        let hwhCnt:number;
        let hwhCnts:number[]=[];
        //cez vsetky stlpceky alebo riadky
        if (this.isInGroupInHave(num,oppositeDir,dim)) continue;
        cellsWithHwhArr[0]=this.findNumsWitPartOfHwhInDir(num,oppositeDir,dim);
        hwhCnt=cellsWithHwhArr[0].length;

        if (arrays.isInArrayNumber(hwhCnts,hwhCnt)) continue;
        hwhCnts.push(hwhCnt);
        lastDim=dim;
        if (lastDim===9) break;
        
        for(let i:number=1; i<hwhCnt; i++){
          let foundArr:boolean=false;
          for(let dim=lastDim+1;dim<this.app.cols;dim++){
            //este raz cez vsetky stlpceky
            if (this.isInGroupInHave(num,oppositeDir,dim)) continue;
            if (this.haveDimInNumerComponentArray(cellsWithHwhArr,dim,oppositeDir)) continue; //okrem tych ktore uz mam.
            let cellsWithHwh=this.findNumsWitPartOfHwhInDir(num,oppositeDir,dim);
            if (cellsWithHwh.length!==hwhCnt) continue;
            if (!this.alreadyHaveEachDim(cellsWithHwhArr,cellsWithHwh,dir)) continue;
            cellsWithHwhArr[i]=cellsWithHwh;
            foundArr=true;
            break;
          }
          if (!foundArr) break;
        }

        if (hwhCnt==cellsWithHwhArr.length){
          //ak mam vsade rovnaky pocet prvkov
          for(let ai0:number=0;ai0<hwhCnt;ai0++){
            let dim:number;
            if (dir==groupType.horizontal){
              dim=cellsWithHwhArr[0][ai0].thisrow;
            } else if (dir==groupType.vertical){
              dim=cellsWithHwhArr[0][ai0].thiscol;
            }
            let group:GroupWithNum=this.app.getGroupFromCellCoord(dim,dim,dir);
            for(let ai1:number=0;ai1<group.myNumbers.length;ai1++){
              let cellToRemHwh:NumberComponent=group.myNumbers[ai1];
              let isIncellsWithHwhArr:boolean=false;
              for(let ai2:number=0;ai2<cellsWithHwhArr.length;ai2++){
                if (cellsWithHwhArr[ai2][ai0]===cellToRemHwh){
                isIncellsWithHwhArr=true;
                break;
                }
              }
              if (isIncellsWithHwhArr) continue;
              if (group.myNumbers[ai1].bindedText) continue;
              this.removeHopeWillHaveButIgnore(num,group.myNumbers[ai1],undefined);
            }
          }
        }
      }
    }
  }
  private makeSillFromSingle(){
    for(let i=0; i<this.app.numbers.length; i++){
      let element:NumberComponent = this.app.numbers[i];
      if (!element.bindedText)
      {
        this.transformHopeWillHaveToGonnaHave(element);
      }
    }
  }
  private transformHopeWillHaveToGonnaHave(num:NumberComponent)
  {
    if (num.hopeWillHave.length==1) {
      num.bindedText=num.hopeWillHave[0].toString();
      this.transformed.push(num);
    }
  }
  private makeStillFromSingleInGroup(){
    for(let i=0; i<this.app.numbers.length; i++){
      let element:NumberComponent = this.app.numbers[i];
      if (!element.bindedText)
      {
        this.transformOneHopeWillHaveInGroupToGonnaHave(element);
      }
    }
  }
  private isNumNull(num:NumberComponent):boolean{
    return (num==undefined) || (num==null);
  }
  private hasSameCoords(row1:number,col1:number,row2:number,col2:number):boolean{
    return (row1==row2) && (col1==col2);
  }
  private getNextHopeWillHaveH(origRow:number,origCol:number,i:number):number{
    let row:number=origRow;
    let col:number=0;
    let num:NumberComponent=this.app.getNumber(row,col);
    while(!this.isNumNull(num) && (i>=num.hopeWillHave.length) || this.hasSameCoords(row,col,origRow,origCol)){
      if (this.hasSameCoords(row,col,origRow,origCol))
      {
        col++;
        num=this.app.getNumber(row,col);
        if (this.isNumNull(num))break;
      }
      else
      {
        i-=num.hopeWillHave.length;
        col++;
        num=this.app.getNumber(row,col);
      }
    }
    if (this.isNumNull(num)) return -1;
    return num.hopeWillHave[i];
  }
  private getNextHopeWillHaveV(origRow:number,origCol:number,i:number):number{
    let row:number=0;
    let col:number=origCol;
    let num:NumberComponent=this.app.getNumber(row,col);
    while(!this.isNumNull(num) && (i>=num.hopeWillHave.length) || this.hasSameCoords(row,col,origRow,origCol)){
      if (this.hasSameCoords(row,col,origRow,origCol))
      {
        row++;
        num=this.app.getNumber(row,col);
        if (this.isNumNull(num))break;
      }
      else
      {
        i-=num.hopeWillHave.length;
        row++;
        num=this.app.getNumber(row,col);
      }
    }
    if (this.isNumNull(num)) return -1;
    return num.hopeWillHave[i];
  }
  private getNextHopeWillHaveC(origRow:number,origCol:number,i:number):number{
    let baseRow:number=origRow - origRow % 3;
    let baseCol:number=origCol - origCol % 3;
    let newRow:number=baseRow;
    let newCol:number=baseCol;
    let offseOfPosition:number=0;
    newRow=baseRow+Math.floor((offseOfPosition)/3);
    newCol=baseCol+offseOfPosition%3;

    let num:NumberComponent=this.app.getNumber(newRow,newCol);
    while(!this.isNumNull(num) && (i>=num.hopeWillHave.length) || this.hasSameCoords(newRow,newCol,origRow,origCol)){

      if (this.hasSameCoords(newRow,newCol,origRow,origCol))
      {
        offseOfPosition++;
        newRow=baseRow+Math.floor((offseOfPosition)/3);
        newCol=baseCol+offseOfPosition%3;
        num=this.app.getNumber(newRow,newCol);
        if (this.isNumNull(num))break;
      }
      else
      {
        i-=num.hopeWillHave.length;
        offseOfPosition++;
        newRow=baseRow+Math.floor((offseOfPosition)/3);
        newCol=baseCol+offseOfPosition%3;
        num=this.app.getNumber(newRow,newCol);
      }

      if (offseOfPosition>=this.app.cols) num=undefined;
    }
    if (this.isNumNull(num)) return -1;
    return num.hopeWillHave[i];
  }
  private getNextHopeWillHave(origRow:number,origCol:number,i:number,dir:groupType):number{
    if (dir==groupType.horizontal) return this.getNextHopeWillHaveH(origRow,origCol,i);
    else if (dir==groupType.vertical) return this.getNextHopeWillHaveV(origRow,origCol,i);
    else if (dir==groupType.cube) return this.getNextHopeWillHaveC(origRow,origCol,i);
  }
  private getThisHopeWillHave(origRow:number,origCol:number,i:number){
    let num:NumberComponent=this.app.getNumber(origRow,origCol);
    if ((num==undefined)||(num==null)) return -1;
    if (i>=num.hopeWillHave.length) return -1;
    return num.hopeWillHave[i];
  }
  public transformed:NumberComponent[]=[];
  private transformOneHopeWillHaveInGroupToGonnaHave(num:NumberComponent)
  {  
    let oneAppearance:number=-1;
    let actOneAppearance:number=-1;
    for(let i=0; i<groupType.values().length-1; i++){
      let cnts:NumCnt[] = this.createCntsHopeWillHaveType(num,groupType[groupType[i]]);
      if (this.cntOfOneAppearance(cnts)==1) {
        actOneAppearance = this.findOneAppearance(cnts,0);

        if (oneAppearance<0) oneAppearance=actOneAppearance;
        else if (oneAppearance!=actOneAppearance) {
          return;
        }
      }
    }
    if (oneAppearance>-1) {
      num.bindedText=oneAppearance.toString();
      this.transformed.push(num);
    }
  }
  public clearTranformed(){
    this.transformed.forEach(element=>{
      element.hopeWillHave=[];
      this.hopeWillHaveChangedToBindedText=true;
    });
    this.transformed=[];
  }
  
  private createCntsHopeWillHaveType(num:NumberComponent,which:groupType):NumCnt[]{    
    let ret:NumCnt[]=[];
    let gotit:boolean=false;
    let i = 0;
    let thisNum:number=this.getThisHopeWillHave(num.thisrow,num.thiscol,i);
    let nextNum:number;
    
    while(thisNum>-1) {
      gotit=false;
      ret.forEach(actcnt=>{
        if (!gotit && actcnt.num==thisNum){
          actcnt.cnt++;
          gotit=true;
        }
      });
      if (!gotit) {
        ret.push(new NumCnt(thisNum,1))
      }
      i++;
      thisNum=this.getThisHopeWillHave(num.thisrow,num.thiscol,i);
    }
    i=0;

    do{
      nextNum=this.getNextHopeWillHave(num.thisrow,num.thiscol,i,which);
      if (nextNum>-1){
        ret.forEach(actcnt=>{
          if (actcnt.num==nextNum) actcnt.cnt++;
        });
      }

      i++;
    }while(nextNum>-1);

    return ret;
  }
  private cntOfOneAppearance(numscnt:NumCnt[]):number{
    let ret:number=0;
    for(let i=0; i<numscnt.length; i++){
      if (numscnt[i].cnt==1) ret++;
    }
    return ret;
  }
  private findOneAppearance(numscnt:NumCnt[],cnt:number):number{
    let count=cnt;
    for(let i=0; i<numscnt.length; i++){
      if (numscnt[i].cnt==1) {
        if (count>=0) count--;
        if (count<0) return numscnt[i].num;
      }
    }
    return -1;
  }


  undoBrutForceStack:brutForceResultStackType;

  brutForce(){
    let step:brutForceResultType;
    if (this.haveUnsolvableSolution()){
      step=this.undoBrutForceStack.undoLast();
      if (step.hwhl<=step.addedHwhI+1) return true;
    } else {
      step=this.undoBrutForceStack.createNew();
    }
    this.undoBrutForceStack.saveNew();
    this.undoBrutForceStack.doBFStep(step,this);
    return false;
  }
  haveUnsolvableSolution(){
    for(let i:number=0;i<this.app.numbers.length;i++){
      let element:NumberComponent=this.app.numbers[i];
      if (element.bindedText) continue;
      if (element.hopeWillHave.length==0) return true;
    }
    return false;
  }
}