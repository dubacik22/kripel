import { NumberComponent } from '../number/number.component';
import { creatingArgs } from './creatingArgs';
import { groupType } from './groupType';
import { errEnum } from './errEnum';
import { SudokuComponent } from '../sudoku.component';
import { changeArgs } from './changeArgs';

export abstract class GroupWithNum{
    app: SudokuComponent;
    row: number;
    col: number;
    type: number;
    allnumbers: NumberComponent[];
    myNumbers: NumberComponent[]=[];
    validating:boolean;

    onGroupCreated(app: SudokuComponent){
      this.app=app;
      this.allnumbers=app.numbers;
      //ulozit moje cisla z uz vytvorenych cisel
      app.numbers.forEach((element)=>{this.saveMyNumber(element)})
    }
    onNumberCreate(args: creatingArgs){
      //ulozit moje cislo z prave vytvoreneho cisla
      this.saveMyNumber(args.number);
    }
    saveMyNumber(number: NumberComponent){}

    validateAll(chngArgs:changeArgs, createArgs:creatingArgs, tryUndo:boolean){
      this.validating=true;
      if (tryUndo){
        this.onRestErrUndo(chngArgs,createArgs);
      }
      if (createArgs.number.bindedText){
        this.tryCreateErr(chngArgs,createArgs);
      }
      
      this.validating=false;
    }
    private tryCreateErr(chngArgs:changeArgs,createArgs:creatingArgs){
      this.myNumbers.forEach(element=>{

        if (createArgs.number!==element){
          if (element.bindedText==chngArgs.newVal){
            createArgs.number.addErr(errEnum.err);
            element.addErr(errEnum.err);
          }
        }
      });
    }
    private onRestErrUndo(chngAgs:changeArgs,createArgs:creatingArgs){
      this.myNumbers.forEach(element=>{

          if ((element.bindedText==chngAgs.oldVal) && (element.problem!=errEnum.ok)){
            this.app.tryFindOrRemoveErr(changeArgs.createDummyArqs(element.bindedText),creatingArgs.createDummyArqs(element),true)
          }
          
      });
    }
    private getGroup(row:number,col:number):GroupWithNum{
      return this.app.getGroupFromCellCoord(row,col,this.getType());
    }
    public setGroup(row:number,col:number,what:GroupWithNum){
      return this.app.setGroup(row,col,this.getType(),what);
    }

    public abstract getType():groupType;
}