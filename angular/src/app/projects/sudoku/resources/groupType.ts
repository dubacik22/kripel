export enum groupType {
  horizontal = 0,
  vertical = 1,
  cube = 2,
}

export namespace groupType {

  export function values():Array<string> {
    return Object.keys(groupType).filter(
      (type) => isNaN(<any>type) && (type !== 'values')
    );
  }
  
}