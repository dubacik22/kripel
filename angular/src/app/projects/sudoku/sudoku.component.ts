import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { creatingArgs } from './resources/creatingArgs'
import { horizontalGroupWithNum } from './resources/horizontalGroupWithNum'
import { verticalGroupWithNum } from './resources/verticalGroupWithNum'
import { cubeGroupWithNum } from './resources/cubeGroupWithNum'
import { groupType } from './resources/groupType'
import { errEnum } from './resources/errEnum'
import { NumberComponent } from './number/number.component';
import { GroupWithNum } from './resources/GroupWithNum';
import { changeArgs } from './resources/changeArgs';
import { solverClass } from './resources/solverClass';
import { tuple } from './resources/tuple';
import { gameCreator } from './resources/gameCreator';


@Component({
  selector: 'app-sudoku',
  templateUrl: './sudoku.component.html',
  styleUrls: ['./sudoku.component.css', './styles.css']
})
export class SudokuComponent implements OnInit, AfterViewInit {
  @ViewChild('mainWrapper') mainWrapperRef:ElementRef;
  public get mainWrapper():HTMLElement{
    return this.mainWrapperRef.nativeElement;
  }
  createdCols: number[] = [];
  createdRows: number[] = [];
  numberGroups: GroupWithNum[][][] = new Array<Array<Array<GroupWithNum>>>();
  numbers: NumberComponent[] = [];
  appDisabled:boolean;
  public winVisibility:string = 'none';
  public get win():boolean{
    if (this.winVisibility=='none') return false;
    return true;
  }
  public set win(value:boolean){
    if (value) this.winVisibility="";
    else this.winVisibility="none";
  }
  bfInfo: string="";
  q1: any="";
  cols:number=9;
  maxCubeCoord:number=3;
  increment:number=0;
  increment2:number=0;

  //cubesInRow:number=3;

  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private chDref: ChangeDetectorRef){}
  
  ngOnInit() {
    let count = this.cols * this.cols;
    let rows = this.cols;

    for(let col=0;col<this.cols;col++){
      this.createdCols.push(col);
    }
    for(let row=0;row<rows;row++){
      this.createdRows.push(row)
    }
  }
  ngAfterViewInit() {
   // this.chDref.detach();
  }
  onArrowPressedInNum(keyArgs:creatingArgs){
    let row:number=keyArgs.number.thisrow;
    let col:number=keyArgs.number.thiscol;
    
    if (keyArgs.text==NumberComponent.arrowKeys[1]){
      if (row==0) return;
      row--;
    } else if (keyArgs.text==NumberComponent.arrowKeys[0]){
      if (row==this.cols-1) return;
      row++;
    } else if (keyArgs.text==NumberComponent.arrowKeys[2]){
      if (col==0) return;
      col--;
    } else if (keyArgs.text==NumberComponent.arrowKeys[3]){
      if (col==this.cols-1) return;
      col++
    }
    
    if (this.WantToHave) this.getNumber(row,col).wthHolder.nativeElement.focus();
    else this.getNumber(row,col).numHolder.nativeElement.focus();
  }
  getNumber(row:number,col:number):NumberComponent{
    if ((row>=this.cols)||(col>=this.cols)) return undefined;
    let offset=row+col*this.cols;
    if (this.numbers==undefined) return undefined;
    if (this.numbers.length<=offset) return undefined;
    if (this.numbers[offset]===undefined) return null;
    return this.numbers[offset];
  }
  setNumber(row:number,col:number,number:NumberComponent){
    if ((row>=this.cols)||(col>=this.cols)) return;
    let offset=row+col*this.cols;
    if (this.numbers==undefined) return;
    if (this.numbers.length<=offset) while(this.numbers.length<=offset) this.numbers.push(undefined);
    this.numbers[offset]=number;
  }
  public static getGroupColFromCellCol(col:number,which:groupType){
    if (which==groupType.horizontal){
      col=horizontalGroupWithNum.getGroupColFromCellCol(col);
    }else if (which==groupType.vertical){
      col=verticalGroupWithNum.getGroupColFromCellCol(col);
    }else if (which==groupType.cube){
      col=cubeGroupWithNum.getGroupColFromCellCol(col);
    }
    return col;
  }
  public static getGroupRowFromCellRow(row:number,which:groupType):number{
    if (which==groupType.horizontal){
      row=horizontalGroupWithNum.getGroupRowFromCellRow(row);
    }else if (which==groupType.vertical){
      row=verticalGroupWithNum.getGroupRowFromCellRow(row);
    }else if (which==groupType.cube){
      row=cubeGroupWithNum.getGroupRowFromCellRow(row);
    }
    return row;
  }
  getGroupFromCellCoord(row:number,col:number, which:groupType):GroupWithNum{
    row=SudokuComponent.getGroupRowFromCellRow(row,which);
    col=SudokuComponent.getGroupColFromCellCol(col,which);
    if (this.numberGroups==undefined) return undefined;
    if (this.numberGroups[row]===undefined) return undefined;
    if (this.numberGroups[row][col]===undefined) return undefined;
    return this.numberGroups[row][col][which];
  }
  setGroup(row:number,col:number,which:groupType,value:GroupWithNum){
    row=SudokuComponent.getGroupRowFromCellRow(row,which);
    col=SudokuComponent.getGroupColFromCellCol(col,which);
    if (this.numberGroups[row]===undefined) this.numberGroups[row] = new Array<Array<GroupWithNum>>();
    if (this.numberGroups[row][col]===undefined) this.numberGroups[row][col] = new Array<GroupWithNum>();
    this.numberGroups[row][col][which]=value;
  }
  public getCube(row:number,col:number):cubeGroupWithNum{
    let ret:GroupWithNum;
    row=row*this.maxCubeCoord;
    col=col*this.maxCubeCoord;
    ret = this.getGroupFromCellCoord(row,col,groupType.cube);
    //ret.row=row;
    //ret.col=col;
    return ret as cubeGroupWithNum;
  }
  public getCubeInVertical(row:number,col:number,offset:number):cubeGroupWithNum{
    let ret:GroupWithNum;
    let row2=((row+offset)*this.maxCubeCoord)%this.cols;
    let col2=(col*this.maxCubeCoord)%this.cols;
    ret = this.getGroupFromCellCoord(row2,col2,groupType.cube);
    return ret as cubeGroupWithNum;
  }
  public getCubeInHorizontal(row:number,col:number,offset:number):cubeGroupWithNum{
    let ret:GroupWithNum;
    let row2=(row*this.maxCubeCoord)%this.cols;
    let col2=((col+offset)*this.maxCubeCoord)%this.cols;
    ret = this.getGroupFromCellCoord(row2,col2,groupType.cube);
    return ret as cubeGroupWithNum;
  }

  public static createGroup(which:groupType):GroupWithNum{
    let ret:GroupWithNum;
    if (which==groupType.horizontal){
      ret = new horizontalGroupWithNum();
    }else if (which==groupType.vertical){
      ret = new verticalGroupWithNum();
    }else if (which==groupType.cube){
      ret = new cubeGroupWithNum();
    }
    ret.type=which;
    return ret;
  }
  onNumCreate(args: creatingArgs){
    args.number.App=this;
    this.setNumber(args.row,args.col,args.number);
    if ((args.row>=this.cols)||(args.col>=this.cols)) return;
    this.numberGroups.forEach(el1=>{
      el1.forEach(el2=>{
        el2.forEach(el3=>{
          el3.onNumberCreate(args);
        });
      });
    });
    let newGroup: GroupWithNum;
    groupType.values().forEach(gt=>{
      if (this.getGroupFromCellCoord(args.row, args.col, groupType[gt])===undefined) {
        newGroup=SudokuComponent.createGroup(groupType[gt]);
        newGroup.row=args.row;
        newGroup.col=args.col;
        newGroup.onGroupCreated(this);
        this.setGroup(args.row, args.col, groupType[gt], newGroup);
      }
    });
  }

  onNumValidate(args: tuple<changeArgs,creatingArgs>){
    if (this.BuildGame)
    {
      this.onInsertBuildNumber(args.one,args.twoo);
      this.tryFindOrRemoveErr(args.one,args.twoo,true);
    }
    else
    {
      this.tryFindOrRemoveErr(args.one,args.twoo,true);
    }
    this.checkWin();
  }
  private onInsertBuildNumber(chngArgs: changeArgs,createArgs: creatingArgs){
    if (chngArgs.newVal)
    {
      createArgs.number.ReadonlyColor=true;
    }
    else
    {
      createArgs.number.ReadonlyColor=false;
    }
  }
  tryFindOrRemoveErr(chngArgs: changeArgs,createArgs: creatingArgs, tryUndo:boolean){
    createArgs.number.removeErr();
    let group : GroupWithNum = undefined;
    groupType.values().forEach(gt=>{
      group=this.getGroupFromCellCoord(createArgs.row, createArgs.col, groupType[gt]);
      if (group!==undefined) group.validateAll(chngArgs,createArgs,tryUndo);
    });
  }
  checkWin(){
    let cont: boolean = true;
    this.win = false;

    this.numbers.forEach((element: NumberComponent) => {
      if (!element.bindedText) {
        cont = false;
        return;
      }
      if (element.problem!=errEnum.ok){
        cont = false;
        return;
      }
    });
   
    if (!cont) {
      this.detectChanges();
    } else {
      this.win = true;
      this.detectChanges();
    }
  }

  createGame(what:number[][]){
    let iteratorX=-1;
    let iteratorY=-1;
    let actNumber: NumberComponent;
    for(let i=0; i<what.length; i++)
    {
      let innerElement:number[]=what[i];
      iteratorY++;
      iteratorX=-1;

      for(let j=0; j<innerElement.length; j++)
      {
        let element:number=innerElement[j];
        iteratorX++;
        if (element!=0) {
          actNumber=this.getNumber(iteratorX,iteratorY);
          if ((actNumber==undefined) || (actNumber==null)) return;
          actNumber.bindedText=element.toString();
          actNumber.ReadonlyInput=true;
        }
      }
    }
  }
  private buildGame:boolean=false;
  public buildBtnText:string="vytvor";
  public buildSolveBtnDisabled:boolean=false;
  set BuildGame(value:boolean){
    this.buildGame=value;
    if (this.buildGame)
    {
      this.buildBtnText="lusti";
      this.buildWantHaveDisabled=true;
      this.numbers.forEach(element=>{
        let olCol=element.ReadonlyColor;
        element.ReadonlyInput=false;
        element.ReadonlyColor=olCol;

        this.tryFindOrRemoveErr(changeArgs.createDummyArqs(element.bindedText),creatingArgs.createDummyArqs(element),true);
      });
    }
    else
    {
      this.buildBtnText="vytvor";
      this.buildWantHaveDisabled=false;
      this.numbers.forEach(element=>{
        element.ReadonlyInput=element.ReadonlyColor;
        this.tryFindOrRemoveErr(changeArgs.createDummyArqs(element.bindedText),creatingArgs.createDummyArqs(element),true);
      });
    }
  }
  get BuildGame():boolean{
    return this.buildGame;
  }
  toggleBuildSolve(){
    this.BuildGame=!this.BuildGame;
  }
  private _wantHave:boolean=false;
  public wantHaveBtnText:string="naznac";
  public buildWantHaveDisabled:boolean=false;
  set WantToHave(value:boolean){
    this._wantHave=value;
    if (this._wantHave)
    {
      this.wantHaveBtnText="zadaj";
      this.buildSolveBtnDisabled=true;
      this.clearAllWantToHave();
    }
    else
    {
      this.wantHaveBtnText="naznac";
      this.buildSolveBtnDisabled=false;
      this.clearAllWantToHave();
    }
  }
  get WantToHave():boolean{
    return this._wantHave;
  }
  toggleWantHave(){
    this.WantToHave=!this.WantToHave;
  }
  clearAllWantToHave(){
    this.numbers.forEach(element=>{
      element.wantHaveItem='';
    });
  }
  clearAllClicked(){
    this.numbers.forEach(element=>{
      element.hopeWillHave=[];
      element.text="";
      element.bindedText="";
      element.ReadonlyInput=false;
      element._canhave=[];
      element.wantHave=[];
      element.refreshCannave();
   });
   this.checkWin();
  }
  clearClicked(){
    this.BuildGame=false;
    this.numbers.forEach(element=>{
      if (!element.ReadonlyInput){
        element.hopeWillHave=[];
        element.refreshCannave();
        element.text="";
        element._canhave=[];
        element.wantHave=[];
        element.bindedText="";
      }
      else
      {
        element.problem=errEnum.ok;
      }
   });
   this.bfInfo='';
   this.checkWin();
  }
  createWorkClicked(which:number){
    if (which==0)
    {
      this.createGame([[5,3,0,0,7,0,0,0,0],[6,0,0,1,9,5,0,0,0],[0,9,8,0,0,0,0,6,0],[8,0,0,0,6,0,0,0,3],[4,0,0,8,0,3,0,0,1],[7,0,0,0,2,0,0,0,6],[0,6,0,0,0,0,2,8,0],[0,0,0,4,1,9,0,0,5],[0,0,0,0,8,0,0,7,9]]);
    }
    else if (which==1)
    {
      this.createGame([[0,0,0,8,4,6,0,9,3],[0,0,0,2,0,9,1,0,0],[0,0,0,5,0,1,4,0,0],[6,0,0,3,5,4,9,0,8],[0,0,8,0,1,7,3,0,4],[0,3,4,0,8,2,0,5,0],[0,0,0,0,2,5,8,3,0],[8,0,5,0,6,3,0,4,0],[7,0,3,0,9,8,0,0,0]]);
    }
    else if (which==2)
    {
      this.createGame([[0,5,0,0,7,0,9,0,0],[0,6,0,0,0,0,0,0,7],[0,0,2,5,4,0,6,0,0],[0,0,8,0,0,0,0,0,0],[2,0,6,4,0,0,3,0,0],[0,0,0,9,2,0,5,0,1],[0,7,0,0,8,0,0,0,0],[0,0,0,3,0,0,0,0,0],[0,0,9,1,0,7,0,0,2]]);
    }
    else if (which==3)
    {
      this.createGame([[1,5,3,6,7,2,9,8,4],[9,6,4,8,3,1,2,5,7],[7,8,2,5,4,9,6,1,3],[5,1,8,7,6,3,4,2,9],[2,9,6,4,1,5,3,7,8],[4,3,7,9,2,8,5,6,1],[3,7,5,2,8,4,1,9,6],[8,2,1,3,9,6,7,4,5],[6,4,9,1,5,7,8,3,2]]);
    }
    else if (which==4)
    {
      this.createGame([[0,0,0,7,2,3,5,0,0],[9,2,3,4,0,5,6,0,7],[5,7,0,0,0,0,3,2,0],[0,0,5,1,4,7,9,3,2],[7,3,9,8,5,2,1,4,6],[1,4,2,0,3,0,7,0,0],[0,0,0,0,7,0,0,0,0],[2,0,7,0,0,1,4,0,3],[0,0,1,0,9,4,0,7,0]]);
    }
    else if (which==5)
    {
      this.createGame([[2,9,0,0,0,0,0,0,3],[1,0,0,0,0,7,0,0,0],[0,0,8,2,9,0,0,0,0],[3,7,0,0,0,0,0,5,1],[0,4,0,0,1,0,0,8,0],[0,8,1,0,7,0,0,4,2],[0,0,0,0,6,9,1,0,0],[0,0,0,7,0,0,0,0,5],[6,0,0,0,0,0,0,3,4]]);
      //this.createGame([[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0],[2,8,0,0,1,4,0,0,0],[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0]]);
    }
    else if (which==6){
      this.createGame([[0,0,4,5,0,0,0,0,7],[0,0,5,0,4,0,0,0,0],[1,2,0,0,0,0,0,6,0],[4,0,8,0,0,9,0,0,6],[6,0,0,8,0,5,0,0,2],[9,0,0,7,0,0,1,0,3],[0,1,0,0,0,0,0,2,4],[0,0,0,0,1,0,6,0,0],[2,0,0,0,0,6,3,0,0]]);
    }
    else if (which==7){
      this.createGame([[0,0,0,0,2,0,0,0,0],[0,0,0,0,9,0,0,0,0],[0,8,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,8,0],[0,0,0,0,1,0,0,0,0],[0,0,0,0,4,0,0,0,0],[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0]])
    }
    else if (which==8){
      this.createGame([[4,8,0,0,0,0,0,0,1],[0,0,0,0,8,0,0,0,0],[0,0,0,1,0,9,0,7,0],[0,0,6,5,0,0,0,0,0],[0,0,0,7,0,0,0,6,0],[0,0,7,6,0,0,0,0,0],[0,0,0,0,0,0,6,0,0],[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0]]);
    }
    else if (which==9){
      this.createGame([[8,0,1,7,6,0,0,4,3],[0,0,2,4,5,0,0,1,0],[0,0,0,3,2,0,0,6,7],[6,2,0,0,0,0,0,0,0],[4,1,3,0,0,0,0,0,0],[7,0,0,0,0,0,0,0,0],[4,0,0,0,0,0,0,0,0],[1,7,5,0,0,0,0,0,0],[2,3,6,0,0,0,0,0,0]]);
    }
  }

  gc:gameCreator=new gameCreator(this);
  createGameClicked(difficulty:number){
    this.gc.create(difficulty);
  }
  solvingClicked(){
    if (this.BuildGame) this.toggleBuildSolve();
    let solver:solverClass = new solverClass(this);
    solver.solveWithBrutForce();
  }
  coto(){
    let ret:string='[';
    for(let i=0;i<this.cols;i++){
      ret+='[';
      for(let j=0;j<this.cols;j++){
        let number:NumberComponent=this.getNumber(j,i);
        if (number.bindedText) ret+=number.bindedText;
        else ret+='0';
        if (j<this.cols-1) ret+=',';
      }
      ret+=']';
      if (i<this.cols-1) ret+=',';
    }
    ret+=']';
    alert(ret);
  }
}
