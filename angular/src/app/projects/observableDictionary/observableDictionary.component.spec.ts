import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObservableDictionaryComponent } from './observableDictionary.component';

describe('ObservableDictionaryComponent', () => {
  let component: ObservableDictionaryComponent;
  let fixture: ComponentFixture<ObservableDictionaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObservableDictionaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObservableDictionaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
