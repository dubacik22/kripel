import { Component, Input, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { iNotifyPropertyChanged } from 'src/stuff/iNotifyProperyChanged';
import { ButtonConf } from 'src/stuff/dialogs/res/ButtonConf';
import { DialogBoxComponent } from 'src/stuff/dialogs/modalDialog.component';

@Component({
  selector: 'app-doodling',
  templateUrl: './app-doodling.component.html',
  styleUrls: ['./app-doodling.component.css']
})
export class AppDoodlingComponent implements AfterViewInit {
  private _lineColors: string[] = [
    'white',
    'red',
    'green',
    'yellow',
    'purple',
    'brown',
    'blue',
    'grey',
    'pink',
    'magenta',
    'lime',
    'orange',
    'olive',
    'sienna',
    'PowderBlue',
    'teal',
    'tomato',
    'oldlace',
  ];
  
  private _color: string;
  public get color(): string {
    return this._color;
  }
  public set color(value: string) {
    if (this._color == value) return;
    this._color = value;
    this.detectChanges();
  }

  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private chDref: ChangeDetectorRef) { 
  }

  ngAfterViewInit() {
    this.chDref.detach();
  }

  public selectColor(event: MouseEvent) {
    let buttons: ButtonConf[] = [];
    for (let i: number = 0; i < this._lineColors.length; i++) {
      let button: ButtonConf;
      button = new ButtonConf('');
      button.content = '';
      button.style = {
        'height': '40px',
        'width': '40px',
        'padding': '0',
        'margin-left': '10px',
        'margin-right': '10px',
        'background-color': this._lineColors[i]
      };
      button.func = (n: string) => {this.color = this._lineColors[i];}//+Palette.paletteOffset);}
      buttons.push(button);
    }
    DialogBoxComponent.set('aka farba?', buttons);
    DialogBoxComponent.show();
  }

  unlistenTo(subscribtion: Subscription) {
    if (subscribtion) subscribtion.unsubscribe();
  }
  listenTo(to: iNotifyPropertyChanged, toDo: (propName: string) => void): Subscription {
      if (to) return to.propertyChange.subscribe(toDo);
      return undefined;
  }
}
