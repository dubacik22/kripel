import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppDoodlingComponent } from './app-doodling.component';

describe('AppDoodlingComponent', () => {
  let component: AppDoodlingComponent;
  let fixture: ComponentFixture<AppDoodlingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppDoodlingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppDoodlingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
