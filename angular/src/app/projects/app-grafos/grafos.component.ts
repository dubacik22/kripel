import { Component, Input, ChangeDetectorRef, AfterViewInit, OnDestroy } from '@angular/core';
import { Communicator } from './res/Communicator';
import { MeasLine } from './res/MeasLine';
import { Session } from 'src/stuff/injectables/Session';
import { User } from 'src/app/app-login/res/User';
import { EChartsOption } from './res/EChartsOption';
import { ButtonConf } from 'src/stuff/dialogs/res/ButtonConf';
import { DialogBoxComponent } from 'src/stuff/dialogs/modalDialog.component';
import { Limit } from './res/Limit';
import { LiveUpdater } from './res/LiveUpdater';
import { Sender } from './res/Sender';

@Component({
  selector: 'app-grafos',
  templateUrl: './grafos.component.html',
  styleUrls: ['./grafos.component.css']
})
export class GrafosComponent implements AfterViewInit,OnDestroy {
  private updater: LiveUpdater;
  private miliSecInHour: number = 1000 * 60 * 60;
  public sender: Sender;

  private _minValue:number;
  public get minValue(): number {
    return this._minValue;
  }
  public set minValue(val: number) {
    this._minValue = val;
    this.detectChanges();
  }
  private _maxValue:number;
  public get maxValue(): number {
    return this._maxValue;
  }
  public set maxValue(val: number) {
    this._maxValue = val;
    this.detectChanges();
  }

  public chartOptions: Array<EChartsOption>;
  public lastValue: number;

  private _compactSubClass:string;
  @Input()
  public get compactSubClass():string{
    return this._compactSubClass;
  }
  public set compactSubClass(value:string) {
    if (value == this._compactSubClass) return;
    this._compactSubClass = value;
    this.detectChanges();
  }
  
  constructor(private communicator: Communicator, private chDref: ChangeDetectorRef, public session: Session) {}
  
  public ngAfterViewInit() {
    this.chDref.detach();
    this.updater = new LiveUpdater(this.communicator, this.session);
    this.updater.runLiveUpdate = true;
    this.updater.onUpdate = (line: MeasLine) => this.updateLines(line);
    this.updater.detectChanges = () => this.detectChanges();
    this.chartOptions = [];
    this.updater.beginUpdate();
    this.sender = new Sender(this.communicator.http, this.communicator.paths);
    //this.sender.startSending(this.session.getEncryptedSession());
  }
  public ngOnDestroy() {
    this.updater.runLiveUpdate = false;;
  }
  public onChartInit(eChartsOption: EChartsOption, ec: any) {
    eChartsOption.echartsInstance = ec;
  }

  public recreateTables() {
    this.communicator.recreateAll(this.session.getEncryptedSession(), () => DialogBoxComponent.showJustInfo("ok"));
  }
  public tryEraseMeas(which: EChartsOption) {
    let buttons: ButtonConf[] = [];
    buttons.push(new ButtonConf('Čo si! Nebude', () => {
      this.eraseMeas(which.measLine.measId);
    }));
    buttons.push(new ButtonConf('Tak už nedaj preč'));
    DialogBoxComponent.set('To myslíš vážne? Ti nebude ľúto?', buttons);
    DialogBoxComponent.show();
  }
  public tryEndMeas(which: EChartsOption) {
    let buttons: ButtonConf[] = [];
    buttons.push(new ButtonConf('Čo si! Nebude', () => {
      this.endMeas(which);
    }));
    buttons.push(new ButtonConf('Tak už neukončti'));
    DialogBoxComponent.set('To myslíš vážne? Ti nebude ľúto?', buttons);
    DialogBoxComponent.show();
  }
  private eraseMeas(measId: string) {
    this.communicator.eraseMeasurement(measId, this.session.getEncryptedSession(), () => {
      this.chartOptions = [];
      this.updater.oneTimeUpdate();
    });
  }
  private endMeas(chartOption: EChartsOption) {
    this.communicator.endMeasurement(chartOption.measLine.uid, chartOption.measId, this.session.getEncryptedSession(), () => {
      chartOption.finished = true;
      this.detectChanges();
    });
  }
  
  private updateLines(line: MeasLine) {
    this.addMeasLinesRetToCharts(line);
    this.updateAllLimitsOfLines(line);
  }
  private detectLimitsChanges() {
    this.chartOptions.forEach(options => options.detectLimitChanges());
  }
  private updateAllLimitsOfLines(line: MeasLine) {
    let chartOption = this.chartOptions.find(option => option.measLine.measId == line.measId);
    this.communicator.getAllLimits(chartOption.measLine.measId, (ret) => {
      if (!ret.success) {
        chartOption.limits = [];
        return;
      }
      let limiters = ret.limiters;
      limiters.forEach(limit => Limit.createOtheParams(limit));
      chartOption.limits = limiters;
      this.detectLimitsChanges();
    });
  }
  private addMeasLinesRetToCharts(line: MeasLine): void {
    if (this.chartOptions == undefined) return;
    let existingChartIndex = this.findChart(line.measId, this.chartOptions);
    let measLine: MeasLine;
    if (existingChartIndex < 0) {
      measLine = line;
      let chartOptions = this.createChartFromMeasLineRet(measLine);
      this.chartOptions.push(chartOptions);
    } else {
      let chartOptions = this.chartOptions[existingChartIndex];
      measLine = chartOptions.measLine;
      MeasLine.append(line, measLine);
      let newData = this.createChartFromMeasLineRet(measLine);
      chartOptions.copy(newData);
      chartOptions.makeChartUnprintable();
    }
  }
  private createChartFromMeasLineRet(measLine: MeasLine): EChartsOption {
    if (this.chartOptions == undefined) return;
    let isFromThisUser = (measLine.userId == this.session.getItem(User.userIdPropName)) || this.session.isUberAdmin();
    let sliderStep = 1 / this.miliSecInHour;
    let ret = EChartsOption.createOneChartOptions(measLine, isFromThisUser, sliderStep);
    ret.chDrefChart = this.chDref;
    return ret;
  }
  
  private findChart(name: string, charts: EChartsOption[]): number {
    for (let i = 0; i < charts.length; i++) {
      let chart = charts[i];
      if (chart.measId == name) return i;
    }
    return -1;
  }

  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
}
