import { Component, Input, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { DialogBoxComponent } from 'src/stuff/dialogs/modalDialog.component';
import { Session } from 'src/stuff/injectables/Session';
import { Communicator } from '../Communicator';
import { EChartsOption } from '../EChartsOption';
import { Limit } from '../Limit';

@Component({
  selector: 'grafos-limits',
  templateUrl: './limits.component.html',
  styleUrls: ['./limits.component.css']
})
export class LimitsComponent implements AfterViewInit {
  private _meas: EChartsOption;
  @Input()
  public get meas(): EChartsOption{
    return this._meas;
  }
  public set meas(value: EChartsOption) {
    if (value == this._meas) return;
    this._meas = value;
    value.chDrefLimits = this.chDref;
  }
  public get operation(): string {
    return "operation"+this._meas.measLine.measId;
  }
  public get gtId(): string {
    return "gt"+this._meas.measLine.measId;
  }
  public get ltId(): string {
    return "lt"+this._meas.measLine.measId;
  }
  public value: string;
  public email: string;
  public gt: string = "gt";
  public lt: string = "lt";
  public gtlt: string = this.gt;

  constructor(private communicator: Communicator, private chDref: ChangeDetectorRef, public session: Session) {}

  public ngAfterViewInit() {
    this.chDref.detach();
    this.email = this.session.getEmail();
    this.detectChanges();
  }


  public addLimit() {
    if (this.meas == undefined) return;
    let value = this.toFloat(this.value);
    if (value == undefined) return;
    let email = this.toEmail(this.email);
    if (email == undefined) return;
    this.communicator.saveLimit(this.meas.measLine.measId, value, this.gtlt, email, this.session.getEncryptedSession(), () => this.readLimits());
  }
  public readLimits() {
    this.meas.limits = [];
    this.communicator.getAllLimits(this.meas.measLine.measId, (ret) => {
      if (!ret.success) return;
      let limiters = ret.limiters;
      limiters.forEach(limit => Limit.createOtheParams(limit));
      this.meas.limits = limiters;
      this.detectChanges();
    });
    this.detectChanges();
  }
  public eraseLimit(limit: Limit) {
    this.communicator.eraseLimit(limit.limitId, this.session.getEncryptedSession(), () => this.readLimits());
  }

  private toFloat(sValue: string): number {
    if (sValue == undefined) return undefined;
    let value = sValue.replace(",", ".");
    let fValue = parseFloat(value);
    if (fValue.toString() == value) return fValue;
    DialogBoxComponent.showJustInfo("čo? to akože text '" + sValue + "' je číslo? som nevedel.");
    return undefined;
  }
  private toInt(sValue: string): number {
    if (sValue == undefined) return undefined;
    let fValue = parseInt(sValue);
    if (fValue.toString() == sValue) return fValue;
    DialogBoxComponent.showJustInfo("čo? to akože text '" + sValue + "' je číslo? som nevedel.");
    return undefined;
  }
  private toEmail(sValue: string): string {
    if (sValue == undefined) return undefined;
    if (this.validateEmail(sValue)) return sValue;
    DialogBoxComponent.showJustInfo("čo? to akože text '" + sValue + "' je emailoá adresa? som nevedel.");
    return undefined;
  }
  private validateEmail(email: string) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.toLowerCase());
  }

  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
}
