export class Limit {
    public limitId: string;
    public measId: string;
    public value: string;
    public operation: string;
    public email: string;
    public reached: string;

    private static readonly reachedClass: string = 'reached';
    private static readonly unreachedClass: string = 'unreached';
    
    public gtlt: string;
    public reachedClass: string;

    public static createOtheParams(limit: Limit): void {
        limit.gtlt = this.getGtLt(limit.operation);
        limit.reachedClass = this.getReachedClass(limit.reached);
    }
    private static getGtLt(operation: string): string {
      if (operation.toLowerCase().startsWith("g")) return ">";
      if (operation.toLowerCase().startsWith("l")) return "<";
      return "";
    }
    private static getReachedClass(reached: string): string {
      if (reached.startsWith("0")) return this.unreachedClass;
      if (reached.startsWith("1")) return this.reachedClass;
      return this.unreachedClass
    }
}