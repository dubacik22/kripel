import { Temperature } from './Temperature';

export class MeasLine {
    public userId: string;
    public dname: string;
    public measId: string;
    public uid: string;
    public date: string;
    public time: number;
    public temperatures: Temperature[];

    public static append(measline: MeasLine, into: MeasLine): boolean {
        if (into.userId != measline.userId) return false;
        if (into.measId != measline.measId) return false;
        if (into.temperatures == undefined) {
            into.temperatures = measline.temperatures;
            return true;
        }
        into.temperatures = into.temperatures.concat(measline.temperatures);
        return true;
    }
}