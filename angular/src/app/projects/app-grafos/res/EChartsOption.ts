import { EChartOption, ECharts } from 'echarts';
import { Options } from 'ng5-slider';
import { MeasLine } from './MeasLine';
import { ChangeDetectorRef } from '@angular/core';
import { Temperature } from './Temperature';
import { Limit } from './Limit';

export class EChartsOption {
  public chDrefChart: ChangeDetectorRef;
  public chDrefLimits: ChangeDetectorRef;
  public measId: string;
  public options: EChartOption;
  public optionsPrintable: boolean;
  public makePrintableTxt: string;
  public measLine: MeasLine;
  public isFromThisUser: boolean;
  public background: any;
  public axisColor: string;
  private toolbox: any;
  public limits: Limit[];
  public lastValue: number;
  public finished: boolean;
  public dname: string;

  public echartsInstance: ECharts;

  private static saveableAxisColor: string = "rgba(0,0,0,1)";
  private static unsaveableAxisColor: string = "rgba(200, 200, 200, 1)";
  private static makeSaveableTxt: string = "priprav na uloženie";
  private static makeUnsaveableTxt: string = "nepriprav na uloženie";
  private static saveableBackground: any = { background: 'white' };
  private static unsaveableBackground: any = { 'background-color': 'rgba(0,0,0,0.5)' };

  private _fromValue: number;
  public get fromValue(): number {
    return this._fromValue;
  }
  public set fromValue(value: number) {
    this._fromValue = value;
    this.updatePlotRange();
    this.detectAllChanges();
  }
  private _toValue: number;
  public get toValue(): number {
    return this._toValue;
  }
  public set toValue(value: number) {
    this._toValue = value;
    this.updatePlotRange();
    this.detectAllChanges();
  }
  public fromToOptions: Options;

  public toggleChartPrintable() {
    if (this.optionsPrintable) this.makeChartUnprintable();
    else this.makeChartPrintable();
    this.detectAllChanges();
  }
  public makeChartPrintable() {
    if (this.optionsPrintable) return;
    this.optionsPrintable = true;
    this.axisColor = EChartsOption.saveableAxisColor;
    this.toolbox = EChartsOption.getToolbox(true);
    this.options = EChartsOption.getOptions(this);
    this.makePrintableTxt = EChartsOption.makeUnsaveableTxt;
    this.background = EChartsOption.saveableBackground;
    this.detectAllChanges();
  }
  public makeChartUnprintable() {
    if (!this.optionsPrintable) return;
    this.optionsPrintable = false;
    this.axisColor = EChartsOption.unsaveableAxisColor;
    this.toolbox = EChartsOption.getToolbox(false);
    this.fromValue = 0;
    this.toValue = this.fromToOptions.ceil;
    this.options = EChartsOption.getOptions(this);
    this.makePrintableTxt = EChartsOption.makeSaveableTxt;
    this.background = EChartsOption.unsaveableBackground;
    this.detectAllChanges();
  }

  public updatePlotRange() {
    this.options = EChartsOption.getOptions(this);
    this.detectAllChanges();
  }

  public copy(from: EChartsOption): void {
    this.optionsPrintable = from.optionsPrintable;
    this.measId = from.measId;
    this.measLine = from.measLine;
    this.axisColor = from.axisColor;
    this.toolbox = from.toolbox;
    this.options = from.options;
    this.fromToOptions = from.fromToOptions;
    this.fromValue = from.fromValue;
    this.toValue = from.toValue;
    this.makePrintableTxt = from.makePrintableTxt;
    this.isFromThisUser = from.isFromThisUser;
    this.background = from.background;
    this.lastValue = from.lastValue;
    this.dname = from.dname;
  }

  public static createOneChartOptions(measLine: MeasLine, isFromThisUser: boolean, sliderStep: number): EChartsOption {
    let ret = new EChartsOption();
    ret.optionsPrintable = false;
    ret.measId = measLine.measId;
    ret.measLine = measLine;
    ret.axisColor = EChartsOption.unsaveableAxisColor;
    ret.toolbox = EChartsOption.getToolbox(false);
    ret.options = this.getOptions(ret);
    ret.fromToOptions = {
      floor: 0,
      ceil: this.getXMax(measLine),
      step: sliderStep
    };
    ret.fromValue = 0;
    ret.toValue = ret.fromToOptions.ceil;
    ret.makePrintableTxt = EChartsOption.makeSaveableTxt;
    ret.isFromThisUser = (measLine.userId == "") || isFromThisUser;
    ret.background = EChartsOption.unsaveableBackground;
    ret.lastValue = measLine.temperatures[measLine.temperatures.length-1].temp;
    ret.finished = measLine.uid == "";
    ret.dname = measLine.dname;
    return ret;
  }
  private static getOptions(of: EChartsOption): any {
    return {
      legend: this.getLegend(),
      xAxis: this.getXAxis(of.measLine, of.axisColor, of.fromValue, of.toValue),
      yAxis: this.getYAxis(of.measLine, of.axisColor, of.fromValue, of.toValue),
      series: [this.getSerie(of.measId, of.measLine, of.fromValue, of.toValue)],
      toolbox: of.toolbox,
      animation: false
    };
  }
  private static getSerie(name: string, measLine: MeasLine, from: number, to: number): any {
    let ret = {
      name: name,
      type: 'line',
      smooth: false,
      symbolSize: 0,
      data: this.getMeasLineChartLine(measLine, from, to)
    };
    return ret;
  }
  private static getMeasLineChartLine(measLine: MeasLine, from: number, to: number): number[][] {
    let delay: number = NaN;
    let filteredTemperatures = measLine.temperatures.filter((temperature: Temperature) => {
      if (!isNaN(delay)) delay += temperature.delay;
      if (isNaN(delay)) delay = 0;
      let ret = EChartsOption.canPrintTemperature(delay, from, to);
      return ret;
    });
    delay = NaN;
    let ret = filteredTemperatures.map((temperature: Temperature) => {
      let thisDelay = temperature.delay;
      if (!isNaN(delay)) delay += thisDelay;
      if (isNaN(delay)) delay = 0;
      return [delay, temperature.temp]
    });
    return ret;
  }
  private static canPrintTemperature(delay: number, from: number, to: number): boolean {
    if (!isNaN(from)) {
      if (from > delay) return false;
    }
    if (!isNaN(to)) {
      if (to < delay) return false;
    }
    return true;
  }
  private static getXAxis(measLine: MeasLine, axisColor: string, from: number, to: number): any {
    return {
      silent: true,
      name: "čas [min]",
      nameLocation: 'center',
      nameGap: 30,
      axisLine: {
        lineStyle: {
          color: axisColor
        }
      }
    };
  }
  private static getYAxis(measLine: MeasLine, axisColor: string, from: number, to: number): any {
    return {
      type: 'value',
      silent: true,
      name: "teplota [°C]",
      nameLocation: 'center',
      nameGap: 35,
      splitNumber: 5,
      min: this.getYMin(measLine, from, to),
      max: this.getYMax(measLine, from, to),
      axisLine: {
        lineStyle: {
          color: axisColor
        }
      }
    };
  }
  private static getLegend(): any {
    return {
      show: false,
    };
  }
  private static getToolbox(printable: boolean): any {
    return {
      show: printable,
      feature: {
        saveAsImage: {
          title: "save",
          pixelRatio: 3,
          excludeComponents: [
            'toolbox',
            'dataZoom'
          ]
        }
      }
    };
  }
  private static getWholeXMax(measLines: MeasLine[]): number {
    return measLines.map((measLine: MeasLine) => this.getXMax(measLine)).reduce((accumulator: number, currentValue: number) => Math.max(accumulator, currentValue), 0);
  }
  private static getXMax(measLine: MeasLine): number {
    let ret = measLine.temperatures.map((temperature: Temperature) => temperature.delay).
      filter((value: number) => !isNaN(value)).
      reduce((accumulator: number, currentValue: number) => accumulator + currentValue, 0);
    return ret;
  }
  private static getWholeYMin(measLines: MeasLine[], from: number, to: number): number {
    let ret = measLines.map((measLine: MeasLine) => this.getYMin(measLine, from, to)).reduce((accumulator: number, currentValue: number) => Math.min(accumulator, currentValue), Infinity);
    return Math.floor(ret);
  }
  private static getYMin(measLine: MeasLine, from: number, to: number): number {
    let delay: number = NaN;
    let filteredTemperatures = measLine.temperatures.filter((temperature: Temperature) => {
      if (!isNaN(delay)) delay += temperature.delay;
      if (isNaN(delay)) delay = 0;
      let ret = EChartsOption.canPrintTemperature(delay, from, to);
      return ret;
    });
    delay = NaN;
    let ret = filteredTemperatures.map((temperature: Temperature) => temperature.temp).
      filter((value: number) => !isNaN(value)).
      reduce((accumulator: number, currentValue: number) => Math.min(accumulator, currentValue), Infinity);
    return Math.floor(ret);
  }
  private static getWholeYMax(measLines: MeasLine[], from: number, to: number): number {
    let ret = measLines.map((measLine: MeasLine) => this.getYMax(measLine, from, to)).reduce((accumulator: number, currentValue: number) => Math.max(accumulator, currentValue), 0);
    return Math.ceil(ret);
  }
  private static getYMax(measLine: MeasLine, from: number, to: number): number {let delay: number = NaN;
    let filteredTemperatures = measLine.temperatures.filter((temperature: Temperature) => {
      if (!isNaN(delay)) delay += temperature.delay;
      if (isNaN(delay)) delay = 0;
      let ret = EChartsOption.canPrintTemperature(delay, from, to);
      return ret;
    });
    delay = NaN;
    let ret = filteredTemperatures.map((temperature: Temperature) => temperature.temp).
      filter((value: number) => !isNaN(value)).
      reduce((accumulator: number, currentValue: number) => Math.max(accumulator, currentValue), 0);
    return Math.ceil(ret);
  }


  private changeDetection4ChartInProgress: boolean = false;
  public detectChartChanges() {
    if (!this.chDrefChart) return;
    if (this.changeDetection4ChartInProgress) return;
    this.changeDetection4ChartInProgress = true;
    this.chDrefChart.detectChanges();
    this.changeDetection4ChartInProgress = false;
  }
  private changeDetection4LimitsInProgress: boolean = false;
  public detectLimitChanges() {
    if (!this.chDrefLimits) return;
    if (this.changeDetection4LimitsInProgress) return;
    this.changeDetection4LimitsInProgress = true;
    this.chDrefLimits.detectChanges();
    this.changeDetection4LimitsInProgress = false;
  }
  public detectAllChanges() {
    this.detectChartChanges();
    this.detectLimitChanges();
  }
}