import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Paths } from 'src/stuff/injectables/Paths';
import { MeasLinesRet } from './MeasLinesRet';
import { LimitsRet } from './LimitsRet';

@Injectable()
export class Communicator {
    public verbose = true;
    private serverEventSource: EventSource;
    private toDoList: ((data: any) => void)[] = [];
    private static createReadAllMeasurementsFromData(time: number): any {
        let ret = {
            'toDo': 'temperaturer_readAllMeases',
            'time': time,
        }
        return ret;
    }
    private static createReadAllUsersMeasurementsFromData(time: number, session: string): any {
        let ret = {
            'toDo': 'temperaturer_readAllUsersMeases',
            'time': time,
            'session': session,
        }
        return ret;
    }
    private static createEraseMeasurementData(measId: string, session: string): any {
        let ret = {
            'toDo': 'temperaturer_eraseMeasId',
            'session': session,
            'measId': measId
        }
        return ret;
    }
    private static createEndMeasurementData(uid: string, measId: string, session: string): any {
        let ret = {
            'toDo': 'temperaturer_endUidsMeasId',
            'session': session,
            'uid': uid,
            'measId': measId
        }
        return ret;
    }
    private static createSaveLimitData(measId: string, value: number, operation: string, email: string, session: string): any {
        let ret = {
            'toDo': 'temperaturer_saveLimit',
            'session': session,
            'measId': measId,
            'value': value,
            'operation': operation,
            'email': email
        }
        return ret;
    }
    private static createEraseLimitData(id: string, session: string): any {
        let ret = {
            'toDo': 'temperaturer_eraseLimit',
            'session': session,
            'id': id
        }
        return ret;
    }
    private static createReadAllLimitsData(measId: string): any {
        let ret = {
            'toDo': 'temperaturer_readLimits4MeasId',
            'measId': measId
        }
        return ret;
    }
    private static createRecreateAllData(session: string): any {
        let ret = {
            'toDo': 'temperaturer_recreateAll',
            'session': session,
        }
        return ret;
    }
    constructor(public http: HttpClient, public paths: Paths) {
    }
    public connectToServerEvent() {
        if (this.serverEventSource) return;
        this.serverEventSource = new EventSource(this.paths.phpCommunicator);
        this.serverEventSource.onmessage = (event: MessageEvent) => {
            console.log(event)
            this.toDoList.forEach((toDo: (data: any) => void) => {
                toDo(event);
            });
        };
        this.serverEventSource.onopen = (event: any) => {
            //console.log(event)
        };
    }
    public disconnectFromServerEvent() {
        if (!this.serverEventSource) return;
        this.serverEventSource.close();
        this.serverEventSource = undefined;
    }
    public doOnServerEvent(toDo: (data: any) => void) {
        //this.addToDo = true;
        this.toDoList.push(toDo);
        //this.addToDo = false;
    }
    public getAllMeasurements(time: number, whatNext?: (measLines: MeasLinesRet) => void) {
        let messageData = Communicator.createReadAllMeasurementsFromData(time);
        if (this.verbose) {
            console.log("We sent to server readAllMeases:");
            console.log(messageData);
        }
        this.http.post(this.paths.phpCommunicator, messageData, {responseType: 'json'})
        .subscribe((measLinesRet: MeasLinesRet) => {
            if (this.verbose) {
                console.log("Server returned at readAllMeases:");
                console.log(measLinesRet);
            }
            if (measLinesRet && whatNext) whatNext(measLinesRet);
        });
    }
    public getAllUsersMeasurements(time: number, session: string, whatNext?: (measLines: MeasLinesRet) => void) {
        let messageData = Communicator.createReadAllUsersMeasurementsFromData(time, session);
        if (this.verbose) {
            console.log("We sent to server readAllMeasesFrom:");
            console.log(messageData);
        }
        let httpHeaders = new HttpHeaders({});
        this.http.post(this.paths.phpCommunicator, messageData, {headers: httpHeaders, responseType: 'json'})
        .subscribe((measLinesRet: MeasLinesRet) => {
            if (this.verbose) {
                console.log("Server returned at readAllMeasesFrom:");
                console.log(measLinesRet);
            }
            if (measLinesRet && whatNext) whatNext(measLinesRet);
        });
    }
    public getAllLimits(measId: string, whatNext?: (limits: LimitsRet) => void) {
        let messageData = Communicator.createReadAllLimitsData(measId);
        if (this.verbose) {
            console.log("We sent to server getAllLimits:");
            console.log(messageData);
        }
        this.http.post(this.paths.phpCommunicator, messageData, {responseType: 'json'})
        .subscribe((limitsRet: LimitsRet) => {
            if (this.verbose) {
                console.log("Server returned at getAllLimits:");
                console.log(limitsRet);
            }
            if (limitsRet && whatNext) whatNext(limitsRet);
        });
    }
    public saveLimit(measId: string, value: number, operation: string, email: string, session: string, whatNext?: () => void) {
        let messageData = Communicator.createSaveLimitData(measId, value, operation, email, session);
        if (this.verbose) {
            console.log("We sent to server saveLimit:");
            console.log(messageData);
        }
        this.http.post(this.paths.phpCommunicator, messageData, {responseType: 'json'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at saveLimit:");
                console.log(retData);
            }
            if (retData && whatNext) whatNext();
        });
    }
    public eraseLimit(id: string, session: string, whatNext?: () => void) {
        let messageData = Communicator.createEraseLimitData(id, session);
        if (this.verbose) {
            console.log("We sent to server eraseLimit:");
            console.log(messageData);
        }
        this.http.post(this.paths.phpCommunicator, messageData, {responseType: 'json'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at eraseLimit:");
                console.log(retData);
            }
            if (retData && whatNext) whatNext();
        });
    }
    public eraseMeasurement(measId: string, session: string, whatNext?: () => void) {
        let messageData = Communicator.createEraseMeasurementData(measId, session);
        if (this.verbose) {
            console.log("We sent to server eraseMeasurement:");
            console.log(messageData);
        }
        this.http.post(this.paths.phpCommunicator, messageData, {responseType: 'json'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at eraseMeasurement:");
                console.log(retData);
            }
            if (retData && whatNext) whatNext();
        });
    }
    public endMeasurement(uid: string, measId: string, session: string, whatNext?: () => void) {
        let messageData = Communicator.createEndMeasurementData(uid, measId, session);
        if (this.verbose) {
            console.log("We sent to server endMeasurement:");
            console.log(messageData);
        }
        this.http.post(this.paths.phpCommunicator, messageData, {responseType: 'json'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at endMeasurement:");
                console.log(retData);
            }
            if (retData && whatNext) whatNext();
        });
    }
    
    public recreateAll(session: string, whatNext?: () => void) {
        let messageData = Communicator.createRecreateAllData(session);
        if (this.verbose) {
            console.log("We sent to server createTable:");
            console.log(messageData);
        }
        
        this.http.post(this.paths.phpCommunicator, messageData, {responseType: 'json'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at createTable:");
                console.log(retData);
            }
            if (retData && retData.success && whatNext) whatNext();
        });
    }
}
