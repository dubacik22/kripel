import { HttpClient } from '@angular/common/http';
import { Paths } from 'src/stuff/injectables/Paths';
 
export class Sender {
  private first: boolean = true;
  private period: number = 1000;
  private session: string;
  public dname: string;
  public detectChanges: () => void;

  private getData(): object{
    let rand = Math.random();
    return {
      'session':this.session,
      'toDo':'temperaturer_save',
      'temp': rand,
      'isFirst': this.first,
      'delay':'1000',
      'uid': 'qwe',
      'dname': this.dname,
    };
  }
 
  constructor(private http: HttpClient, private paths: Paths) {
  }
 
  public startSending(session: string): void {
    this.session=session;
    this.tryConnect(); 
  }
  private tryConnect(): void {
    let data = this.getData();
    console.log("Sender sent");
    console.log(data);
    this.http.post(this.paths.phpCommunicator, data, {responseType: 'json'})
    .subscribe((ret) => {
      console.log("value sent. returned:")
      console.log(ret);
      });
    this.first=false;
    setTimeout(() => this.tryConnect(), this.period); 
  }
}