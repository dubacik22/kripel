import { MeasLine } from './MeasLine';

export class MeasLinesRet {
    public success: boolean;
    public measurements: MeasLine[];
}