import { Limit } from './Limit';

export class LimitsRet {
    public success: boolean;
    public limiters: Limit[];
}