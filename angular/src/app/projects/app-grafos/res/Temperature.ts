export class Temperature {
    public valueId: string;
    public measId: string;
    public delay: number;
    public temp: number;
    public time: number;

    public static CreteCopy(value: Temperature) {
        let ret = new Temperature;
        ret.temp = value.temp;
        ret.delay = value.delay;
        ret.time = value.time;
        ret.measId = value.measId;
        return ret;
    }
}