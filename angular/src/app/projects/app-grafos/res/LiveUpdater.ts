import { Session } from 'src/stuff/injectables/Session';
import { Communicator } from './Communicator';
import { MeasLine } from './MeasLine';
import { MeasLinesRet } from './MeasLinesRet';
import { Temperature } from './Temperature';

export class LiveUpdater {
  private miliSecInHour: number = 1000 * 60 * 60;
  private miliSecInMinute: number = 1000 * 60;
  public onUpdate: (lines: MeasLine) => void;
  public detectChanges: () => void;
  public runLiveUpdate: boolean;
  private period: number = -10000;
  private latestTime: number = 0;
  private communicator: Communicator;
  private notRecievedCnt: number;

  public constructor(communicator: Communicator, public session: Session) {
    this.communicator = communicator;
  }

  public beginUpdate(): void {
    this.notRecievedCnt = 0;
    this.updateAllMeases(0, false);
  }
  public oneTimeUpdate(): void {
    this.updateAllMeases(0, true);
  }
  
  private afterFetchData(lines: MeasLinesRet, runNext: boolean) {
    let runNextTime = runNext && this.shouldLiveUpdate();
    if (!lines.success) this.afterFetchNoData(runNextTime);
    else this.afterFetchSomeData(lines, runNextTime);
  }
  private afterFetchNoData(runNexTime: boolean): void {
    if (!runNexTime) return;
    this.notRecievedCnt++;
    let period = this.tryAgainIn(this.period, this.notRecievedCnt);
    if (period <= 0) return;
    period = this.randomizePeriod(period);
    console.log("nothing recieved. try again in " + period + "ms");
    setTimeout(() => this.updateAllMeases(this.latestTime, false), period);
  }
  private afterFetchSomeData(lines: MeasLinesRet, runNexTime: boolean): void {
    this.notRecievedCnt = 0;
    this.insertNumbers(lines);
    this.latestTime = Math.max(this.latestTime, this.getLatestTime(lines));
    this.period = this.getLastPeriod(lines);
    for (let newMeasLine of lines.measurements) this.onUpdate(newMeasLine);
    this.detectChanges();
    if (!runNexTime) return;
    setTimeout(() => this.updateAllMeases(this.latestTime, false), this.randomizePeriod(this.period));
  }
  private updateAllMeases(time: number, justOneTime: boolean): void {
    if (!this.runLiveUpdate) return;
    if (!this.isSomeoneLogged() || this.session.isUberAdmin()) this.communicator.getAllMeasurements(time, (measLines: MeasLinesRet) => this.afterFetchData(measLines, !justOneTime));
    else this.communicator.getAllUsersMeasurements(time, this.session.getEncryptedSession(), (measLines: MeasLinesRet) => this.afterFetchData(measLines, !justOneTime));
  }
  private getLastPeriod(lines: MeasLinesRet): number {
    let accumulator: Temperature = new Temperature();
    accumulator.time = 0;
    accumulator.delay = Infinity;
    return lines.measurements.map((measLine: MeasLine) => {
      let lastTemp = Temperature.CreteCopy(measLine.temperatures[measLine.temperatures.length - 1]);
      lastTemp.delay = lastTemp.delay * this.miliSecInHour;
      return lastTemp;
    }).reduce((accumulator: Temperature, currentValue: Temperature) => {
      if (accumulator.time < currentValue.time) return currentValue;
      else return accumulator;
    }, accumulator).delay;
  }
  private getLatestTime(lines: MeasLinesRet): number {
    return lines.measurements.map((measLine: MeasLine) => {
      let ret = measLine.temperatures
      .map((temperature: Temperature) => temperature.time)
      .reduce((accumulator: number, currentValue: number) => Math.max(accumulator, currentValue), 0);
      return ret;
    }).reduce((accumulator: number, currentValue: number) => Math.max(accumulator, currentValue), 0);
  }
  private insertNumbers(lines: MeasLinesRet) : void {
    if (lines == undefined) return;
    for (let measLine of lines.measurements) {
      if (measLine == undefined) continue;
      measLine.temperatures.forEach((temperature: Temperature) => {
        temperature.delay = parseInt(temperature.delay.toString(), 10) / this.miliSecInMinute;
        temperature.temp = parseFloat(temperature.temp.toString());
      });
    }
  }
  private randomizePeriod(period: number): number {
    console.log("using period " + period);
    let random = Math.random() * period;
    period = (1.2 * period) + random;
    console.log("actual period " + period + " (will truly wait)");
    return period;
  }
  private isSomeoneLogged(): boolean {
    return (this.session.getLoginName() != undefined) && (this.session.getLoginName() != "");
  }
  private shouldLiveUpdate(): boolean {
    return this.runLiveUpdate && (this.session.isUberAdmin() || this.session.isLogged());
  }
  private tryAgainIn(period: number, notRecievedCnt: number): number {
    if (period <= 0) return -1;
    if (notRecievedCnt < 0) return -1;
    if (period <= 5000) return this.tryAgainIn4Xs(4 * period, notRecievedCnt, 10);
    if (period <= 10000) return this.tryAgainIn4Xs(2 * period, notRecievedCnt, 10);
    else return this.tryAgainIn4Xs(period, notRecievedCnt, 10);
  }
  private tryAgainIn4Xs(period: number, notRecievedCnt: number, maxTimes: number): number {
    if (notRecievedCnt <= maxTimes) return notRecievedCnt * period;
    return -1;
  }
}