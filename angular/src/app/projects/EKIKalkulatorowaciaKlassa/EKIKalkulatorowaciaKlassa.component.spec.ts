import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EKIKalkulatorowaciaKlassaComponent } from './EKIKalkulatorowaciaKlassa.component';

describe('EKIKalkulatorowaciaKlassaComponent', () => {
  let component: EKIKalkulatorowaciaKlassaComponent;
  let fixture: ComponentFixture<EKIKalkulatorowaciaKlassaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EKIKalkulatorowaciaKlassaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EKIKalkulatorowaciaKlassaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
