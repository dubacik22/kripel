import { Component, ChangeDetectorRef, AfterViewInit, Input, ViewChild, ElementRef, OnDestroy, EventEmitter, Output } from '@angular/core';
import { Strings } from 'src/app/Strings';
import { MyRouting } from 'src/stuff/injectables/MyRouting';

@Component({
  selector: 'EKProperty',
  templateUrl: './EKProperty.component.html',
  styleUrls: ['./EKProperty.component.css'],
})
export class EKPropertyComponent implements AfterViewInit, OnDestroy {

  private _toDo: string;
  @Output('toDoChange') toDoChange: EventEmitter <string> = new EventEmitter();
  @Input() public get toDo(): string {
    return this._toDo;
  }
  public set toDo(value: string) {
    if (value == this._toDo) return;
    this._toDo = value;
    this.toDoChange.emit(this._toDo);
    this.detectChanges();
  }
  
  public get EKIKalkulatorowaciaKlassaUrl(): string { return Strings.EKIKalkulatorowaciaKlassaUrl; }

  private _compactSubClass:string;
  @Input()
  public get compactSubClass():string{
    return this._compactSubClass;
  }
  public set compactSubClass(value:string) {
    if (value == this._compactSubClass) return;
    this._compactSubClass = value;
    this.detectChanges();
  }
  private _scdStart: number;
  @Input()
  public get scdStart(): number {
    return this._scdStart;
  }
  public set scdStart(value: number) {
    if (this._scdStart == value) return;
    this._scdStart = value;
    this.detectChanges();
  }
  private _scdEnd: number;
  @Input()
  public get scdEnd(): number {
    return this._scdEnd;
  }
  public set scdEnd(value: number) {
    if (this._scdEnd == value) return;
    this._scdEnd = value;
    this.detectChanges();
  }

  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private chDref: ChangeDetectorRef, private router: MyRouting) { }

  public ngAfterViewInit() {
    this.chDref.detach();
  }
  public ngOnDestroy() {
  }

  public showItem(item: string) {
    this.toDo = item;
    this.detectChanges();
  }
}
