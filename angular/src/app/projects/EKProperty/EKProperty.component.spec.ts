import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EKPropertyComponent } from './EKProperty.component';

describe('EKPropertyComponent', () => {
  let component: EKPropertyComponent;
  let fixture: ComponentFixture<EKPropertyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EKPropertyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EKPropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
