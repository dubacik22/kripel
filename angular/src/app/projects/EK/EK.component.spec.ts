import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EKComponent } from './EK.component';

describe('EKComponent', () => {
  let component: EKComponent;
  let fixture: ComponentFixture<EKComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EKComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EKComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
