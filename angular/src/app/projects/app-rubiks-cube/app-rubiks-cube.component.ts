import { Component, Input, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { iNotifyPropertyChanged } from 'src/stuff/iNotifyProperyChanged';

@Component({
  selector: 'app-rubiks-cube',
  templateUrl: './app-rubiks-cube.component.html',
  styleUrls: ['./app-rubiks-cube.component.css']
})
export class AppRubiksCubeComponent implements AfterViewInit {
  public _cubeSize: number = 3;
  public get cubeSize(): number {
    return this._cubeSize;
  }
  public set cubeSize(value: number) {
    if (value == this._cubeSize) return;
    this._cubeSize = value;
    this.detectChanges();
  }
  private _compactSubClass:string;
  @Input()
  public get compactSubClass():string{
    return this._compactSubClass;
  }
  public set compactSubClass(value:string) {
    if (value == this._compactSubClass) return;
    this._compactSubClass=value;
    this.detectChanges();
  }

  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private chDref: ChangeDetectorRef) { 
  }

  ngAfterViewInit() {
    this.chDref.detach();
  }
  private _color: string;
  public get color(): string {
    return this._color;
  }
  public set color(value: string) {
    if (this._color == value) return;
    this._color = value;
    this.detectChanges();
  }

  

  unlistenTo(subscribtion: Subscription) {
    if (subscribtion) subscribtion.unsubscribe();
  }
  listenTo(to: iNotifyPropertyChanged, toDo: (propName: string) => void): Subscription {
      if (to) return to.propertyChange.subscribe(toDo);
      return undefined;
  }
}
