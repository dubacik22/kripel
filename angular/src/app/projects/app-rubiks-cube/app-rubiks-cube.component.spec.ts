import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppRubiksCubeComponent } from './app-rubiks-cube.component';

describe('AppRubiksCubeComponent', () => {
  let component: AppRubiksCubeComponent;
  let fixture: ComponentFixture<AppRubiksCubeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppRubiksCubeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppRubiksCubeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
