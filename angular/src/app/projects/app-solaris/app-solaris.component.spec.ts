import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppSolarisComponent } from './app-solaris.component';

describe('AppSolarisComponent', () => {
  let component: AppSolarisComponent;
  let fixture: ComponentFixture<AppSolarisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppSolarisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppSolarisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
