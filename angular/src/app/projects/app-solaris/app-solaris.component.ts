import { Component, Input, ChangeDetectorRef, AfterViewInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { iNotifyPropertyChanged } from 'src/stuff/iNotifyProperyChanged';
import { PlanetSystem } from 'src/stuff/solaris/PlanetSystem';
import { Vector } from 'src/stuff/solaris/Vector';
import { MathExt } from 'src/stuff/MathExt';
import { SolarisMessenger } from 'src/stuff/solaris/SolarisMessenger';

@Component({
  selector: 'app-solaris',
  templateUrl: './app-solaris.component.html',
  styleUrls: ['./app-solaris.component.css']
})
export class AppSolarisComponent implements AfterViewInit {

  private _compact:boolean;
  @Input()
  public get compact():boolean{
    return this._compact;
  }
  public set compact(value:boolean) {
    if (value == this._compact) return;
    this._compact=value;
    this.menuVisible = false;
    this.detectChanges();
  }

  @ViewChild('mainWrapper') mainWrapperRef: ElementRef;
  public get mainWrapper(): HTMLElement {
    return this.mainWrapperRef.nativeElement;
  }

  private defaultDispUnitDiv = 1.496e11 / 200;

  public _stepLen: number = 20000;
  public get stepLen(): number {
    return this._stepLen;
  }
  @Input('stepLen')
  public set stepLen(value: number) {
    if (value == this._stepLen) return;
    this._stepLen = value;
    this.detectChanges();
  }

  private _refreshRate: number = 50;
  public get refreshRate(): number {
    return this._refreshRate;
  }
  @Input('refreshRate')
  public set refreshRate(value: number) {
    if (value == this._refreshRate) return;
    this._refreshRate = value;
    this.detectChanges();
  }

  public _spaceFriction: number = 0;
  public get spaceFriction(): number {
    return this._spaceFriction;
  }
  @Input('spaceFriction')
  public set spaceFriction(value: number) {
    if (value == this._spaceFriction) return;
    this._spaceFriction = value;
    this.detectChanges();
  }

  public _planetsCnt: number = 50;
  public get planetsCnt(): number {
    return this._planetsCnt;
  }
  @Input('planetsCnt')
  public set planetsCnt(value: number) {
    if (value == this._planetsCnt) return;
    this._planetsCnt = value;
    this.detectChanges();
  }
  public fps: number = 0;
  public cnt: number = 0;
  public zoom: number = 1;

  private _planetSystem: PlanetSystem;
  public get planetSystem(): PlanetSystem {
    return this._planetSystem;
  }
  public set planetSystem(value: PlanetSystem) {
    if (this._planetSystem == value) return;
    this._planetSystem = value;
    this.detectChanges();
  }
  public get rotateMove():string {
    if (this.drag) {
      return "drag";
    } else {
      return "rotate";
    }
  }
  public drag: boolean = true;
  private messenger: SolarisMessenger = new SolarisMessenger();

  private oldTime: number;
  private myPlanetSystemCalcUpdate: (o: any) => void = (o: any) => {
    let now = Date.now();
    this.fps = (1000 / (now-this.oldTime));
    this.oldTime = now;
    if (this.planetSystem !== undefined) {
      this.cnt = this.planetSystem.planets.length;
    }
    this.detectChanges();
  };
  private myPlanetSystemUpdate: (o: any) => void = (o: any) => {
    if (this.planetSystem !== undefined) {
      this.cnt = this.planetSystem.planets.length;
      this.zoom = this.defaultDispUnitDiv / this.planetSystem.dispUnitsDiv;
    }
    this.detectChanges();
  };

  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private chDref: ChangeDetectorRef) {
  }

  ngAfterViewInit() {
    this.chDref.detach();
    this.messenger.subscribe(SolarisMessenger.SYST_CHANGED, this.myPlanetSystemUpdate);
    this.messenger.subscribe(SolarisMessenger.PLANETS_UPDATE, this.myPlanetSystemCalcUpdate);
  }

  public menuVisible: boolean = true;
  public toggleMenuWrapper() {
    this.menuVisible = !this.menuVisible;
    this.detectChanges();
  }

  public toggleDrag(): void {
    this.drag = !this.drag;
    this.detectChanges();
  }

  public clear(): void {
    let maxMass = 1.989e30;
    let planetSystem = this.planetSystem;
    if (planetSystem === undefined) {
      planetSystem = new PlanetSystem();
    } else {
      planetSystem.stopSystem();
      planetSystem = new PlanetSystem();
    }
    planetSystem.dispPosCor2View = new Vector(0,0,0);
    planetSystem.dispAngCor = new Vector(0,0,0);
    planetSystem.dispUnitsDiv = this.defaultDispUnitDiv;
    planetSystem.radiusDivMassPow2 = 2/Math.sqrt(maxMass);
    planetSystem.planets = []
    this.createCross(planetSystem);
    this.planetSystem = planetSystem;
    this.messenger.notify(SolarisMessenger.PLANETS_UPDATE, undefined);
  }

  private createCross(planetSystem: PlanetSystem):void {
    let ScreenCube = new Vector(this.mainWrapper.offsetWidth, this.mainWrapper.offsetHeight, this.mainWrapper.offsetHeight);
    let screenCubeDiv2 = ScreenCube.divideScalar(2);
    planetSystem.addCross(screenCubeDiv2, 1000);
  }

  public addRandomVelPlanets(): void {
    let planetSystem: PlanetSystem = this.planetSystem;
    if (planetSystem === undefined) {
      this.clear();
      planetSystem = this.planetSystem;
    }
    let maxMass = 1.989e30;
    let minMass = 1e15;
    let ScreenCube = new Vector(this.mainWrapper.offsetWidth, this.mainWrapper.offsetHeight, this.mainWrapper.offsetHeight);
    let screenCubeDiv2 = ScreenCube.divideScalar(2);
    let numberOfPlanets = this.planetsCnt;
    let cubeOfPlanetsSize = new Vector(this.mainWrapper.offsetWidth*2, this.mainWrapper.offsetHeight*2, this.mainWrapper.offsetHeight*2);
    let cubeOfPlanetsSizeDiv2 = Vector.from(cubeOfPlanetsSize).divideScalar(2);
    let screenCubeDiv2MinusCubeOfPlanetsSizeDiv2 = Vector.from(screenCubeDiv2).substractVector(cubeOfPlanetsSizeDiv2);
    for (let i = 0; i < numberOfPlanets; i++) {
      let mass = MathExt.randomNumFromTo(minMass, maxMass);
      let center = MathExt.randomVectorToV(cubeOfPlanetsSize).addVector(screenCubeDiv2MinusCubeOfPlanetsSizeDiv2);
      let center2 = MathExt.randomVectorFromTo(47, 53).rotateVector(MathExt.randomVectorTo(Math.PI*2)).addVector(center);
      
      planetSystem.addMassPlanet("", "rgba(0,0,0,1)", mass, center);
      let planet = planetSystem.addMassPlanet("", "rgba(0,0,0,1)", mass, center2);
      planet.oldVelocityVector = MathExt.randomVectorTo(1e5).rotateVector(MathExt.randomVectorTo(Math.PI*2));
    }
    if (this.planetSystem === undefined) {
      this.planetSystem = planetSystem;
    }
    this.messenger.notify(SolarisMessenger.PLANETS_UPDATE, undefined);
  }

  public addRandomStaticPlanets(): void {
    let planetSystem: PlanetSystem = this.planetSystem;
    if (planetSystem === undefined) {
      this.clear();
      planetSystem = this.planetSystem;
    }
    let maxMass = 1.989e30;
    let minMass = 1e15;
    let ScreenCube = new Vector(this.mainWrapper.offsetWidth, this.mainWrapper.offsetHeight, this.mainWrapper.offsetHeight);
    
    let screenCubeDiv2 = Vector.from(ScreenCube).divideScalar(2);
    let numberOfPlanets = this.planetsCnt;
    let cubeOfPlanetsSize = new Vector(this.mainWrapper.offsetWidth*2, this.mainWrapper.offsetHeight*2, this.mainWrapper.offsetHeight*2);
    let cubeOfPlanetsSizeDiv2 = Vector.from(cubeOfPlanetsSize).divideScalar(2);
    let screenCubeDiv2MinusCubeOfPlanetsSizeDiv2 = screenCubeDiv2.substractVector(cubeOfPlanetsSizeDiv2);

    let scx5 = ScreenCube.x/5;
    console.log(ScreenCube.x)
    console.log(scx5)
    for (let i=0; i<=ScreenCube.x; i+=ScreenCube.x/9) {
      for (let j=0; j<=ScreenCube.y; j+=ScreenCube.y/5) {
        for (let k=0; k<ScreenCube.z; k+=ScreenCube.z/5) {
          let center = new Vector(i,j,k)

          planetSystem.addMassPlanet("", "rgba(0,0,0,1)", minMass, center);
        }
      }
    }

    /*for (let i = 0; i < numberOfPlanets; i++) {
      let mass = MathExt.randomNumFromTo(minMass, maxMass);
      let center = MathExt.randomVectorToV(cubeOfPlanetsSize).addVector(screenCubeDiv2MinusCubeOfPlanetsSizeDiv2);
      let center2 = MathExt.randomVectorFromTo(47, 53).rotateVector(MathExt.randomVectorTo(Math.PI*2)).addVector(center);
      
      planetSystem.addMassPlanet("", "rgba(0,0,0,1)", mass, center);
      planetSystem.addMassPlanet("", "rgba(0,0,0,1)", mass, center2);
    }*/
    if (this.planetSystem === undefined) {
      this.planetSystem = planetSystem;
    }
    this.messenger.notify(SolarisMessenger.PLANETS_UPDATE, undefined);
  }
  
  public createSomeMassSystem(): void {
    if (this.planetSystem !== undefined) {
      this.planetSystem.stopSystem();
    }
    let planetSystem = new PlanetSystem();
    let maxMass = 1.989e30;
    planetSystem.radiusDivMassPow2 = 2/Math.sqrt(maxMass);
    planetSystem.dispUnitsDiv = 1.496e11 / 200;

    let s =planetSystem.addMassPlanet("1", "rgba(0,0,0,1)", maxMass, new Vector(250,300,0));
    let p = planetSystem.addMassPlanet("2", "rgba(0,0,0,1)", 1e30, new Vector(300,350,0));
    p.oldVelocityVector = new Vector(1e4, -5e4, 0)


    s =planetSystem.addMassPlanet("1", "rgba(0,0,0,1)", maxMass, new Vector(650,300,0));
    p = planetSystem.addMassPlanet("2", "rgba(0,0,0,1)", 1e20, new Vector(700,350,0));
    p.oldVelocityVector = new Vector(1e4, -5e4, 0)

    planetSystem.log();

    this.planetSystem = planetSystem;
    this.detectChanges();
  }

  public pause(): void {
    if (this.planetSystem === undefined) return;
    //this.planetSystem.step()
    //return;
    if (this.planetSystem.isRun) {
      this.planetSystem.stopSystem();
    } else {
      this.planetSystem.runSystem();
    }
  }

  unlistenTo(subscribtion: Subscription) {
    if (subscribtion) subscribtion.unsubscribe();
  }
  listenTo(to: iNotifyPropertyChanged, toDo: (propName: string) => void): Subscription {
      if (to) return to.propertyChange.subscribe(toDo);
      return undefined;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    //this.resetSolarSystem();
  }
}
