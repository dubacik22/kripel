import { MyPoint } from './MyPoint';

export class SubsquareData{
    public bkgLayerSize: number = 0;
    public frgLayerSize: number = 0;

    public frgTranslateX: number = 0;
    public frgTranslateY: number = 0;
    public frgTranslateZ: number = 0;

    public bkgTranslateX: number = 0;
    public bkgTranslateY: number = 0;
    public bkgTranslateZ: number = 0;

    public firstRotateX: number = 0;
    public firstRotateY: number = 0;
    public firstRotateZ: number = 0;

    public secondRotateX: number = 0;
    public secondRotateY: number = 0;
    public secondRotateZ: number = 0;

    public xCoord: number = 0;
    public yCoord: number = 0;
    public zCoord: number = 0;

    public color: string;
    public zIndex: number;

    public verbose: boolean;
    public draw: boolean;

    public points: MyPoint[] = [];

    public get centerPoint(): MyPoint {
        let x: number = 0;
        let y: number = 0;
        let z: number = 0;
        let cnt: number = this.points.length;
        this.points.forEach((point: MyPoint) => {
            x += point.x;
            y += point.y;
            z += point.z;
        });
        x /= cnt;
        y /= cnt;
        z /= cnt;
        return new MyPoint(x, y, z);
    }
    
    public constructor(points: MyPoint[]) {
        this.points = points;
    }
    public reset() {
        this.points.forEach((point: MyPoint) => {
            point.reset();
        });
    }
    public addPoint(x: number, y: number, z: number) {
        if (!this.points) this.points = [];
        let point: MyPoint = new MyPoint(x, y, z);
        point.verbose = this.verbose;
        this.points.push(point);
    }
    public translate(translateX: number, translateY: number, translateZ: number) {
        this.points.forEach((point: MyPoint) => {
            point.translate(translateX, translateY, translateZ);
        });
    }
    public rotateDeg(rotateX: number, rotateY: number, rotateZ: number) {
        this.points.forEach((point: MyPoint) => {
            point.rotateDeg(rotateX, rotateY, rotateZ);
        });
    }
    public get smallestZPoint(): number {
        let actZ: number = Number.POSITIVE_INFINITY;
        this.points.forEach((point: MyPoint) => {
            if (point.z < actZ) actZ = point.z;
        });
        return actZ;
    }
}