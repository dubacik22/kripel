import { MyPoint } from './MyPoint';
import { SubsquareData } from './SubsquareData';

export class ZIndexer {
    private datas: SubsquareData[] = [];
    public constructor() {}

    public addElement(data: SubsquareData) {
        this.datas.push(data);
    }
    public createElements() {
        this.datas = [];
    }

    public setZIndexes() {
        this.sortByZ(this.datas);
        this.setZIndexFromOrder(this.datas);
    }

    private setZIndexFromOrder(elements: SubsquareData[]) {
        for (let i: number = 0; i < elements.length; i++) {
            let subCube: SubsquareData = elements[i];
            subCube.zIndex = i;
        }
    }
    private sortByZ(elements: SubsquareData[]) {
        elements.sort((a: SubsquareData, b: SubsquareData) => {
            return b.smallestZPoint - a.smallestZPoint;
        });
    }
    public reset(element: SubsquareData) {
        element.reset();
    }


    public get line(): string {
        let ret: string = '';
        this.datas.forEach((element: SubsquareData) => {
            if (!element.points) return;
            //if (!element.draw) return;
            if (element.points.length < 2) return;
            let hadFirst: boolean = false;
            element.points.forEach((point: MyPoint) => {
                if (!hadFirst) ret += 'M'+point.strLine;
                else ret += ' L'+point.strLine;
                hadFirst = true;
            });
            ret += ' Z ';
        });
        return ret;
    }
}