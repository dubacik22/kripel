import { SubsquareData } from './SubsquareData';

export class SubsquareMouseDownData {
    public subcube: SubsquareData;
    public mx: number;
    public my: number;
}