import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RubiksSubCubeComponent } from './rubiks-sub-cube.component';

describe('RubiksSubCubeComponent', () => {
  let component: RubiksSubCubeComponent;
  let fixture: ComponentFixture<RubiksSubCubeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RubiksSubCubeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RubiksSubCubeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
