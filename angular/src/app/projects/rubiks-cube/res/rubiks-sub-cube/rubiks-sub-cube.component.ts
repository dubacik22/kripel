import { Component, ChangeDetectorRef, AfterViewInit, OnDestroy, Input, ElementRef, ViewChild, HostListener, Output, EventEmitter } from '@angular/core';
import { SubsquareData } from '../SubsquareData';
import { SubsquareMouseDownData } from '../SubsquareMouseDownData';

@Component({
  selector: 'rubiks-sub-cube',
  templateUrl: './rubiks-sub-cube.component.html',
  styleUrls: ['./rubiks-sub-cube.component.css'],
})
export class RubiksSubCubeComponent implements AfterViewInit, OnDestroy {
  @Input() public verbose: boolean = false;
  @Input() public draw: boolean = false;

  private _subsquareData: SubsquareData;
  @Input('subsquareData')
  public get subsquareData(): SubsquareData {
    return this._subsquareData;
  }
  public set subsquareData(value: SubsquareData) {
    if (value == this._subsquareData) return;
    this._subsquareData = value;
    this.detectChanges();
  }
  private _bkgSquareSize: number;
  @Input('bkgSquareSize')
  public get bkgSquareSize(): number {
    return this._bkgSquareSize;
  }
  public set bkgSquareSize(value: number) {
    if (value == this._bkgSquareSize) return;
    this._bkgSquareSize = value;
    this.detectChanges();
  }
  private _frgSquareSize: number;
  @Input('frgSquareSize')
  public get frgSquareSize(): number {
    return this._frgSquareSize;
  }
  public set frgSquareSize(value: number) {
    if (value == this._frgSquareSize) return;
    this._frgSquareSize = value;
    this.detectChanges();
  }

  private _squareColor: string;
  @Input('squareColor')
  public get squareColor(): string {
    return this._squareColor;
  }
  public set squareColor(value: string) {
    if (value == this._squareColor) return;
    this._squareColor = value;
    this.detectChanges();
  }
  
  private _zIndex: number = 0;
  @Input('zIndex')
  public get zIndex(): number {
    return this._zIndex;
  }
  public set zIndex(value: number) {
    if (this._zIndex == value) return;
    this._zIndex = value;
    this.detectChanges();
  }

  private frgSquareTransform: string = '';
  private bkgSquareTransform: string = '';

  public get frgSquareStyle(): any {
    return {width: this._frgSquareSize+'px',
            height: this._frgSquareSize+'px',
            'background-color': this._squareColor,
            zIndex: this._zIndex,
            transform: this.frgSquareTransform};
  }
  public get bkgSquareStyle(): any {
    return {width: this._bkgSquareSize+'px',
            height: this._bkgSquareSize+'px',
            'background-color': 'black',
            zIndex: this._zIndex-1,
            transform: this.bkgSquareTransform};
  }

  private _firstFrgTranslationX: number;
  @Input('fftX')
  public get firstFrgTranlationX(): number {
    return this._firstFrgTranslationX;
  }
  public set firstFrgTranlationX(value: number) {
    if (this._firstFrgTranslationX == value) return;
    this._firstFrgTranslationX = value;
    this.setFrgRotation();
    this.detectChanges();
  }
  private _firstFrgTranslationY: number;
  @Input('fftY')
  public get firstFrgTranslationY(): number {
    return this._firstFrgTranslationY;
  }
  public set firstFrgTranslationY(value: number) {
    if (this._firstFrgTranslationY == value) return;
    this._firstFrgTranslationY = value;
    this.setFrgRotation();
    this.detectChanges();
  }
  private _firstFrgTranslationZ: number;
  @Input('fftZ')
  public get firstFrgTranslationZ(): number {
    return this._firstFrgTranslationZ;
  }
  public set firstFrgTranslationZ(value: number) {
    if (this._firstFrgTranslationZ == value) return;
    this._firstFrgTranslationZ = value;
    this.setFrgRotation();
    this.detectChanges();
  }
  private _firstBkgTranslationX: number;
  @Input('fbtX')
  public get firstBkgTranslationX(): number {
    return this._firstBkgTranslationX;
  }
  public set firstBkgTranslationX(value: number) {
    if (this._firstBkgTranslationX == value) return;
    this._firstBkgTranslationX = value;
    this.setFrgRotation();
    this.detectChanges();
  }
  private _firstBkgTranslationY: number;
  @Input('fbtY')
  public get firstBkgTranslationY(): number {
    return this._firstBkgTranslationY;
  }
  public set firstBkgTranslationY(value: number) {
    if (this._firstBkgTranslationY == value) return;
    this._firstBkgTranslationY = value;
    this.setBkgRotation();
    this.detectChanges();
  }
  private _firstBkgTranslationZ: number;
  @Input('fbtZ')
  public get firstBkgTranslationZ(): number {
    return this._firstBkgTranslationZ;
  }
  public set firstBkgTranslationZ(value: number) {
    if (this._firstBkgTranslationZ == value) return;
    this._firstBkgTranslationZ = value;
    this.setBkgRotation();
    this.detectChanges();
  }

  private _firstRotationX: number;
  @Input('frX')
  public get firstRotationX(): number {
    return this._firstRotationX;
  }
  public set firstRotationX(value: number) {
    if (this._firstRotationX == value) return;
    this._firstRotationX = value;
    this.setFrgRotation();
    this.setBkgRotation();
    this.detectChanges();
  }
  private _firstRotationY: number;
  @Input('frY')
  public get firstRotationY(): number {
    return this._firstRotationY;
  }
  public set firstRotationY(value: number) {
    if (this._firstRotationY == value) return;
    this._firstRotationY = value;
    this.setFrgRotation();
    this.setBkgRotation();
    this.detectChanges();
  }
  private _firstRotationZ: number;
  @Input('frZ')
  public get firstRotationZ(): number {
    return this._firstRotationZ;
  }
  public set firstRotationZ(value: number) {
    if (this._firstRotationZ == value) return;
    this._firstRotationZ = value;
    this.setFrgRotation();
    this.setBkgRotation();
    this.detectChanges();
  }

  private _secondRotationX: number;
  @Input('srX')
  public get secondRotationX(): number {
    return this._secondRotationX;
  }
  public set secondRotationX(value: number) {
    if (this._secondRotationX == value) return;
    this._secondRotationX = value;
    this.setFrgRotation();
    this.setBkgRotation();
    this.detectChanges();
  }
  private _secondRotationY: number;
  @Input('srY')
  public get secondRotationY(): number {
    return this._secondRotationY;
  }
  public set secondRotationY(value: number) {
    if (this._secondRotationY == value) return;
    this._secondRotationY = value;
    this.setFrgRotation();
    this.setBkgRotation();
    this.detectChanges();
  }
  private _secondRotationZ: number;
  @Input('srZ')
  public get secondRotationZ(): number {
    return this._secondRotationZ;
  }
  public set secondRotationZ(value: number) {
    if (this._secondRotationZ == value) return;
    this._secondRotationZ = value;
    this.setFrgRotation();
    this.setBkgRotation();
    this.detectChanges();
  }

  private _thirdRotationX: number;
  @Input('trX')
  public get thirdRotationX(): number {
    return this._thirdRotationX;
  }
  public set thirdRotationX(value: number) {
    if (this._thirdRotationX == value) return;
    this._thirdRotationX = value;
    this.setFrgRotation();
    this.setBkgRotation();
    this.detectChanges();
  }
  private _thirdRotationY: number;
  @Input('trY')
  public get thirdRotationY(): number {
    return this._thirdRotationY;
  }
  public set thirdRotationY(value: number) {
    if (this._thirdRotationY == value) return;
    this._thirdRotationY = value;
    this.setFrgRotation();
    this.setBkgRotation();
    this.detectChanges();
  }
  private _thirdRotationZ: number;
  @Input('trZ')
  public get thirdRotationZ(): number {
    return this._thirdRotationZ;
  }
  public set thirdRotationZ(value: number) {
    if (this._thirdRotationZ == value) return;
    this._thirdRotationZ = value;
    this.setFrgRotation();
    this.setBkgRotation();
    this.detectChanges();
  }

  private _lastRotationX: number;
  @Input('mouseMoveRotationX')
  public get lastRotationX(): number {
    return this._lastRotationX;
  }
  public set lastRotationX(value: number) {
    if (this._lastRotationX == value) return;
    this._lastRotationX = value;
    this.setFrgRotation();
    this.setBkgRotation();
    this.detectChanges();
  }
  private _lastRotationY: number;
  @Input('mouseMoveRotationY')
  public get lastRotationY(): number {
    return this._lastRotationY;
  }
  public set lastRotationY(value: number) {
    if (this._lastRotationY == value) return;
    this._lastRotationY = value;
    this.setFrgRotation();
    this.setBkgRotation();
    this.detectChanges();
  }
  private _lastRotationZ: number;
  @Input('mouseMoveRotationZ')
  public get lastRotationZ(): number {
    return this._lastRotationZ;
  }
  public set lastRotationZ(value: number) {
    if (this._lastRotationZ == value) return;
    this._lastRotationZ = value;
    this.setFrgRotation();
    this.setBkgRotation();
    this.detectChanges();
  }

  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private chDref: ChangeDetectorRef) {}
  public ngAfterViewInit() {
    this.chDref.detach();
  }
  public ngOnDestroy() {}

  @Output('subcubeMouseDown') subcubeMouseDown: EventEmitter<SubsquareMouseDownData> = new EventEmitter();
  private executeSubsquareMouseDown(clientX: number, clientY: number) {
    let subcubeMouseDownData: SubsquareMouseDownData = new SubsquareMouseDownData();
    subcubeMouseDownData.mx = clientX;
    subcubeMouseDownData.my = clientY;
    subcubeMouseDownData.subcube = this.subsquareData;
    this.subcubeMouseDown.emit(subcubeMouseDownData);
  }
  onMouseDown(e: MouseEvent) {
    if (e.buttons != 1) return;
    this.executeSubsquareMouseDown(e.clientX, e.clientY);
  }
  onTouchStart(e: TouchEvent) {
    let touch: Touch = e.touches[0];
    this.executeSubsquareMouseDown(touch.clientX, touch.clientY);
  }

  private setFrgRotation() {
    let transform: string = '';
    transform += this.getLastTransform();
    transform += this.getSecondTransform();
    transform += this.getFirstFrgTransform();
    this.frgSquareTransform = transform;
  }
  private setBkgRotation() {
    let transform: string = '';
    transform += this.getLastTransform();
    transform += this.getSecondTransform();
    transform += this.getFirstBkgTransform();
    this.bkgSquareTransform = transform;
  }

  private getFirstFrgTransform(): string {
    let ret: string = '';
    if (this._firstRotationX !== undefined) ret += ' rotateX(' + this._firstRotationX + 'deg)';
    if (this._firstRotationY !== undefined) ret += ' rotateY(' + this._firstRotationY + 'deg)';
    if (this._firstRotationZ !== undefined) ret += ' rotateZ(' + this._firstRotationZ + 'deg)';
    if (this._firstFrgTranslationX !== undefined) ret += ' translateX(' + this._firstFrgTranslationX + 'px)';
    if (this._firstFrgTranslationY !== undefined) ret += ' translateY(' + this._firstFrgTranslationY + 'px)';
    if (this._firstFrgTranslationZ !== undefined) ret += ' translateZ(' + this._firstFrgTranslationZ + 'px)';
    return ret;
  }
  private getFirstBkgTransform(): string {
    let ret: string = '';
    if (this._firstRotationX !== undefined) ret += ' rotateX(' + this._firstRotationX + 'deg)';
    if (this._firstRotationY !== undefined) ret += ' rotateY(' + this._firstRotationY + 'deg)';
    if (this._firstRotationZ !== undefined) ret += ' rotateZ(' + this._firstRotationZ + 'deg)';
    if (this._firstBkgTranslationX !== undefined) ret += ' translateX(' + this._firstBkgTranslationX + 'px)';
    if (this._firstBkgTranslationY !== undefined) ret += ' translateY(' + this._firstBkgTranslationY + 'px)';
    if (this._firstBkgTranslationZ !== undefined) ret += ' translateZ(' + this._firstBkgTranslationZ + 'px)';
    return ret;
  }
  private getSecondTransform(): string {
    let rx: number = 0;
    let ry: number = 0;
    let rz: number = 0;
    if (this.secondRotationX !== undefined) rx += this._secondRotationX;
    if (this.secondRotationY !== undefined) ry += this._secondRotationY;
    if (this.secondRotationZ !== undefined) rz += this._secondRotationZ;
    let ret: string = ' rotateX(' + rx + 'deg) rotateY(' + ry + 'deg) rotateZ(' + rz + 'deg)';
    return ret;
  }
  private getLastTransform(): string {
    let rx: number = 0;
    let ry: number = 0;
    let rz: number = 0;
    if (this.thirdRotationX !== undefined) rx += this._thirdRotationX;
    if (this.thirdRotationY !== undefined) ry += this._thirdRotationY;
    if (this.thirdRotationZ !== undefined) rz += this._thirdRotationZ;
    if (this.lastRotationX !== undefined) rx += this._lastRotationX;
    if (this.lastRotationY !== undefined) ry += this._lastRotationY;
    if (this.lastRotationZ !== undefined) rz += this._lastRotationZ;
    let ret: string = ' rotateX(' + rx + 'deg) rotateY(' + ry + 'deg) rotateZ(' + rz + 'deg)';
    return ret;
  }
}
