export class MyPoint {
    private ox: number;
    private oy: number;
    private oz: number;
    public verbose: boolean;
    public x: number;
    public y: number;
    public z: number;
    public constructor(x: number, y: number, z: number) {
        this.ox = x;
        this.oy = y;
        this.oz = z;
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public reset() {
        this.x = this.ox;
        this.y = this.oy;
        this.z = this.oz;
    }
    public translate(trX: number, trY: number, trZ: number) {
        if (trX) this.x += trX;
        if (trY) this.y += trY;
        if (trZ) this.z += trZ;
    }
    private rotateX(rot: number) {
        let rrot: number = this.radians(-rot);
        let cos: number = Math.cos(rrot);
        let sin: number = Math.sin(rrot);
        //if (this.verbose) console.log('rotX'+rot)
        //let y: number = (cos*this.y) -(sin*this.z);
        //let z: number = (cos*this.z) -(sin*this.y);
        let y: number = +(cos*this.y) +(sin*this.z);
        let z: number = +(cos*this.z) -(sin*this.y);

        this.y = y;
        this.z = z;
    }
    private rotateY(rot: number) {
        let rrot: number = this.radians(rot);
        let cos: number = Math.cos(rrot);
        let sin: number = Math.sin(rrot);
        //if (this.verbose) console.log('rotY'+rot)
        //let x: number = (cos*this.x) -(sin*this.z);
        //let z: number = (cos*this.z) -(sin*this.x);
        let x: number = +(cos*this.x) -(sin*this.z);
        let z: number = +(cos*this.z) +(sin*this.x);

        this.x = x;
        this.z = z;
    }
    private rotateZ(rot: number) {
        let rrot: number = this.radians(rot);
        let cos: number = Math.cos(rrot);
        let sin: number = Math.sin(rrot);
        //if (this.verbose) console.log('rotZz'+rot)
        let x: number = (cos*this.x) - (sin*this.y);
        let y: number = (sin*this.x) + (cos*this.y);

        this.x = x;
        this.y = y;
    }
    public rotateDeg(rotX: number, rotY: number, rotZ: number) {
       //if (this.verbose) console.log('rotX:'+rotX+' rotY:'+rotY+' rotZ:'+rotZ)
        this.rotateZ(rotZ);
        this.rotateY(rotY);
        this.rotateX(rotX);
    }
    private radians(degrees: number) {
        return degrees * Math.PI / 180;
    }
    public get str(): string {
        let x: number = Math.round(this.x*10)/10;
        let y: number = Math.round(this.y*10)/10;
        let z: number = Math.round(this.z*10)/10;
        return '['+x+','+y+','+z+']';
    }
    public get strLine(): string {
        let x: number = this.x;
        let y: number = this.y;
        x = Math.round(x*10)/10;
        y = Math.round(y*10)/10;
        return x+' '+y;
    }
}