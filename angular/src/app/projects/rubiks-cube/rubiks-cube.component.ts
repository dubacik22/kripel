import { Component, ChangeDetectorRef, AfterViewInit, OnDestroy, Input, ViewChildren, QueryList, ElementRef, HostListener } from '@angular/core';
import { ZIndexer } from './res/ZIndexer';
import { Stroke } from 'angular-svg';
import { SubsquareData } from './res/SubsquareData';
import { MyPoint } from './res/MyPoint';
import { SubsquareMouseDownData } from './res/SubsquareMouseDownData';

@Component({
  selector: 'rubiks-cube',
  templateUrl: './rubiks-cube.component.html',
  styleUrls: ['./rubiks-cube.component.css'],
})
export class RubiksCubeComponent implements AfterViewInit, OnDestroy {
  //row column layer
  @Input('cubeCoverage')
  public cubeCoverage: number = 1;

  private subcubeColor: string[] = ['red', 'white', 'yellow', 'blue', 'green', 'orange'];
  public subcubeFrgSize: number = 0;
  public gap: number = 0;
  public halfGap: number = 0;

  private _subcubeBkgSize: number = 0;
  @Input('subcubeSize')
  public get subcubeBkgSize(): number {
    return this._subcubeBkgSize;
  }
  public set subcubeBkgSize(value: number) {
    if (value == this._subcubeBkgSize) return; 
    this._subcubeBkgSize = value * 1;
    this.createsubsquaresData();
    this.refreshLine();
    this.detectChanges();
  }

  private _subcubesCnt: number = 0
  @Input('subcubesCnt')
  public get subcubesCnt(): number {
    return this._subcubesCnt;
  }
  public set subcubesCnt(value: number) {
    if (value == this._subcubesCnt) return;
    this._subcubesCnt = value * 1;
    this.createsubsquaresData();
    this.refreshLine();
    this.detectChanges();
  }
  public rotations: number[] = [];

  private moveMultiplier: number = 0.01;
  public get subcubeStyle(): any {
    return {width: this.subcubeFrgSize+'px', height: this.subcubeFrgSize+'px'};
  }
  public subsquaresData: SubsquareData[];
  private zIndexer: ZIndexer = new ZIndexer();
  private _mouseMoveRotationX: number = 0;
  public get mouseMoveRotationX(): number { return this._mouseMoveRotationX; }
  private _mouseMoveRotationY: number = 0;
  public get mouseMoveRotationY(): number { return this._mouseMoveRotationY; }

  private _thirdRotationX: number = 0;
  @Input('trX')
  public get thirdRotationX(): number {
    return this._thirdRotationX;
  }
  public set thirdRotationX(value: number) {
    if (this._thirdRotationX == value) return;
    this._thirdRotationX = value * 1;
    this.createsubsquaresData();
    this.refreshLine();
    this.detectChanges();
  }
  private _thirdRotationY: number = 0;
  @Input('trY')
  public get thirdRotationY(): number {
    return this._thirdRotationY;
  }
  public set thirdRotationY(value: number) {
    if (this._thirdRotationY == value) return;
    this._thirdRotationY = value * 1;
    this.createsubsquaresData();
    this.refreshLine();
    this.detectChanges();
  }
  private _thirdRotationZ: number = 0;
  @Input('trZ')
  public get thirdRotationZ(): number {
    return this._thirdRotationZ;
  }
  public set thirdRotationZ(value: number) {
    if (this._thirdRotationZ == value) return;
    this._thirdRotationZ = value * 1;
    this.createsubsquaresData();
    this.refreshLine();
    this.detectChanges();
  }

  private mouseX: number;
  private mouseY: number;

  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private chDref: ChangeDetectorRef) {}
  public ngAfterViewInit() {
    this.chDref.detach();
  }
  public ngOnDestroy() {}

  private rotateCubeOnMouseMove(rotX: number, rotY: number) {
    let mouseIncrementX: number = (rotX - this.mouseX) * this.moveMultiplier;
    let mouseIncrementY: number = (rotY - this.mouseY) * this.moveMultiplier;
    this.mouseX = rotX;
    this.mouseY = rotY;
    this._mouseMoveRotationX += this.calcRotation(mouseIncrementY);
    this._mouseMoveRotationY += this.calcRotation(-mouseIncrementX);
    this.rotateOverflowedMouseMoveRot();
    this.subsquaresData.forEach((data: SubsquareData) => {
      this.transformMySubcube(data);
    });
    this.setZIndexes();
    this.detectChanges();
    this.refreshLine();
  }
  private rotateFloor() {

  }
  @HostListener('document:mousedown', ['$event'])
  onMouseDown(e: MouseEvent) {
    if (e.buttons != 1) return;
    this.mouseX = e.clientX;
    this.mouseY = e.clientY;
  }
  @HostListener('document:touchstart', ['$event'])
  onTouchStart(e: TouchEvent) {
    let touch: Touch = e.touches[0];
    this.mouseX = touch.clientX;
    this.mouseY = touch.clientY;
  }
  @HostListener('document:touchmove', ['$event'])
  onTouchMove(e: TouchEvent) {
    if (!this.rotatingFloors) {
      let touch: Touch = e.touches[0];
      this.rotateCubeOnMouseMove(touch.clientX, touch.clientY);
    } else {
      this.rotateFloor();
    }
  }
  @HostListener('document:mousemove', ['$event'])
  onMouseMove(e: MouseEvent) {
    if (e.buttons != 1) return;
    if (!this.rotatingFloors) {
      this.rotateCubeOnMouseMove(e.clientX, e.clientY);
    } else {
      this.rotateFloor();
    }
  }
  @HostListener('document:mouseup', ['$event'])
  onMouseUp(e: MouseEvent) {
    this.rotatingFloors = false;
  }
  @HostListener('document:touchend', ['$event'])
  onTouchEnd(e: TouchEvent) {
    this.rotatingFloors = false;
  }
  private findSubcube() {

  }
  private rotatingFloors: boolean = false;
  public subcubeMouseDown(subcubeMouseDownData: SubsquareMouseDownData) {
    this.rotatingFloors = true;
    this.detectChanges();
  }
  private transformMySubcube(subcubeData: SubsquareData) {
    subcubeData.reset();
    subcubeData.translate(subcubeData.frgTranslateX, subcubeData.frgTranslateY, subcubeData.frgTranslateZ);
    subcubeData.rotateDeg(subcubeData.firstRotateX, -subcubeData.firstRotateY, subcubeData.firstRotateZ);
    subcubeData.rotateDeg(subcubeData.secondRotateX, -subcubeData.secondRotateY, subcubeData.secondRotateZ);
    subcubeData.rotateDeg(this._mouseMoveRotationX+this.thirdRotationX, -this._mouseMoveRotationY-this.thirdRotationY, this.thirdRotationZ);
  }
  private createSubcubesPositions(evenCnt: boolean, halfSubcubesCnt: number, halfSubcubeSize: number, halfGap: number): number[] {
    let ret: number[] = [];
    if (evenCnt) {
      for (let i: number = -halfSubcubesCnt; i <= halfSubcubesCnt; i++) {
        ret.push(this._subcubeBkgSize * i + halfSubcubeSize);
      }
    } else {
      for (let i: number = -halfSubcubesCnt; i <= halfSubcubesCnt; i++) {
        ret.push(this._subcubeBkgSize * i);
      }
    }
    return ret;
  }
  private createSubsquare(): SubsquareData {
    let firstPos: number = this.subcubeFrgSize / 2;
    let points: MyPoint[] = [];
    points.push(new MyPoint(-firstPos,-firstPos,0));
    points.push(new MyPoint(firstPos,-firstPos,0));
    points.push(new MyPoint(firstPos,firstPos,0));
    points.push(new MyPoint(-firstPos,firstPos,0));
    let ret: SubsquareData = new SubsquareData(points);
    ret.frgLayerSize = this.subcubeFrgSize;
    ret.bkgLayerSize = this._subcubeBkgSize;
    return ret;
  }
  private resetSizes() {
    this.subcubeFrgSize = this._subcubeBkgSize * this.cubeCoverage;
    this.gap = this._subcubeBkgSize - this.subcubeFrgSize;
    this.halfGap = this.gap / 2;
  }
  private createsubsquaresData() {
    this.zIndexer.createElements();
    this.resetSizes();
    let halfSubcubeSize: number = this._subcubeBkgSize / 2;
    let halfSubcubesCnt: number = Math.floor(this._subcubesCnt / 2);
    this.subsquaresData = [];
    let evenSubcubesCnt: boolean = halfSubcubesCnt == this._subcubesCnt / 2;
    let subCubePosition: number[] = this.createSubcubesPositions(evenSubcubesCnt, halfSubcubesCnt, halfSubcubeSize, this.halfGap);

    for (let sideIndex: number = 0; sideIndex < 6; sideIndex++) {
      for (let i: number = 0; i < this._subcubesCnt; i++) {
        for (let j: number = 0; j < this._subcubesCnt; j++) {
          let subsquareData: SubsquareData = this.createSubsquare();
          subsquareData.frgTranslateX = subCubePosition[j];
          subsquareData.frgTranslateY = subCubePosition[i];
          subsquareData.bkgTranslateX = subCubePosition[j] - this.halfGap;
          subsquareData.bkgTranslateY = subCubePosition[i] - this.halfGap;
          let coord1: number;
          let coord2: number;
          if (evenSubcubesCnt) {
            coord1 = i-halfSubcubesCnt;
            if (i >= halfSubcubesCnt) coord1++;
            coord2 = j-halfSubcubesCnt;
            if (j >= halfSubcubesCnt) coord2++;
          } else {
            coord1 = i-halfSubcubesCnt;
            coord2 = j-halfSubcubesCnt;
          }
          switch (sideIndex) {
          case 0:
            this.setBottomProps(evenSubcubesCnt, subsquareData, halfSubcubesCnt, halfSubcubeSize, coord1, coord2);
            break;
          case 1:
            this.setLeftProps(evenSubcubesCnt, subsquareData, halfSubcubesCnt, halfSubcubeSize, coord1, coord2);
            break;
          case 2:
            this.setRightProps(evenSubcubesCnt, subsquareData, halfSubcubesCnt, halfSubcubeSize, coord1, coord2);
            break;
          case 3:
            this.setFrontProps(evenSubcubesCnt, subsquareData, halfSubcubesCnt, halfSubcubeSize, coord1, coord2);
            break;
          case 4:
            this.setBackProps(evenSubcubesCnt, subsquareData, halfSubcubesCnt, halfSubcubeSize, coord1, coord2);
            break;
          case 5:
            this.setTopProps(evenSubcubesCnt, subsquareData, halfSubcubesCnt, halfSubcubeSize, coord1, coord2);
            break;
          default:
            break;
          }
          subsquareData.color = this.subcubeColor[sideIndex];
          this.subsquaresData.push(subsquareData);
          this.zIndexer.addElement(subsquareData);
          this.transformMySubcube(subsquareData);
        }
      }
    }
    //this.rotAX();
    this.setZIndexes();
  }
  private rotAX() {
    this.subsquaresData.forEach((data: SubsquareData) => {
      if (data.zCoord == 1) {
        data.secondRotateZ = 5;
        this.transformMySubcube(data);
      }
    });
  }
  private setLeftProps(evenCnt: boolean, data: SubsquareData,
                       halfSubcubesCnt: number, halfSubcubeSize: number,
                       yCoord: number, zCoord: number) {
    data.firstRotateY = 90;
    data.frgTranslateZ = -halfSubcubesCnt * this._subcubeBkgSize;
    if (!evenCnt) data.frgTranslateZ -= halfSubcubeSize;
    data.bkgTranslateZ = data.frgTranslateZ + this.halfGap;
    data.xCoord = -halfSubcubesCnt;
    data.yCoord = -yCoord;
    data.zCoord = zCoord;
  }
  private setRightProps(evenCnt: boolean, data: SubsquareData,
                        halfSubcubesCnt: number, halfSubcubeSize: number,
                        yCoord: number, zCoord: number) {
    data.firstRotateY = 90;
    data.frgTranslateZ = halfSubcubesCnt * this._subcubeBkgSize;
    if (!evenCnt) data.frgTranslateZ += halfSubcubeSize;
    data.bkgTranslateZ = data.frgTranslateZ - this.halfGap;
    data.xCoord = halfSubcubesCnt;
    data.yCoord = -yCoord;
    data.zCoord = zCoord;
  }
  private setFrontProps(evenCnt: boolean, data: SubsquareData,
                        halfSubcubesCnt: number, halfSubcubeSize: number,
                        yCoord: number, xCoord: number) {
    data.frgTranslateZ = -halfSubcubesCnt * this._subcubeBkgSize;
    if (!evenCnt) data.frgTranslateZ -= halfSubcubeSize;
    data.bkgTranslateZ = data.frgTranslateZ + this.halfGap;
    data.xCoord = xCoord;
    data.yCoord = -yCoord;
    data.zCoord = halfSubcubesCnt;
  }
  private setBackProps(evenCnt: boolean, data: SubsquareData,
                       halfSubcubesCnt: number, halfSubcubeSize: number,
                       yCoord: number, xCoord: number) {
    data.frgTranslateZ = halfSubcubesCnt * this._subcubeBkgSize;
    if (!evenCnt) data.frgTranslateZ += halfSubcubeSize;
    data.bkgTranslateZ = data.frgTranslateZ - this.halfGap;
    data.xCoord = xCoord;
    data.yCoord = -yCoord;
    data.zCoord = -halfSubcubesCnt;
  }
  private setTopProps(evenCnt: boolean, data: SubsquareData,
                      halfSubcubesCnt: number, halfSubcubeSize: number,
                      zCoord: number, xCoord: number) {
    data.firstRotateX = 90;
    data.frgTranslateZ = halfSubcubesCnt * this._subcubeBkgSize;
    if (!evenCnt) data.frgTranslateZ += halfSubcubeSize;
    data.bkgTranslateZ = data.frgTranslateZ - this.halfGap;
    data.xCoord = xCoord;
    data.yCoord = halfSubcubesCnt;
    data.zCoord = -zCoord;
  }
  private setBottomProps(evenCnt: boolean, data: SubsquareData,
                         halfSubcubesCnt: number, halfSubcubeSize: number,
                         zCoord: number, xCoord: number) {
    data.firstRotateX = 90;
    data.frgTranslateZ = -halfSubcubesCnt * this._subcubeBkgSize;
    if (!evenCnt) data.frgTranslateZ -= halfSubcubeSize;
    data.bkgTranslateZ = data.frgTranslateZ + this.halfGap;
    data.xCoord = xCoord;
    data.yCoord = -halfSubcubesCnt;
    data.zCoord = -zCoord;
  }

  private setZIndexes() {
    this.zIndexer.setZIndexes();
  }

  private rotateOverflowedMouseMoveRot() {
    if (this._mouseMoveRotationX > 360) this._mouseMoveRotationX -= 360;
    if (this._mouseMoveRotationY > 360) this._mouseMoveRotationY -= 360;
    if (this._mouseMoveRotationX < -360) this._mouseMoveRotationX += 360;
    if (this._mouseMoveRotationY < -360) this._mouseMoveRotationY += 360;
  }
  private calcRotation(value: number): number {
    value = value * 90;
    return value;
  }

  public line: string;
  refreshLine() {
    this.line = this.zIndexer.line;

  }
  public zeroPt: string = '';

  public stroke = new Stroke("transparent", 'chartreuse', 0, 2, 1);

}
