import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppAboutScrollChangeComponent } from './about-scrollChange.component';

describe('AppAboutScrollChangeComponent', () => {
  let component: AppAboutScrollChangeComponent;
  let fixture: ComponentFixture<AppAboutScrollChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppAboutScrollChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppAboutScrollChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
