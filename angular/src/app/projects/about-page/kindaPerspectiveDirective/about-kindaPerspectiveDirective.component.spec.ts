import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppAboutKindaPerspectiveDirectiveComponent } from './about-kindaPerspectiveDirective.component';

describe('AppAboutKindaPerspectiveDirectiveComponent', () => {
  let component: AppAboutKindaPerspectiveDirectiveComponent;
  let fixture: ComponentFixture<AppAboutKindaPerspectiveDirectiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppAboutKindaPerspectiveDirectiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppAboutKindaPerspectiveDirectiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
