import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppAboutSlideMenuComponent } from './about-slideMenu.component';

describe('AppAboutSlideMenuComponent', () => {
  let component: AppAboutSlideMenuComponent;
  let fixture: ComponentFixture<AppAboutSlideMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppAboutSlideMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppAboutSlideMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
