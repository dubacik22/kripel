import { Component, ChangeDetectorRef, AfterViewInit, Input, ViewChild, ElementRef } from '@angular/core';
import { MyRouting } from 'src/stuff/injectables/MyRouting';
import { rotate } from '../Animations'

@Component({
  selector: 'about-slideMenu',
  templateUrl: './about-slideMenu.component.html',
  styleUrls: ['./about-slideMenu.component.css'],
  animations: [
    rotate
  ]
})
export class AppAboutSlideMenuComponent implements AfterViewInit {
  private _compactSubClass:string;
  @Input()
  public get compactSubClass():string{
    return this._compactSubClass;
  }
  public set compactSubClass(value:string) {
    if (value == this._compactSubClass) return;
    this._compactSubClass = value;
    this.detectChanges();
  }
  private _scdStart: number;
  @Input()
  public get scdStart(): number {
    return this._scdStart;
  }
  public set scdStart(value: number) {
    if (this._scdStart == value) return;
    this._scdStart = value;
    this.detectChanges();
  }
  private _scdEnd: number;
  @Input()
  public get scdEnd(): number {
    return this._scdEnd;
  }
  public set scdEnd(value: number) {
    if (this._scdEnd == value) return;
    this._scdEnd = value;
    this.detectChanges();
  }
  private _codeVisible: boolean;
  @Input()
  public get codeVisible(): boolean {
    return this._codeVisible;
  }
  public set codeVisible(value: boolean) {
    if (this._codeVisible == value) return;
    this._codeVisible = value;
    this.detectChanges();
  }
  private _selectedItem: string;
  public get selectedItem(): string {
    return this._selectedItem;
  }
  public set selectedItem(value: string) {
    if (this._selectedItem == value) return;
    this._selectedItem = value;
    this.detectChanges();
  }
  public ugly: boolean = false;
  private readonly niceStylePattern = {'margin-top': '160px'};
  private readonly uglyStylePattern = {'margin-top': '0px'};
  public uglyStyle = this.niceStylePattern;

  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private chDref: ChangeDetectorRef, private router: MyRouting) { 
  }

  ngAfterViewInit() {
    this.chDref.detach();
  }

  public showCode() {
    this.codeVisible = !this.codeVisible;
    this.detectChanges();
  }
  
  public toggleUgly() {
    this.ugly = !this.ugly;
    if (this.ugly) this.uglyStyle = this.uglyStylePattern;
    else this.uglyStyle = this.niceStylePattern;
    this.detectChanges();
  }
  public detailsVisible2rotation(visible: boolean): string {
    return (visible)?'down':'up';
  }
  public select(item: string) {
    this.selectedItem = item;
  }
}
