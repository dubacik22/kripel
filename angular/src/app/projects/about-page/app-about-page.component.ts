import { Component, ChangeDetectorRef, AfterViewInit, Input, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { MyRouting, routerListener } from 'src/stuff/injectables/MyRouting';
import { rotate } from './Animations'
import { Strings } from 'src/app/Strings';

@Component({
  selector: 'app-about-page',
  templateUrl: './app-about-page.component.html',
  styleUrls: ['./app-about-page.component.css'],
  animations: [
    rotate
  ]
})
export class AppAboutPageComponent implements AfterViewInit, OnDestroy {
  public get fixedScrollableUrl(): string { return Strings.fixedScrollableUrl; }
  public get scrollChangeDirUrl(): string { return Strings.scrollChangeDirUrl; }
  public get kindaPerspectiveDirectiveUrl(): string { return Strings.kindaPerspectiveDirectiveUrl; }
  public get kindaPerspectiveComponentUrl(): string { return Strings.kindaPerspectiveComponentUrl; }
  public get slideMenuUrl(): string { return Strings.slideMenuUrl; }

  private _toDo: string;
  public get toDo(): string {
    return this._toDo;
  }
  public set toDo(value: string) {
    if (this._toDo == value) return;
    this._toDo = value;
    this.setRoute(value);
  }
  private setRoute(item: string) {
    this.router.insertQueryS(Strings.aboutToDoQueryParamName, item);
    this.router.insert();
  }

  private _compactSubClass:string;
  @Input()
  public get compactSubClass():string{
    return this._compactSubClass;
  }
  public set compactSubClass(value:string) {
    if (value == this._compactSubClass) return;
    this._compactSubClass = value;
    this.detectChanges();
  }
  private _scdStart: number;
  @Input()
  public get scdStart(): number {
    return this._scdStart;
  }
  public set scdStart(value: number) {
    if (this._scdStart == value) return;
    this._scdStart = value;
    this.detectChanges();
  }
  private _scdEnd: number;
  @Input()
  public get scdEnd(): number {
    return this._scdEnd;
  }
  public set scdEnd(value: number) {
    if (this._scdEnd == value) return;
    this._scdEnd = value;
    this.detectChanges();
  }
  private routeListener: routerListener;
  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private chDref: ChangeDetectorRef, private router: MyRouting) { }

  public ngAfterViewInit() {
    this.chDref.detach();
    this.routeListener = this.router.onChangeRouteQueries((pathQueries: Map<string, string>)=>{this.takeCareOfQueries(pathQueries)});
  }
  public ngOnDestroy() {
    this.routeListener.unsubscribe();
  }

  private takeCareOfQueries(pathQueries: Map<string, string>) {
    if (pathQueries.has(Strings.aboutToDoQueryParamName)) {
      let toDo: string = pathQueries.get(Strings.aboutToDoQueryParamName);
      this.toDo = toDo;
    } else {
      this.toDo = undefined;
    }
    this.detectChanges();
  }

  public showItem(item: string) {
    this.toDo = item;
    this.detectChanges();
  }
  public showGallery() {
    this.toDo = Strings.galleryUrl;
    window.scrollTo(0, 0);
  }
}
