import { trigger, transition, animate, style, state } from '@angular/animations'
export const rotate = trigger('rotate', [
    state('up', style({
      transform: 'rotate(0deg)'
    })),
    state('right', style({
      transform: 'rotate(90deg)'
    })),
    state('down', style({
      transform: 'rotate(180deg)'
    })),
    state('left', style({
      transform: 'rotate(270deg)'
    })),
    transition('* => up', animate('200ms ease-in-out')),
    transition('* => right', animate('200ms ease-in-out')),
    transition('* => down', animate('200ms ease-in-out')),
    transition('* => left', animate('200ms ease-in-out'))
]);