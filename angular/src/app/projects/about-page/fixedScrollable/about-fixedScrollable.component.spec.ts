import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppAboutFixedScrollableComponent } from './about-fixedScrollable.component';

describe('AppAboutFixedScrollableComponent', () => {
  let component: AppAboutFixedScrollableComponent;
  let fixture: ComponentFixture<AppAboutFixedScrollableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppAboutFixedScrollableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppAboutFixedScrollableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
