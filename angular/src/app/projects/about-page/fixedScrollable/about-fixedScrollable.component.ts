import { Component, ChangeDetectorRef, AfterViewInit, Input, ViewChild, ElementRef } from '@angular/core';
import { MyRouting } from 'src/stuff/injectables/MyRouting';
import { rotate } from '../Animations'

@Component({
  selector: 'about-fixedScrollable',
  templateUrl: './about-fixedScrollable.component.html',
  styleUrls: ['./about-fixedScrollable.component.css'],
  animations: [
    rotate
  ]
})
export class AppAboutFixedScrollableComponent implements AfterViewInit {
  private _compactSubClass:string;
  @Input()
  public get compactSubClass():string{
    return this._compactSubClass;
  }
  public set compactSubClass(value:string) {
    if (value == this._compactSubClass) return;
    this._compactSubClass = value;
    this.detectChanges();
  }
  private _scdStart: number;
  @Input()
  public get scdStart(): number {
    return this._scdStart;
  }
  public set scdStart(value: number) {
    if (this._scdStart == value) return;
    this._scdStart = value;
    this.detectChanges();
  }
  private _scdEnd: number;
  @Input()
  public get scdEnd(): number {
    return this._scdEnd;
  }
  public set scdEnd(value: number) {
    if (this._scdEnd == value) return;
    this._scdEnd = value;
    this.detectChanges();
  }

  private _codeVisible: boolean = true;
  @Input()
  public get codeVisible(): boolean {
    return this._codeVisible;
  }
  public set codeVisible(value: boolean) {
    if (this._codeVisible == value) return;
    this._codeVisible = value;
    this.detectChanges();
  }
  
  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private chDref: ChangeDetectorRef) { 
  }

  ngAfterViewInit() {
    this.chDref.detach();
  }

  public showCode() {
    this.codeVisible = !this.codeVisible;
    this.detectChanges();
  }
  
  public detailsVisible2rotation(visible: boolean): string {
    return (visible)?'down':'up';
  }
}
