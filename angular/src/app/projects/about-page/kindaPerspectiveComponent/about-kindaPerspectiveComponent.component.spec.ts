import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppAboutKindaPerspectiveComponentComponent } from './about-kindaPerspectiveComponent.component';

describe('AppAboutKindaPerspectiveComponentComponent', () => {
  let component: AppAboutKindaPerspectiveComponentComponent;
  let fixture: ComponentFixture<AppAboutKindaPerspectiveComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppAboutKindaPerspectiveComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppAboutKindaPerspectiveComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
