import { Component, ChangeDetectorRef, AfterViewInit, Input, ViewChild, ElementRef } from '@angular/core';
import { MyRouting } from 'src/stuff/injectables/MyRouting';
import { rotate } from '../Animations'

@Component({
  selector: 'about-kindaPerspectiveComponent',
  templateUrl: './about-kindaPerspectiveComponent.component.html',
  styleUrls: ['./about-kindaPerspectiveComponent.component.css'],
  animations: [
    rotate
  ]
})
export class AppAboutKindaPerspectiveComponentComponent implements AfterViewInit {
  private _compactSubClass:string;
  @Input()
  public get compactSubClass():string{
    return this._compactSubClass;
  }
  public set compactSubClass(value:string) {
    if (value == this._compactSubClass) return;
    this._compactSubClass = value;
    this.detectChanges();
  }
  private _scdStart: number;
  @Input()
  public get scdStart(): number {
    return this._scdStart;
  }
  public set scdStart(value: number) {
    if (this._scdStart == value) return;
    this._scdStart = value;
    this.detectChanges();
  }
  private _scdEnd: number;
  @Input()
  public get scdEnd(): number {
    return this._scdEnd;
  }
  public set scdEnd(value: number) {
    if (this._scdEnd == value) return;
    this._scdEnd = value;
    this.detectChanges();
  }

  private _codeTsVisible: boolean;
  @Input()
  public get codeTsVisible(): boolean {
    return this._codeTsVisible;
  }
  public set codeTsVisible(value: boolean) {
    if (this._codeTsVisible == value) return;
    this._codeTsVisible = value;
    this.detectChanges();
  }
  private _codeHtmlVisible: boolean;
  @Input()
  public get codeHtmlVisible(): boolean {
    return this._codeHtmlVisible;
  }
  public set codeHtmlVisible(value: boolean) {
    if (this._codeHtmlVisible == value) return;
    this._codeHtmlVisible = value;
    this.detectChanges();
  }
  private _codeCssVisible: boolean;
  @Input()
  public get codeCssVisible(): boolean {
    return this._codeCssVisible;
  }
  public set codeCssVisible(value: boolean) {
    if (this._codeCssVisible == value) return;
    this._codeCssVisible = value;
    this.detectChanges();
  }

  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private chDref: ChangeDetectorRef, private router: MyRouting) { 
  }

  ngAfterViewInit() {
    this.chDref.detach();
  }

  public showTsCode() {
    this.codeTsVisible = !this.codeTsVisible;
    this.detectChanges();
  }
  public showHtmlCode() {
    this.codeHtmlVisible = !this.codeHtmlVisible;
    this.detectChanges();
  }
  public showCssCode() {
    this.codeCssVisible = !this.codeCssVisible;
    this.detectChanges();
  }
  
  public detailsVisible2rotation(visible: boolean): string {
    return (visible)?'down':'up';
  }
}
