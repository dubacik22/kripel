import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EquationerComponent } from './equationer.component';

describe('EquationerComponent', () => {
  let component: EquationerComponent;
  let fixture: ComponentFixture<EquationerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EquationerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquationerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
