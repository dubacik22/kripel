import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EKEquationComponent } from './EKEquation.component';

describe('EKEquationComponent', () => {
  let component: EKEquationComponent;
  let fixture: ComponentFixture<EKEquationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EKEquationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EKEquationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
