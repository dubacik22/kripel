import { Component, ChangeDetectorRef, AfterViewInit, Input, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { MyRouting } from 'src/stuff/injectables/MyRouting';

@Component({
  selector: 'EKEquation',
  templateUrl: './EKEquation.component.html',
  styleUrls: ['./EKEquation.component.css'],
})
export class EKEquationComponent implements AfterViewInit, OnDestroy {

  private _compactSubClass:string;
  @Input()
  public get compactSubClass():string{
    return this._compactSubClass;
  }
  public set compactSubClass(value:string) {
    if (value == this._compactSubClass) return;
    this._compactSubClass = value;
    this.detectChanges();
  }
  private _scdStart: number;
  @Input()
  public get scdStart(): number {
    return this._scdStart;
  }
  public set scdStart(value: number) {
    if (this._scdStart == value) return;
    this._scdStart = value;
    this.detectChanges();
  }
  private _scdEnd: number;
  @Input()
  public get scdEnd(): number {
    return this._scdEnd;
  }
  public set scdEnd(value: number) {
    if (this._scdEnd == value) return;
    this._scdEnd = value;
    this.detectChanges();
  }
  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private chDref: ChangeDetectorRef, private router: MyRouting) { }

  public ngAfterViewInit() {
    this.chDref.detach();
  }
  public ngOnDestroy() {
  }

}
