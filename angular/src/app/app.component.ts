import { Component, AfterViewInit, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
//import { DeviceDetectorService, DeviceInfo } from 'ngx-device-detector';
import { MenuItem } from 'src/stuff/menu/MenuItem';
import { Subscription } from 'rxjs';
import { MyRouting, routerListener } from 'src/stuff/injectables/MyRouting';
import { Session } from 'src/stuff/injectables/Session';
import { User } from './app-login/res/User';
import { SessionValue } from 'src/stuff/injectables/SessionValue';
import { ProjectsComponent } from './app-projectos/projects.component';
import { Strings } from 'src/app/Strings';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {
  public get aboutUrl(): string { return Strings.aboutUrl; }
  public get projectsUrl(): string { return Strings.projectsUrl; }
  public get losAngularosUrl(): string { return Strings.losAngularosUrl; }
  public get dotNetteUrl(): string { return Strings.dotNetteUrl; }
  public get aboutPageUrl(): string { return Strings.aboutPageUrl; }
  public get sudokuUrl(): string { return Strings.sudokuUrl; }
  public get testerUrl(): string { return Strings.testerUrl; }
  public get colorDotsUrl(): string { return Strings.colorDotsUrl; }
  public get doodlingUrl(): string { return Strings.doodlingUrl; }
  public get rubiksUrl(): string { return Strings.rubiksUrl; }
  public get grafosUrl(): string { return Strings.grafosUrl; }
  public get solarisUrl(): string { return Strings.solarisUrl; }
  public get galleryUrl(): string { return Strings.galleryUrl; }
  public get EKUrl(): string { return Strings.EKUrl; }
  public get EKEquationUrl(): string { return Strings.EKEquationUrl; }
  public get EKPropertyUrl(): string { return Strings.EKPropertyUrl; }
  public get EKIKalkulatorowaciaKlassaUrl(): string { return Strings.EKIKalkulatorowaciaKlassaUrl; }
  
  public get equationerUrl(): string { return Strings.equationerUrl; }
  public get rtbUrl(): string { return Strings.rtbUrl; }
  public get loginUrl(): string { return Strings.loginUrl; }
  public get loginOthersUrl(): string { return Strings.loginOthersUrl; }

  menuItems: MenuItem[];
  private readonly menuLastPosNonCompact: number = 8;
  private readonly menuLastPosCompact: number = 16;
  private readonly menuFirstPosNonCompact: number = 100;
  private readonly menuFirstPosCompact: number = 16;
  private readonly headerLastHeightNonCompact: number = 65;
  private readonly headerLastHeightCompact: number = 50;
  private readonly headerFirstHeightNonCompact: number = 150;
  private readonly headerFirstHeightCompact: number = 50;
  private readonly scdStartNonCompact: number = 120;
  private readonly scdStartCompact: number = 50;
  private readonly scdEndNonCompact: number = 50;
  private readonly scdEndCompact: number = 50;
  private readonly creatorLastScrollNonCompact: number = 26;
  private readonly creatorLastScrollCompact: number = undefined;
  private readonly creatorSSHeightNonCompact: number = 150;
  private readonly creatorSSHeightCompact: number = undefined;
  lastMenuPos: number = this.menuLastPosNonCompact;
  firstMenuPos: number = this.menuFirstPosNonCompact;
  compactMenuSubClass: string = Strings.compactSubClassNonCompact;
  compactSubClass: string = Strings.compactSubClassNonCompact;
  animatingMenuSubclass: string = '';
  headerLastHeight: number = this.headerLastHeightNonCompact;
  headerFirstHeight: number = this.headerFirstHeightNonCompact;
  scdStart: number = this.scdStartNonCompact;
  scdEnd: number = this.scdEndNonCompact;
  creatorLastScroll: number = this.creatorLastScrollNonCompact;
  creatorSSHeight: number = this.creatorSSHeightNonCompact;
  loggedUser: string = Strings.loggedNoone;
  private _menuDefaulting: boolean = false;
  private _toDo: string;
  public get toDo(): string {
    return this._toDo;
  }
  public set toDo(value: string) {
    if (this._toDo == value) return;
    this._toDo = value;
    if (!this._menuDefaulting) {
      this.setRoute(value);
    }
    if (value) this._menuDefaulting = false;
    this.detectChanges();
  }

  public get isProjects(): boolean {
    if (this.toDo == this.projectsUrl) return true;
    if (ProjectsComponent.isLosAbgularos(this.toDo)) return true;
    if (ProjectsComponent.isDotNette(this.toDo)) return true;
    return false;
  }

  private routeListener: routerListener;
  private userSubscribtion: Subscription;
  private resizeEvent:() => void = () => { this.onWinResize();};
  //private deviceInfo:DeviceInfo;

  private changeDetectionInProgress: boolean = false;
  private needChangeDetectionRunWhileInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) {
      this.needChangeDetectionRunWhileInProgress = true;
      return;
    }
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
    if (this.needChangeDetectionRunWhileInProgress) {
      this.needChangeDetectionRunWhileInProgress = false;
      this.detectChanges();
    }
  }
  constructor(/*private deviceService: DeviceDetectorService,*/
    private chDref: ChangeDetectorRef, private router: MyRouting,
    public session: Session) {
  }
  public ngOnInit() {
    this.setCompactFromDevice();
    window.addEventListener('resize', this.resizeEvent, true);
  }
  public ngAfterViewInit() {
    this.chDref.detach();
    this.setCompactFromWidth();
    this.setMenuContent(this._compact || this._ultraCompact);
    this.userSubscribtion = this.session.values$.subscribe((values: Map<string, SessionValue>) => {
      this.refreshLoggedUserName(values);
    });
    this.routeListener = this.router.onChangeRouteQueries((pathQueries: Map<string, string>) => {
      this.takeCareOfQueries(pathQueries);
    });
    this.detectChanges();
  }
  public ngOnDestroy() {
    this.userSubscribtion.unsubscribe();
    this.routeListener.unsubscribe();
  }

  private setRoute(item: string) {
    this.router.insertQueryS(Strings.toDoQueryParamName, item);
    this.router.insert();
  }
  private refreshLoggedUserName(values: Map<string, SessionValue>) {
    if (!this.session.isLogged()) this.loggedUser = Strings.loggedNoone;
    else this.loggedUser = values.get(User.loginNamePropName).value;
    this.setMenuContent(this._compact || this._ultraCompact);
    this.detectChanges();
  }
  private takeCareOfQueries(pathQueries: Map<string, string>) {
    if (pathQueries.has(Strings.toDoQueryParamName)) {
      let toDo: string = pathQueries.get(Strings.toDoQueryParamName);
      this.toDo = toDo;
    } else {
      this._menuDefaulting = true;
      this.toDo = '';
    }
    this.detectChanges();
  }
  private onWinResize(){
    this.setCompactFromWidth();
  }
  private setMenuContent(compact: boolean) {
    let newMenuItems: MenuItem[] = [];
    let mi:MenuItem = new MenuItem();
    mi.displayName = 'o nme';
    mi.whenActiveToDo = this.aboutUrl;
    mi.default = true;
    newMenuItems.push(mi);
    mi = new MenuItem();
    mi.displayName = 'projektos';
    mi.whenActiveToDo = this.projectsUrl;
    mi.whenToDoActive = this.projectsUrl + ',' + this.losAngularosUrl + ',' + this.dotNetteUrl + ',' + this.sudokuUrl + ',' + this.testerUrl + ',' + this.colorDotsUrl + ','
                      + this.doodlingUrl + ',' + this.aboutPageUrl + ',' + this.rubiksUrl + ',' + this.grafosUrl + ',' + this.EKUrl + ',' + this.equationerUrl;
    newMenuItems.push(mi);
    mi = new MenuItem();
    mi.displayName = 'fotke';
    mi.whenActiveToDo = this.galleryUrl;
    newMenuItems.push(mi);
    mi = new MenuItem();
    mi.displayName = 'drísty';
    mi.whenActiveToDo = this.rtbUrl;
    newMenuItems.push(mi);
    mi = new MenuItem();
    let loginText: string;
    if (this.session.hasItem(User.loginNamePropName)) {
      loginText = this.session.getLoginName();
    } else {
      loginText = 'jogín';
    }
    mi.displayName = loginText;
    mi.whenActiveToDo = this.loginUrl;
    mi.whenToDoActive = this.loginUrl + ',' + this.loginOthersUrl;
    mi.mini = true;
    newMenuItems.push(mi);

    this.menuItems = newMenuItems;
  }
  private setCompactFromWidth(){
    if (window.innerWidth <= 525) {
      this.ultraCompact = true;
      this.compact = false;
    } else {
      if (window.innerWidth <= 900) {
        this.compact = true;
        this.ultraCompact = false;
      } else {
        this.compact = false;
        this.ultraCompact = false;
      }
    }
  }
  private setCompactFromDevice(){/*
    this.deviceInfo = this.deviceService.getDeviceInfo();
    const isMobile:boolean = this.deviceService.isMobile();
    const isTablet:boolean = this.deviceService.isTablet();
    const isDesktopDevice:boolean = this.deviceService.isDesktop();
    if (!isDesktopDevice) this.compact = true;
    else this.compact = false;*/
  }
  private _compact: boolean = false;
  public get compact(): boolean{
    return this._compact;
  }
  public set compact(value: boolean) {
    if (value == this._compact) return;
    this._compact = value;
    this.appCompactized();

    this.detectChanges();
  }
  private _ultraCompact: boolean = false;
  public get ultraCompact(): boolean {
    return this._ultraCompact;
  }
  public set ultraCompact(value: boolean) {
    if (value == this._ultraCompact) return;
    this._ultraCompact = value;
    this.appCompactized();
    this.detectChanges();
  }

  public appCompactized(){
    if (this._ultraCompact) {
      this.lastMenuPos = this.menuLastPosCompact;
      this.firstMenuPos = this.menuFirstPosCompact;
      this.compactSubClass = Strings.compactSubClassUltraCompact;
      this.headerFirstHeight = this.headerFirstHeightCompact;
      this.headerLastHeight = this.headerLastHeightCompact;
      this.scdStart = this.scdStartCompact;
      this.scdEnd = this.scdEndCompact;
      this.creatorLastScroll = this.creatorLastScrollCompact;
      this.creatorSSHeight=this.creatorSSHeightCompact;
    } else if (this._compact) {
      this.lastMenuPos = this.menuLastPosCompact;
      this.firstMenuPos = this.menuFirstPosCompact;
      this.compactSubClass = Strings.compactSubClassCompact;
      this.headerFirstHeight = this.headerFirstHeightCompact;
      this.headerLastHeight = this.headerLastHeightCompact;
      this.scdStart = this.scdStartCompact;
      this.scdEnd = this.scdEndCompact;
      this.creatorLastScroll = this.creatorLastScrollCompact;
      this.creatorSSHeight=this.creatorSSHeightCompact;
    } else {
      this.lastMenuPos = this.menuLastPosNonCompact;
      this.firstMenuPos = this.menuFirstPosNonCompact;
      this.compactSubClass = Strings.compactSubClassNonCompact;
      this.compactMenuSubClass = Strings.compactSubClassNonCompact;
      this.headerFirstHeight = this.headerFirstHeightNonCompact;
      this.headerLastHeight = this.headerLastHeightNonCompact;
      this.scdStart = this.scdStartNonCompact;
      this.scdEnd = this.scdEndNonCompact;
      this.creatorLastScroll = this.creatorLastScrollNonCompact;
      this.creatorSSHeight=this.creatorSSHeightNonCompact;
    }

    this.setMenuContent(this._compact || this._ultraCompact);
  }
  public menuCompactized() {
    if (this._ultraCompact || this._compact) {
      this.compactMenuSubClass = Strings.compactSubClassCompact;
    }
    //this.chDref.detectChanges();
    this.detectChanges();
  }
  public menuItemChanged() {
    //console.log('app menu changed')
    this.router.clearAll();
    this.animatingMenuSubclass = 'transitionHeightTop';
    this.detectChanges();
    window.scrollTo(0, 0);
    setTimeout(() => {
      this.animatingMenuSubclass = '';
      this.detectChanges();
    }, 450);
  }
}
