export class User{
    public static readonly userIdPropName: string = 'userId';
    public static readonly userNamePropName: string = 'userName';
    public static readonly loginNamePropName: string = 'loginName';
    public static readonly emailPropName: string = 'email';
    public static readonly permissionsPropName: string = 'permissions';
    public static readonly encryptedSessionPropName: string = 'encryptedSession';

    public id: string;
    public userName: string;
    public loginName: string;
    public email: string;
    public permissions: string;
}