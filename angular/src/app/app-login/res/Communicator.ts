import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Md5 } from 'ts-md5/dist/md5';
import { Paths } from 'src/stuff/injectables/Paths';
import { User } from './User';

@Injectable()
export class Communicator {
    public verbose: boolean = false;
    private static createReadAllUserNamesData(session: string): any {
        let ret = {
            'toDo': 'users_readAllNames',
            'session': session,
        }
        return ret;
    }
    private static createSaveUserData(loginNane: string, userName: string, email: string, pass: string | Int32Array): any {
        let ret = {
            'toDo': 'users_save',
            'loginName': loginNane,
            'userName': userName,
            'email': email,
            'password': pass,
        }
        return ret;
    }
    private static createEraseUserData(loginNane: string, session: string): any {
        let ret = {
            'toDo': 'users_erase',
            'loginName': loginNane,
            'session': session,
        }
        return ret;
    }
    private static createUsersChangePassData(loginNane: string, pass: string | Int32Array, session: string): any {
        let ret = {
            'toDo': 'users_changePass',
            'loginName': loginNane,
            'password': pass,
            'session': session,
        }
        return ret;
    }
    private static createEditUserData(id: string, loginNane: string, userName: string, email: string, pass: string | Int32Array, session: string): any {
        let ret = {
            'toDo': 'users_edit',
            'id': id,
            'loginName': loginNane,
            'userName': userName,
            'email': email,
            'password': pass,
            'session': session,
        }
        return ret;
    }
    private static createLoginUserData(loginName: string, pass: string | Int32Array): any {
        let ret = {
            'toDo': 'users_login',
            'loginName': loginName,
            'password': pass,
        }
        return ret;
    }
    private static createCreateTableData(session: string): any {
        let ret = {
            'toDo': 'users_createTable',
            'session': session,
        }
        return ret;
    }
    private static makeTimoUber(): any {
        let ret = {
            'toDo': 'users_makeDubacik22Uber',
        }
        return ret;
    }
    private static makeTimoOnlyUber(): any {
        let ret = {
            'toDo': 'users_makeDubacik22OnlyUber',
        }
        return ret;
    }

    private static createGetGameIdData(): any {
        let ret = {
            'toDo': 'galaxy_getNewId',
        }
        return ret;
    }
    
    constructor(private http: HttpClient, private paths: Paths) {
    }
    public readGameId(session: string, whatNext?: (users: User[]) => void) {
        let messageData = Communicator.createGetGameIdData();
        if (this.verbose) {
            console.log("We sent to server getGameIdData:");
            console.log(messageData);
        }
        
        this.http.post(this.paths.phpCommunicator, messageData, {responseType: 'json'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at getGameIdData:");
                console.log(retData);
            }
            if (retData && retData.success && whatNext) whatNext(retData.users);
        });
    }
    public createTable(session: string, whatNext?: () => void) {
        let messageData = Communicator.createCreateTableData(session);
        if (this.verbose) {
            console.log("We sent to server createTable:");
            console.log(messageData);
        }
        
        this.http.post(this.paths.phpCommunicator, messageData, {responseType: 'json'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at createTable:");
                console.log(retData);
            }
            if (retData && retData.success && whatNext) whatNext();
        });
    }
    public readAllUserNames(session: string, whatNext?: (users: User[]) => void) {
        let messageData = Communicator.createReadAllUserNamesData(session);
        if (this.verbose) {
            console.log("We sent to server readAllUserNames:");
            console.log(messageData);
        }
        
        this.http.post(this.paths.phpCommunicator, messageData, {responseType: 'json'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at readAllUserNames:");
                console.log(retData);
            }
            if (retData && retData.success && whatNext) whatNext(retData.users);
        });
    }
    public saveUser(loginName: string, userName: string, email: string, password: string,
        whatNext?: (success: boolean, message: string) => void) {
        if (!loginName || !password) return;
        if (!userName) userName = loginName;

        let md5 = new Md5();
        let md5Pass: string|Int32Array = md5.appendStr(password).end();
        let userData = Communicator.createSaveUserData(loginName, userName, email, md5Pass);
        if (this.verbose) {
            console.log("We sent to server addUser:");
            console.log(userData);
        }
        
        this.http.post(this.paths.phpCommunicator, userData, {responseType: 'json'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned addUser:");
                console.log(retData);
            }
            if (retData && whatNext) whatNext(retData.success, retData.message);
        });
    }
    public eraseUser(loginName: string, session: string, whatNext?: () => void) {
        let userData = Communicator.createEraseUserData(loginName, session);
        if (this.verbose) {
            console.log("We sent to server eraseUser:");
            console.log(userData);
        }
        
        this.http.post(this.paths.phpCommunicator, userData, {responseType: 'json'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at eraseUser:");
                console.log(retData);
            }
            if (retData && whatNext) whatNext();
        });
    }
    public changeUserPass(loginName: string, pass: string, session: string, whatNext?: () => void) {
        let md5 = new Md5();
        let md5Pass: string|Int32Array = md5.appendStr(pass).end();
        let userData = Communicator.createUsersChangePassData(loginName, md5Pass, session);
        if (this.verbose) {
            console.log("We sent to server changeUserPass:");
            console.log(userData);
        }
        
        this.http.post(this.paths.phpCommunicator, userData, {responseType: 'text'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at changeUserPass:");
                console.log(retData);
            }
            if (retData && whatNext) whatNext();
        });
    }
    public loginUser(lName: string, pass: string,
        whatNext?: (success: boolean, message: string, user: User, encryptedSession: string) => void) {
        if (!lName || !pass) return;
        let md5 = new Md5();
        let md5Pass: string|Int32Array = md5.appendStr(pass).end();
        let userData = Communicator.createLoginUserData(lName, md5Pass);
        if (this.verbose) {
            console.log("We sent to server loginUser:");
            let uuserData = Communicator.createLoginUserData(lName, md5Pass);
            uuserData['password'] = '-';
            console.log(uuserData);
        }

        this.http.post(this.paths.phpCommunicator, userData, {responseType: 'json'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned loginUser:");
                console.log(retData);
            }
            if (whatNext) whatNext(retData.success, retData.message, retData.user, retData.session);
        });
    }
    public editUser(id: string, loginName: string, userName: string, email: string, pass: string, session: string,
        whatNext?: (success: boolean, message: string, user: User, encryptedSession: string) => void) {
        let md5Pass: string|Int32Array;
        if (!pass) md5Pass = '';
        else {
            let md5 = new Md5();
            md5Pass = md5.appendStr(pass).end();
        }
        let userData = Communicator.createEditUserData(id, loginName, userName, email, md5Pass, session);
        if (this.verbose) {
            console.log("We sent to server editUser:");

            let uuserData = Communicator.createEditUserData(id, loginName, userName, email, md5Pass, session);
            uuserData['password']='-';
            console.log(uuserData);
        }
        
        this.http.post(this.paths.phpCommunicator, userData, {responseType: 'json'})
        .subscribe((retData: any) => {
            if (this.verbose) {
                console.log("Server returned at editUser:");
                console.log(retData);
            }
            if (whatNext) whatNext(retData.success, retData.message, retData.user ,retData.session);
        });
    }
    public makeDubacik22OnlyUber() {
        let userData = Communicator.makeTimoOnlyUber();
        if (this.verbose) {
            console.log("We sent to server makeTimoOnlyUber:");
            console.log(userData);
        }
        
        this.http.post(this.paths.phpCommunicator, userData, {responseType: 'json'})
        .subscribe(() => {
            if (this.verbose) console.log("sent makeTimoOnlyUber:");
        });
    }
    public makeDubcik22Uber() {
        let userData = Communicator.makeTimoUber();
        if (this.verbose) {
            console.log("We sent to server makeTimoUber:");
            console.log(userData);
        }
        
        this.http.post(this.paths.phpCommunicator, userData, {responseType: 'json'})
        .subscribe(() => {
            if (this.verbose) console.log("sent makeTimoUber:");
        });
    }
}
