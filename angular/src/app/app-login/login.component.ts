import { Component, Input, ChangeDetectorRef, AfterViewInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Communicator } from './res/Communicator';
import { Subscription } from 'rxjs';
import { Session } from 'src/stuff/injectables/Session';
import { User } from './res/User';
import { DialogBoxComponent } from 'src/stuff/dialogs/modalDialog.component';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements AfterViewInit, OnDestroy {
  private readonly admin: string = 'admin';
  public get loginNamePropName(): string {
    return User.loginNamePropName;
  }
  private _scdStart:number;
  @Input()
  public get scdStart(): number {
    return this._scdStart;
  }
  public set scdStart(value: number) {
    this._scdStart = value;
    this.detectChanges();
  }
  private _scdEnd:number;
  @Input()
  public get scdEnd(): number {
    return this._scdEnd;
  }
  public set scdEnd(value: number) {
    this._scdEnd = value;
    this.detectChanges();
  }
  private _compactSubClass: string;
  @Input() public get compactSubClass(): string{
    return this._compactSubClass;
  }
  public set compactSubClass(value: string){
    if (value == this._compactSubClass) return;
    this._compactSubClass = value;
    //this.setCompact();
    //this.detectChanges();
  }
  private _loginName: string = '';
  public get loginName(): string {
    return this._loginName;
  }
  public set loginName(value: string) {
    if (this._loginName == value) return;
    this._loginName = value;
    this.detectChanges();
  }
  private _userName: string = '';
  public get userName(): string {
    return this._userName;
  }
  public set userName(value: string) {
    if (this._userName == value) return;
    this._userName = value;
    this.detectChanges();
  }
  private _email: string = '';
  public get email(): string {
    return this._email;
  }
  public set email(value: string) {
    if (this._email == value) return;
    this._email = value;
    this.detectChanges();
  }
  private _otherUsers: User[] = [];
  public get otherUsers(): User[] {
    return this._otherUsers;
  }
  public set otherUsers(value: User[]) {
    this._otherUsers = value;
    this.detectChanges();
  }
  public origPass: string = '';
  public pass: string = '';
  public pass2: string = '';
  public editMe: boolean = false;
  public new: boolean = false;
  public editUser: boolean = false;

  private userSubscribtion: Subscription;

  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }
  constructor(private chDref: ChangeDetectorRef, private communicator: Communicator,
    public session: Session) { }

  ngAfterViewInit() {
    this.communicator.createTable(this.session.getEncryptedSession());
    this.chDref.detach();
    this.setRestUsers();
  }
  ngOnDestroy() {
    if (this.userSubscribtion) this.userSubscribtion.unsubscribe();
  }

  private innerLogin(loginName: string, pass: string, whatNext?: (user: User, sesion: string) => void) {
    if (!pass) DialogBoxComponent.showJustInfo('a heslo nič?');
    this.communicator.loginUser(loginName, pass, (success: boolean, message: string, user: User, encSession: string) => {
      if (success) {
        if (whatNext) whatNext(user, encSession);
      } else {
        if (message.indexOf('zero') >= 0) DialogBoxComponent.showJustInfo('\''+loginName+'\' užívateľ neexistuje.');
        else if (message.indexOf('pass') >= 0) DialogBoxComponent.showJustInfo('nesprávne heslo.');
      }
      this.detectChanges();
    });
  }
  public login() {
    this.innerLogin(this.loginName, this.pass, (user: User, session: string) => {
      this.setUser(user, session);
      this.setRestUsers();
      this.pass = '';
      this.editMe = false;
    });
  }
  public logout() {
    this.logoutUser();
    this.pass = '';
    this.pass2 = '';
    this.origPass = '';
    this.loginName = '';
    this.userName = '';
    this.email = '';
    this.editMe = false;
    this.editUser = false;
    this.detectChanges();
  }

  public endChangeThisUserInfo() {
    this.editMe = !this.editMe;
    this.loginName = '';
    this.userName = '';
    this.email = '';
    this.pass = '';
    this.pass2 = '';
    this.origPass = '';
    this.detectChanges();
  }
  public wantNew() {
    this.new = !this.new;
    this.detectChanges();
  }
  public addNew() {
    if (this.loginName == this.admin) {
      DialogBoxComponent.showJustInfo('prihlasovacie meno nieje povolené');
      return;
    }
    if (!this.pass) {
      DialogBoxComponent.showJustInfo('nie je zadané heslo');
      return;
    }
    if (this.pass != this.pass2) {
      DialogBoxComponent.showJustInfo('heslo nieje rovnaké');
      return;
    }
    this.communicator.saveUser(this.loginName, this.userName, this.email, this.pass, (success: boolean, message: string) => {
      if (success) {
        this.login();
        this.new = false;
        this.pass = '';
        this.pass2 = '';
        this.detectChanges();
      } else {
        if (message.indexOf('user already exist') >= 0) {
          DialogBoxComponent.showJustInfo('užívateľ \''+this.loginName+'\' už existuje.');
        }
      }
    });
  }
  private innerChangeUserInfo(origUser: User, whatNext: () => void) {
    if ((origUser.loginName != this.admin) && (this.loginName == this.admin)) {
      DialogBoxComponent.showJustInfo('prihlasovacie meno nieje povolené');
      return;
    }
    if ((origUser.userName != this.admin) && (this.userName == this.admin)) {
      DialogBoxComponent.showJustInfo('užívaťeľské meno nieje povolené');
      return;
    }
    if (!this.loginName) {
      DialogBoxComponent.showJustInfo('musíte zadať prihlasovacie meno');
      return;
    }
    if (!this.userName) {
      DialogBoxComponent.showJustInfo('musíte zadať užívaťeľské meno');
      return;
    }
    if (this.pass != this.pass2) {
        DialogBoxComponent.showJustInfo('nové heslo nieje rovnaké');
        return;
    }
    this.communicator.editUser(origUser.id, this.loginName, this.userName, this.email, this.pass,
      this.session.getEncryptedSession(),
      (success: boolean, message: string) => {
      if (success) {
        whatNext();
      } else {
        if (message.indexOf('read returned array with zero size') >= 0) {
          DialogBoxComponent.showJustInfo('\''+this.loginName+'\' užívateľ neexistuje.');
        } else if (message.indexOf('user already exist') >= 0) {
          DialogBoxComponent.showJustInfo('užívaťeľské meno '+this.loginName+' už existuje.');
          this.loginName = origUser.loginName;
        }
      }
      this.detectChanges();
    });
  }
  public changeThisUserInfo() {
    this.editMe = true;
    this.editUser = false;

    let origUser: User = new User();
    origUser.id = this.session.getItem(User.userIdPropName);
    origUser.loginName = this.session.getLoginName();
    origUser.userName = this.session.getItem(User.userNamePropName);

    if (!this.origPass) {
      DialogBoxComponent.showJustInfo('na zmenu údajov je nutné zadať pôvodné heslo.');
      return;
    }
    this.innerLogin(this.loginName, this.origPass, (user: User, session: string) => {
      this.innerChangeUserInfo(origUser, () => {
        if (!this.pass) this.pass = this.origPass;
        this.login();
        this.setRestUsers();
        this.pass = '';
        this.pass2 = '';
        this.origPass = '';
        this.editMe = false;
      });
    });
  }
  public changeOtherUserInfo() {
    if (!this.otherUser) {
      this.pass = '';
      this.pass2 = '';
      this.origPass = '';
      this.editUser = false;
      this.detectChanges();
    } else {
      this.innerChangeUserInfo(this.otherUser, () => {
        if (!this.pass) this.pass = this.origPass;
        this.setRestUsers();
        this.pass = '';
        this.pass2 = '';
        this.origPass = '';
        this.editUser = false;
        this.otherUser = undefined;

      });
    }
  }
  public changeUserInfo() {
    if (this.editMe) this.changeThisUserInfo();
    else if (this.editUser) this.changeOtherUserInfo();
  }
  private otherUser: User;
  public startEditOtherUser(editedUser: User) {
    this.editUser = true;
    this.editMe = false;
    this.otherUser = editedUser;
    this.loginName = editedUser.loginName;
    this.userName = editedUser.userName;
    this.email = editedUser.email;
    this.detectChanges();
  }
  public startEditThisUser() {
    this.editMe = true;
    this.editUser = false;
    this.loginName = this.session.getLoginName();
    this.userName = this.session.getItem(User.userNamePropName);
    this.email = this.session.getItem(User.emailPropName);
    this.detectChanges();
  }
  public remOtherUser() {
    this.communicator.eraseUser(this.loginName, this.session.getEncryptedSession(), () => {
      if (this.loginName == this.session.getLoginName()) this.logout();
      this.loginName = '';
      this.userName = '';
      this.email = '';
      this.editUser = false;
      this.setRestUsers();
    });
  }
  private logoutUser() {
    this.session.remItems([User.userNamePropName, User.userIdPropName, User.loginNamePropName, User.encryptedSessionPropName]);
  }
  private setRestUsers() {
    this._otherUsers = [];
    if (this.session.isUberAdmin()) {
      this.communicator.readAllUserNames(this.session.getEncryptedSession(), (users: User[]) => {
        if (users && (users.length > 0)) this._otherUsers = users;
        this.detectChanges();
      });
    } else {
      this.detectChanges();
    }
  }
  private setUser(user: User, encryptedSession: string) {
    let data: Map<string, string> = new Map();
    data.set(User.userIdPropName, user.id);
    data.set(User.userNamePropName, user.userName);
    data.set(User.loginNamePropName, user.loginName);
    data.set(User.emailPropName, user.email);
    data.set(User.permissionsPropName, user.permissions);
    data.set(User.encryptedSessionPropName, encryptedSession);
    this.session.addItems2Local(data);
  }
  public passKeyPress(event: KeyboardEvent) {
    if ((event.key == 'Enter') && !this.new && !this.editMe) this.login();
  }
  public pass2KeyPress(event: KeyboardEvent) {
    if (event.key == 'Enter') {
      if (this.new) this.addNew();
    }
  }
  public makeDubacik22Uber() {
    this.communicator.makeDubcik22Uber();
  }
  public makeDubacik22OnlyUber() {
    this.communicator.makeDubacik22OnlyUber();
  }
}
