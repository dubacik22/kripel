import { HttpClient } from '@angular/common/http';
import { Component, AfterViewInit, Input, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Strings } from 'src/app/Strings';
import { MyRouting, routerListener } from 'src/stuff/injectables/MyRouting';
import { GalleryPath } from 'src/stuff/gallery/res/GalleryPath';
import { GalleryObject } from './res/GalleyObject';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements AfterViewInit, OnDestroy {
  public static readonly galleryQueryName: string = 'gallery';
  public static readonly imgQueryName: string = 'img';

  private _scdStart:number;
  @Input()
  public get scdStart(): number {
    return this._scdStart;
  }
  public set scdStart(value: number) {
    this._scdStart = value;
    this.detectChanges();
  }
  private _scdEnd:number;
  @Input()
  public get scdEnd(): number {
    return this._scdEnd;
  }
  public set scdEnd(value: number) {
    this._scdEnd = value;
    this.detectChanges();
  }
  private _compactSubClass: string;
  @Input() public get compactSubClass(): string{
    return this._compactSubClass;
  }
  public set compactSubClass(value: string){
    if (value == this._compactSubClass) return;
    this._compactSubClass = value;
    this.setCompact();
    this.detectChanges();
  }
  public galleries: GalleryObject[];
  private readonly gsNonCompact: number = 1;
  private readonly gsCompact: number = 0.7;
  private readonly gsUltraCompact: number = 0.5;
  galleryScale: number = this.gsNonCompact;
  galleryCompact: boolean = false;
  private _innerGalleryName: string = '';
  private get innerGalleryName(): string {
    return this._innerGalleryName;
  }
  private set innerGalleryName(value: string) {
    if (this._innerGalleryName == value) return;
    this._innerGalleryName = value;
  }
  public get galleryName(): string {
    return this.innerGalleryName;
  }
  public set galleryName(value: string) {
    this.innerGalleryName = value;
    let galleryPath: GalleryPath = new GalleryPath();
    galleryPath.gallery = this.galleryName;
    this.updateRouterWithPath(galleryPath);
  }
  private _innerOpenedImage: string = '';
  private get innerOpenedImage(): string {
    return this._innerOpenedImage;
  }
  private set innerOpenedImage(value: string) {
    if (this._innerOpenedImage == value) return;
    this._innerOpenedImage = value;
  }
  public get openedImage(): string {
    return this.innerOpenedImage;
  }
  public set openedImage(value: string) {
    this.innerOpenedImage = value;
    this.detectChanges();
  }
  private changeDetectionInProgress: boolean = false;
  private detectChanges() {
    if (this.changeDetectionInProgress) return;
    this.changeDetectionInProgress = true;
    this.chDref.detectChanges();
    this.changeDetectionInProgress = false;
  }

  constructor(private http: HttpClient, private router: MyRouting, private chDref: ChangeDetectorRef) {
    //router.ignore();
  }
  ngAfterViewInit() {
    this.chDref.detach();
    this.chRQLambda = this.router.onChangeRouteQueries((query: Map<string, string>) => { this.takeCareOfQuery(query);});
    this.initGalleries();
    //this.router.unignore();
  }
  private chRQLambda: routerListener;
  ngOnDestroy() { 
    this.chRQLambda.unsubscribe();
  }
  public static routerPointsToMe(router: MyRouting): boolean {
    let pathQueries: Map<string, string> = router.getSentQueries();
    if (!pathQueries.has(GalleryComponent.galleryQueryName)) return false;
    if (!pathQueries.has(GalleryComponent.imgQueryName)) return false;
    return true;
  }
  public setGallery(name: string) {
    this.galleryName = name;
    this.detectChanges();
  }
  private takeCareOfQuery(pathQueries: Map<string, string>) {
    if (pathQueries.has(GalleryComponent.galleryQueryName)) {
      let neededGalleryName: string = pathQueries.get(GalleryComponent.galleryQueryName);
      if (!pathQueries.has(GalleryComponent.imgQueryName)) {
        this.innerGalleryName = neededGalleryName;
        this.innerOpenedImage = undefined;
      } else {
        let imgName: string = pathQueries.get(GalleryComponent.imgQueryName);
        this.innerGalleryName = neededGalleryName;
        this.innerOpenedImage = imgName;
      }
    } else {
      if (pathQueries.has(GalleryComponent.imgQueryName)) this.router.remQuery(GalleryComponent.imgQueryName);
      this.innerGalleryName = undefined;
      this.innerOpenedImage = undefined;
    }
    this.detectChanges();
  }
  imgHolderResized(){
    
  }
  private oldPath: GalleryPath = undefined;
  public updateRouterWithPath(path: GalleryPath) {
    if (GalleryPath.isSame(this.oldPath, path)) return;
    this.oldPath = path;
    if (path && path.gallery) {
      this.router.insertQueryS(GalleryComponent.galleryQueryName, path.gallery);
      if (path.img) {
        this.router.insertQueryS(GalleryComponent.imgQueryName, path.img);
      } else {
        this.router.remQuery(GalleryComponent.imgQueryName);
      }
    } else {
      this.router.remQuery(GalleryComponent.galleryQueryName);
      this.router.remQuery(GalleryComponent.imgQueryName);
    }
    this.router.insert();
  }
  private setCompact() {
    if (this._compactSubClass == Strings.compactSubClassUltraCompact){
      this.galleryScale = this.gsUltraCompact;
      this.galleryCompact = true;
    } else if (this._compactSubClass == Strings.compactSubClassCompact){
      this.galleryScale = this.gsCompact;
      this.galleryCompact = false;
    } else {
      this.galleryScale = this.gsNonCompact;
      this.galleryCompact = false;
    }
  }
  private initGalleries() {
    this.http.get('./assets/imgs/gallery/galleries.json').subscribe((data:[]) => {
      this.galleries = [];
      data.forEach((item) => {
        let gp: GalleryObject = new GalleryObject();
        gp.loadFromJson(item);
        this.galleries.push(gp);
      });
      this.detectChanges();
    });
  }
}
