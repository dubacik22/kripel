
export class GalleryObject {
    public name: string;
    public path: string;
  
    public loadFromJson(jsonObj) {
        this.name = jsonObj.name;
        this.path = jsonObj.path;
    }
}